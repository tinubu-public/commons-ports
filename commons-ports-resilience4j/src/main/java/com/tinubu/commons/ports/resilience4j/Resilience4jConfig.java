/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.resilience4j;

import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isGreaterThanOrEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isLessThanOrEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.NumberRules.isPositive;
import static com.tinubu.commons.ddd2.invariant.rules.NumberRules.isStrictlyPositive;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static java.lang.Math.max;
import static java.lang.Math.min;

import java.time.Duration;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.ParameterValue;
import com.tinubu.commons.lang.util.MechanicalSympathy;

/**
 * Resilience4j generic resilience configuration.
 */
public class Resilience4jConfig extends AbstractValue {
   private final BulkheadConfig bulkhead;
   private final ThreadPoolBulkheadConfig threadPoolBulkhead;
   private final TimeLimiterConfig timeLimiter;
   private final RateLimiterConfig rateLimiter;
   private final RetryConfig retry;

   public Resilience4jConfig(Resilience4jConfigBuilder builder) {
      this.bulkhead = builder.bulkhead;
      this.threadPoolBulkhead = builder.threadPoolBulkhead;
      this.timeLimiter = builder.timeLimiter;
      this.rateLimiter = builder.rateLimiter;
      this.retry = builder.retry;
   }

   @Override
   public Fields<? extends Resilience4jConfig> defineDomainFields() {
      return Fields
            .<Resilience4jConfig>builder()
            .field("bulkhead", v -> v.bulkhead)
            .field("threadPoolBulkhead", v -> v.threadPoolBulkhead)
            .field("timeLimiter", v -> v.timeLimiter)
            .field("rateLimiter", v -> v.rateLimiter)
            .field("retry", v -> v.retry)
            .build();
   }

   /**
    * Bulkhead configuration. Bulkhead configuration is exclusive with {@link #threadPoolBulkhead()} and will
    * be used in priority if both are configured.
    *
    * @return bulkhead configuration
    */
   public BulkheadConfig bulkhead() {
      return bulkhead;
   }

   /**
    * Thread pool bulkhead configuration. Tread pool bulkhead configuration is exclusive with
    * {@link #bulkhead()} and will *not* be used in priority if both are configured.
    *
    * @return thread pool bulkhead configuration
    */
   public ThreadPoolBulkheadConfig threadPoolBulkhead() {
      return threadPoolBulkhead;
   }

   /**
    * Time limiter configuration.
    *
    * @return time limiter configuration
    */
   public TimeLimiterConfig timeLimiter() {
      return timeLimiter;
   }

   /**
    * Rate limiter configuration.
    *
    * @return rate limiter configuration
    */
   public RateLimiterConfig rateLimiter() {
      return rateLimiter;
   }

   /**
    * Retry configuration.
    *
    * @return retry configuration
    */
   public RetryConfig retry() {
      return retry;
   }

   public static class Resilience4jConfigBuilder extends DomainBuilder<Resilience4jConfig> {
      private BulkheadConfig bulkhead;
      private ThreadPoolBulkheadConfig threadPoolBulkhead;
      private TimeLimiterConfig timeLimiter;
      private RateLimiterConfig rateLimiter;
      private RetryConfig retry;

      public Resilience4jConfigBuilder bulkhead(BulkheadConfig bulkhead) {
         this.bulkhead = bulkhead;
         return this;
      }

      public Resilience4jConfigBuilder threadPoolBulkhead(ThreadPoolBulkheadConfig threadPoolBulkhead) {
         this.threadPoolBulkhead = threadPoolBulkhead;
         return this;
      }

      public Resilience4jConfigBuilder timeLimiter(TimeLimiterConfig timeLimiter) {
         this.timeLimiter = timeLimiter;
         return this;
      }

      public Resilience4jConfigBuilder rateLimiter(RateLimiterConfig rateLimiter) {
         this.rateLimiter = rateLimiter;
         return this;
      }

      public Resilience4jConfigBuilder retry(RetryConfig retry) {
         this.retry = retry;
         return this;
      }

      @Override
      protected Resilience4jConfig buildDomainObject() {
         return new Resilience4jConfig(this);
      }
   }

   public static class BulkheadConfig extends AbstractValue {
      private static final Duration DEFAULT_MAX_WAIT_DURATION = Duration.ZERO;
      private static final boolean DEFAULT_FAIR_CALL_HANDLING_STRATEGY = true;

      private final Integer maxConcurrentCalls;
      private final Duration maxWaitDuration;
      private final boolean fairCallHandlingStrategy;

      public BulkheadConfig(BulkheadConfigBuilder builder) {
         this.maxConcurrentCalls = builder.maxConcurrentCalls;
         this.maxWaitDuration = nullable(builder.maxWaitDuration, DEFAULT_MAX_WAIT_DURATION);
         this.fairCallHandlingStrategy =
               nullable(builder.fairCallHandlingStrategy, DEFAULT_FAIR_CALL_HANDLING_STRATEGY);
      }

      @Override
      public Fields<? extends BulkheadConfig> defineDomainFields() {
         return Fields
               .<BulkheadConfig>builder()
               .field("maxConcurrentCalls", v -> v.maxConcurrentCalls, isPositive())
               .field("maxWaitDuration",
                      v -> v.maxWaitDuration,
                      isNotNull().andValue(isGreaterThanOrEqualTo(value(Duration.ZERO))))
               .field("fairCallHandlingStrategy", v -> v.fairCallHandlingStrategy)
               .build();
      }

      /**
       * Bulkhead max concurrent calls. Must be >= 0.
       */
      public int maxConcurrentCalls() {
         return maxConcurrentCalls;
      }

      /**
       * Bulkhead max wait duration for available space, when bulkhead has no more space available. Can be set
       * to {@code 0}
       * to disable wait time and immediately fail. Default to {@link #DEFAULT_MAX_WAIT_DURATION}.
       */
      public Duration maxWaitDuration() {
         return maxWaitDuration;
      }

      /**
       * Set to {@code true} to keep operations ordered in wait queue. Default to
       * {@value DEFAULT_FAIR_CALL_HANDLING_STRATEGY}.
       */
      public boolean fairCallHandlingStrategy() {
         return fairCallHandlingStrategy;
      }

      public static class BulkheadConfigBuilder extends DomainBuilder<BulkheadConfig> {
         private Integer maxConcurrentCalls;
         private Duration maxWaitDuration;
         private Boolean fairCallHandlingStrategy;

         public BulkheadConfigBuilder maxConcurrentCalls(Integer maxConcurrentCalls) {
            this.maxConcurrentCalls = maxConcurrentCalls;
            return this;
         }

         public BulkheadConfigBuilder maxWaitDuration(Duration maxWaitDuration) {
            this.maxWaitDuration = maxWaitDuration;
            return this;
         }

         public BulkheadConfigBuilder fairCallHandlingStrategy(Boolean fairCallHandlingStrategy) {
            this.fairCallHandlingStrategy = fairCallHandlingStrategy;
            return this;
         }

         @Override
         protected BulkheadConfig buildDomainObject() {
            return new BulkheadConfig(this);
         }
      }

   }

   public static class ThreadPoolBulkheadConfig extends AbstractValue {
      private static final int DEFAULT_CORE_THREAD_POOL_SIZE =
            max(1, MechanicalSympathy.optimalConcurrency());
      private static final int DEFAULT_MAX_THREAD_POOL_SIZE = MechanicalSympathy.optimalConcurrency();

      private final int maxThreadPoolSize;
      private final int coreThreadPoolSize;
      private final Duration keepAliveDuration;
      private final Integer queueCapacity;

      public ThreadPoolBulkheadConfig(ThreadPoolBulkheadConfigBuilder builder) {
         this.maxThreadPoolSize = nullable(builder.maxThreadPoolSize, DEFAULT_MAX_THREAD_POOL_SIZE);
         this.coreThreadPoolSize =
               nullable(builder.coreThreadPoolSize, min(DEFAULT_CORE_THREAD_POOL_SIZE, maxThreadPoolSize));
         this.keepAliveDuration = builder.keepAliveDuration;
         this.queueCapacity = builder.queueCapacity;
      }

      @Override
      public Fields<? extends ThreadPoolBulkheadConfig> defineDomainFields() {
         ParameterValue<Integer> maxThreadPoolSize = value(this.maxThreadPoolSize, "maxThreadPoolSize");

         return Fields
               .<ThreadPoolBulkheadConfig>builder()
               .field("maxThreadPoolSize", v -> v.maxThreadPoolSize, isStrictlyPositive())
               .field("coreThreadPoolSize",
                      v -> v.coreThreadPoolSize,
                      isStrictlyPositive().andValue(isLessThanOrEqualTo(maxThreadPoolSize).ifIsSatisfied(
                            maxThreadPoolSize::nonNull)))
               .field("keepAliveDuration",
                      v -> v.keepAliveDuration,
                      isGreaterThanOrEqualTo(value(Duration.ZERO)))
               .field("queueCapacity", v -> v.queueCapacity, isStrictlyPositive())
               .build();
      }

      /**
       * Bulkhead thread pool core size. Must be > 0. Default to {@link #DEFAULT_CORE_THREAD_POOL_SIZE}.
       */
      public int coreThreadPoolSize() {
         return coreThreadPoolSize;
      }

      /**
       * Bulkhead thread pool max size. Must be > 0. Default to {@link #DEFAULT_MAX_THREAD_POOL_SIZE}.
       */
      public int maxThreadPoolSize() {
         return maxThreadPoolSize;
      }

      /**
       * When the number of threads is greater than the core size, this is the maximum time duration
       * that idle threads will wait for new tasks before terminating. Must be >= 0.
       */
      public Duration keepAliveDuration() {
         return keepAliveDuration;
      }

      /**
       * Working queue capacity. Queue is holding tasks being executed. Must be > 0.
       */
      public int queueCapacity() {
         return queueCapacity;
      }

      public static class ThreadPoolBulkheadConfigBuilder extends DomainBuilder<ThreadPoolBulkheadConfig> {
         private Integer coreThreadPoolSize;
         private Integer maxThreadPoolSize;
         private Duration keepAliveDuration;
         private Integer queueCapacity;

         public ThreadPoolBulkheadConfigBuilder coreThreadPoolSize(Integer coreThreadPoolSize) {
            this.coreThreadPoolSize = coreThreadPoolSize;
            return this;
         }

         public ThreadPoolBulkheadConfigBuilder maxThreadPoolSize(Integer maxThreadPoolSize) {
            this.maxThreadPoolSize = maxThreadPoolSize;
            return this;
         }

         public ThreadPoolBulkheadConfigBuilder keepAliveDuration(Duration keepAliveDuration) {
            this.keepAliveDuration = keepAliveDuration;
            return this;
         }

         public ThreadPoolBulkheadConfigBuilder queueCapacity(Integer queueCapacity) {
            this.queueCapacity = queueCapacity;
            return this;
         }

         @Override
         protected ThreadPoolBulkheadConfig buildDomainObject() {
            return new ThreadPoolBulkheadConfig(this);
         }
      }

   }

   public static class TimeLimiterConfig extends AbstractValue {
      private final Duration timeoutDuration;

      public TimeLimiterConfig(TimeLimiterConfigBuilder builder) {
         this.timeoutDuration = builder.timeoutDuration;
      }

      @Override
      public Fields<? extends TimeLimiterConfig> defineDomainFields() {
         return Fields
               .<TimeLimiterConfig>builder()
               .field("timeoutDuration",
                      v -> v.timeoutDuration,
                      isNotNull().andValue(isGreaterThanOrEqualTo(value(Duration.ZERO))))
               .build();
      }

      /**
       * Maximum task duration until timeout. Must be >= 0.
       */
      public Duration timeoutDuration() {
         return timeoutDuration;
      }

      public static class TimeLimiterConfigBuilder extends DomainBuilder<TimeLimiterConfig> {
         private Duration timeoutDuration;

         public TimeLimiterConfigBuilder timeoutDuration(Duration timeoutDuration) {
            this.timeoutDuration = timeoutDuration;
            return this;
         }

         @Override
         protected TimeLimiterConfig buildDomainObject() {
            return new TimeLimiterConfig(this);
         }
      }

   }

   public static class RateLimiterConfig extends AbstractValue {
      private final Duration limitRefreshPeriod;
      private final Integer limitForPeriod;
      private final Duration timeoutDuration;

      public RateLimiterConfig(RateLimiterConfigBuilder builder) {
         this.limitRefreshPeriod = builder.limitRefreshPeriod;
         this.limitForPeriod = builder.limitForPeriod;
         this.timeoutDuration = builder.timeoutDuration;
      }

      @Override
      public Fields<? extends RateLimiterConfig> defineDomainFields() {
         return Fields
               .<RateLimiterConfig>builder()
               .field("limitRefreshPeriod",
                      v -> v.limitRefreshPeriod,
                      isNotNull().andValue(isGreaterThanOrEqualTo(value(Duration.ofNanos(1)))))
               .field("limitForPeriod", v -> v.limitForPeriod, isPositive())
               .field("timeoutDuration",
                      v -> v.timeoutDuration,
                      isNotNull().andValue(isGreaterThanOrEqualTo(value(Duration.ZERO))))
               .build();
      }

      /**
       * Duration after which limit is refreshed. Used accordingly with {@link #limitForPeriod()}. Must be >=
       * 1 ns.
       */
      public Duration limitRefreshPeriod() {
         return limitRefreshPeriod;
      }

      /**
       * Maximum limit value for a period. Used accordingly with {@link #limitRefreshPeriod()}. Must be >= 0.
       */
      public int limitForPeriod() {
         return limitForPeriod;
      }

      /**
       * Maximum wait duration for permission. Must be >= 0.
       */
      public Duration timeoutDuration() {
         return timeoutDuration;
      }

      public static class RateLimiterConfigBuilder extends DomainBuilder<RateLimiterConfig> {
         private Duration limitRefreshPeriod;
         private Integer limitForPeriod;
         private Duration timeoutDuration;

         public RateLimiterConfigBuilder limitRefreshPeriod(Duration limitRefreshPeriod) {
            this.limitRefreshPeriod = limitRefreshPeriod;
            return this;
         }

         public RateLimiterConfigBuilder limitForPeriod(Integer limitForPeriod) {
            this.limitForPeriod = limitForPeriod;
            return this;
         }

         public RateLimiterConfigBuilder timeoutDuration(Duration timeoutDuration) {
            this.timeoutDuration = timeoutDuration;
            return this;
         }

         @Override
         protected RateLimiterConfig buildDomainObject() {
            return new RateLimiterConfig(this);
         }
      }

   }

   public static class RetryConfig extends AbstractValue {
      private final Integer maxAttempts;
      private final Duration waitDuration;

      public RetryConfig(RetryConfigBuilder builder) {
         this.maxAttempts = builder.maxAttempts;
         this.waitDuration = builder.waitDuration;
      }

      @Override
      public Fields<? extends RetryConfig> defineDomainFields() {
         return Fields
               .<RetryConfig>builder()
               .field("maxAttempts", v -> v.maxAttempts, isStrictlyPositive())
               .field("waitDuration",
                      v -> v.waitDuration,
                      isNotNull().andValue(isGreaterThanOrEqualTo(value(Duration.ZERO))))
               .build();
      }

      /**
       * Maximum retry attempts. Must be > 0.
       */
      public int maxAttempts() {
         return maxAttempts;
      }

      /**
       * Wait duration between each attempt. Must be >= 0.
       */
      public Duration waitDuration() {
         return waitDuration;
      }

      public static class RetryConfigBuilder extends DomainBuilder<RetryConfig> {
         private Integer maxAttempts;
         private Duration waitDuration;

         public RetryConfigBuilder maxAttempts(Integer maxAttempts) {
            this.maxAttempts = maxAttempts;
            return this;
         }

         public RetryConfigBuilder waitDuration(Duration waitDuration) {
            this.waitDuration = waitDuration;
            return this;
         }

         @Override
         protected RetryConfig buildDomainObject() {
            return new RetryConfig(this);
         }
      }

   }

}
