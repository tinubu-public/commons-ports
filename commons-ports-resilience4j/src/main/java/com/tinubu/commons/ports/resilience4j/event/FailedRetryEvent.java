/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.resilience4j.event;

import static com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport.checkInvariants;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.NumberRules.isPositive;

import com.tinubu.commons.ddd2.domain.type.Fields;

import io.github.resilience4j.retry.event.RetryOnErrorEvent;
import io.github.resilience4j.retry.event.RetryOnIgnoredErrorEvent;

/**
 * A retryable call definitively failed after a certain number of attempt.
 * The failure cause is available in {@link #errorCause()}.
 */
public final class FailedRetryEvent extends Resilience4jEvent {

   private final RetryErrorCause errorCause;
   private final int retriedAttempts;

   public FailedRetryEvent(Object source, RetryOnErrorEvent event) {
      super(source, event.getCreationTime().toInstant(), event.getName());
      this.errorCause = RetryErrorCause.MAX_ATTEMPTS_REACHED;
      this.retriedAttempts = event.getNumberOfRetryAttempts();

      checkInvariants(this);
   }

   public FailedRetryEvent(Object source, RetryOnIgnoredErrorEvent event) {
      super(source, event.getCreationTime().toInstant(), event.getName());
      this.errorCause = RetryErrorCause.NON_RETRYABLE_EXCEPTION;
      this.retriedAttempts = event.getNumberOfRetryAttempts();

      checkInvariants(this);
   }

   @Override
   @SuppressWarnings("unchecked")
   protected Fields<? extends FailedRetryEvent> defineDomainFields() {
      return Fields
            .<FailedRetryEvent>builder()
            .superFields((Fields<FailedRetryEvent>) super.defineDomainFields())
            .field("errorCause", v -> v.errorCause, isNotNull())
            .field("retriedAttempts", v -> v.retriedAttempts, isPositive())
            .build();
   }

   /**
    * Number of retried attempts.
    *
    * @return number of retried attempts
    */
   public int retriedAttempts() {
      return retriedAttempts;
   }

   /**
    * Error cause of retryable call failure.
    *
    * @return error cause of retryable call failure
    */
   public RetryErrorCause errorCause() {
      return errorCause;
   }

   @Override
   public String toString() {
      return super.toString() + " > Retried call failed on " + errorCause;
   }

   public enum RetryErrorCause {
      /**
       * Retry failed because the maximum number of attempts has been reached.
       */
      MAX_ATTEMPTS_REACHED,
      /**
       * Retry failed because an exception has been caught, and is configured as non-retryable.
       */
      NON_RETRYABLE_EXCEPTION
   }
}
