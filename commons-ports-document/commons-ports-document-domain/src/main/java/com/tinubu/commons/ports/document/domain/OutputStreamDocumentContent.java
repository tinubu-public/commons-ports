/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.hasNoNullElements;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.OptionalUtils.optionalInstanceOf;
import static com.tinubu.commons.lang.util.StreamUtils.reversedStream;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

import org.apache.commons.io.output.WriterOutputStream;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.valueformatter.HiddenValueFormatter;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;

/**
 * Document content stream implementation for write access.
 * This content is represented with an {@link OutputStream}.
 * This implementation should be used only by
 * {@link DocumentRepository#openDocument(DocumentPath, boolean, boolean, OpenDocumentMetadata)}
 * implementations.
 */
public class OutputStreamDocumentContent extends AbstractValue implements DocumentContent {

   /** Source output stream unmodified. */
   protected final OutputStream sourceOutputStream;
   /** Document content as {@link OutputStream} decorated with {@link AutoFinalizingOutputStream}. */
   protected final OutputStream content;
   protected final Charset contentEncoding;
   protected final Charset sourceEncoding;
   /**
    * Optional content output stream mappers to wrap the content output stream.
    * Mappers are added in order, but will be applied in reverse order to comply with output stream
    * composition logic : {@code [ gzipArchiver, b64encoder ] => gzipArchiver(b64encoder(content))}.
    */
   protected final List<Function<OutputStream, OutputStream>> contentMappers;
   /**
    * Instantiation of the document content decorated with all the content mappers applied.
    */
   protected final OutputStream mappedContent;

   protected OutputStreamDocumentContent(OutputStreamDocumentContentBuilder builder) {
      this.sourceOutputStream = builder.content;
      this.sourceEncoding = builder.sourceEncoding;
      this.content = nullable(sourceOutputStream,
                              content -> new AutoFinalizingOutputStream(content,
                                                                        nullable(builder.finalize,
                                                                                 __ -> { })));
      this.contentEncoding = builder.contentEncoding;
      this.contentMappers = list(builder.contentMappers);
      this.mappedContent = mappedContent(content, list(builder.contentMappers));
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends OutputStreamDocumentContent> defineDomainFields() {
      return Fields
            .<OutputStreamDocumentContent>builder()
            .superFields((Fields<OutputStreamDocumentContent>) super.defineDomainFields())
            .field("sourceOutputStream", v -> v.sourceOutputStream, new HiddenValueFormatter(), isNotNull())
            .field("sourceEncoding", v -> v.sourceEncoding)
            .field("content", v -> v.content, new HiddenValueFormatter(), isNotNull())
            .technicalField("mappedContent", v -> v.mappedContent, new HiddenValueFormatter(), isNotNull())
            .field("contentEncoding", v -> v.contentEncoding)
            .field("contentMappers", v -> v.contentMappers, hasNoNullElements())
            .build();
   }

   @Override
   public String stringContent(Charset defaultContentEncoding) {
      throw new IllegalStateException("Unsupported operation for this content implementation");
   }

   @Override
   public String stringContent() {
      return stringContent(contentEncoding().orElseThrow(() -> new IllegalStateException(
            "No content encoding set, you should specify one to encode this content")));
   }

   @Override
   public InputStream inputStreamContent() {
      throw new IllegalStateException("Unsupported operation for this content implementation");
   }

   /**
    * {@inheritDoc}
    * Content output stream mappers are applied to content in reverse order to comply with output stream
    * composition logic constraints.
    */
   @Override
   public OutputStream outputStreamContent() {
      return mappedContent;
   }

   /**
    * Returns specified content stream mapped with specified mappers.
    *
    * @param content content to map, or {@code null}
    * @param contentMappers optional content output stream mappers to wrap the content output stream.
    *       Mappers are added in order, but will be applied in reverse order to comply with output stream
    *       composition logic : {@code [ gzipArchiver, b64encoder ] => gzipArchiver(b64encoder(content))}.
    *
    * @return mapped output stream, or {@code null}
    */
   private static OutputStream mappedContent(OutputStream content,
                                             List<Function<OutputStream, OutputStream>> contentMappers) {
      OutputStream out = content;

      if (out != null) {
         for (Function<OutputStream, OutputStream> contentMapper : list(reversedStream(contentMappers))) {
            out = contentMapper.apply(out);
         }
      }

      return out;
   }

   /**
    * Returns the content {@link OutputStream}, wrapped with {@link AutoFinalizingOutputStream}, but
    * without content mappers applied.
    *
    * @return raw document content
    */
   public OutputStream unmappedOutputStreamContent() {
      return content;
   }

   /**
    * Returns the source {@link OutputStream}, without any decorators applied.
    *
    * @return source output stream
    */
   public OutputStream sourceOutputStream() {
      return sourceOutputStream;
   }

   /**
    * Optional source encoding. This is the source output stream encoding.
    */
   @Getter
   public Optional<Charset> sourceEncoding() {
      return nullable(sourceEncoding);
   }

   @Override
   public InputStreamReader readerContent(Charset defaultContentEncoding) {
      throw new IllegalStateException("Unsupported operation for this content implementation");
   }

   @Override
   public InputStreamReader readerContent() {
      throw new IllegalStateException("Unsupported operation for this content implementation");
   }

   @Override
   public Writer writerContent(Charset defaultContentEncoding) {
      notNull(defaultContentEncoding, "defaultContentEncoding");

      return new OutputStreamWriter(outputStreamContent(), sourceEncoding().orElse(defaultContentEncoding));
   }

   @Override
   public Writer writerContent() {
      return writerContent(sourceEncoding().orElseThrow(() -> new IllegalStateException(
            "No content encoding set, you should specify one to encode this content")));
   }

   @Override
   @Getter
   public Optional<Long> contentSize() {
      return optional();
   }

   @Override
   @Getter
   public byte[] content() {
      throw new IllegalStateException("Unsupported operation for this content implementation");
   }

   /**
    * {@inheritDoc}
    * <p>
    * This is the encoding of the content resulting from decorated
    * streams.
    */
   @Override
   @Getter
   public Optional<Charset> contentEncoding() {
      return nullable(contentEncoding);
   }

   @Override
   public void close() {
      try {
         this.mappedContent.close();
      } catch (IOException e) {
         throw new DocumentAccessException(e);
      }
   }

   public static OutputStreamDocumentContentBuilder reconstituteBuilder() {
      return new OutputStreamDocumentContentBuilder().reconstitute();
   }

   public static class OutputStreamDocumentContentBuilder extends DomainBuilder<OutputStreamDocumentContent> {
      private Charset sourceEncoding;
      private OutputStream content;
      private Charset contentEncoding;
      private Consumer<OutputStream> finalize;
      private List<Function<OutputStream, OutputStream>> contentMappers = list();

      public static OutputStreamDocumentContentBuilder from(DocumentContent documentContent) {
         return new OutputStreamDocumentContentBuilder()
               .<OutputStreamDocumentContentBuilder>reconstitute()
               .sourceEncoding(optionalInstanceOf(documentContent, OutputStreamDocumentContent.class)
                                     .flatMap(OutputStreamDocumentContent::sourceEncoding)
                                     .orElse(null))
               .content(optionalInstanceOf(documentContent, OutputStreamDocumentContent.class)
                              .map(OutputStreamDocumentContent::sourceOutputStream)
                              .orElseGet(documentContent::outputStreamContent))
               .contentEncoding(documentContent.contentEncoding().orElse(null))
               .contentMappers(optionalInstanceOf(documentContent, OutputStreamDocumentContent.class)
                                     .map(dc -> dc.contentMappers)
                                     .orElse(null));
      }

      @Setter
      public OutputStreamDocumentContentBuilder sourceEncoding(Charset sourceEncoding) {
         ensureReconstitute();
         this.sourceEncoding = sourceEncoding;
         return this;
      }

      public OutputStreamDocumentContentBuilder content(OutputStream content, Charset contentEncoding) {
         this.content = content;
         this.contentEncoding = contentEncoding;
         if (!isReconstitute()) {
            this.sourceEncoding = contentEncoding;
         }
         return this;
      }

      public OutputStreamDocumentContentBuilder content(Writer content, Charset contentEncoding) {
         return content(new WriterOutputStream(notNull(content, "content"),
                                               notNull(contentEncoding, "contentEncoding")), contentEncoding);
      }

      @Setter
      public OutputStreamDocumentContentBuilder content(OutputStream content) {
         if (isReconstitute()) {
            this.content = content;
         } else {
            content(content, null);
         }
         return this;
      }

      @Setter
      public OutputStreamDocumentContentBuilder contentEncoding(Charset contentEncoding) {
         ensureReconstitute();
         this.contentEncoding = contentEncoding;
         return this;
      }

      @Setter
      public OutputStreamDocumentContentBuilder contentMappers(List<Function<OutputStream, OutputStream>> contentMappers) {
         this.contentMappers = list(contentMappers);
         return this;
      }

      public OutputStreamDocumentContentBuilder contentMapper(Function<OutputStream, OutputStream> contentMapper) {
         this.contentMappers.add(contentMapper);
         return this;
      }

      public OutputStreamDocumentContentBuilder finalize(Consumer<OutputStream> finalize) {
         this.finalize = finalize;
         return this;
      }

      @Override
      public OutputStreamDocumentContent buildDomainObject() {
         return new OutputStreamDocumentContent(this);
      }

   }

   /**
    * {@link OutputStream} that automatically calls an arbitrary finalize operation at close.
    */
   private static class AutoFinalizingOutputStream extends OutputStream {

      private final OutputStream outputStream;
      private final Consumer<OutputStream> finalize;
      private boolean closed = false;

      public AutoFinalizingOutputStream(OutputStream outputStream, Consumer<OutputStream> finalize) {
         this.outputStream = notNull(outputStream, "outputStream");
         this.finalize = notNull(finalize, "finalize");
      }

      @Override
      public void write(byte[] b) throws IOException {
         outputStream.write(b);
      }

      @Override
      public void write(byte[] b, int off, int len) throws IOException {
         outputStream.write(b, off, len);
      }

      @Override
      public void flush() throws IOException {
         outputStream.flush();
      }

      @Override
      public void write(int b) throws IOException {
         outputStream.write(b);
      }

      @Override
      public void close() throws IOException {
         if (!closed) {
            closed = true;

            try {
               outputStream.close();
            } finally {
               finalize.accept(outputStream);
            }
         }
      }
   }
}


