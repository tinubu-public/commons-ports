/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.event;

import static com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport.checkInvariants;

import java.net.URI;
import java.time.Duration;

import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentRepository;

/**
 * Document successfully deleted event.
 */
public final class DocumentDeleted extends DocumentRepositoryOperationEvent {

   private final DocumentEntry document;

   public DocumentDeleted(DocumentRepository repository,
                          DocumentEntry document,
                          URI documentUri,
                          Duration duration) {
      super(repository, document.documentId(), documentUri, duration);
      this.document = document;

      checkInvariants(this);
   }

   public DocumentDeleted(DocumentRepository repository, DocumentEntry document, URI documentUri) {
      super(repository, document.documentId(), documentUri);
      this.document = document;

      checkInvariants(this);
   }

   /**
    * Document for which this event has been sent.
    *
    * @return document entry
    */
   @Getter
   public DocumentEntry document() {
      return document;
   }

   @Override
   public String toString() {
      return String.format("%s : Document '%s' -> '%s' deleted in %d ms",
                           repositoryName(),
                           documentId.stringValue(),
                           documentDisplayId(),
                           duration.toMillis());
   }
}
