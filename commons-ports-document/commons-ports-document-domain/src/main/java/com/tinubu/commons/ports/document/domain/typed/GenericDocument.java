/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.typed;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.nio.charset.Charset;

import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentPath;

/**
 * Generic {@link Document}. Very similar to directly using a {@link Document}, but provides
 * {@code open(...)} and {@code write(...)} operations.
 * Document content-type is not required.
 * Document content-encoding is only required when using read/write operations that require it.
 */
public class GenericDocument extends TypedDocument<GenericDocument> {

   /**
    * Default line separator for {@link #writeLn(String)} operation.
    *
    * @apiNote Never use {@link System#lineSeparator()} here to keep deterministic behavior.
    */
   protected static final String DEFAULT_LINE_SEPARATOR = "\n";

   protected GenericDocument(GenericDocumentBuilder builder) {
      super(builder);
   }

   /**
    * Creates a new generic document from existing document. Source document metadata are not changed.
    *
    * @param document existing document to base on
    * @param assumeContentType if set, assume document content-type if content-type is unknown or has no
    *       encoding, otherwise if {@code null}, does not assume content-type
    *
    * @return new generic document
    */
   public static GenericDocument of(Document document, MimeType assumeContentType) {
      notNull(document, "document");

      return new GenericDocumentBuilder()
            .reconstitute()
            .copy(document)
            .assumeContentType(assumeContentType != null, assumeContentType)
            .build();
   }

   public static GenericDocument of(Document document) {
      return of(document, null);
   }

   public static GenericDocument open(DocumentPath documentId,
                                      OutputStream contentOutputStream,
                                      Charset contentEncoding,
                                      MimeType contentType) {
      notNull(documentId, "documentId");

      return new GenericDocumentBuilder()
            .open(documentId, contentOutputStream, contentEncoding, contentType)
            .build();
   }

   public static GenericDocument open(DocumentPath documentId,
                                      OutputStream contentOutputStream,
                                      Charset contentEncoding) {
      return open(documentId, contentOutputStream, contentEncoding, null);
   }

   public static GenericDocument open(DocumentPath documentId, Charset contentEncoding) {
      return open(documentId, null, contentEncoding);
   }

   public static GenericDocument open(DocumentPath documentId, OutputStream contentOutputStream) {
      return open(documentId, contentOutputStream, null);
   }

   public static GenericDocument open(DocumentPath documentId) {
      return open(documentId, null, null);
   }

   @Override
   protected boolean requiredContentType() {
      return false;
   }

   @Override
   public GenericDocument write(byte[] buffer, int offset, int length) {
      return super.write(buffer, offset, length);
   }

   @Override
   public GenericDocument write(byte[] buffer) {
      return super.write(buffer);
   }

   @Override
   public GenericDocument write(InputStream inputStream) {
      return super.write(inputStream);
   }

   @Override
   public GenericDocument write(String string) {
      return super.write(string);
   }

   @Override
   public GenericDocument write(Reader reader) {
      return super.write(reader);
   }

   @Override
   public GenericDocument write(Document document) {
      return super.write(document);
   }

   @Override
   public GenericDocument writeLn(String string) {
      return super.writeLn(string);
   }

   @Override
   public String lineSeparator() {
      return DEFAULT_LINE_SEPARATOR;
   }

   @Override
   public GenericDocument finish() {
      return super.finish();
   }

   @Override
   public GenericDocumentBuilder documentBuilder() {
      return new GenericDocumentBuilder();
   }

   public static GenericDocumentBuilder reconstituteBuilder() {
      return new GenericDocumentBuilder().reconstitute();
   }

   public static class GenericDocumentBuilder
         extends TypedDocumentBuilder<GenericDocument, GenericDocumentBuilder> {

      /**
       * Copy constructor for {@link GenericDocument}
       *
       * @param document document to copy
       *
       * @return generic document builder
       */
      public static GenericDocumentBuilder from(Document document) {
         notNull(document, "document");

         return new DocumentBuilder().<GenericDocumentBuilder>reconstitute().copy(document);
      }

      protected GenericDocumentBuilder assumeContentType(boolean assumeContentType, MimeType contentType) {
         return super.assumeContentType(assumeContentType, assumeContentType, contentType);
      }

      @Override
      public GenericDocument buildDomainObject() {
         return new GenericDocument(this);
      }

   }
}
