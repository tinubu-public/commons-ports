/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.noNullElements;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.ServiceLoader;

/**
 * Document repository factory.
 */
// FIXME tests
public class DocumentRepositoryFactory {

   /**
    * Default repositories.
    */
   private static List<UriAdapter> defaultUriAdapters = defaultUriAdapters();

   /**
    * Finds document repository by URI in specified adapters. First adapter
    * {@link UriAdapter#supportsUri(URI, boolean) supporting} the specified URI will be used.
    *
    * @param documentRepositoryUri document repository URI, can be the repository root or a document URI
    *       in this repository
    *
    * @return found document repository or {@link Optional#empty()} if repository not found
    */
   public Optional<DocumentRepository> documentRepository(URI documentRepositoryUri,
                                                          List<? extends UriAdapter> uriAdapters) {
      notNull(documentRepositoryUri, "documentRepositoryUri");
      noNullElements(uriAdapters, "uriAdapters");

      return stream(uriAdapters).filter(repository -> repository.supportsUri(documentRepositoryUri, false))
            .map(DocumentRepository.class::cast)
            .findFirst();
   }

   /**
    * Finds document repository by URI in specified adapters. First adapter
    * {@link UriAdapter#supportsUri(URI, boolean) supporting} the specified URI will be used.
    *
    * @param documentRepositoryUri document repository URI, can be the repository root or a document URI
    *       in this repository
    *
    * @return found document repository or {@link Optional#empty()} if repository not found
    */
   public Optional<DocumentRepository> documentRepository(URI documentRepositoryUri,
                                                          UriAdapter... uriAdapters) {
      return documentRepository(documentRepositoryUri, list(notNull(uriAdapters, "uriAdapters")));
   }

   /**
    * Finds document repository by URI in default provided adapters. First adapter
    * {@link UriAdapter#supportsUri(URI, boolean) supporting} the specified URI will be used.
    * Default provided adapters are dynamically loaded using {@link ServiceLoader} registered
    * {@link UriAdapter}s.
    * <p>
    * Out-of-the-box registered adapters :
    * <ul>
    * <li>{@code file}</li>
    * <li>{@code classpath}</li>
    * </ul>
    *
    * @param documentRepositoryUri document repository URI, can be the repository root or a document URI
    *       in this repository
    *
    * @return found document repository or {@link Optional#empty()} if repository not found
    */
   public Optional<DocumentRepository> documentRepository(URI documentRepositoryUri) {
      return documentRepository(documentRepositoryUri, defaultUriAdapters);
   }

   private synchronized static List<UriAdapter> defaultUriAdapters() {
      ServiceLoader<UriAdapter> uriAdapters = ServiceLoader.load(UriAdapter.class);

      return list(stream(uriAdapters));
   }

   public synchronized static void resetDefaultUriAdapters() {
      DocumentRepositoryFactory.defaultUriAdapters = list();
   }

   public synchronized static void setDefaultUriAdapters(List<UriAdapter> uriAdapters) {
      noNullElements(uriAdapters, "uriAdapters");

      DocumentRepositoryFactory.defaultUriAdapters = list(uriAdapters);
   }

   public synchronized static void addDefaultUriAdapters(List<UriAdapter> uriAdapters) {
      noNullElements(uriAdapters, "uriAdapters");

      DocumentRepositoryFactory.defaultUriAdapters.addAll(uriAdapters);
   }
}
