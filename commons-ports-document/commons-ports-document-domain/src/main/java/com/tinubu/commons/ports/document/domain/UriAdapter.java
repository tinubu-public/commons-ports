/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import java.net.URI;
import java.util.Optional;

import com.tinubu.commons.ports.document.domain.capability.RepositoryCapability;
import com.tinubu.commons.ports.document.domain.capability.UnsupportedCapabilityException;

/**
 * {@link URI} adapter for document repositories.
 */
public interface UriAdapter {

   /**
    * Returns the current repository as an absolute URI with a scheme specific to this
    * repository.
    * <p>
    * URIs are implementation specific and should uniquely identify implementation's physical repository.
    *
    * @return repository-specific absolute URI
    *
    * @throws UnsupportedCapabilityException if implementing repository does not support
    *       {@link RepositoryCapability#REPOSITORY_URI} capability
    */
   URI toUri();

   /**
    * Returns the specified document as an absolute URI with a scheme specific to this
    * repository.
    * <p>
    * URIs are implementation specific and should uniquely identify implementation's physical document.
    * <p>
    * The specified document is not required to exist on the repository and should not be
    * checked in this operation.
    *
    * @param documentId document identifier
    *
    * @return document as a repository-specific absolute URI
    *
    * @throws UnsupportedCapabilityException if implementing repository does not support
    *       {@link RepositoryCapability#DOCUMENT_URI} capability
    */
   URI toUri(DocumentPath documentId);

   /**
    * Returns {@code true} if the specified URI is supported by this repository. An URI is supported if it has
    * the correct format, and if it identifies the same implementation's physical repository.
    *
    * @param uri URI
    * @param documentUri whether specified URI must be a document URI. If set to {@code false}, URI can
    *       be either a repository base URI or a document URI
    *
    * @return {@code true} if the specified URI is supported by this repository
    *
    * @throws UnsupportedCapabilityException if implementing repository does not support
    *       {@link RepositoryCapability#REPOSITORY_URI} or {@link RepositoryCapability#DOCUMENT_URI}
    *       capability
    */
   boolean supportsUri(URI uri, boolean documentUri);

   /**
    * Returns {@code true} if the specified document URI is supported by this repository.  An URI is supported
    * if it has the correct format, and if it identifies the same implementation's physical repository.
    *
    * @param documentUri document URI
    *
    * @return {@code true} if the specified document URI is supported by this repository
    *
    * @throws UnsupportedCapabilityException if implementing repository does not support
    *       {@link RepositoryCapability#DOCUMENT_URI} capability
    */
   default boolean supportsDocumentUri(URI documentUri) {
      return supportsUri(documentUri, true);
   }

   /**
    * Finds document by URI.
    *
    * @param documentUri document URI
    *
    * @return found document or {@link Optional#empty()} if document not found
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    * @throws UnsupportedCapabilityException if implementing repository does not support
    *       {@link RepositoryCapability#DOCUMENT_URI} capability
    * @throws IllegalArgumentException if URI is not supported by this repository, use
    *       {@link #supportsUri(URI, boolean)} with {@code documentUri} to {@code true} before calling this
    *       method
    */
   Optional<Document> findDocumentByUri(URI documentUri);

   /**
    * Finds document entry by URI.
    *
    * @param documentUri document URI
    *
    * @return found document entry or {@link Optional#empty()} if document not found
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    * @throws UnsupportedCapabilityException if implementing repository does not support
    *       {@link RepositoryCapability#READABLE} capability
    * @throws IllegalArgumentException if URI is not supported by this repository, use
    *       {@link #supportsUri(URI, boolean)} with {@code documentUri} to {@code true} before calling this
    *       method
    */
   Optional<DocumentEntry> findDocumentEntryByUri(URI documentUri);

}
