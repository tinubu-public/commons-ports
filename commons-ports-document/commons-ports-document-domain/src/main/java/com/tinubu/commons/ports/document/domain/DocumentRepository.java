/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.listConcat;
import static com.tinubu.commons.lang.util.ExceptionUtils.smartFinalize;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.lang.validation.Validate.satisfies;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.DOCUMENT_URI;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.REPOSITORY_URI;

import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

import com.tinubu.commons.ddd2.domain.event.DomainEvent;
import com.tinubu.commons.ddd2.domain.event.DomainEventListener;
import com.tinubu.commons.ddd2.domain.repository.CrudRepository;
import com.tinubu.commons.ddd2.domain.specification.Specification;
import com.tinubu.commons.lang.util.CheckedConsumer;
import com.tinubu.commons.lang.util.CheckedFunction;
import com.tinubu.commons.lang.util.CheckedPredicate;
import com.tinubu.commons.lang.validation.CheckReturnValue;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata.OpenDocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.capability.RepositoryCapability;
import com.tinubu.commons.ports.document.domain.capability.UnsupportedCapabilityException;
import com.tinubu.commons.ports.document.domain.event.DocumentAccessed;
import com.tinubu.commons.ports.document.domain.event.DocumentDeleted;
import com.tinubu.commons.ports.document.domain.event.DocumentSaved;
import com.tinubu.commons.ports.document.domain.event.LoggingDocumentRepositoryListener;
import com.tinubu.commons.ports.document.domain.metrology.DocumentRepositoryMetrology;
import com.tinubu.commons.ports.document.domain.processor.DocumentProcessor;
import com.tinubu.commons.ports.document.domain.processor.common.DocumentRenamer;
import com.tinubu.commons.ports.document.domain.processor.common.NoopDocumentRenamer;
import com.tinubu.commons.ports.document.domain.repository.NoopDocumentRepository;
import com.tinubu.commons.ports.document.domain.repository.UnionDocumentRepository;

/**
 * Domain repository to access {@link Document} documents. This abstraction enable to store or retrieve
 * documents.
 * <p>
 * A {@link DocumentPath} uniquely identify a document in the repository backend. It must be a relative path.
 *
 * @implSpec Repository implementations can implement {@link #close()} to release resources on
 *       repository release.
 */
// FIXME cache capabilities in all implmentations
public interface DocumentRepository
      extends CrudRepository<Document, DocumentPath>, UriAdapter, AutoCloseable {

   /**
    * Lists current repository capabilities.
    * <p>
    * Repository capabilities is the list of supported metadata, or main features for this repository. Having
    * a repository list a {@link RepositoryCapability#allMetadataCapabilities() metadata capability} does
    * <em>not</em> mean that the value is always present, as saved documents metadata can be partial for
    * example, but that the repository support it if provided.
    * Moreover, in some implementations, some listed capabilities can be dynamically supported, or not,
    * depending on context.
    *
    * @return current repository capabilities
    */
   default HashSet<RepositoryCapability> capabilities() {
      return RepositoryCapability.allCapabilities();
   }

   /**
    * Returns {@code true} if specified capability is supported for this repository.
    *
    * @param capability capability to check for
    *
    * @return {@code true} if specified capability is supported for this repository
    */
   @SuppressWarnings("ObjectEquality")
   default boolean hasCapability(RepositoryCapability capability) {
      HashSet<RepositoryCapability> capabilities = capabilities();

      return capabilities == RepositoryCapability.allCapabilities()
             ? true
             : capabilities.contains(capability);
   }

   /**
    * Generates a document identity in this repository, for display purpose, with best-effort.
    *
    * @param documentId document identifier
    *
    * @return document id representation
    */
   default String documentDisplayId(DocumentPath documentId) {
      if (hasCapability(DOCUMENT_URI)) {
         return toUri(documentId).toString();
      } else if (hasCapability(REPOSITORY_URI)) {
         return toUri() + "::" + documentId.stringValue();
      } else {
         return documentId.stringValue();
      }
   }

   /**
    * Returns a new repository with same configuration but all documents are identified relatively to
    * specified sub path.
    * Depending on repository implementation, parent repository context can be shared with this newly created
    * sub path repository, e.g.: client, connection pool, bulkhead, ... In this case, no repository sharing
    * the same context should be closed while another one is still in use.
    *
    * @param subPath relative sub path to configure new repository
    * @param shareContext whether to share parent repository context into this new sub path repository
    *
    * @return new repository identifying documents from specified sub path
    *
    * @throws UnsupportedCapabilityException if implementing repository does not support
    *       {@link RepositoryCapability#SUBPATH} capability
    */
   DocumentRepository subPath(Path subPath, boolean shareContext);

   /**
    * Generates a new {@link UnionDocumentRepository} with this repository on the lowest layer and specified
    * repositories on upper layers.
    * {@link NoopDocumentRepository} repositories are ignored.
    *
    * @param documentRepositories repositories to stack, from the lower to the upper layer
    *
    * @return new union document repository
    */
   default DocumentRepository stackRepositories(DocumentRepository... documentRepositories) {
      notNull(documentRepositories, "documentRepositories");

      return UnionDocumentRepository.ofLayers(listConcat(stream(this), stream(documentRepositories)));
   }

   /**
    * Generates a new {@link UnionDocumentRepository} with this repository on the lowest layer and specified
    * repository on upper layer.
    * {@link NoopDocumentRepository} repositories are ignored.
    *
    * @param documentRepository repository to stack to the upper layer
    * @param closeOnRepositoryClose whether to automatically close specified repository on
    *       {@link UnionDocumentRepository#close()}. If not enabled, this is
    *       the caller responsibility to later close the specified repository. If specified layer is a
    *       {@link UnionDocumentRepository} it is always recursively closed, you can't change this behavior
    *
    * @return new union document repository
    */
   default DocumentRepository stackRepository(DocumentRepository documentRepository,
                                              boolean closeOnRepositoryClose) {
      notNull(documentRepository, "documentRepository");

      UnionDocumentRepository unionDocumentRepository =
            UnionDocumentRepository.ofLayers(listConcat(stream(this), stream(documentRepository)));

      if (closeOnRepositoryClose) {
         unionDocumentRepository =
               unionDocumentRepository.appendLayerCloseStrategy(layer -> layer == documentRepository);
      }
      return unionDocumentRepository;
   }

   /**
    * Opens a new {@link Document} for creation or in append mode. The document content will provide an
    * {@link OutputStream} for direct write access to the underlying repository. The provided document does
    * not require to be saved using {@link DocumentRepository#saveDocument(Document, boolean)}, content is
    * directly written to underlying repository storage.
    * <p>
    * Overwrite/Append option combinations results :
    * <table>
    * <tr><th></th><th colspan="2"> overwrite </th><th colspan="2"> !overwrite </th></tr>
    * <tr><td></td><td>append</td><td>!append</td><td>append</td><td>!append</td></tr>
    * <tr><td>exists</td><td> APPEND </td><td> TRUNCATE </td><td> APPEND </td><td> return {@link Optional#empty() EMPTY} </td></tr>
    * <tr><td>not exists</td><td> CREATE </td><td> CREATE </td><td> CREATE </td><td> CREATE </td></tr>
    * </table>
    * <p>
    * If nothing is written to returned document's output stream, the expected behavior is that an empty
    * document is nonetheless created/truncated on repository.
    * <p>
    * Events :
    * <ul>
    *    <li>{@link DocumentSaved} is sent to this repository event listener for document on open success</li>
    * </ul>
    *
    * @param documentId document id to create
    * @param overwrite whether to overwrite an existing document with the same name
    * @param append whether to append content to an existing document with the same name
    * @param metadata document metadata (content-type, document path, attributes, ...)
    *
    * @return opened document, or {@link Optional#empty()} if document already exists and both overwrite and
    *       append are not set
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    * @throws UnsupportedCapabilityException if implementing repository does not support
    *       {@link RepositoryCapability#WRITABLE} capability
    */
   @CheckReturnValue
   Optional<Document> openDocument(DocumentPath documentId,
                                   boolean overwrite,
                                   boolean append,
                                   OpenDocumentMetadata metadata);

   /**
    * Opens a new {@link Document} for creation or in append mode. The document content will provide an
    * {@link OutputStream} for direct write access to the underlying repository.
    * <p>
    * If nothing is written to returned document's output stream, the expected behavior is that an empty
    * document is nonetheless created/truncated on repository.
    * <p>
    * Events :
    * <ul>
    *    <li>{@link DocumentSaved} is sent to this repository event listener for document on open success</li>
    * </ul>
    *
    * @param documentId document id to create
    * @param overwrite whether to overwrite an existing document with the same name
    * @param append whether to append content to an existing document with the same name
    *
    * @return opened document, or {@link Optional#empty()} if document already exists and both overwrite and
    *       append are not set
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    * @throws UnsupportedCapabilityException if implementing repository does not support
    *       {@link RepositoryCapability#WRITABLE} capability
    */
   @CheckReturnValue
   default Optional<Document> openDocument(DocumentPath documentId, boolean overwrite, boolean append) {
      return openDocument(documentId, overwrite, append, new OpenDocumentMetadataBuilder().build());
   }

   /**
    * Opens a new {@link Document} for creation. The document content will provide an {@link OutputStream}
    * for direct write access to the underlying repository.
    * <p>
    * If nothing is written to returned document's output stream, the expected behavior is that an empty
    * document is nonetheless created/truncated on repository.
    * <p>
    * Events :
    * <ul>
    *    <li>{@link DocumentSaved} is sent to this repository event listener for document on open success</li>
    * </ul>
    *
    * @param documentId document id to create
    * @param overwrite whether to overwrite an existing document with the same name
    *
    * @return opened document, or {@link Optional#empty()} if document already exists and both overwrite and
    *       append are not set
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    * @throws UnsupportedCapabilityException if implementing repository does not support
    *       {@link RepositoryCapability#WRITABLE} capability
    */
   @CheckReturnValue
   default Optional<Document> openDocument(DocumentPath documentId, boolean overwrite) {
      return openDocument(documentId, overwrite, false, new OpenDocumentMetadataBuilder().build());
   }

   /**
    * Finds document by id.
    * Returned document {@link Document#content()} must be {@link DocumentContent#close() closed}.
    * <p>
    * Events :
    * <ul>
    *    <li>{@link DocumentAccessed} is sent to this repository event listener for document on find success</li>
    * </ul>
    *
    * @param documentId document id
    *
    * @return found document or {@link Optional#empty()} if document not found
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    * @throws UnsupportedCapabilityException if implementing repository does not support
    *       {@link RepositoryCapability#READABLE} capability
    */
   Optional<Document> findDocumentById(DocumentPath documentId);

   /**
    * Finds document by document entry specification.
    * Returned {@link Stream} must be {@link Stream#close() closed}, for example using try-with-resources, to
    * free physical resources.
    * <p>
    * Events :
    * <ul>
    *    <li>{@link DocumentAccessed} is sent to this repository event listener for each document matching specification on success</li>
    * </ul>
    *
    * @param basePath starting document base path for all documents to search. Must be relative. Using
    *       this parameter may be used by implementation to optimally list documents from there instead of
    *       recursing from root
    * @param specification document entry specification
    *       Use empty directory to search for all documents.
    *
    * @return found documents
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    * @throws UnsupportedCapabilityException if implementing repository does not support
    *       {@link RepositoryCapability#QUERYABLE} capability
    * @implSpec Implementation should optimize repository crawling, ignoring whole navigation tree
    *       branches, using dedicated {@link DocumentEntrySpecification#satisfiedBySubPath(Path)}.
    * @see DocumentEntryCriteria
    */
   Stream<Document> findDocumentsBySpecification(Path basePath, Specification<DocumentEntry> specification);

   /**
    * Finds document by document entry specification from backend default directory. Using {@link
    * #findDocumentsBySpecification(Path, Specification)} with a start path is more efficient.
    * Returned {@link Stream} must be {@link Stream#close() closed}, for example using try-with-resources, to
    * free physical resources.
    * <p>
    * Events :
    * <ul>
    *    <li>{@link DocumentAccessed} is sent to this repository event listener for each document matching specification on success</li>
    * </ul>
    *
    * @param specification document entry specification
    *
    * @return found documents
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    * @throws UnsupportedCapabilityException if implementing repository does not support
    *       {@link RepositoryCapability#QUERYABLE} capability
    * @implSpec Implementation should optimize repository crawling, ignoring whole navigation tree
    *       branches, using dedicated {@link DocumentEntrySpecification#satisfiedBySubPath(Path)}.
    * @see DocumentEntryCriteria
    */
   default Stream<Document> findDocumentsBySpecification(Specification<DocumentEntry> specification) {
      return findDocumentsBySpecification(Paths.get(""), specification);
   }

   /**
    * Finds document entry by id. This enables to only retrieve metadata and not document content.
    * If document exists, a document metadata is always returned with available data.
    * <p>
    * Events :
    * <ul>
    *    <li>{@link DocumentAccessed} is sent to this repository event listener for document on find success</li>
    * </ul>
    *
    * @param documentId document id
    *
    * @return found document entry or {@link Optional#empty()} if document not found
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    * @throws UnsupportedCapabilityException if implementing repository does not support
    *       {@link RepositoryCapability#READABLE} capability
    */
   Optional<DocumentEntry> findDocumentEntryById(DocumentPath documentId);

   /**
    * Finds document entries by document entry specification.
    * Returned {@link Stream} must be {@link Stream#close() closed}, for example using try-with-resources, to
    * free physical resources.
    * <p>
    * Events :
    * <ul>
    *    <li>{@link DocumentAccessed} is sent to this repository event listener for each document matching specification on success</li>
    * </ul>
    *
    * @param basePath starting document base path for all documents to search. Must be relative. Using
    *       this parameter may be used by implementation to optimally list documents from there instead of
    *       recursing from root
    * @param specification document entry specification
    *       Use empty directory to search for all documents.
    *
    * @return found document entries
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    * @throws UnsupportedCapabilityException if implementing repository does not support
    *       {@link RepositoryCapability#QUERYABLE} capability
    * @implSpec Implementation should optimize repository crawling, ignoring whole navigation tree
    *       branches, using dedicated {@link DocumentEntrySpecification#satisfiedBySubPath(Path)}.
    * @see DocumentEntryCriteria
    */
   Stream<DocumentEntry> findDocumentEntriesBySpecification(Path basePath,
                                                            Specification<DocumentEntry> specification);

   /**
    * Finds document entries by document entry specification from backend default directory. Using {@link
    * #findDocumentEntriesBySpecification(Path, Specification)} with a start path is more efficient.
    * Returned {@link Stream} must be {@link Stream#close() closed}, for example using try-with-resources, to
    * free physical resources.
    * <p>
    * Events :
    * <ul>
    *    <li>{@link DocumentAccessed} is sent to this repository event listener for each document matching specification on success</li>
    * </ul>
    *
    * @param specification document entry specification
    *
    * @return found document entries
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    * @throws UnsupportedCapabilityException if implementing repository does not support
    *       {@link RepositoryCapability#QUERYABLE} capability
    * @implSpec Implementation should optimize repository crawling, ignoring whole navigation tree
    *       branches, using dedicated {@link DocumentEntrySpecification#satisfiedBySubPath(Path)}.
    * @see DocumentEntryCriteria
    */
   default Stream<DocumentEntry> findDocumentEntriesBySpecification(Specification<DocumentEntry> specification) {
      return findDocumentEntriesBySpecification(Paths.get(""), specification);
   }

   /**
    * Saves specified document.
    * <p>
    * Returned document has the following characteristics :
    * <ul>
    *    <li>document {@link Document#documentId()} is always the same</li>
    *    <li>document content and metadata can be different if document is overwritten concurrently</li>
    * </ul>
    * <p>
    * Events :
    * <ul>
    *    <li>{@link DocumentSaved} is sent to this repository event listener for document on save success</li>
    * </ul>
    *
    * @param document document to save
    * @param overwrite accept to overwrite pre-existing document with same if {@code true}
    *
    * @return saved document entry, potentially different from original document, if document has been saved
    *       or {@link Optional#empty} otherwise : <ul>
    *       <li>if document with same id already exists and overwrite is not set</li>
    *       <li>saved document has been externally deleted just before returning</li>
    *       </ul>
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    * @throws UnsupportedCapabilityException if implementing repository does not support
    *       {@link RepositoryCapability#WRITABLE} capability
    */
   @CheckReturnValue
   Optional<DocumentEntry> saveDocument(Document document, boolean overwrite);

   /**
    * Saves specified document and returns saved document as a single operation.
    * <p>
    * Returned document has the following characteristics :
    * <ul>
    *    <li>document {@link Document#documentId()} is always the same</li>
    *    <li>document content and metadata can be different if document is overwritten concurrently</li>
    *    <li>document must be ready to be read, meaning that its {@link DocumentContent#inputStreamContent()} can always be consumed</li>
    * </ul>
    * Returned document {@link Document#content()} must be {@link DocumentContent#close() closed}.
    *
    * <p>
    * You should always use {@link #saveDocument(Document, boolean)} for performance, except if you really
    * need to work on saved {@link Document} and save an extra call.
    * <p>
    * Events :
    * <ul>
    *    <li>{@link DocumentSaved} is sent to this repository event listener for document on save success</li>
    * </ul>
    *
    * @param document document to save
    * @param overwrite accept to overwrite pre-existing document with same if {@code true}
    *
    * @return saved document, potentially different from original document, if document has been saved
    *       or {@link Optional#empty} otherwise : <ul>
    *       <li>if document with same id already exists and overwrite is not set</li>
    *       <li>saved document has been externally deleted just before returning</li>
    *       </ul>
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    * @throws UnsupportedCapabilityException if implementing repository does not support
    *       {@link RepositoryCapability#WRITABLE} capability
    */
   @CheckReturnValue
   Optional<Document> saveAndReturnDocument(Document document, boolean overwrite);

   /**
    * Deletes specified document.
    * <p>
    * Events :
    * <ul>
    *    <li>{@link DocumentDeleted} is sent to this repository event listener for document on delete success</li>
    * </ul>
    *
    * @param documentId document to delete
    *
    * @return document entry of deleted document, if document has been deleted or {@link Optional#empty}
    *       otherwise
    *       : document not found
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    * @throws UnsupportedCapabilityException if implementing repository does not support
    *       {@link RepositoryCapability#WRITABLE} capability
    */
   @CheckReturnValue
   Optional<DocumentEntry> deleteDocumentById(DocumentPath documentId);

   /**
    * Deletes documents matching document entry specification..
    * Returned {@link Stream} must be {@link Stream#close() closed}, for example using try-with-resources, to
    * free physical resources.
    * <p>
    * Events :
    * <ul>
    *    <li>{@link DocumentDeleted} is sent to this repository event listener for document on delete success</li>
    * </ul>
    *
    * @param basePath starting document base path for all documents to delete. Must be relative. Using
    *       this parameter may be used by implementation to optimally delete documents from there instead of
    *       recursing from root
    * @param specification document entry specification
    *       Use empty directory to delete for all documents in specified base path.
    *
    * @return deleted document entries
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    * @throws UnsupportedCapabilityException if implementing repository does not support
    *       {@link RepositoryCapability#WRITABLE} capability
    * @implSpec Returned collection is a {@link List} instead of a {@link Stream} because effective
    *       documents deletion must not depend on returned stream consumption which is informational.
    */
   @CheckReturnValue
   List<DocumentEntry> deleteDocumentsBySpecification(Path basePath,
                                                      Specification<DocumentEntry> specification);

   /**
    * Deletes documents matching document entry specification from backend default directory. Using {@link
    * #deleteDocumentsBySpecification(Path, Specification)} with a start path is more efficient.
    * Returned {@link Stream} must be {@link Stream#close() closed}, for example using try-with-resources, to
    * free physical resources.
    * <p>
    * Events :
    * <ul>
    *    <li>{@link DocumentDeleted} is sent to this repository event listener for document on delete success</li>
    * </ul>
    *
    * @param specification document entry specification
    *       Use empty directory to delete all documents.
    *
    * @return deleted document entries
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    * @throws UnsupportedCapabilityException if implementing repository does not support
    *       {@link RepositoryCapability#WRITABLE} capability
    * @implSpec Returned collection is a {@link List} instead of a {@link Stream} because effective
    *       documents deletion must not depend on returned stream consumption which is informational.
    */
   @CheckReturnValue
   default List<DocumentEntry> deleteDocumentsBySpecification(Specification<DocumentEntry> specification) {
      return deleteDocumentsBySpecification(Paths.get(""), specification);
   }

   /**
    * Deletes specified document.
    * <p>
    * Events :
    * <ul>
    *    <li>{@link DocumentDeleted} is sent to this repository event listener for document on delete success</li>
    * </ul>
    *
    * @param documentId document to delete
    *
    * @return document entry of deleted document, if document has been deleted or {@link Optional#empty}
    *       otherwise
    *       : document not found
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    * @throws UnsupportedCapabilityException if implementing repository does not support
    *       {@link RepositoryCapability#WRITABLE} capability
    * @deprecated Use renamed {@link #deleteDocumentById(DocumentPath)} instead.
    */
   @CheckReturnValue
   @Deprecated
   default Optional<DocumentEntry> deleteDocument(DocumentPath documentId) {
      return deleteDocumentById(documentId);
   }

   /**
    * Transfers specified document to another repository.
    * Document is copied to target repository and not deleted from source repository.
    * <p>
    * Events :
    * <ul>
    *    <li>{@link DocumentAccessed} is sent to this repository event listener for document on find success</li>
    *    <li>{@link DocumentSaved} is sent to target repository event listener for document on save success</li>
    * </ul>
    *
    * @param documentId document to transfer
    * @param targetDocumentRepository target repository to transfer document to
    * @param overwrite accept to overwrite pre-existing document with same identifier
    * @param processor document processor to apply on transferred document
    *
    * @return new document entry in target repository or {@link Optional#empty} if document not
    *       transferred. Document is considered not transferred if not found, or already existing and
    *       overwrite is not set
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    * @throws UnsupportedCapabilityException if implementing repository does not support
    *       {@link RepositoryCapability#READABLE} and {@link RepositoryCapability#WRITABLE} capabilities
    */
   @CheckReturnValue
   default Optional<DocumentEntry> transferDocument(DocumentPath documentId,
                                                    DocumentRepository targetDocumentRepository,
                                                    boolean overwrite,
                                                    DocumentProcessor processor) {
      notNull(documentId, "documentId");
      notNull(targetDocumentRepository, "targetDocumentRepository");
      notNull(processor, "processor");
      satisfies(targetDocumentRepository,
                targetRepository -> !sameRepositoryAs(targetRepository) || !overwrite,
                "targetDocumentRepository",
                "Overwrite can't be set when source and target repositories are the same");

      return findDocumentById(documentId).flatMap(document -> {
         Document newDocument = processor.process(document);

         return targetDocumentRepository.saveDocument(newDocument, overwrite);
      });
   }

   /**
    * Transfers specified document to another repository.
    * Document is copied to target repository and not deleted from source repository.
    * <p>
    * Events :
    * <ul>
    *    <li>{@link DocumentAccessed} is sent to this repository event listener for document on find success</li>
    *    <li>{@link DocumentSaved} is sent to target repository event listener for document on save success</li>
    * </ul>
    *
    * @param documentId document to transfer
    * @param targetDocumentRepository target repository to transfer document to
    * @param overwrite accept to overwrite pre-existing document with same identifier
    * @param renamer document identifier mapper for target documents
    *
    * @return new document entry in target repository or {@link Optional#empty} if document not
    *       transferred. Document is considered not transferred if not found, or already existing and
    *       overwrite is not set
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    * @throws UnsupportedCapabilityException if implementing repository does not support
    *       {@link RepositoryCapability#READABLE} and {@link RepositoryCapability#WRITABLE} capabilities
    */
   @CheckReturnValue
   default Optional<DocumentEntry> transferDocument(DocumentPath documentId,
                                                    DocumentRepository targetDocumentRepository,
                                                    boolean overwrite,
                                                    DocumentRenamer renamer) {
      notNull(documentId, "documentId");
      notNull(targetDocumentRepository, "targetDocumentRepository");
      notNull(renamer, "renamer");
      satisfies(targetDocumentRepository,
                targetRepository -> !sameRepositoryAs(targetRepository) || !overwrite,
                "targetDocumentRepository",
                "Overwrite can't be set when source and target repositories are the same");

      return transferDocument(documentId, targetDocumentRepository, overwrite, (DocumentProcessor) renamer);
   }

   /**
    * Transfers specified document to another repository, with default document identifier mapper for target
    * document. Default mapper reuse source document identifier.
    * Document is copied to target repository and not deleted from source repository.
    * <p>
    * Events :
    * <ul>
    *    <li>{@link DocumentAccessed} is sent to this repository event listener for document on find success</li>
    *    <li>{@link DocumentSaved} is sent to target repository event listener for document on save success</li>
    * </ul>
    *
    * @param documentId document to transfer
    * @param targetDocumentRepository target repository to transfer document to
    * @param overwrite accept to overwrite pre-existing document with same identifier
    *
    * @return new document entry in target repository or {@link Optional#empty} if document not
    *       transferred. Document is considered not transferred if not found, or already existing and
    *       overwrite is not set
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    * @throws UnsupportedCapabilityException if implementing repository does not support
    *       {@link RepositoryCapability#READABLE} and {@link RepositoryCapability#WRITABLE} capabilities
    */
   @CheckReturnValue
   default Optional<DocumentEntry> transferDocument(DocumentPath documentId,
                                                    DocumentRepository targetDocumentRepository,
                                                    boolean overwrite) {
      return transferDocument(documentId, targetDocumentRepository, overwrite, new NoopDocumentRenamer());
   }

   /**
    * Transfers documents satisfied by specification to another repository.
    * Document is copied to target repository and not deleted from source repository.
    * <p>
    * Events :
    * <ul>
    *    <li>{@link DocumentAccessed} is sent to this repository event listener for each document matching specification on success</li>
    *    <li>{@link DocumentSaved} is sent to target repository event listener for each document matching specification on save success</li>
    * </ul>
    *
    * @param documentSpecification document specification to identify documents to transfer
    * @param targetDocumentRepository target repository to transfer documents to
    * @param overwrite accept to overwrite pre-existing document with same identifier
    * @param processor document processor to apply on transferred document
    *
    * @return transferred document entries in target repository. Documents are considered not transferred
    *       if already existing and overwrite is not set
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    * @throws UnsupportedCapabilityException if implementing repository does not support
    *       {@link RepositoryCapability#QUERYABLE} and {@link RepositoryCapability#WRITABLE} capabilities
    * @implSpec Returned collection is a {@link List} instead of a {@link Stream} because effective
    *       documents transfer must not depend on returned stream consumption which is informational.
    */
   // FIXME how to know what elements have been saved before an exception occurs ?
   default List<DocumentEntry> transferDocuments(Specification<DocumentEntry> documentSpecification,
                                                 DocumentRepository targetDocumentRepository,
                                                 boolean overwrite,
                                                 DocumentProcessor processor) {
      notNull(documentSpecification, "documentSpecification");
      notNull(targetDocumentRepository, "targetDocumentRepository");
      notNull(processor, "processor");
      satisfies(targetDocumentRepository,
                targetRepository -> !sameRepositoryAs(targetRepository) || !overwrite,
                "targetDocumentRepository",
                "Overwrite can't be set when source and target repositories are the same");

      return list(findDocumentsBySpecification(documentSpecification).flatMap(document -> {
         Document newDocument = processor.process(document);

         return targetDocumentRepository.saveDocument(newDocument, overwrite).stream();
      }));
   }

   /**
    * Transfers documents satisfied by specification to another repository.
    * Document is copied to target repository and not deleted from source repository.
    * <p>
    * Events :
    * <ul>
    *    <li>{@link DocumentAccessed} is sent to this repository event listener for each document matching specification on success</li>
    *    <li>{@link DocumentSaved} is sent to target repository event listener for each document matching specification on save success</li>
    * </ul>
    *
    * @param documentSpecification document specification to identify documents to transfer
    * @param targetDocumentRepository target repository to transfer documents to
    * @param overwrite accept to overwrite pre-existing document with same identifier
    * @param renamer document identifier mapper for target documents
    *
    * @return transferred document entries in target repository. Documents are considered not transferred
    *       if already existing and overwrite is not set
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    * @throws UnsupportedCapabilityException if implementing repository does not support
    *       {@link RepositoryCapability#QUERYABLE} and {@link RepositoryCapability#WRITABLE} capabilities
    * @implSpec Returned collection is a {@link List} instead of a {@link Stream} because effective
    *       documents transfer must not depend on returned stream consumption which is informational.
    */
   default List<DocumentEntry> transferDocuments(Specification<DocumentEntry> documentSpecification,
                                                 DocumentRepository targetDocumentRepository,
                                                 boolean overwrite,
                                                 DocumentRenamer renamer) {
      notNull(documentSpecification, "documentSpecification");
      notNull(targetDocumentRepository, "targetDocumentRepository");
      notNull(renamer, "renamer");
      satisfies(targetDocumentRepository,
                targetRepository -> !sameRepositoryAs(targetRepository) || !overwrite,
                "targetDocumentRepository",
                "Overwrite can't be set when source and target repositories are the same");

      return transferDocuments(documentSpecification,
                               targetDocumentRepository,
                               overwrite,
                               (DocumentProcessor) renamer);
   }

   /**
    * Transfers documents satisfied by specification to another repository,with default document identifier
    * mapper for target document. Default mapper reuse source document identifier.
    * Document is copied to target repository and not deleted from source repository.
    * <p>
    * Events :
    * <ul>
    *    <li>{@link DocumentAccessed} is sent to this repository event listener for each document matching specification on success</li>
    *    <li>{@link DocumentSaved} is sent to target repository event listener for each document matching specification on save success</li>
    * </ul>
    *
    * @param documentSpecification document specification to identify documents to transfer
    * @param targetDocumentRepository target repository to transfer documents to
    * @param overwrite accept to overwrite pre-existing document with same identifier
    *
    * @return transferred document entries in target repository. Documents are considered not transferred
    *       if already existing and overwrite is not set
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    * @throws UnsupportedCapabilityException if implementing repository does not support
    *       {@link RepositoryCapability#QUERYABLE} and {@link RepositoryCapability#WRITABLE} capabilities
    * @implSpec Returned collection is a {@link List} instead of a {@link Stream} because effective
    *       documents transfer must not depend on returned stream consumption which is informational.
    */
   default List<DocumentEntry> transferDocuments(Specification<DocumentEntry> documentSpecification,
                                                 DocumentRepository targetDocumentRepository,
                                                 boolean overwrite) {
      return transferDocuments(documentSpecification,
                               targetDocumentRepository,
                               overwrite,
                               new NoopDocumentRenamer());
   }

   /**
    * Registers event listeners for this repository.
    *
    * @param eventListener event listener
    * @param eventClasses event type classes
    * @param <E> event type
    */
   @SuppressWarnings("unchecked")
   <E extends DomainEvent> void registerEventListener(DomainEventListener<? super E> eventListener,
                                                      Class<? extends E>... eventClasses);

   /**
    * Registers event listener for all "write" events for this repository.
    *
    * @param eventListener event listener
    */
   @SuppressWarnings("unchecked")
   default void registerWriteEventsListener(DomainEventListener<? super DomainEvent> eventListener) {
      registerEventListener(eventListener, DocumentSaved.class, DocumentDeleted.class);
   }

   /**
    * Registers event listener for all "access" events for this repository.
    *
    * @param eventListener event listener
    */
   @SuppressWarnings("unchecked")
   default void registerAccessEventsListener(DomainEventListener<? super DomainEvent> eventListener) {
      registerEventListener(eventListener, DocumentAccessed.class);
   }

   /**
    * Registers logging event listener for this repository.
    *
    * @param eventClasses event type class
    * @param <E> event type
    */
   @SuppressWarnings("unchecked")
   default <E extends DomainEvent> void registerEventLoggingListener(Class<? extends E>... eventClasses) {
      registerEventListener(new LoggingDocumentRepositoryListener<>(), eventClasses);
   }

   /**
    * Registers logging event listener for all "write" events for this repository.
    */
   @SuppressWarnings("unchecked")
   default void registerWriteEventsLoggingListener() {
      registerEventLoggingListener(DocumentSaved.class, DocumentDeleted.class);
   }

   /**
    * Registers logging event listener for all "access" events for this repository.
    */
   @SuppressWarnings("unchecked")
   default void registerAccessEventsLoggingListener() {
      registerEventLoggingListener(DocumentAccessed.class);
   }

   /**
    * Registers a new {@link DocumentRepositoryMetrology metrology} component.
    *
    * @param metrology metrology component
    */
   @SuppressWarnings("unchecked")
   default <T extends DocumentRepositoryMetrology> void registerMetrology(T metrology) {
      registerEventListener(metrology, DomainEvent.class);
   }

   /**
    * Unregisters all event listeners.
    * Note that this also unregisters metrology.
    */
   void unregisterEventListeners();

   /**
    * Default no-op close operation.
    */
   @Override
   default void close() { }

   /**
    * {@inheritDoc}
    * <p>
    * Returned {@link Stream} must be {@link Stream#close() closed}, for example using try-with-resources, to
    * free physical resources.
    * <p>
    * Events :
    * <ul>
    *    <li>{@link DocumentAccessed} is sent to this repository event listener for each document matching specification on success</li>
    * </ul>
    *
    * @throws UnsupportedCapabilityException if implementing repository does not support
    *       {@link RepositoryCapability#QUERYABLE} capability
    */
   @Override
   default Stream<Document> findBySpecification(Specification<Document> specification) {
      throw new UnsupportedOperationException("Only DocumentEntry specifications are supported");
   }

   /**
    * {@inheritDoc}
    * <p>
    * Returned {@link Stream} must be {@link Stream#close() closed}, for example using try-with-resources, to
    * free physical resources.
    * <p>
    * Events :
    * <ul>
    *    <li>{@link DocumentAccessed} is sent to this repository event listener for each document on success</li>
    * </ul>
    *
    * @throws UnsupportedCapabilityException if implementing repository does not support
    *       {@link RepositoryCapability#ITERABLE} capability
    */
   @Override
   default Stream<Document> findAll() {
      return findDocumentsBySpecification(DocumentEntrySpecification.allDocuments());
   }

   /**
    * {@inheritDoc}
    * <p>
    * Returned document {@link Document#content()} must be {@link DocumentContent#close() closed}.
    * <p>
    * Events :
    * <ul>
    *    <li>{@link DocumentAccessed} is sent to this repository event listener for document on find success</li>
    * </ul>
    *
    * @throws UnsupportedCapabilityException if implementing repository does not support
    *       {@link RepositoryCapability#READABLE} capability
    */
   @Override
   default Optional<Document> findById(DocumentPath documentId) {
      return findDocumentById(documentId);
   }

   /**
    * {@inheritDoc}
    * <p>
    * Returned document {@link Document#content()} must be {@link DocumentContent#close() closed}.
    * <p>
    * Events :
    * <ul>
    *    <li>{@link DocumentSaved} is sent to this repository event listener for document on save success</li>
    * </ul>
    *
    * @throws UnsupportedCapabilityException if implementing repository does not support
    *       {@link RepositoryCapability#WRITABLE} capability
    * @implSpec overwrite is conservatively set to {@code false} when it can't be specified
    */
   @Override
   @CheckReturnValue
   default Optional<Document> save(Document document) {
      return saveAndReturnDocument(document, false);
   }

   /**
    * {@inheritDoc}
    * <p>
    * Returned document {@link Document#content()} must be {@link DocumentContent#close() closed}.
    * <p>
    * Events :
    * <ul>
    *    <li>{@link DocumentDeleted} is sent to this repository event listener for document on delete success</li>
    * </ul>
    *
    * @throws UnsupportedCapabilityException if implementing repository does not support
    *       {@link RepositoryCapability#WRITABLE} capability
    */
   @Override
   @CheckReturnValue
   default Optional<Document> delete(DocumentPath documentId) {
      return findById(documentId).flatMap(d -> deleteDocumentById(d.documentId()).map(__ -> d));
   }

   /**
    * As part of I/O resource cleanup rationale, wraps a {@link Document} function in an auto-closing function
    * to be used in {@link Stream#map(Function)} operation, for example.
    * {@link Document} content will always be closed automatically once user-function is applied.
    *
    * @param mapper user-function to apply to document
    * @param <T> document type
    * @param <U> mapper return type
    *
    * @return wrapping function
    *
    * @see #closingDocument(Consumer)
    * @see #closingFilteredDocument(Predicate)
    */
   static <T extends Document, U> CheckedFunction<T, U> closingDocument(Function<? super T, ? extends U> mapper) {
      return d -> smartFinalize(() -> mapper.apply(d), () -> d.content().close());
   }

   /**
    * As part of I/O resource cleanup rationale, wraps a {@link Document} consumer in an auto-closing consumer
    * to be used in {@link Stream#forEach(Consumer)} or {@link Stream#peek(Consumer)} operations, for example.
    * {@link Document} content will always be closed automatically once user-function is applied.
    *
    * @param action user-action to apply to document
    * @param <T> document type
    *
    * @return wrapping consumer
    *
    * @see #closingDocument(Function)
    * @see #closingFilteredDocument(Predicate)
    */
   static <T extends Document> CheckedConsumer<T> closingDocument(Consumer<? super T> action) {
      return d -> smartFinalize(() -> action.accept(d), () -> d.content().close());
   }

   /**
    * As part of I/O resource cleanup rationale, wraps a {@link Document} predicate in an auto-closing
    * predicate to be used in {@link Stream#filter(Predicate)} operation, for example.
    * {@link Document} content will be closed automatically only if predicate is not satisfied, or if an error
    * occurs while evaluating user-predicate.
    *
    * @param filter user-filter to apply to document
    * @param <T> document type
    *
    * @return wrapping predicate
    *
    * @see #closingDocument(Function)
    * @see #closingDocument(Consumer)
    */
   static <T extends Document> CheckedPredicate<T> closingFilteredDocument(Predicate<? super T> filter) {
      return d -> {
         try {
            boolean test = filter.test(d);
            if (!test) {
               d.content().close();
            }
            return test;
         } catch (Exception e) {
            return smartFinalize(e, () -> d.content().close());
         }
      };
   }

}
