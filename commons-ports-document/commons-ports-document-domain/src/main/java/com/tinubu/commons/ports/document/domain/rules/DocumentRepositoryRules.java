/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.rules;

import static com.tinubu.commons.ddd2.invariant.MessageValue.parameter;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.satisfiesValue;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.lang.util.StreamUtils.stream;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.MessageValue;
import com.tinubu.commons.ddd2.invariant.ParameterValue;
import com.tinubu.commons.ddd2.invariant.formatter.DefaultMessageFormat;
import com.tinubu.commons.ddd2.invariant.formatter.FastStringFormat;
import com.tinubu.commons.ddd2.invariant.formatter.MessageFormatter;
import com.tinubu.commons.ddd2.valueformatter.IterableValueFormatter;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.capability.RepositoryCapability;

/**
 * Invariant rules for {@link DocumentRepository}.
 */
public final class DocumentRepositoryRules {

   private DocumentRepositoryRules() { }

   private static final IterableValueFormatter ITERABLE_VALUE_FORMATTER = new IterableValueFormatter();

   public static <T extends DocumentRepository> InvariantRule<T> hasCapabilities(final ParameterValue<Iterable<RepositoryCapability>> capabilities,
                                                                                 final MessageFormatter<T> messageFormatter) {
      return isNotNull().andValue(satisfiesValue(v -> stream(capabilities.requireNonNullValue()).allMatch(v::hasCapability),
                                                 messageFormatter).ruleContext(
            "DocumentRepositoryRules.hasCapabilities",
            capabilities));
   }

   public static <T extends DocumentRepository> InvariantRule<T> hasCapabilities(final ParameterValue<Iterable<RepositoryCapability>> capabilities,
                                                                                 final String message,
                                                                                 final MessageValue<T>... values) {
      return hasCapabilities(capabilities, DefaultMessageFormat.of(message, values));
   }

   public static <T extends DocumentRepository> InvariantRule<T> hasCapabilities(final ParameterValue<Iterable<RepositoryCapability>> capabilities) {
      return hasCapabilities(capabilities,
                             FastStringFormat.of("'",
                                                 validatingObject(),
                                                 "' must have capabilities ",
                                                 parameter(0, ITERABLE_VALUE_FORMATTER)));
   }
}
