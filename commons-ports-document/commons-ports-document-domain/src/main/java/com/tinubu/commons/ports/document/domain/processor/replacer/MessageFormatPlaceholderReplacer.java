/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.processor.replacer;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import java.nio.charset.Charset;
import java.text.MessageFormat;
import java.util.Map;
import java.util.function.Function;

/**
 * Document content processor to replace {@link MessageFormat}-link placeholderswith specified model values.
 * Document must not be in binary format. {@link #DEFAULT_CONTENT_ENCODING} is assumed for document without
 * explicit content-type.
 * <p>
 * Model is not referenced by index like in {@link MessageFormat} but by name (i.e.: model key), supported
 * formats are :
 * <ul>
 *    <li>{ ArgumentName }</li>
 *    <li>{ ArgumentName , FormatType }</li>
 *    <li>{ ArgumentName , FormatType , FormatStyle }</li>
 * </ul>
 */
public class MessageFormatPlaceholderReplacer extends PlaceholderReplacer {

   public static final String PLACEHOLDER_BEGIN = "{";
   public static final String PLACEHOLDER_END = "}";

   private MessageFormatPlaceholderReplacer(Function<String, Object> replacementFunction,
                                            int bufferSize,
                                            Charset defaultContentEncoding) {
      super(replacementFunction, PLACEHOLDER_BEGIN, PLACEHOLDER_END, bufferSize, defaultContentEncoding);
   }

   public static MessageFormatPlaceholderReplacer of(Map<String, Object> model) {
      return new MessageFormatPlaceholderReplacer(messageFormatReplacementFunction(model),
                                                  DEFAULT_BUFFER_SIZE,
                                                  DEFAULT_CONTENT_ENCODING);
   }

   public MessageFormatPlaceholderReplacer bufferSize(int bufferSize) {
      return new MessageFormatPlaceholderReplacer(replacementFunction, bufferSize, defaultContentEncoding);
   }

   public MessageFormatPlaceholderReplacer defaultContentEncoding(Charset defaultContentEncoding) {
      return new MessageFormatPlaceholderReplacer(replacementFunction, bufferSize, defaultContentEncoding);
   }

   private static Function<String, Object> messageFormatReplacementFunction(Map<String, Object> model) {
      return key -> {
         int optionsIndex = key.indexOf(',');
         String modelKey = key.substring(0, optionsIndex == -1 ? key.length() : optionsIndex).trim();
         String options = key.substring(optionsIndex == -1 ? key.length() : optionsIndex);

         return nullable(model.get(modelKey))
               .map(value -> MessageFormat.format("{0," + options + "}", value))
               .orElse(null);
      };
   }

}

