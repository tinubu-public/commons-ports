/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.processor.common;

import java.nio.file.Path;

import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.processor.DocumentProcessor;

/**
 * Simple renamer processor for documents.
 * Supports renaming of both document id and metadata document path.
 */
@FunctionalInterface
public interface DocumentRenamer extends DocumentProcessor {

   /**
    * Provides new document id.
    *
    * @param documentEntry document entry of document being renamed
    *
    * @return new document id
    */
   Path renameDocumentId(DocumentEntry documentEntry);

   /**
    * Provides new metadata document path.
    *
    * @param documentEntry document entry of document being renamed
    *
    * @return new metadata document path
    */
   default Path renameDocumentPath(DocumentEntry documentEntry) {
      return documentEntry.metadata().documentPath();
   }

   /**
    * {@inheritDoc}
    *
    * @implNote New {@link DocumentPath} characteristics must be preserved (newObject, ...).
    */
   @Override
   default Document process(Document document) {
      DocumentPath newDocumentId =
            document.documentEntry().documentId().value(renameDocumentId(document.documentEntry()));
      Path newDocumentPath = renameDocumentPath(document.documentEntry());

      return DocumentBuilder
            .from(document)
            .documentId(newDocumentId)
            .metadata(m -> m.documentPath(newDocumentPath))
            .build();
   }
}
