/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.lazyValue;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.property;
import static com.tinubu.commons.ddd2.invariant.rules.EqualsRules.isEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.EqualsRules.isNotEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.MimeTypeRules.matches;
import static com.tinubu.commons.ddd2.invariant.rules.OptionalRules.isEmpty;
import static com.tinubu.commons.ddd2.invariant.rules.OptionalRules.optionalValue;
import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.mimeType;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.ports.document.domain.rules.DocumentRules.DocumentMetadataRules.optionalContentEncoding;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.UnaryOperator;

import com.tinubu.commons.ddd2.domain.type.AbstractEntity;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ddd2.invariant.ParameterValue;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.lang.datetime.ApplicationClock;
import com.tinubu.commons.lang.io.ReferencedFileOutputStream;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.lang.mimetype.registry.MimeTypeRegistry;
import com.tinubu.commons.ports.document.domain.DocumentEntry.DocumentEntryBuilder;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent.InputStreamDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.LoadedDocumentContent.LoadedDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent.OutputStreamDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.processor.DocumentProcessor;
import com.tinubu.commons.ports.document.domain.rules.DocumentRules.DocumentMainRules;
import com.tinubu.commons.ports.document.domain.rules.DocumentRules.DocumentMetadataRules;

/**
 * Generic document representation entity with metadata and content.
 * <p>
 * By design, document identifier is always relative to ease document portability on multiple systems. You
 * have to manage the document root logic on client side, depending on your needs. For example, {@link
 * DocumentRepository document repositories} implementations manage their own root logic to persist documents.
 * <p>
 * Document creation date and last update date are managed by document logic, but they can be overridden, or
 * deleted by {@link DocumentRepository} specific implementations on save/reconstitution.
 */
public class Document extends AbstractEntity<DocumentPath> {

   protected final DocumentPath documentId;
   protected final DocumentMetadata metadata;
   protected final DocumentContent content;

   protected Document(DocumentBuilder builder) {
      this.documentId = builder.documentId;
      this.content = builder.content;
      this.metadata = resolveMetadata(builder);
   }

   @Override
   public Fields<? extends Document> defineDomainFields() {
      return Fields
            .<Document>builder()
            .field("documentId", v -> v.documentId, isNotNull())
            .field("metadata",
                   v -> v.metadata,
                   isNotNull().andValue(isValidContentSize()).andValue(isValidContentTypeEncoding()))
            .field("content", v -> v.content, isNotNull())
            .build();
   }

   @Override
   public DocumentPath id() {
      return documentId();
   }

   /**
    * Returns document identifier.
    *
    * @return document identifier
    */
   @Getter
   public DocumentPath documentId() {
      return documentId;
   }

   /**
    * Returns document metadata.
    *
    * @return document metadata
    */
   @Getter
   public DocumentMetadata metadata() {
      return metadata;
   }

   /**
    * Returns document content.
    *
    * @return document content
    */
   @Getter
   public DocumentContent content() {
      return content;
   }

   /**
    * Returns document content as a {@code DocumentContent & AutoCloseable} interface.
    *
    * @return {@link AutoCloseable} document content
    */
   @SuppressWarnings("unchecked")
   public <T extends DocumentContent & AutoCloseable> T closeableContent() {
      return (T) new AutoCloseableDocumentContent() {

         @Override
         public byte[] content() {
            return content.content();
         }

         @Override
         public String stringContent(Charset defaultContentEncoding) {
            return content.stringContent(defaultContentEncoding);
         }

         @Override
         public String stringContent() {
            return content.stringContent();
         }

         @Override
         public InputStream inputStreamContent() {
            return content.inputStreamContent();
         }

         @Override
         public OutputStream outputStreamContent() {
            return content.outputStreamContent();
         }

         @Override
         public Reader readerContent(Charset defaultContentEncoding) {
            return content.readerContent(defaultContentEncoding);
         }

         @Override
         public Reader readerContent() {
            return content.readerContent();
         }

         @Override
         public Writer writerContent(Charset defaultContentEncoding) {
            return content.writerContent(defaultContentEncoding);
         }

         @Override
         public Writer writerContent() {
            return content.writerContent();
         }

         @Override
         public Optional<Long> contentSize() {
            return content.contentSize();
         }

         @Override
         public Optional<Charset> contentEncoding() {
            return content.contentEncoding();
         }

         @Override
         public void close() {
            content.close();
         }
      };
   }

   /**
    * Constructs a {@link DocumentEntry} from this document.
    *
    * @return a new {@link DocumentEntry} of this document
    */
   public DocumentEntry documentEntry() {
      return new DocumentEntryBuilder().documentId(documentId).metadata(metadata).build();
   }

   /**
    * Checks document encoding is equal to current document encoding.
    * Both document's content encodings must be unset, or set and match exactly.
    *
    * @return document invariant rule
    */
   protected InvariantRule<Document> hasCompatibleContentEncoding() {
      return isNotNull().andValue(DocumentMainRules.metadata(property(DocumentMetadata::contentEncoding,
                                                                      "contentEncoding",
                                                                      isEqualTo(value(content.contentEncoding(),
                                                                                      "contentEncoding")))));
   }

   /**
    * Invariant rule checking document content-type matches current document content-type. Parameters are not
    * taken into account.
    *
    * @param matchTypeOnly if {@code false}, both document's content types must match exactly (type and
    *       subtype), otherwise, only the content-type's type is matched (e.g.: text/plain matches
    *       text/html).
    *
    * @return document invariant rule
    */
   protected InvariantRule<Document> hasCompatibleContentType(boolean matchTypeOnly) {
      ParameterValue<MimeType> contentType = lazyValue(() -> metadata
            .contentType()
            .map(MimeType::strippedParameters)
            .map(ct -> matchTypeOnly ? ct.subtypeWildcard() : ct)
            .orElseThrow(() -> new IllegalStateException(String.format(
                  "'%s' document content-type must be set",
                  documentId.stringValue()))), "contentType");

      return isNotNull().andValue(DocumentMainRules.metadata(DocumentMetadataRules.contentType(matches(
            contentType))));
   }

   /**
    * Returns a "readable content" version of this document (using either {@link LoadedDocumentContent} or
    * {@link InputStreamDocumentContent}).
    * <p>
    * All document metadata are preserved excepting :
    * <ul>
    * <li>content size, if it was previously unset</li>
    * <li>last update date</li>
    * </ul>
    * <p>
    * In the case of a {@link OutputStreamDocumentContent} content, document content
    * will be loaded as a {@link LoadedDocumentContent} only if document content
    * {@link OutputStreamDocumentContent#sourceOutputStream()} is a supported stream :
    * <ul>
    *    <li>{@link ByteArrayOutputStream}</li>
    *    <li>{@link ReferencedFileOutputStream}</li>
    * </ul>
    * {@link OutputStreamDocumentContent} must be closed before calling this method.
    *
    * @return this document with {@link LoadedDocumentContent} or {@link InputStreamDocumentContent} content
    *       and adapted metadata
    *
    * @throws DocumentAccessException if an I/O error occurs while reading content
    * @implNote When loading {@link OutputStreamDocumentContent}, content must be closed prior to
    *       extracting data from output stream because some decorating mappers (e.g.: Base64Encoder, ...)
    *       really flush data at close.
    */
   public Document readableContent() {
      if (content instanceof LoadedDocumentContent || content instanceof InputStreamDocumentContent) {
         return this;
      } else if (content instanceof OutputStreamDocumentContent) {
         OutputStream sourceOutputStream = ((OutputStreamDocumentContent) content).sourceOutputStream();

         if (sourceOutputStream instanceof ByteArrayOutputStream) {
            LoadedDocumentContent loadedContent = new LoadedDocumentContentBuilder()
                  .content(((ByteArrayOutputStream) sourceOutputStream).toByteArray(),
                           this.content.contentEncoding().orElse(null))
                  .build();

            return content(loadedContent);
         } else if (sourceOutputStream instanceof ReferencedFileOutputStream) {
            File file = ((ReferencedFileOutputStream) sourceOutputStream).file();

            try {
               InputStreamDocumentContent inputContent = new InputStreamDocumentContentBuilder()
                     .content(new FileInputStream(file), this.content.contentEncoding().orElse(null))
                     .build();
               return content(inputContent);
            } catch (FileNotFoundException e) {
               throw new IllegalStateException(String.format("Can't find '%s' referenced file", file), e);
            }
         } else {
            throw new IllegalStateException(String.format(
                  "Unsupported '%s' source output stream for this operation (supported : ByteArrayOutputStream,ReferencedFileOutputStream)",
                  sourceOutputStream.getClass().getSimpleName()));
         }
      } else {
         throw new IllegalStateException(String.format("Unsupported '%s' document content",
                                                       content.getClass().getSimpleName()));
      }
   }

   /**
    * Returns a "loaded content" version of this document.
    * This always imply to load the whole document content into memory.
    * <p>
    * if content is a {@link OutputStreamDocumentContent}, {@link #readableContent()} is called first.
    * <p>
    * All document metadata are preserved excepting :
    * <ul>
    * <li>content size, if it was previously unset</li>
    * <li>last update date</li>
    * </ul>
    * <p>
    * This operation is necessary if you plan to read the document content multiple times, or if you just want
    * to "disconnect" it from repository.
    * <p>
    * This operation does nothing if the document content is already "loaded".
    *
    * @return this document with {@link LoadedDocumentContent} content and adapted metadata
    *
    * @throws DocumentAccessException if an I/O error occurs while reading content
    * @apiNote Original content stream is closed after the operation if successful.
    */
   public Document loadContent() {
      if (content instanceof LoadedDocumentContent) {
         return this;
      } else if (content instanceof OutputStreamDocumentContent) {
         return readableContent().loadContent();
      } else {
         try (InputStream streamContent = this.content.inputStreamContent()) {
            LoadedDocumentContent loadedContent = new LoadedDocumentContentBuilder()
                  .content(streamContent, this.content.contentEncoding().orElse(null))
                  .build();

            return content(loadedContent);
         } catch (IOException e) {
            throw new DocumentAccessException(e);
         }
      }
   }

   /**
    * Updates document metadata document name.
    *
    * @param documentName document metadata path
    *
    * @return new document
    *
    * @deprecated See renamed function {@link #documentPath(Path)}
    */
   @Deprecated
   public Document documentName(Path documentName) {
      return metadata(builder -> builder.documentPath(documentName));
   }

   /**
    * Updates document metadata document path.
    *
    * @param documentPath document metadata path
    *
    * @return new document
    */
   public Document documentPath(Path documentPath) {
      return metadata(builder -> builder.documentPath(documentPath));
   }

   /**
    * Updates document metadata content type. Updating content encoding using charset parameter is not
    * allowed here, it can be present, but it must match actual content encoding.
    *
    * @param contentType document metadata content type
    *
    * @return new document
    */
   public Document contentType(MimeType contentType) {
      notNull(contentType, "contentType");

      MimeType newContentType = contentType.charset().isPresent()
                                ? contentType
                                : content
                                      .contentEncoding()
                                      .map(ce -> mimeType(contentType, ce))
                                      .orElse(contentType);

      return metadata(builder -> builder.contentType(newContentType));
   }

   /**
    * Updates document metadata attributes.
    *
    * @param attributes document metadata attributes
    *
    * @return new document
    */
   public Document attributes(Map<String, String> attributes) {
      notNull(attributes, "attributes");

      return metadata(builder -> builder.attributes(attributes));
   }

   /**
    * Updates document metadata attributes.
    *
    * @param attributes document metadata attributes
    *
    * @return new document
    */
   public Document addAttributes(Map<String, String> attributes) {
      notNull(attributes, "attributes");

      Map<String, String> metadataAttributes = new HashMap<>(metadata.attributes());
      metadataAttributes.putAll(attributes);

      return attributes(metadataAttributes);
   }

   /**
    * Updates document metadata attributes.
    *
    * @param key document metadata attribute key
    * @param value document metadata attribute value
    *
    * @return new document
    */
   public Document addAttribute(String key, String value) {
      notBlank(key, "key");
      notNull(value, "value");

      Map<String, String> metadataAttributes = new HashMap<>(metadata.attributes());
      metadataAttributes.put(key, value);

      return attributes(metadataAttributes);
   }

   /**
    * Provides document builder instance to provide support for document subtypes.
    * Document inheriting implementations should override this operation.
    *
    * @return implementation builder instance
    */
   protected DocumentBuilder documentBuilder() {
      return new DocumentBuilder();
   }

   /**
    * Updates document content, but not its identifier, name, content type. Metadata will be corrected
    * accordingly, including new content encoding and content size if any.
    * <p>
    * If previous content is totally replaced by this operation, it's your responsibility to correctly close
    * previous content.
    *
    * @param content document content to set for this document
    *
    * @return new document
    */
   public Document content(DocumentContent content) {
      notNull(content, "content");

      MimeType encoding = metadata
            .contentType()
            .map(contentType -> content.contentEncoding().map(contentType::charset).orElse(contentType))
            .orElse(null);

      return documentBuilder().<DocumentBuilder>reconstitute().copy(this)
            .content(content)
            .metadata(DocumentMetadataBuilder
                            .from(metadata)
                            .contentSize(content.contentSize().orElse(null))
                            .contentType(encoding)
                            .chain(this::updateLastUpdateDate)
                            .build())
            .build();
   }

   /**
    * Returns a new instance of this document processed by the specified processor.
    *
    * @param processor document processor to apply to this document
    *
    * @return new document
    *
    * @throws DocumentAccessException if an errors occurs in processor
    * @implNote processor resulting document is recreated using {@link #documentBuilder()} so that
    *       returned document is correctly sub-typed.
    */
   public Document process(DocumentProcessor processor) {
      notNull(processor, "processor");

      try {
         return documentBuilder().<DocumentBuilder>reconstitute().copy(processor.process(this)).build();
      } catch (DocumentAccessException e) {
         throw e;
      } catch (Exception e) {
         throw new DocumentAccessException(e.getMessage(), e);
      }
   }

   /**
    * Duplicates this document as a new document identified with specified identifier.
    * <p>
    * Metadata creation date and last update date are re-initialized as for a new document. If you need more
    * control on document, consider to reconstruct a new document from {@link DocumentBuilder} satisfying
    * your
    * needs.
    * <p>
    * Duplicating a document as no effect on original content which is referenced in new document. Hence, you
    * can close both original and new document content because close is idempotent.
    *
    * @param newDocumentId new document identifier
    * @param updateMetadataDocumentPath whether to also update metadata document path with new document
    *       id value
    *
    * @return new duplicated document with new identifier
    */
   public Document duplicate(DocumentPath newDocumentId, boolean updateMetadataDocumentPath) {
      validate(newDocumentId, "newDocumentId", isNotEqualTo(value(documentId, "documentId"))).orThrow();

      return documentBuilder()
            .<DocumentBuilder>reconstitute()
            .copy(this)
            .documentId(newDocumentId)
            .metadata(mb -> {
               Instant currentDate = ApplicationClock.nowAsInstant();

               return mb
                     .creationDate(currentDate)
                     .lastUpdateDate(currentDate)
                     .conditionalChain(__ -> updateMetadataDocumentPath,
                                       b -> b.documentPath(documentId.value()));
            })
            .build();
   }

   /**
    * Duplicates this document as a new document identified with specified identifier.
    * <p>
    * Metadata document name is not updated in the process so both documents will have the same metadata
    * name, but not the same document id (see {@link #duplicate(DocumentPath, boolean)}). Metadata creation
    * date and last update date are re-initialized as for a new document.
    * <p>
    * Duplicating a document as no effect on original content which is referenced in new document. Hence, you
    * can close both original and new document content because close is idempotent.
    *
    * @param newDocumentId new document identifier
    *
    * @return new duplicated document with new identifier
    *
    * @see #duplicate(DocumentPath, boolean)
    */
   public Document duplicate(DocumentPath newDocumentId) {
      return duplicate(newDocumentId, false);
   }

   /**
    * Derives {@link DocumentMetadata} from builder data.
    *
    * @param builder document builder
    *
    * @return document metadata
    *
    * @apiNote Document metadata document name use document identifier path by default.
    * @implNote If builder is in reconstitute mode, values are just passed through.
    */
   protected static DocumentMetadata resolveMetadata(DocumentBuilder builder) {
      if (builder.isReconstitute()) {
         return builder.metadata;
      } else {
         try {
            return new DocumentMetadataBuilder()
                  .mimeTypeRegistry(builder.mimeTypeRegistry)
                  .documentPath(nullable(builder.documentPath,
                                         nullable(builder.documentId).map(DocumentPath::value).orElse(null)))
                  .contentType(nullable(builder.contentType).orElse(null),
                               nullable(builder.content)
                                     .flatMap(DocumentContent::contentEncoding)
                                     .orElse(null))
                  .attributes(builder.attributes)
                  .contentSize(nullable(builder.content).flatMap(DocumentContent::contentSize).orElse(null))
                  .build();
         } catch (InvariantValidationException e) {
            if (builder.documentId != null) {
               throw e.addContext(builder.documentId);
            } else {
               throw e;
            }
         }
      }
   }

   /**
    * Internally updates metadata. Metadata last update date is always updated to current date.
    *
    * @param metadataOperator document metadata operator to use to update metadata.
    *
    * @return new document
    */
   protected Document metadata(UnaryOperator<DocumentMetadataBuilder> metadataOperator) {
      return documentBuilder().<DocumentBuilder>reconstitute().copy(this)
            .metadata(DocumentMetadataBuilder
                            .from(metadata)
                            .chain(metadataOperator)
                            .chain(this::updateLastUpdateDate)
                            .build())
            .build();
   }

   /**
    * Metadata builder chain operation to update last update date.
    *
    * @param documentMetadataBuilder metadata builder
    *
    * @return metadata builder
    */
   protected DocumentMetadataBuilder updateLastUpdateDate(DocumentMetadataBuilder documentMetadataBuilder) {
      return documentMetadataBuilder.lastUpdateDate(ApplicationClock.nowAsInstant());
   }

   /**
    * Checks if metadata content size correctly matches effective document content size, if effective document
    * content size is available.
    */
   protected InvariantRule<DocumentMetadata> isValidContentSize() {
      ParameterValue<Long> effectiveContentSize =
            lazyValue(() -> content.contentSize().orElse(null), "content.contentSize");

      return property(DocumentMetadata::contentSize,
                      "contentSize",
                      isEmpty().orValue(optionalValue(isEqualTo(effectiveContentSize).ifNonNull(
                            effectiveContentSize))));
   }

   /**
    * Checks if metadata content type charset correctly matches effective document content encoding.
    */
   protected <T extends DocumentMetadata> InvariantRule<T> isValidContentTypeEncoding() {
      ParameterValue<Charset> contentEncoding =
            lazyValue(() -> content.contentEncoding().orElse(null), "content.contentEncoding");

      return optionalContentEncoding(isEqualTo(contentEncoding).ifNonNull(contentEncoding));
   }

   public static DocumentBuilder reconstituteBuilder() {
      return new DocumentBuilder().reconstitute();
   }

   /**
    * Internal {@code DocumentContent & AutoCloseable} interface for {@link #closeableContent()}
    * implementation.
    */
   private interface AutoCloseableDocumentContent extends DocumentContent, AutoCloseable { }

   public static class DocumentBuilder extends DomainBuilder<Document> {
      protected DocumentPath documentId;
      protected Path documentPath;
      protected MimeType contentType;
      protected Map<String, String> attributes;
      protected DocumentMetadata metadata;
      protected DocumentContent content;
      protected MimeTypeRegistry mimeTypeRegistry;

      /**
       * Copy constructor for {@link Document}
       *
       * @param document document to copy
       *
       * @return document builder
       */
      public static DocumentBuilder from(Document document) {
         notNull(document, "document");

         return new DocumentBuilder().<DocumentBuilder>reconstitute().copy(document);
      }

      /**
       * {@link Document} copy operation.
       * All Document implementation classes must override this method to copy implementation specific
       * fields.
       * <p>
       * E.g.:
       * <p>
       * {@code return super.copy(document).optionalChain(nullableInstanceOf(document, MyDocument.class), (b,
       * d) -> b.myField(d.myField)); }
       *
       * @param document document to copy
       *
       * @return copy function
       */
      protected DocumentBuilder copy(Document document) {
         notNull(document, "document");

         return documentId(document.documentId).metadata(document.metadata).content(document.content);
      }

      /**
       * Helper builder operation to open a new document.
       *
       * @param documentId document identifier
       * @param contentOutputStream optional content output stream. A {@link ByteArrayOutputStream} will
       *       be used if not specified
       * @param contentEncoding optional content encoding. No encoding will be set if not specified
       * @param contentType optional content type. Content-type auto-detection will be used if not
       *       specified
       *
       * @return this builder
       */
      public DocumentBuilder open(DocumentPath documentId,
                                  OutputStream contentOutputStream,
                                  Charset contentEncoding,
                                  MimeType contentType) {
         notNull(documentId, "documentId");

         return documentId(documentId)
               .content(new OutputStreamDocumentContentBuilder()
                              .content(nullable(contentOutputStream, new ByteArrayOutputStream()),
                                       contentEncoding)
                              .build())
               .contentType(contentType);
      }

      /**
       * Helper builder operation to open a new document in memory.
       *
       * @param documentId document identifier
       *
       * @return this builder
       */
      public DocumentBuilder open(DocumentPath documentId) {
         return open(documentId, null, null, null);
      }

      /**
       * Helper builder operation to open a new document in memory.
       *
       * @param documentId document identifier
       * @param contentEncoding optional content encoding. No encoding will be set if not specified
       *
       * @return this builder
       */
      public DocumentBuilder open(DocumentPath documentId, Charset contentEncoding) {
         return open(documentId, null, contentEncoding, null);
      }

      public DocumentBuilder mimeTypeRegistry(MimeTypeRegistry mimeTypeRegistry) {
         ensureNotReconstitute();
         this.mimeTypeRegistry = mimeTypeRegistry;
         return this;
      }

      @Setter
      public DocumentBuilder documentId(DocumentPath documentId) {
         this.documentId = documentId;
         return this;
      }

      public DocumentBuilder documentPath(Path documentPath) {
         ensureNotReconstitute();
         this.documentPath = documentPath;
         return this;
      }

      public DocumentBuilder documentPath(String documentPath, String... moreDocumentPath) {
         return documentPath(Paths.get(documentPath, moreDocumentPath));
      }

      /**
       * @param documentName document path
       *
       * @return this builder
       *
       * @deprecated see {@link #documentPath(Path)} instead
       */
      @Deprecated
      public DocumentBuilder documentName(Path documentName) {
         return documentPath(documentName);
      }

      /**
       * @param documentName document path
       *
       * @return this builder
       *
       * @deprecated see {@link #documentPath(String, String[])} instead
       */
      @Deprecated
      public DocumentBuilder documentName(String documentName, String... moreDocumentName) {
         return documentPath(documentName, moreDocumentName);
      }

      public DocumentBuilder contentType(MimeType contentType) {
         ensureNotReconstitute();
         this.contentType = contentType;
         return this;
      }

      public DocumentBuilder attributes(Map<String, String> attributes) {
         ensureNotReconstitute();
         this.attributes = attributes;
         return this;
      }

      @Setter
      public DocumentBuilder metadata(DocumentMetadata metadata) {
         ensureReconstitute();
         this.metadata = metadata;
         return this;
      }

      /**
       * Document metadata builder accessor. Use this method to operate on current builder's metadata.
       * Metadata last update date is unmodified by this operation.
       *
       * @param metadataOperator metadata builder operations
       *
       * @return this builder
       */
      public DocumentBuilder metadata(UnaryOperator<DocumentMetadataBuilder> metadataOperator) {
         ensureReconstitute();
         this.metadata = DocumentMetadataBuilder.from(metadata).chain(metadataOperator).build();
         return this;
      }

      /**
       * Reconstitutes document identifier and metadata from specified document entry.
       *
       * @param documentEntry document entry
       *
       * @return this builder
       */
      public DocumentBuilder documentEntry(DocumentEntry documentEntry) {
         ensureReconstitute();
         this.documentId = documentEntry.documentId();
         this.metadata = documentEntry.metadata();
         return this;
      }

      @Setter
      public DocumentBuilder content(DocumentContent content) {
         this.content = content;
         return this;
      }

      public DocumentBuilder loadedContent(byte[] content, Charset contentEncoding) {
         return content(new LoadedDocumentContentBuilder().content(content, contentEncoding).build());
      }

      public DocumentBuilder loadedContent(byte[] content) {
         return content(new LoadedDocumentContentBuilder().content(content).build());
      }

      public DocumentBuilder loadedContent(String content, Charset contentEncoding) {
         return content(new LoadedDocumentContentBuilder().content(content, contentEncoding).build());
      }

      public DocumentBuilder loadedContent(InputStream content, Charset contentEncoding) {
         return content(new LoadedDocumentContentBuilder().content(content, contentEncoding).build());
      }

      public DocumentBuilder loadedContent(InputStream content) {
         return content(new LoadedDocumentContentBuilder().content(content).build());
      }

      public DocumentBuilder loadedContent(Reader content, Charset contentEncoding) {
         return content(new LoadedDocumentContentBuilder().content(content, contentEncoding).build());
      }

      public DocumentBuilder streamContent(byte[] content, Charset contentEncoding) {
         return content(new InputStreamDocumentContentBuilder().content(content, contentEncoding).build());
      }

      public DocumentBuilder streamContent(byte[] content) {
         return content(new InputStreamDocumentContentBuilder().content(content).build());
      }

      public DocumentBuilder streamContent(InputStream content, Charset contentEncoding, Long contentSize) {
         return content(new InputStreamDocumentContentBuilder()
                              .content(content, contentEncoding, contentSize)
                              .build());
      }

      public DocumentBuilder streamContent(InputStream content, Charset contentEncoding) {
         return content(new InputStreamDocumentContentBuilder().content(content, contentEncoding).build());
      }

      public DocumentBuilder streamContent(InputStream content, Long contentSize) {
         return content(new InputStreamDocumentContentBuilder().content(content, contentSize).build());
      }

      public DocumentBuilder streamContent(InputStream content) {
         return content(new InputStreamDocumentContentBuilder().content(content).build());
      }

      public DocumentBuilder streamContent(String content, Charset contentEncoding) {
         return content(new InputStreamDocumentContentBuilder().content(content, contentEncoding).build());
      }

      public DocumentBuilder streamContent(Reader content, Charset contentEncoding) {
         return content(new InputStreamDocumentContentBuilder().content(content, contentEncoding).build());
      }

      @Override
      public Document buildDomainObject() {
         return new Document(this);
      }

   }

}

