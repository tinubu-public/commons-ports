/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.capability;

import static com.tinubu.commons.lang.util.CollectionUtils.collection;

import java.net.URI;
import java.util.HashSet;

import com.tinubu.commons.ports.document.domain.DocumentMetadata;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;

/**
 * A {@link DocumentRepository} capability.
 * Each capability is standalone, it means that non requires or implies another capability for an operation to
 * be used (e.g.: {@link #ITERABLE} does not implies {@link #READABLE} capability).
 */
public enum RepositoryCapability {
   /**
    * Repository supports {@link URI} representation.
    */
   REPOSITORY_URI("repository URI representation"),
   /**
    * Repository supports {@link URI} representation for individual documents.
    */
   DOCUMENT_URI("repository document URI representation"),

   /**
    * Repository is writable (save/delete operations).
    */
   WRITABLE("writable repository"),
   /**
    * Repository is readable (findById operations).
    */
   READABLE("readable repository"),
   /**
    * Repository is iterable (findAll operations).
    */
   ITERABLE("iterable repository (retrieve all documents)"),
   /**
    * Repository is queryable (findBySpecification operations).
    */
   QUERYABLE("queryable repository (retrieve document subset using specification)"),
   /**
    * Repository supports open document.
    */
   OPEN("open document"),
   /**
    * Repository supports open document in append mode.
    */
   OPEN_APPEND("open document in append mode"),
   /** Repository supports creation of a new repository that is a sub-path of this one. */
   SUBPATH("sub-path repository"),

   /**
    * Repository supports {@link DocumentMetadata#documentPath()} persistence. Document path metadata is not
    * necessarily equals to the {@link DocumentPath document identifier} value.
    */
   METADATA_DOCUMENT_PATH("document metadata path"),
   /**
    * Repository supports {@link DocumentMetadata#creationDate()} persistence.
    */
   METADATA_CREATION_DATE("document metadata creation date"),
   /**
    * Repository supports {@link DocumentMetadata#lastUpdateDate()} persistence.
    */
   METADATA_LAST_UPDATE_DATE("document metadata last update date"),
   /**
    * Repository supports {@link DocumentMetadata#contentType()} persistence. Charset is not required to be
    * persisted in content-type
    * for this capability to be considered as supported (see {@link #METADATA_CONTENT_ENCODING}).
    */
   METADATA_CONTENT_TYPE("document metadata content-type (with or without encoding)"),
   /**
    * Repository supports {@link DocumentMetadata#contentEncoding()} persistence.
    */
   METADATA_CONTENT_ENCODING("document metadata content encoding"),
   /**
    * Repository supports {@link DocumentMetadata#contentSize()} persistence, so that content size can be
    * known without actually load it.
    */
   METADATA_CONTENT_SIZE("document metadata content size"),
   /**
    * Repository supports {@link DocumentMetadata#attributes()} persistence.
    */
   METADATA_ATTRIBUTES("document metadata attributes");

   private static final HashSet<RepositoryCapability> allCapabilities =
         collection(HashSet::new, RepositoryCapability.values());

   private final String description;

   RepositoryCapability(String description) {
      this.description = description;
   }

   public String description() {
      return description;
   }

   /**
    * Returns all feature capabilities.
    *
    * @return all feature capabilities
    */
   public static HashSet<RepositoryCapability> allFeatureCapabilities() {
      return collection(HashSet::new,
                        REPOSITORY_URI,
                        DOCUMENT_URI,
                        WRITABLE,
                        READABLE,
                        ITERABLE,
                        QUERYABLE,
                        OPEN,
                        OPEN_APPEND,
                        SUBPATH);
   }

   /**
    * Returns all metadata capabilities.
    *
    * @return all metadata capabilities
    */
   public static HashSet<RepositoryCapability> allMetadataCapabilities() {
      return collection(HashSet::new,
                        METADATA_DOCUMENT_PATH,
                        METADATA_CREATION_DATE,
                        METADATA_LAST_UPDATE_DATE,
                        METADATA_CONTENT_TYPE,
                        METADATA_CONTENT_ENCODING,
                        METADATA_CONTENT_SIZE,
                        METADATA_ATTRIBUTES);
   }

   /**
    * Returns all capabilities.
    *
    * @return all  capabilities
    */
   public static HashSet<RepositoryCapability> allCapabilities() {
      return allCapabilities;
   }

}
