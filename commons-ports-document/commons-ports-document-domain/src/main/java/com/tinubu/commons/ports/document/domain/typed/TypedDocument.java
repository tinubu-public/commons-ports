/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.typed;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObjectInitialValue;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.as;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isAlwaysValid;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isFalse;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isTrue;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.property;
import static com.tinubu.commons.ddd2.invariant.rules.ClassRules.optionalInstanceOf;
import static com.tinubu.commons.ddd2.invariant.rules.NumberRules.isPositive;
import static com.tinubu.commons.lang.util.CheckedSupplier.unchecked;
import static com.tinubu.commons.lang.util.ExceptionUtils.smartFinalize;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.ports.document.domain.rules.DocumentRules.DocumentMetadataRules.hasContentType;
import static com.tinubu.commons.ports.document.domain.rules.DocumentRules.DocumentMetadataRules.optionalContentType;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.util.Map;
import java.util.function.UnaryOperator;

import org.apache.commons.io.IOUtils;

import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.Invariant;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.formatter.FastStringFormat;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.lang.mimetype.registry.MimeTypeRegistry;
import com.tinubu.commons.lang.util.MechanicalSympathy;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentContent;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentMetadata;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.LoadedDocumentContent;
import com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.processor.DocumentProcessor;
import com.tinubu.commons.ports.document.domain.rules.DocumentRules.DocumentMainRules;

/**
 * Typed document base aimed at specific document types implementations.
 * <p>
 * Note that only {@link LoadedDocumentContent}'s documents are pre-validated for content using
 * {@link #hasValidLoadedContent()}.
 *
 * @param <T> typed document type
 */
public abstract class TypedDocument<T extends TypedDocument<T>> extends Document {

   /**
    * Buffer size for content output stream and writers. Buffering is disabled if set to 0. Set to
    * {@link MechanicalSympathy#generalBufferSize()} by default.
    */
   protected final int outputBufferSize;
   /**
    * Buffer size for content input stream and readers. Buffering is disabled if set to 0. Set to
    * {@link MechanicalSympathy#generalBufferSize()} by default.
    */
   protected final int inputBufferSize;

   protected OutputStream contentOutputStream;
   protected Writer contentWriter;
   protected InputStream contentInputStream;
   protected Reader contentReader;
   protected boolean finished = false;

   protected TypedDocument(TypedDocumentBuilder<T, ?> builder) {
      super(builder);

      this.outputBufferSize = nullable(builder.outputBufferSize, MechanicalSympathy.generalBufferSize());
      this.inputBufferSize = nullable(builder.inputBufferSize, MechanicalSympathy.generalBufferSize());
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends T> defineDomainFields() {
      return Fields
            .<T>builder()
            .superFields((Fields<T>) super.defineDomainFields())
            .technicalField("outputBufferSize", v -> v.outputBufferSize, isPositive())
            .technicalField("inputBufferSize", v -> v.inputBufferSize, isPositive())
            .invariants(Invariant.of(this::self,
                                     DocumentMainRules.metadata(hasContentType()
                                                                      .ifIsSatisfied(this::requiredContentType)
                                                                      .andValue(optionalContentType(
                                                                            hasValidContentType())))),
                        Invariant.of(this::self, property(Document::content, "content", hasValidContent())))
            .build();
   }

   /**
    * Returns this instance cast as {@link T}.
    *
    * @return this instance as {@link T}
    */
   @SuppressWarnings("unchecked")
   protected T self() {
      return (T) this;
   }

   /**
    * Whether document content-type is required in domain validation. {@link #hasValidContentType()} is
    * evaluated only if this method returns {@code true}.
    *
    * @return whether the document content-type is required for domain validation
    */
   protected boolean requiredContentType() {
      return true;
   }

   /**
    * Validates typed document content-type. You can override this method in implementations.
    * Validating content-type is not stripped, and can contain parameters (charset, ...).
    *
    * @return content-type invariant rule
    */
   protected InvariantRule<MimeType> hasValidContentType() {
      return isAlwaysValid();
   }

   /**
    * Validates typed document content. You can override this method in implementations.
    *
    * @return content invariant rule
    */
   protected InvariantRule<LoadedDocumentContent> hasValidLoadedContent() {
      return isAlwaysValid();
   }

   /**
    * Validate document content. You should not override this method, overrides
    * {@link #hasValidLoadedContent()} instead.
    *
    * @return content invariant rule
    */
   protected InvariantRule<DocumentContent> hasValidContent() {
      return optionalInstanceOf(LoadedDocumentContent.class, hasValidLoadedContent());
   }

   /**
    * Writes specified byte array to document content using a buffered layer. Byte array must be encoded
    * using current document {@link DocumentContent#contentEncoding()} (or @link
    * DocumentMetadata#contentEncoding()}).
    * You can use {@link #flush()} to flush the buffered layer and underlying output stream.
    * Depending on document implementation, you should call {@link #finish()} to finish the content
    * generation.
    * <p>
    * This operation only supports {@link OutputStreamDocumentContent} content.
    *
    * @param buffer byte array to write
    * @param offset byte array starting offset to write
    * @param length byte array length to write
    *
    * @return this document
    *
    * @throws DocumentAccessException if an I/O error occurs while writing
    * @see #finish()
    */
   @SuppressWarnings("unchecked")
   protected T write(byte[] buffer, int offset, int length) {
      notNull(buffer, "buffer");

      OutputStream contentOutputStream = contentOutputStream();

      if (contentOutputStream != null) {
         try {
            contentOutputStream.write(buffer, offset, length);
         } catch (IOException e) {
            throw new DocumentAccessException(String.format("Can't write to '%s'", documentId.stringValue()),
                                              e);
         }
         return (T) this;
      } else {
         throw new IllegalStateException("Unsupported operation, content must be OutputStreamDocumentContent");
      }
   }

   /**
    * Reads content to specified byte array from document content using a buffered layer.
    * This operation only supports {@link InputStreamDocumentContent} or {@link LoadedDocumentContent}
    * content.
    *
    * @param buffer byte array to fill
    * @param offset byte array starting offset to fill
    * @param length byte array length to fill
    *
    * @return this document
    *
    * @throws DocumentAccessException if an I/O error occurs while reading
    */
   protected int read(byte[] buffer, int offset, int length) {
      notNull(buffer, "buffer");

      InputStream contentInputStream = contentInputStream();

      if (contentInputStream != null) {
         try {
            return contentInputStream.read(buffer, offset, length);
         } catch (IOException e) {
            throw new DocumentAccessException(String.format("Can't read from '%s'", documentId.stringValue()),
                                              e);
         }
      } else {
         throw new IllegalStateException(
               "Unsupported operation, content must be InputStreamDocumentContent or LoadedDocumentContent");
      }
   }

   /**
    * Writes specified byte array to document content using a buffered layer. Byte array must be encoded
    * using current document {@link DocumentContent#contentEncoding()} (or @link
    * DocumentMetadata#contentEncoding()}).
    * You can use {@link #flush()} to flush the buffered layer and underlying output stream.
    * Depending on document implementation, you should call {@link #finish()} to finish the content
    * generation.
    * <p>
    * This operation only supports {@link OutputStreamDocumentContent} content.
    *
    * @param buffer byte array to write
    *
    * @return this document
    *
    * @throws DocumentAccessException if an I/O error occurs while writing
    * @see #finish()
    */
   @SuppressWarnings("unchecked")
   protected T write(byte[] buffer) {
      notNull(buffer, "buffer");

      write(buffer, 0, buffer.length);

      return (T) this;
   }

   /**
    * Reads content to specified byte array from document content using a buffered layer.
    * This operation only supports {@link InputStreamDocumentContent} or {@link LoadedDocumentContent}
    * content.
    *
    * @param buffer byte array to fill
    *
    * @return this document
    *
    * @throws DocumentAccessException if an I/O error occurs while reading
    */
   protected int read(byte[] buffer) {
      notNull(buffer, "buffer");

      return read(buffer, 0, buffer.length);
   }

   /**
    * Writes specified input stream content to current document content using a buffered layer.
    *
    * @param inputStream input stream that must have compatible encoding with current document
    *
    * @return this document
    *
    * @throws DocumentAccessException if an I/O error occurs while writing
    * @see #finish()
    */
   @SuppressWarnings("unchecked")
   protected T write(InputStream inputStream) {
      notNull(inputStream, "inputStream");

      OutputStream contentOutputStream = contentOutputStream();

      if (contentOutputStream != null) {
         try {
            IOUtils.copy(inputStream, contentOutputStream);
         } catch (IOException e) {
            throw new DocumentAccessException(String.format("Can't write to '%s'", documentId.stringValue()),
                                              e);
         }
         return (T) this;
      } else {
         throw new IllegalStateException("Unsupported operation, content must be OutputStreamDocumentContent");
      }
   }

   /**
    * Reads content to specified output stream from document content using a buffered layer.
    * This operation only supports {@link InputStreamDocumentContent} or {@link LoadedDocumentContent}
    * content.
    *
    * @param outputStream output stream to write to
    *
    * @return this document
    *
    * @throws DocumentAccessException if an I/O error occurs while reading
    */
   protected int read(OutputStream outputStream) {
      notNull(outputStream, "outputStream");

      InputStream contentInputStream = contentInputStream();

      if (contentInputStream != null) {
         try {
            return IOUtils.copy(contentInputStream, outputStream);
         } catch (IOException e) {
            throw new DocumentAccessException(String.format("Can't read from '%s'", documentId.stringValue()),
                                              e);
         }
      } else {
         throw new IllegalStateException(
               "Unsupported operation, content must be InputStreamDocumentContent or LoadedDocumentContent");
      }

   }

   /**
    * Writes specified string to document content using a buffered layer.
    * You can use {@link #flush()} to flush the buffered layer and underlying output stream.
    * Depending on document implementation, you should call {@link #finish()} to finish the content
    * generation.
    * <p>
    * This operation only supports {@link OutputStreamDocumentContent} content.
    *
    * @param string string to write
    *
    * @return this document
    *
    * @throws DocumentAccessException if an I/O error occurs while writing
    * @see #finish()
    */
   @SuppressWarnings("unchecked")
   protected T write(String string) {
      notNull(string, "string");

      Writer contentWriter = contentWriter();

      if (contentWriter != null) {
         try {
            contentWriter.write(string);
         } catch (IOException e) {
            throw new DocumentAccessException(String.format("Can't write to '%s'", documentId.stringValue()),
                                              e);
         }
         return (T) this;
      } else {
         throw new IllegalStateException(
               "Unsupported operation, document content must be OutputStreamDocumentContent");
      }
   }

   /**
    * Writes specified reader content to current document content using a buffered layer.
    *
    * @param reader reader
    *
    * @return this document
    *
    * @throws DocumentAccessException if an I/O error occurs while writing
    * @see #finish()
    */
   @SuppressWarnings("unchecked")
   protected T write(Reader reader) {
      notNull(reader, "reader");

      Writer contentWriter = contentWriter();

      if (contentWriter != null) {
         try {
            IOUtils.copy(reader, contentWriter);
         } catch (IOException e) {
            throw new DocumentAccessException(String.format("Can't write to '%s'", documentId.stringValue()),
                                              e);
         }
         return (T) this;
      } else {
         throw new IllegalStateException("Unsupported operation, content must be OutputStreamDocumentContent");
      }
   }

   /**
    * Reads content to specified writer from document content using a buffered layer.
    * This operation only supports {@link InputStreamDocumentContent} or {@link LoadedDocumentContent}
    * content.
    *
    * @param writer writer to write to
    *
    * @return this document
    *
    * @throws DocumentAccessException if an I/O error occurs while reading
    */
   protected int read(Writer writer) {
      notNull(writer, "writer");

      Reader contentReader = contentReader();

      if (contentReader != null) {
         try {
            return IOUtils.copy(contentReader, writer);
         } catch (IOException e) {
            throw new DocumentAccessException(String.format("Can't read from '%s'", documentId.stringValue()),
                                              e);
         }
      } else {
         throw new IllegalStateException(
               "Unsupported operation, content must be InputStreamDocumentContent or LoadedDocumentContent");
      }
   }

   /**
    * Writes specified document {@link DocumentContent#inputStreamContent()} to current document content using
    * a buffered layer.
    *
    * @param document document with compatible content encoding
    *
    * @return this document
    *
    * @throws DocumentAccessException if an I/O error occurs while writing
    * @see #finish()
    */
   protected T write(Document document) {
      validate(document, "document", hasCompatibleContentEncoding()).orThrow();

      try (InputStream documentContent = document.content().inputStreamContent()) {
         return write(documentContent);
      } catch (IOException e) {
         throw new DocumentAccessException(String.format("Can't write to '%s'", documentId.stringValue()), e);
      }
   }

   /**
    * Reads content to specified document {@link DocumentContent#outputStreamContent()} from document content
    * using a buffered layer.
    * This operation only supports {@link InputStreamDocumentContent} or {@link LoadedDocumentContent}
    * content.
    *
    * @param document document to write to
    *
    * @return this document
    *
    * @throws DocumentAccessException if an I/O error occurs while reading
    */
   protected int read(Document document) {
      validate(document, "document", hasCompatibleContentEncoding()).orThrow();

      try (OutputStream documentContent = document.content().outputStreamContent()) {
         return read(documentContent);
      } catch (IOException e) {
         throw new DocumentAccessException(String.format("Can't read from '%s'", documentId.stringValue()),
                                           e);
      }
   }

   /**
    * Writes specified line to document content using a buffered layer.
    * An extra {@link #lineSeparator()} is appended to specified string.
    * You can use {@link #flush()} to flush the buffered layer and underlying output stream.
    * Depending on document implementation, you should call {@link #finish()} to finish the content
    * generation.
    * <p>
    * This operation only supports {@link OutputStreamDocumentContent} content.
    *
    * @param string string to write
    *
    * @return this document
    *
    * @throws DocumentAccessException if an I/O error occurs while writing
    * @see #lineSeparator()
    * @see #finish()
    */
   @SuppressWarnings("unchecked")
   protected T writeLn(String string) {
      notNull(string, "string");

      write(string + lineSeparator());

      return (T) this;
   }

   /**
    * Reads a line from document content.
    * Line separator ignores {@link #lineSeparator()} and always use {@code LF}, {@code CRLF}, {@code CR}
    * separators.
    * <p>
    * This operation only supports {@link InputStreamDocumentContent} or {@link LoadedDocumentContent}
    * content.
    *
    * @return this document
    *
    * @throws DocumentAccessException if an I/O error occurs while reading
    */
   protected String readLn() {
      Reader contentReader = contentReader();

      if (contentReader instanceof BufferedReader) {
         try {
            return ((BufferedReader) contentReader).readLine();
         } catch (IOException e) {
            throw new DocumentAccessException(String.format("Can't read from '%s'", documentId.stringValue()),
                                              e);
         }
      } else {
         throw new IllegalStateException(
               "Unsupported operation, content must be InputStreamDocumentContent or LoadedDocumentContent, and content must be buffered");
      }
   }

   /**
    * Line separator to use in {@link #writeLn(String)} operations.
    * Supported line separators depends on document type implementation.
    * <p>
    * You can override this method in specialized document implementations. You should not use
    * {@link System#lineSeparator()} to keep deterministic behavior in document format generation.
    *
    * @return line separator
    */
   protected abstract String lineSeparator();

   /**
    * Finishes document I/O operations. Signal that content is either completely read or completely written.
    * Document content is {@link DocumentContent#close() closed} in the process.
    * Typed document implementations must ensure this method is always called internally or externally.
    *
    * @return this document
    *
    * @throws DocumentAccessException if an I/O error occurs
    * @implNote {@link #contentOutputStream} and {@link #contentWriter} are only flushed because both
    *       of them are <em>buffered</em>, and ultimately reference the same output stream, so closing them
    *       would close the underlying stream, preventing the other buffer to be flushed. Finally closing the
    *       content will close the streams that really need to be closed.
    *       It's important that buffers are flushed here, for example if {@link #readableContent()} is called.
    */
   protected T finish() {
      validate(self(), isNotFinished()).orThrow();

      try {
         return unchecked(() -> smartFinalize(this::flush, content::close));
      } finally {
         finished = true;
      }
   }

   /**
    * Returns whether this document has been {@link #finish() finished}
    *
    * @return whether this document has been finished
    */
   protected boolean finished() {
      return finished;
   }

   protected InvariantRule<T> isNotFinished() {
      return as(TypedDocument::finished,
                isFalse(FastStringFormat.of("Document '",
                                            validatingObjectInitialValue(d -> ((Document) d)
                                                  .documentId()
                                                  .stringValue()),
                                            "' must not be finished")));
   }

   protected InvariantRule<T> isFinished() {
      return as(TypedDocument::finished,
                isTrue(FastStringFormat.of("Document '",
                                           validatingObjectInitialValue(d -> ((Document) d)
                                                 .documentId()
                                                 .stringValue()),
                                           "' must be finished")));
   }

   /**
    * Flushes write buffers.
    * Depending on implementation, some content can be really flushed only when {@link #finish()} is
    * called.
    *
    * @return this document
    *
    * @throws DocumentAccessException if an I/O error occurs
    */
   @SuppressWarnings("unchecked")
   public T flush() {
      validate(self(), isNotFinished()).orThrow();

      if (contentOutputStream != null) {
         try {
            contentOutputStream.flush();
         } catch (IOException e) {
            throw new DocumentAccessException(e);
         }
      }
      if (contentWriter != null) {
         try {
            contentWriter.flush();
         } catch (IOException e) {
            throw new DocumentAccessException(e);
         }
      }
      return (T) this;
   }

   /**
    * {@inheritDoc}
    * <p>
    * {@link OutputStreamDocumentContent} must be {@link #finish() finished} before calling this method.
    */
   @Override
   @SuppressWarnings("unchecked")
   public T readableContent() {
      validate(self(), isFinished()).orThrow();

      return (T) super.readableContent();
   }

   /**
    * {@inheritDoc}
    * <p>
    * Covariant override.
    */
   @Override
   @SuppressWarnings("unchecked")
   public T loadContent() {
      return (T) super.loadContent();
   }

   /**
    * {@inheritDoc}
    * <p>
    * Covariant override.
    */
   @Override
   @SuppressWarnings("unchecked")
   public T content(DocumentContent content) {
      return (T) super.content(content);
   }

   /**
    * {@inheritDoc}
    * <p>
    * Covariant override.
    */
   @Override
   @SuppressWarnings("unchecked")
   public T process(DocumentProcessor processor) {
      return (T) super.process(processor);
   }

   /**
    * {@inheritDoc}
    * <p>
    * Covariant override.
    */
   @Override
   @SuppressWarnings("unchecked")
   public T duplicate(DocumentPath newDocumentId) {
      return (T) super.duplicate(newDocumentId);
   }

   /**
    * {@inheritDoc}
    * <p>
    * Covariant override.
    */
   @Override
   @SuppressWarnings("unchecked")
   protected T metadata(UnaryOperator<DocumentMetadataBuilder> metadataOperator) {
      return (T) super.metadata(metadataOperator);
   }

   /**
    * {@inheritDoc}
    * <p>
    * Covariant override.
    */
   @Override
   @SuppressWarnings("unchecked")
   public T documentPath(Path documentPath) {
      return (T) super.documentPath(documentPath);
   }

   /**
    * {@inheritDoc}
    * <p>
    * Covariant override.
    */
   @Override
   @SuppressWarnings("unchecked")
   public T contentType(MimeType contentType) {
      return (T) super.contentType(contentType);
   }

   /**
    * {@inheritDoc}
    * <p>
    * Covariant override.
    */
   @Override
   @SuppressWarnings("unchecked")
   public T attributes(Map<String, String> attributes) {
      return (T) super.attributes(attributes);
   }

   /**
    * {@inheritDoc}
    * <p>
    * Covariant override.
    */
   @Override
   @SuppressWarnings("unchecked")
   public T addAttributes(Map<String, String> attributes) {
      return (T) super.addAttributes(attributes);
   }

   /**
    * {@inheritDoc}
    * <p>
    * Covariant override.
    */
   @Override
   @SuppressWarnings("unchecked")
   public T addAttribute(String key, String value) {
      return (T) super.addAttribute(key, value);
   }

   /**
    * Lazily initializes content output stream for bytes write operations.
    *
    * @return content output stream, or {@code null} if content type is not supported
    */
   protected OutputStream contentOutputStream() {
      if (contentOutputStream == null) {
         if (content instanceof OutputStreamDocumentContent) {
            contentOutputStream =
                  outputBufferSize > 0
                  ? new BufferedOutputStream(content.outputStreamContent(),
                                             outputBufferSize)
                  : content.outputStreamContent();
         }
      }

      return contentOutputStream;
   }

   /**
    * Lazily initializes content writer for character write operations.
    * If {@link DocumentMetadata#contentEncoding()} is present, use it as default encoding for writer,
    * otherwise content must have the encoding set, otherwise it fails.
    *
    * @return content writer, or {@code null} if content type is not supported
    *
    * @throws DocumentAccessException if content has no encoding set and not default encoding is
    *       available
    */
   protected Writer contentWriter() {
      if (contentWriter == null) {
         if (content instanceof OutputStreamDocumentContent) {
            contentWriter = outputBufferSize > 0
                            ? new BufferedWriter(metadata
                                                       .contentEncoding()
                                                       .map(content::writerContent)
                                                       .orElseGet(content::writerContent),
                                                 outputBufferSize)
                            : content.writerContent();
         }
      }

      return contentWriter;
   }

   /**
    * Lazily initializes content input stream for bytes read operations.
    *
    * @return content input stream, or {@code null} if content type is not supported
    */
   protected InputStream contentInputStream() {
      if (contentInputStream == null) {
         if (content instanceof InputStreamDocumentContent || content instanceof LoadedDocumentContent) {
            contentInputStream = inputBufferSize > 0
                                 ? new BufferedInputStream(content.inputStreamContent(), inputBufferSize)
                                 : content.inputStreamContent();
         }
      }

      return contentInputStream;
   }

   /**
    * Lazily initializes content reader for character read operations.
    * If {@link DocumentMetadata#contentEncoding()} is present, use it as default encoding for reader,
    * otherwise content must have the encoding set, otherwise it fails.
    *
    * @return content reader, or {@code null} if content type is not supported
    *
    * @throws DocumentAccessException if content has no encoding set and not default encoding is
    *       available
    */
   protected Reader contentReader() {
      if (contentReader == null) {
         if (content instanceof InputStreamDocumentContent || content instanceof LoadedDocumentContent) {
            contentReader = inputBufferSize > 0
                            ? new BufferedReader(metadata
                                                       .contentEncoding()
                                                       .map(content::readerContent)
                                                       .orElseGet(content::readerContent),
                                                 inputBufferSize)
                            : content.readerContent();
         }
      }

      return contentReader;
   }

   @Override
   protected abstract DocumentBuilder documentBuilder();

   /**
    * Typed document base builder.
    *
    * @param <T> typed document type
    * @param <B> typed document builder type
    */
   public static abstract class TypedDocumentBuilder<T extends TypedDocument<T>, B extends TypedDocumentBuilder<T, B>>
         extends DocumentBuilder {
      private Integer outputBufferSize;
      private Integer inputBufferSize;

      @Setter
      @SuppressWarnings("unchecked")
      protected B outputBufferSize(Integer outputBufferSize) {
         ensureNotReconstitute();
         this.outputBufferSize = outputBufferSize;
         return (B) this;
      }

      @Setter
      @SuppressWarnings("unchecked")
      protected B inputBufferSize(Integer inputBufferSize) {
         ensureNotReconstitute();
         this.inputBufferSize = inputBufferSize;
         return (B) this;
      }

      @Setter
      protected B assumeContentType(boolean assumeContentType,
                                    boolean assumeContentEncoding,
                                    MimeType contentType) {
         ensureReconstitute();

         return metadata(assumeContentType(assumeContentType, assumeContentEncoding, metadata, contentType));
      }

      @Setter
      protected B assumeContentType(MimeType contentType) {
         return assumeContentType(true, true, contentType);
      }

      /**
       * Helper operation to fill missing content-type and/or content encoding of the specified document with
       * assumed content-type. This is useful when content-type/encoding is missing and we want to create a
       * typed document assuming some metadata.
       *
       * @param assumeContentType whether to assume content-type (without encoding)
       * @param assumeContentEncoding whether to assume content encoding
       * @param metadata document metadata to read content-type from if any
       * @param contentType assumed content-type/encoding
       *
       * @return metadata builder updater that sets the assumed metadata if missing in specified document
       */
      protected static UnaryOperator<DocumentMetadataBuilder> assumeContentType(boolean assumeContentType,
                                                                                boolean assumeContentEncoding,
                                                                                DocumentMetadata metadata,
                                                                                MimeType contentType) {
         MimeType assumedContentType = assumeContentType
                                       ? metadata
                                             .simpleContentType()
                                             .orElseGet(contentType::strippedParameters)
                                       : metadata.simpleContentType().orElse(null);
         Charset assumedContentEncoding = assumeContentEncoding
                                          ? metadata
                                                .contentEncoding()
                                                .orElse(contentType.charset().orElse(null))
                                          : metadata.contentEncoding().orElse(null);

         return md -> md.contentType(assumedContentType, assumedContentEncoding);
      }

      @Override
      @SuppressWarnings("unchecked")
      protected B copy(Document document) {
         return (B) super.copy(document);
      }

      @Override
      @SuppressWarnings("unchecked")
      public B open(DocumentPath documentId,
                    OutputStream contentOutputStream,
                    Charset contentEncoding,
                    MimeType contentType) {
         return (B) super.open(documentId, contentOutputStream, contentEncoding, contentType);
      }

      @Override
      @SuppressWarnings("unchecked")
      public B open(DocumentPath documentId) {
         return (B) super.open(documentId);
      }

      @Override
      @SuppressWarnings("unchecked")
      public B open(DocumentPath documentId, Charset contentEncoding) {
         return (B) super.open(documentId, contentEncoding);
      }

      @Override
      @SuppressWarnings("unchecked")
      public B mimeTypeRegistry(MimeTypeRegistry mimeTypeRegistry) {
         return (B) super.mimeTypeRegistry(mimeTypeRegistry);
      }

      @Override
      @SuppressWarnings("unchecked")
      public B documentId(DocumentPath documentId) {
         return (B) super.documentId(documentId);
      }

      @Override
      @SuppressWarnings("unchecked")
      public B documentPath(Path documentPath) {
         return (B) super.documentPath(documentPath);
      }

      @Override
      @SuppressWarnings("unchecked")
      public B documentPath(String documentPath, String... moreDocumentPath) {
         return (B) super.documentPath(documentPath, moreDocumentPath);
      }

      @Override
      @SuppressWarnings("unchecked")
      public B contentType(MimeType contentType) {
         return (B) super.contentType(contentType);
      }

      @Override
      @SuppressWarnings("unchecked")
      public B attributes(Map<String, String> attributes) {
         return (B) super.attributes(attributes);
      }

      @Override
      @SuppressWarnings("unchecked")
      public B metadata(DocumentMetadata metadata) {
         return (B) super.metadata(metadata);
      }

      @Override
      @SuppressWarnings("unchecked")
      public B metadata(UnaryOperator<DocumentMetadataBuilder> metadataOperator) {
         return (B) super.metadata(metadataOperator);
      }

      @Override
      @SuppressWarnings("unchecked")
      public B documentEntry(DocumentEntry documentEntry) {
         return (B) super.documentEntry(documentEntry);
      }

      @Override
      @SuppressWarnings("unchecked")
      public B content(DocumentContent content) {
         return (B) super.content(content);
      }

      @Override
      @SuppressWarnings("unchecked")
      public B loadedContent(byte[] content, Charset contentEncoding) {
         return (B) super.loadedContent(content, contentEncoding);
      }

      @Override
      @SuppressWarnings("unchecked")
      public B loadedContent(byte[] content) {
         return (B) super.loadedContent(content);
      }

      @Override
      @SuppressWarnings("unchecked")
      public B loadedContent(String content, Charset contentEncoding) {
         return (B) super.loadedContent(content, contentEncoding);
      }

      @Override
      @SuppressWarnings("unchecked")
      public B loadedContent(InputStream content, Charset contentEncoding) {
         return (B) super.loadedContent(content, contentEncoding);
      }

      @Override
      @SuppressWarnings("unchecked")
      public B loadedContent(InputStream content) {
         return (B) super.loadedContent(content);
      }

      @Override
      @SuppressWarnings("unchecked")
      public B loadedContent(Reader content, Charset contentEncoding) {
         return (B) super.loadedContent(content, contentEncoding);
      }

      @Override
      @SuppressWarnings("unchecked")
      public B streamContent(byte[] content, Charset contentEncoding) {
         return (B) super.streamContent(content, contentEncoding);
      }

      @Override
      @SuppressWarnings("unchecked")
      public B streamContent(byte[] content) {
         return (B) super.streamContent(content);
      }

      @Override
      @SuppressWarnings("unchecked")
      public B streamContent(InputStream content, Charset contentEncoding, Long contentSize) {
         return (B) super.streamContent(content, contentEncoding, contentSize);
      }

      @Override
      @SuppressWarnings("unchecked")
      public B streamContent(InputStream content, Charset contentEncoding) {
         return (B) super.streamContent(content, contentEncoding);
      }

      @Override
      @SuppressWarnings("unchecked")
      public B streamContent(InputStream content, Long contentSize) {
         return (B) super.streamContent(content, contentSize);
      }

      @Override
      @SuppressWarnings("unchecked")
      public B streamContent(InputStream content) {
         return (B) super.streamContent(content);
      }

      @Override
      @SuppressWarnings("unchecked")
      public B streamContent(String content, Charset contentEncoding) {
         return (B) super.streamContent(content, contentEncoding);
      }

      @Override
      @SuppressWarnings("unchecked")
      public B streamContent(Reader content, Charset contentEncoding) {
         return (B) super.streamContent(content, contentEncoding);
      }

      @Override
      @SuppressWarnings("unchecked")
      public B reconstitute() {
         return super.reconstitute();
      }

      @Override
      @SuppressWarnings("unchecked")
      protected B finalizeBuild() {
         return super.finalizeBuild();
      }

      @Override
      @SuppressWarnings("unchecked")
      public T build() {
         return (T) super.build();
      }
   }
}
