
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.metrology.jmx;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.lang.util.ExceptionUtils.sneakyThrow;

import java.util.StringJoiner;
import java.util.function.Predicate;

import javax.management.DynamicMBean;
import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanRegistrationException;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;

import com.tinubu.commons.ddd2.domain.event.DomainEvent;
import com.tinubu.commons.lang.metrology.jmx.JmxServer;
import com.tinubu.commons.lang.metrology.jmx.MetrologyMBean;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.metrology.DocumentRepositoryMetrology;

/**
 * JMX metrology bean for all {@link DocumentRepository}.
 * <p>
 * This metrology implementation registers wrapped MBean in JMX, hence, it will propagate events to delegated
 * metrology but does not provide counter accessors by itself.
 * <p>
 * You can {@link DocumentRepository#registerMetrology(DocumentRepositoryMetrology) register} either :
 * <ul>
 *    <li>this class created with {@link #of(DocumentRepositoryMetrology, String)}</li>
 *    <li>the delegated metrology returned from {@link #register(DocumentRepositoryMetrology, String)}</li>
 * </ul>
 */
public class JmxDocumentRepositoryMetrology implements DocumentRepositoryMetrology {

   private static final String JMX_DOMAIN = "com.tinubu.commons-ports.document";

   private final DocumentRepositoryMetrology delegate;

   protected JmxDocumentRepositoryMetrology(DocumentRepositoryMetrology delegate) {
      validate(this.delegate = delegate, "delegate", isNotNull()).orThrow();
   }

   public static JmxDocumentRepositoryMetrology of(DocumentRepositoryMetrology delegate, String name)
         throws InstanceAlreadyExistsException, MBeanRegistrationException, MalformedObjectNameException {
      validate(delegate, "delegate", isNotNull()).and(validate(name, "name", isNotBlank())).orThrow();

      registerMBean(delegate, name);

      return new JmxDocumentRepositoryMetrology(delegate);
   }

   public static JmxDocumentRepositoryMetrology of(DocumentRepositoryMetrology delegate)
         throws InstanceAlreadyExistsException, MBeanRegistrationException {
      registerMBean(delegate);

      return new JmxDocumentRepositoryMetrology(delegate);
   }

   public static <T extends DocumentRepositoryMetrology> T register(T delegate, String name)
         throws InstanceAlreadyExistsException, MBeanRegistrationException, MalformedObjectNameException {
      registerMBean(delegate, name);

      return delegate;
   }

   public static <T extends DocumentRepositoryMetrology> T register(T delegate)
         throws InstanceAlreadyExistsException, MBeanRegistrationException {
      registerMBean(delegate);

      return delegate;
   }

   public static <T extends DocumentRepositoryMetrology> void unregister(Class<T> delegateType, String name)
         throws MBeanRegistrationException, MalformedObjectNameException, InstanceNotFoundException {
      unregisterMBean(delegateType, name);
   }

   public static <T extends DocumentRepositoryMetrology> void unregister(Class<T> delegateType)
         throws MBeanRegistrationException, InstanceNotFoundException {
      unregisterMBean(delegateType);
   }

   protected static <T extends DocumentRepositoryMetrology> void registerMBean(T delegate)
         throws InstanceAlreadyExistsException, MBeanRegistrationException {
      try {
         JmxServer.instance(JMX_DOMAIN)
               .registerMBean(dynamicMBean(delegate, delegate.getClass().getSimpleName()),
                              delegate.getClass().getSimpleName(),
                              delegate.getClass().getSimpleName());
      } catch (NotCompliantMBeanException | MalformedObjectNameException e) {
         throw sneakyThrow(e);
      }
   }

   protected static <T extends DocumentRepositoryMetrology> void registerMBean(T delegate, String name)
         throws InstanceAlreadyExistsException, MBeanRegistrationException, MalformedObjectNameException {
      try {
         JmxServer
               .instance(JMX_DOMAIN)
               .registerMBean(dynamicMBean(delegate, name), delegate.getClass().getSimpleName(), name);
      } catch (NotCompliantMBeanException e) {
         throw sneakyThrow(e);
      }
   }

   protected static <T extends DocumentRepositoryMetrology> void unregisterMBean(Class<? extends DocumentRepositoryMetrology> delegateType,
                                                                                 String name)
         throws MalformedObjectNameException, InstanceNotFoundException, MBeanRegistrationException {
      JmxServer.instance(JMX_DOMAIN).unregisterMBean(delegateType, name);
   }

   protected static <T extends DocumentRepositoryMetrology> void unregisterMBean(Class<? extends DocumentRepositoryMetrology> delegateType)
         throws InstanceNotFoundException, MBeanRegistrationException {
      JmxServer.instance(JMX_DOMAIN).unregisterMBean(delegateType);
   }

   private static <T extends DocumentRepositoryMetrology> DynamicMBean dynamicMBean(T delegate, String name) {
      return new MetrologyMBean<DocumentRepositoryMetrology>(delegate, name);
   }

   @Override
   public void accept(DomainEvent event) {
      delegate.accept(event);
   }

   @Override
   public void resetMetrics() {
      delegate.resetMetrics();
   }

   @Override
   public JmxDocumentRepositoryMetrology filteredListener(Predicate<DomainEvent> filter) {
      return new JmxDocumentRepositoryMetrology(delegate) {
         @Override
         public void accept(DomainEvent event) {
            if (filter.test(event)) {
               JmxDocumentRepositoryMetrology.this.accept(event);
            }
         }
      };
   }

   public String toString() {
      return new StringJoiner(", ", JmxDocumentRepositoryMetrology.class.getSimpleName() + "[", "]")
            .add("delegate=" + delegate)
            .toString();
   }

}
