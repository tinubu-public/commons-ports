/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.typed;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import com.tinubu.commons.ddd2.domain.ids.Uuid;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.processor.common.ContentTypeDocumentRenamer;

/**
 * Special No-op document with empty content.
 */
public class NoopDocument extends TypedDocument<NoopDocument> {

   protected NoopDocument(NoopDocumentBuilder builder) {
      super(builder);
   }

   /**
    * Creates a new noop document. Generated document has random document identifier, with auto-generated
    * extension based on content-type if specified.
    *
    * @param documentId document identifier
    * @param contentType document content-type
    *
    * @return new noop document
    */
   public static NoopDocument noop(DocumentPath documentId, MimeType contentType) {
      validate(documentId, "documentId", isNotNull())
            .and(validate(contentType, "contentType", isNotNull()))
            .orThrow();

      return createNoopDocument(documentId, contentType);
   }

   /**
    * Creates a new noop document. Content-type is auto-detected from document extension.
    *
    * @param documentId document identifier
    *
    * @return new noop document
    */
   public static NoopDocument noop(DocumentPath documentId) {
      validate(documentId, "documentId", isNotNull()).orThrow();

      return createNoopDocument(documentId, null);
   }

   /**
    * Creates a new noop document. Generated document has random document identifier, with auto-generated
    * extension based on content-type.
    *
    * @param contentType document content-type
    *
    * @return new noop document
    */
   public static NoopDocument noop(MimeType contentType) {
      validate(contentType, "contentType", isNotNull()).orThrow();

      return createNoopDocument(null, contentType);
   }

   /**
    * Creates a new noop document without content-type. Generated document has random document identifier
    * without extension.
    *
    * @return new noop document
    */
   public static NoopDocument noop() {
      return createNoopDocument(null, null);
   }

   private static NoopDocument createNoopDocument(DocumentPath documentId, MimeType contentType) {
      NoopDocument noop = new NoopDocumentBuilder()
            .documentId(documentId != null ? documentId : DocumentPath.of(Uuid.newRandomUuid().stringValue()))
            .loadedContent(new byte[0], nullable(contentType).flatMap(MimeType::charset).orElse(null))
            .contentType(contentType)
            .build();

      if (documentId == null) {
         noop = noop.process(new ContentTypeDocumentRenamer(false, true));
      }

      return noop;
   }

   @Override
   protected String lineSeparator() {
      throw new UnsupportedOperationException();
   }

   @Override
   protected boolean requiredContentType() {
      return false;
   }

   @Override
   protected DocumentBuilder documentBuilder() {
      return new NoopDocumentBuilder();
   }

   /**
    * @implNote Builder is not public to force usage of factory methods : {@link #noop()}, ...
    */
   protected static class NoopDocumentBuilder
         extends TypedDocumentBuilder<NoopDocument, NoopDocumentBuilder> {

      /**
       * Copy constructor for {@link NoopDocument}
       *
       * @param document document to copy
       *
       * @return noop document builder
       */
      public static NoopDocumentBuilder from(Document document) {
         notNull(document, "document");

         return new NoopDocumentBuilder().reconstitute().copy(document);
      }

      @Override
      protected NoopDocumentBuilder copy(Document document) {
         return super.copy(document);
      }

      @Override
      public NoopDocument buildDomainObject() {
         return new NoopDocument(this);
      }

   }

}
