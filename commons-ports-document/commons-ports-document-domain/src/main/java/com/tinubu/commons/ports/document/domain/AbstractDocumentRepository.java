/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.lang.util.CheckedRunnable.uncheckedRunnable;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.ExceptionUtils.smartFinalize;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.OptionalUtils.peek;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.DOCUMENT_URI;

import java.io.InputStream;
import java.net.URI;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;

import org.apache.commons.lang3.time.StopWatch;

import com.tinubu.commons.ddd2.domain.event.DomainEvent;
import com.tinubu.commons.ddd2.domain.event.DomainEventListener;
import com.tinubu.commons.ddd2.domain.event.RegistrableDomainEventService;
import com.tinubu.commons.ddd2.domain.event.SynchronousDomainEventService;
import com.tinubu.commons.ports.document.domain.event.DocumentAccessed;
import com.tinubu.commons.ports.document.domain.event.DocumentDeleted;
import com.tinubu.commons.ports.document.domain.event.DocumentRepositoryEvent;
import com.tinubu.commons.ports.document.domain.event.DocumentSaved;

/**
 * Base document repository implementation.
 * Specified {@link RegistrableDomainEventService} can be required to be thread-safe depending on the event
 * registration initialization scheme.
 *
 * @implSpec Immutable class implementation
 */
public abstract class AbstractDocumentRepository implements DocumentRepository {

   protected final RegistrableDomainEventService eventService;

   protected AbstractDocumentRepository(RegistrableDomainEventService eventService) {
      this.eventService = validate(eventService, "eventService", isNotNull()).orThrow();
   }

   protected AbstractDocumentRepository() {
      this(new SynchronousDomainEventService());
   }

   public RegistrableDomainEventService eventService() {
      return eventService;
   }

   @Override
   @SuppressWarnings("unchecked")
   public <E extends DomainEvent> void registerEventListener(DomainEventListener<? super E> eventListener,
                                                             Class<? extends E>... eventClasses) {
      stream(eventClasses).forEach(eventClass -> this.eventService.registerEventListener(eventClass,
                                                                                         eventListener));
   }

   @Override
   public void unregisterEventListeners() {
      eventService.unregisterEventListeners();
   }

   protected <T extends Document> Optional<T> handleDocumentEvent(Supplier<Optional<T>> result,
                                                                  Function<T, DocumentRepositoryEvent> successfulEvent) {
      return peek(result.get(), entry -> publishEvent(successfulEvent.apply(entry)));
   }

   protected <T extends Document> Optional<T> handleDocumentEvent(Supplier<Optional<T>> result,
                                                                  Function<T, DocumentRepositoryEvent> successfulEvent,
                                                                  Supplier<DocumentRepositoryEvent> failureEvent) {
      return handleDocumentEvent(result, successfulEvent).or(() -> {
         publishEvent(failureEvent.get());
         return optional();
      });
   }

   protected <T extends Document> Stream<T> handleDocumentStreamEvent(Supplier<Stream<T>> result,
                                                                      Function<T, DocumentRepositoryEvent> successfulEvent) {
      return result.get().peek(document -> publishEvent(successfulEvent.apply(document)));
   }

   protected <T extends DocumentEntry> Optional<T> handleDocumentEntryEvent(Supplier<Optional<T>> result,
                                                                            Function<T, DocumentRepositoryEvent> successfulEvent) {
      return peek(result.get(), entry -> publishEvent(successfulEvent.apply(entry)));
   }

   protected <T extends DocumentEntry> Optional<T> handleDocumentEntryEvent(Supplier<Optional<T>> result,
                                                                            Function<T, DocumentRepositoryEvent> successfulEvent,
                                                                            Supplier<DocumentRepositoryEvent> failureEvent) {
      return handleDocumentEntryEvent(result, successfulEvent).or(() -> {
         publishEvent(failureEvent.get());
         return optional();
      });
   }

   protected <T extends DocumentEntry> Stream<T> handleDocumentEntryStreamEvent(Supplier<Stream<T>> result,
                                                                                Function<T, DocumentRepositoryEvent> successfulEvent) {
      return result.get().peek(document -> publishEvent(successfulEvent.apply(document)));
   }

   protected <T extends DocumentEntry, C extends Collection<T>> C handleDocumentEntriesEvent(Supplier<C> result,
                                                                                             Function<T, DocumentRepositoryEvent> successfulEvent) {
      C collection = result.get();
      for (T documentEntry : collection) {
         publishEvent(successfulEvent.apply(documentEntry));
      }
      return collection;
   }

   public void publishEvent(DomainEvent event) {
      eventService.publishEvent(event);
   }

   protected DocumentSaved documentSaved(DocumentEntry document, StopWatch watch) {
      return new DocumentSaved(this,
                               document,
                               documentUri(document.documentId()).orElse(null),
                               duration(watch));
   }

   protected DocumentDeleted documentDeleted(DocumentEntry document, StopWatch watch) {
      return new DocumentDeleted(this,
                                 document,
                                 documentUri(document.documentId()).orElse(null),
                                 duration(watch));
   }

   protected DocumentAccessed documentAccessed(DocumentEntry document, StopWatch watch) {
      return new DocumentAccessed(this,
                                  document,
                                  documentUri(document.documentId()).orElse(null),
                                  duration(watch));
   }

   private Optional<URI> documentUri(DocumentPath documentId) {
      if (!this.hasCapability(DOCUMENT_URI)) {
         return optional();
      }

      return optional(this.toUri(documentId));
   }

   protected <T> T watch(StopWatch watch, Supplier<? extends T> supplier, boolean restart) {
      notNull(watch, "watch");
      notNull(supplier, "supplier");

      if (restart) {
         watch.reset();
         watch.start();
      }
      try {
         return supplier.get();
      } catch (Exception e) {
         watch.split();
         throw e;
      } finally {
         watch.split();
      }
   }

   protected <T> T watch(StopWatch watch, Supplier<? extends T> supplier) {
      return watch(watch, supplier, true);
   }

   protected void watch(StopWatch watch, Runnable runnable, boolean restart) {
      notNull(watch, "watch");
      notNull(runnable, "runnable");

      if (restart) {
         watch.reset();
         watch.start();
      }
      try {
         runnable.run();
      } catch (Exception e) {
         watch.split();
         throw e;
      } finally {
         watch.split();
      }
   }

   protected void watch(StopWatch watch, Runnable runnable) {
      watch(watch, runnable, true);
   }

   /**
    * Helper to extract a {@link StopWatch} current duration. Stop watch state should not be changed by this
    * function.
    *
    * @param stopWatch stop watch
    *
    * @return stop watch current duration
    */
   protected static Duration duration(StopWatch stopWatch) {
      if (stopWatch == null) {
         return Duration.ZERO;
      } else {
         return Duration.ofNanos(stopWatch.getNanoTime());
      }
   }

   /**
    * Instruments {@link Stream} of {@link Document} so that for each document content generated by the
    * stream, a registry of {@link DocumentContent#inputStreamContent()} is maintained for automatic removal
    * at {@link Stream#close()} time.
    * <p>
    * The reason is that there's no way to ensure proper stream elements closing in case of exception in
    * stream operations. You have to close the instrumented {@link Stream} to ensure that all contents are
    * closed. However, you should close each {@link DocumentContent#inputStreamContent()} as soon as
    * possible in regular stream usage, to free resources optimally.
    * <p>
    * Requirement : {@link DocumentContent#inputStreamContent()} {@link InputStream#close()} operation must be
    * idem-potent (must support multiple calls).
    *
    * @implNote
    */
   protected Stream<Document> documentContentAutoCloseOnStreamClose(Stream<Document> documents) {
      final List<InputStream> contentAutomaticCleanup = new ArrayList<>();

      return documents
            .peek(d -> contentAutomaticCleanup.add(d.content().inputStreamContent()))
            .onClose(uncheckedRunnable(() -> {
               smartFinalize(() -> { }, list(stream(contentAutomaticCleanup).map(content -> content::close)));
            }));
   }
}
