/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.typed;

import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.mimeType;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_PLAIN;
import static java.nio.charset.StandardCharsets.US_ASCII;
import static java.nio.charset.StandardCharsets.UTF_16;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.typed.TextPlainDocument.TextPlainDocumentBuilder;

public class TextPlainDocumentTest {

   @Test
   public void testOfWhenNominal() {
      Document document = new DocumentBuilder()
            .documentId(DocumentPath.of("test.txt"))
            .loadedContent("test content", UTF_8)
            .build();

      TextPlainDocument textDocument = TextPlainDocument.of(document);

      assertThat(textDocument.documentId()).isEqualTo(DocumentPath.of("test.txt"));
      assertThat(textDocument.metadata().contentType()).hasValue(mimeType(TEXT_PLAIN, UTF_8));
   }

   @Test
   public void testOfWhenNotTextPlainContentType() {
      Document document = new DocumentBuilder()
            .documentId(DocumentPath.of("test.pdf"))
            .loadedContent("test content", UTF_8)
            .build();

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> TextPlainDocument.of(document))
            .withMessage(
                  "Invariant validation error > Context [DocumentPath[value=test.pdf,newObject=false]] > 'contentType=application/pdf;charset=UTF-8' must be equal to 'text/plain' (type and subtype only)");
   }

   @Test
   public void testOpenWhenDefaultLineSeparator() {
      System.setProperty("line.separator", "-");

      TextPlainDocument textDocument = TextPlainDocument.open(DocumentPath.of("test.txt"));

      textDocument.writeLn("Line 1");
      textDocument.writeLn("Line 2");
      textDocument.finish();

      Document document = textDocument.readableContent();

      assertThat(document.content().stringContent()).isEqualTo("Line 1\nLine 2\n");
   }

   @Test
   public void testOpenWhenCustomLineSeparator() {
      TextPlainDocument textDocument = new TextPlainDocumentBuilder()
            .open(DocumentPath.of("test.txt"), UTF_8)
            .lineSeparator("-")
            .build();

      textDocument.writeLn("Line 1");
      textDocument.writeLn("Line 2");
      textDocument.finish();

      Document document = textDocument.readableContent();

      assertThat(document.content().stringContent()).isEqualTo("Line 1-Line 2-");
   }

   @Test
   public void testOpenWhenSystemDependentLineSeparator() {
      TextPlainDocument textDocument = new TextPlainDocumentBuilder()
            .open(DocumentPath.of("test.txt"), UTF_8)
            .systemDependentLineSeparator()
            .build();

      textDocument.writeLn("Line 1");
      textDocument.writeLn("Line 2");
      textDocument.finish();

      Document document = textDocument.readableContent();

      assertThat(document.content().stringContent()).isEqualTo("Line 1"
                                                               + System.lineSeparator()
                                                               + "Line 2"
                                                               + System.lineSeparator());
   }

   @Test
   public void testWriteWhenDoNotRequiresStringReencoding() {
      TextPlainDocument textDocument = TextPlainDocument.open(DocumentPath.of("test.txt"), UTF_8);

      textDocument.write("Write§");
      textDocument.writeLn("WriteLn");
      textDocument.finish();

      Document document = textDocument.readableContent();

      assertThat(document.content().stringContent()).isEqualTo("Write§WriteLn\n");
   }

   @Test
   public void testWriteWhenRequiresStringReencodingToNarrowCharset() {
      TextPlainDocument textDocument = TextPlainDocument.open(DocumentPath.of("test.txt"), US_ASCII);

      textDocument.write("Write§");
      textDocument.writeLn("WriteLn");
      textDocument.finish();

      Document document = textDocument.readableContent();

      assertThat(document.content().stringContent()).isEqualTo("Write?WriteLn\n");
   }

   @Test
   public void testWriteWhenRequiresStringReencodingToWiderCharset() {

      TextPlainDocument textDocument = TextPlainDocument.open(DocumentPath.of("test.txt"), UTF_16);

      textDocument.write("Write§");
      textDocument.writeLn("WriteLn");
      textDocument.finish();

      Document document = textDocument.readableContent();

      assertThat(document.content().stringContent()).isEqualTo("Write§WriteLn\n");
   }

}
