/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.testsuite;

import static com.tinubu.commons.lang.util.CheckedConsumer.checkedConsumer;
import static com.tinubu.commons.lang.util.CheckedFunction.checkedFunction;
import static com.tinubu.commons.lang.util.CloseableUtils.finalAcceptCloseableUnchecked;
import static com.tinubu.commons.lang.util.CloseableUtils.finalApplyCloseableUnchecked;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.StreamUtils.reader;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.ReaderInputStream;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mockito;

import com.tinubu.commons.ddd2.domain.event.DomainEvent;
import com.tinubu.commons.ddd2.domain.event.DomainEventListener;
import com.tinubu.commons.ddd2.domain.type.Entity;
import com.tinubu.commons.ddd2.domain.type.Id;
import com.tinubu.commons.ddd2.domain.type.Value;
import com.tinubu.commons.lang.datetime.ApplicationClock;
import com.tinubu.commons.lang.io.FileUtils;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.lang.util.MechanicalSympathy;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentContent;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentMetadata;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent.InputStreamDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.LoadedDocumentContent.LoadedDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.event.DocumentAccessed;
import com.tinubu.commons.ports.document.domain.event.DocumentDeleted;
import com.tinubu.commons.ports.document.domain.event.DocumentSaved;
import com.tinubu.commons.test.generator.ContentGenerator;

/**
 * Base test context and tooling for {@link DocumentRepository} testsuites.
 */
public abstract class BaseDocumentRepositoryTest {

   protected static final ZonedDateTime FIXED_DATE =
         ZonedDateTime.of(LocalDateTime.of(2021, 11, 10, 14, 0, 0), ZoneId.of("UTC"));

   protected static final ContentGenerator contentGenerator = new ContentGenerator();

   /**
    * Provides the document repository under test from test implementations.
    * <p>
    * The same repository instance must be returned if called several times during a single test life cycle.
    *
    * @return the document repository under test
    */
   protected abstract DocumentRepository documentRepository();

   @BeforeEach
   public void setApplicationClock() {
      ApplicationClock.setFixedClock(FIXED_DATE);
   }

   /**
    * Instruments document repository under test with a mocked listener for all events.
    * This function should not be called several times in a given test method, instead use
    * {@link Mockito#clearInvocations(Object[])} to reset verify invocation counts.
    *
    * @param events events to register
    *
    * @return mocked listener for further test verification
    */
   @SuppressWarnings("unchecked")
   protected DomainEventListener<DomainEvent> instrumentDocumentRepositoryListeners(Class<? extends DomainEvent>... events) {
      DomainEventListener<DomainEvent> domainEventListener =
            (DomainEventListener<DomainEvent>) mock(DomainEventListener.class);
      documentRepository().registerEventListener(domainEventListener, events);

      return domainEventListener;
   }

   /**
    * Instruments document repository under test with a mocked listener for all events.
    * This function should not be called several times in a given test method, instead use
    * {@link Mockito#clearInvocations(Object[])} to reset verify invocation counts.
    *
    * @return mocked listener for further test verification
    */
   protected DomainEventListener<DomainEvent> instrumentDocumentRepositoryListeners() {
      return instrumentDocumentRepositoryListeners(DocumentSaved.class,
                                                   DocumentAccessed.class,
                                                   DocumentDeleted.class);
   }

   /**
    * Abstracted ZipArchiver from implementing modules, because of dependency cycle issue on transformer
    * module.
    */
   protected abstract Document zipTransformer(DocumentPath zipPath, List<Document> documents);

   protected static Path path(String path) {
      return nullable(path).map(Paths::get).orElse(null);
   }

   protected static URI uri(String uri) {
      try {
         return new URI(uri);
      } catch (URISyntaxException e) {
         throw new IllegalArgumentException(e);
      }
   }

   /**
    * Returns repository implementation chunk size, in bytes, if such mechanism is in place. Value must always
    * be set, use the default value even if repository has no chunk mechanism.
    *
    * @return repository chunk size
    */
   protected long chunkSize() {
      return 1024 * 1024;
   }

   /**
    * Returns backend implementation forced default content-type when document metadata content-type is not
    * set.
    *
    * @return backend default content-type
    */
   protected Optional<MimeType> defaultContentType() {
      return optional();
   }

   /**
    * Returns {@code true} if repository updates the creation date when overwriting files, which is not the
    * expected behavior.
    *
    * @return {@code true} if repository updates the creation date when overwriting files
    */
   protected boolean isUpdatingCreationDateOnOverwrite() {
      return false;
   }

   /**
    * Returns {@code true} if repository can store and restore content length independently of raw content.
    *
    * @return {@code true} if repository can read content length
    */
   protected boolean isSupportingContentLength() {
      return true;
   }

   /**
    * Returns {@code true} if repository supports repository URI.
    *
    * @return {@code true} if repository supports repository URI
    */
   protected boolean isSupportingRepositoryUri() {
      return true;
   }

   /**
    * Returns {@code true} if repository supports document URI.
    *
    * @return {@code true} if repository supports document URI
    */
   protected boolean isSupportingDocumentUri() {
      return true;
   }

   /**
    * Returns {@code true} if repository supports subPath operation.
    *
    * @return {@code true} if repository supports subPath operation
    */
   protected boolean isSupportingSubPath() {
      return true;
   }

   /**
    * Returns {@code true} if repository supports document URI.
    *
    * @return {@code true} if repository supports document URI
    */
   protected boolean isSupportingMetadataAttributes() {
      return true;
   }

   /**
    * Returns {@code true} if repository supports transactional write operations.
    *
    * @return {@code true} if repository supports transactional write operations
    */
   protected boolean isTransactionalRepository() {
      return false;
   }

   /**
    * Repository test implementation-specific way of creating an IOException when later accessing specified
    * document path in read or write.
    */
   protected void instrumentIOException(Path documentPath, boolean enabled) { }

   /**
    * @deprecated Use {@link #assertThatDocumentContent(Document, InputStream)}-like instead.
    */
   @Deprecated
   protected void assertThatDocumentContent(DocumentPath documentPath, String expected) {
      Optional<Document> readDocument = documentRepository().findDocumentById(documentPath);

      assertThat(readDocument).hasValueSatisfying(rd -> {
         assertThat(rd.content().stringContent(UTF_8)).isEqualTo(expected);
      });
   }

   protected void assertThatDocumentContent(Document document, InputStream expected) {
      assertThat(document.content().inputStreamContent())
            //.withFailMessage("Document '%s' content differs from expected content",
            //                document.documentId().stringValue())
            .hasSameContentAs(expected);
   }

   protected void assertThatDocumentContent(Document document, Reader expected) {
      assertThatDocumentContent(document, new ReaderInputStream(expected, UTF_8));
   }

   /**
    * Creates a zip archive from specified files.
    *
    * @param zipPath zip archive path
    * @param documentPaths documents to archive
    *
    * @return zip archive document entry
    *
    * @implSpec for testing purpose, if zip path extension is {@code bad}, zip content-type is reset to
    *       test unzipping archive with bad content-type.
    */
   protected DocumentEntry createZipDocument(DocumentPath zipPath, DocumentPath... documentPaths) {
      List<Document> documents = list(stream(documentPaths).map(documentPath -> documentRepository()
            .findDocumentById(documentPath)
            .orElseThrow(IllegalStateException::new)));

      Document zipDocument = zipTransformer(zipPath, documents);

      if ("bad".equals(FileUtils.fileExtension(zipPath.value()))) {
         zipDocument = DocumentBuilder
               .from(zipDocument)
               .metadata(DocumentMetadataBuilder.from(zipDocument.metadata()).contentType(null, null).build())
               .build();
      }

      return createDocument(zipDocument);
   }

   protected DocumentBuilder stubDocument(DocumentPath documentId,
                                          UnaryOperator<DocumentMetadataBuilder> metadataBuilderOperation,
                                          UnaryOperator<InputStreamDocumentContentBuilder> contentBuilderOperation) {
      String content = documentId.stringValue();

      return new DocumentBuilder()
            .<DocumentBuilder>reconstitute()
            .documentId(documentId)
            .metadata(new DocumentMetadataBuilder()
                            .<DocumentMetadataBuilder>reconstitute()
                            .documentPath(documentId.value())
                            .contentType(UTF_8)
                            .contentSize((long) content.length())
                            .creationDate(stubCreationDate())
                            .lastUpdateDate(stubLastUpdateDate())
                            .chain(metadataBuilderOperation)
                            .build())
            .content(new InputStreamDocumentContentBuilder()
                           .<InputStreamDocumentContentBuilder>reconstitute()
                           .content(content, UTF_8)
                           .chain(contentBuilderOperation)
                           .build());
   }

   protected Instant stubLastUpdateDate() {
      return ApplicationClock.nowAsInstant().plusSeconds(60);
   }

   protected Instant stubCreationDate() {
      return ApplicationClock.nowAsInstant();
   }

   protected DocumentBuilder stubDocument(DocumentPath documentId,
                                          UnaryOperator<DocumentMetadataBuilder> metadataBuilderOperation) {
      return stubDocument(documentId, metadataBuilderOperation, UnaryOperator.identity());
   }

   protected DocumentBuilder stubDocument(DocumentPath documentId) {
      return stubDocument(documentId, UnaryOperator.identity(), UnaryOperator.identity());
   }

   /**
    * Copy some metadata from actual metadata in order to pass tests when metadata are incorrectly supported
    * in implementation.
    */
   protected UnaryOperator<DocumentMetadataBuilder> synchronizeExpectedMetadata(DocumentMetadata actual) {
      return builder -> builder
            .creationDate(actual.creationDate().orElse(null))
            .lastUpdateDate(actual.lastUpdateDate().orElse(null));
   }

   /**
    * Copy some content from actual content in order to pass tests when content parameters are incorrectly
    * supported in implementation.
    */
   protected UnaryOperator<LoadedDocumentContentBuilder> synchronizeExpectedContent(DocumentContent actual) {
      return builder -> builder;
   }

   protected Optional<Document> filterDocument(Stream<Document> documents, DocumentPath documentId) {
      return documents.filter(d -> d.documentId().equals(documentId)).findAny();
   }

   protected Optional<DocumentEntry> filterDocumentEntry(Stream<DocumentEntry> documents,
                                                         DocumentPath documentId) {
      return documents.filter(d -> d.documentId().equals(documentId)).findAny();
   }

   protected DocumentEntry createDocument(DocumentRepository documentRepository, Document document) {
      return documentRepository
            .saveDocument(document, true)
            .orElseThrow(() -> new IllegalStateException(String.format("Can't create '%s' document",
                                                                       document.documentId().stringValue())));
   }

   protected DocumentEntry createDocument(Document document) {
      return createDocument(documentRepository(), document);
   }

   protected DocumentEntry createDocument(DocumentRepository documentRepository, Path documentPath) {
      return createDocument(documentRepository, stubDocument(DocumentPath.of(documentPath)).build());
   }

   protected DocumentEntry createDocument(Path documentPath) {
      return createDocument(stubDocument(DocumentPath.of(documentPath)).build());
   }

   protected DocumentEntry createDocument(DocumentRepository documentRepository, String documentPath) {
      return createDocument(documentRepository, path(documentPath));
   }

   protected DocumentEntry createDocument(String documentPath) {
      return createDocument(path(documentPath));
   }

   protected DocumentEntry createDocument(DocumentRepository documentRepository, DocumentPath documentPath) {
      return createDocument(documentRepository, documentPath.value());
   }

   protected DocumentEntry createDocument(DocumentPath documentPath) {
      return createDocument(documentPath.value());
   }

   protected List<DocumentEntry> createDocuments(DocumentRepository documentRepository, Path... paths) {
      return createDocuments(documentRepository, list(paths));
   }

   protected List<DocumentEntry> createDocuments(Path... paths) {
      return createDocuments(list(paths));
   }

   protected List<DocumentEntry> createDocuments(DocumentRepository documentRepository,
                                                 DocumentPath... paths) {
      return createDocuments(documentRepository, list(stream(paths).map(DocumentPath::value)));
   }

   protected List<DocumentEntry> createDocuments(DocumentPath... paths) {
      return createDocuments(list(stream(paths).map(DocumentPath::value)));
   }

   protected List<DocumentEntry> createDocuments(DocumentRepository documentRepository, String... paths) {
      return createDocuments(documentRepository, list(stream(paths).map(Paths::get)));
   }

   protected List<DocumentEntry> createDocuments(String... paths) {
      return createDocuments(list(stream(paths).map(Paths::get)));
   }

   protected List<DocumentEntry> createDocuments(DocumentRepository documentRepository, List<Path> paths) {
      return list(stream(paths).map(documentPath -> createDocument(documentRepository, documentPath)));
   }

   protected List<DocumentEntry> createDocuments(List<Path> paths) {
      return list(stream(paths).map(this::createDocument));
   }

   protected void deleteDocument(DocumentRepository documentRepository, DocumentPath documentId) {
      documentRepository.deleteDocumentById(documentId);
   }

   protected void deleteDocument(DocumentPath documentId) {
      deleteDocument(documentRepository(), documentId);
   }

   protected void deleteDocuments(DocumentRepository documentRepository, DocumentPath... documentIds) {
      stream(documentIds).forEach(documentId -> deleteDocument(documentRepository, documentId));
   }

   protected void deleteDocuments(DocumentPath... documentIds) {
      stream(documentIds).forEach(this::deleteDocument);
   }

   protected void deleteDocuments(DocumentRepository documentRepository, List<Path> paths) {
      stream(paths)
            .map(DocumentPath::of)
            .forEach(documentId -> deleteDocument(documentRepository, documentId));
   }

   protected void deleteDocuments(List<Path> paths) {
      stream(paths).map(DocumentPath::of).forEach(this::deleteDocument);
   }

   protected void deleteDocuments(DocumentRepository documentRepository, String... paths) {
      stream(paths)
            .map(DocumentPath::of)
            .forEach(documentId -> deleteDocument(documentRepository, documentId));
   }

   protected void deleteDocuments(String... paths) {
      stream(paths).map(DocumentPath::of).forEach(this::deleteDocument);
   }

   protected void deleteDocuments(DocumentRepository documentRepository, Path... paths) {
      stream(paths)
            .map(DocumentPath::of)
            .forEach(documentId -> deleteDocument(documentRepository, documentId));
   }

   protected void deleteDocuments(Path... paths) {
      stream(paths).map(DocumentPath::of).forEach(this::deleteDocument);
   }

   protected <T extends Value> Comparator<? super T> valueComparator() {
      return (d1, d2) -> d1.sameValueAs(d2) ? 0 : -1;
   }

   protected <T extends Entity<ID>, ID extends Id> Comparator<? super T> entityComparator() {
      return (d1, d2) -> d1.sameIdentityAs(d2) ? 0 : -1;
   }

   protected <T extends Entity<ID>, ID extends Id> Comparator<? super T> entityComparatorByValue() {
      return (d1, d2) -> d1.sameValueAs(d2) ? 0 : -1;
   }

   /**
    * Specialized document comparator that ignores if content is a stream or loaded content.
    *
    * @param <T> document type
    *
    * @return document comparator
    */
   protected <T extends Document> Comparator<? super T> contentAgnosticDocumentComparator() {
      return (d1, d2) -> d1.documentId().sameValueAs(d2.documentId()) && d1
            .metadata()
            .sameValueAs(d2.metadata()) && d1
                               .content()
                               .contentEncoding()
                               .equals(d2.content().contentEncoding()) && d1
                               .content()
                               .contentSize()
                               .equals(d2.content().contentSize()) && Arrays.equals(d1.content().content(),
                                                                                    d2.content().content())
                         ? 0
                         : -1;
   }

   public static Consumer<Document> writeContent(Reader reader) {
      return d -> {
         try (BufferedWriter writer = new BufferedWriter(d.content().writerContent(UTF_8),
                                                         MechanicalSympathy.generalBufferSize())) {
            IOUtils.copy(reader, writer);
         } catch (IOException e) {
            throw new IllegalStateException(e);
         }
      };
   }

   public static Consumer<Document> writeContent(InputStream inputStream) {
      return d -> {
         try (BufferedOutputStream outputStream = new BufferedOutputStream(d.content().outputStreamContent(),
                                                                           MechanicalSympathy.generalBufferSize())) {
            IOUtils.copy(inputStream, outputStream);
         } catch (IOException e) {
            throw new IllegalStateException(e);
         }
      };
   }

   public static Consumer<Document> writeContent(String content) {
      return writeContent(new StringReader(content));
   }

   public static Consumer<Document> writeZeroContent(int length) {
      return writeContent(reader(contentGenerator.zeroCharacterStream().limit(length)));
   }

   public static <T, U> U applyClosingStream(Stream<T> stream, Function<Stream<T>, U> function) {
      return finalApplyCloseableUnchecked(stream, checkedFunction(function));
   }

   public static <T> void acceptClosingStream(Stream<T> stream, Consumer<Stream<T>> consumer) {
      finalAcceptCloseableUnchecked(stream, checkedConsumer(consumer));
   }

}