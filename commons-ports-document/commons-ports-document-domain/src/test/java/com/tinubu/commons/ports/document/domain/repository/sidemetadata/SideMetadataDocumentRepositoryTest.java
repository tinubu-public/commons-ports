/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.repository.sidemetadata;

import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.mimeType;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_HTML;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_PLAIN;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.allFeatureCapabilities;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.spy;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.Writer;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.capability.RepositoryCapability;
import com.tinubu.commons.ports.document.domain.repository.TestDocumentRepository;
import com.tinubu.commons.ports.document.domain.testsuite.BaseDocumentRepositoryTest;

// FIXME test sd documents auto filtering in sdrepo
public class SideMetadataDocumentRepositoryTest {

   private abstract class BaseSideMetadataDocumentRepositoryTest extends BaseDocumentRepositoryTest {
      protected SideMetadataDocumentRepository documentRepository;
      protected DocumentRepository delegateRepository;

      @BeforeEach
      public void configureDocumentRepository() {
         delegateRepository = spy(new TestDocumentRepository() {
            @Override
            public HashSet<RepositoryCapability> capabilities() {
               return BaseSideMetadataDocumentRepositoryTest.this.delegateCapabilities();
            }
         });
         this.documentRepository = SideMetadataDocumentRepository.of(delegateRepository, sideMetadata());
      }

      protected HashSet<RepositoryCapability> delegateCapabilities() {
         return allFeatureCapabilities();
      }

      protected abstract SideMetadataStorage sideMetadata();

      @Override
      protected SideMetadataDocumentRepository documentRepository() {
         return documentRepository;
      }

      protected DocumentRepository delegateRepository() {
         return delegateRepository;
      }

      @Override
      protected Document zipTransformer(DocumentPath zipPath, List<Document> documents) {
         throw new UnsupportedOperationException();
      }

   }

   private abstract class CommonSideMetadataDocumentRepositoryTest
         extends BaseSideMetadataDocumentRepositoryTest {

      @Test
      public void testRepositoryWhenAllMetadataSupported() {
         delegateRepository = spy(new TestDocumentRepository());
         this.documentRepository = SideMetadataDocumentRepository.of(delegateRepository, sideMetadata());

         DocumentPath testDocument = DocumentPath.of("path/test.txt");
         Document document = stubDocument(testDocument)
               .build()
               .documentPath(Paths.get("real/test.txt"))
               .contentType(mimeType(TEXT_HTML, UTF_8))
               .addAttribute("key", "value");

         assertThat(documentRepository().saveDocument(document, true)).isPresent();

         assertThat(documentRepository().findDocumentById(testDocument)).hasValueSatisfying(d -> {
            assertThat(d.documentId()).isEqualTo(testDocument);
            assertThat(d.metadata().documentPath()).isEqualTo(Paths.get("real/test.txt"));
            assertThat(d.metadata().creationDate()).hasValue(FIXED_DATE.toInstant());
            assertThat(d.metadata().lastUpdateDate()).hasValue(FIXED_DATE.toInstant());
            assertThat(d.metadata().contentType()).hasValue(mimeType(TEXT_HTML, UTF_8));
            assertThat(d.metadata().contentSize()).hasValue(13L);
            assertThat(d.metadata().attributes()).containsEntry("key", "value");
         });
      }

      @Test
      public void testRepositoryWhenAllMetadataUnsupported() {
         DocumentPath testDocument = DocumentPath.of("path/test.txt");
         Document document = stubDocument(testDocument)
               .build()
               .documentPath(Paths.get("real/test.txt"))
               .contentType(mimeType(TEXT_HTML, UTF_8))
               .addAttribute("key", "value");

         assertThat(documentRepository().saveDocument(document, true)).isPresent();

         assertThat(documentRepository().findDocumentById(testDocument)).hasValueSatisfying(d -> {
            assertThat(d.documentId()).isEqualTo(testDocument);
            assertThat(d.metadata().documentPath()).isEqualTo(Paths.get("real/test.txt"));
            assertThat(d.metadata().creationDate()).hasValue(FIXED_DATE.toInstant());
            assertThat(d.metadata().lastUpdateDate()).hasValue(FIXED_DATE.toInstant());
            assertThat(d.metadata().contentType()).hasValue(mimeType(TEXT_HTML, UTF_8));
            assertThat(d.metadata().contentSize()).hasValue(13L);
            assertThat(d.metadata().attributes()).containsEntry("key", "value");
         });
      }

      @Test
      public void testRepositoryWhenMissingSideMetadataAndDefaultFallback() {
         DocumentPath testDocument = DocumentPath.of("path/test.txt");
         Document document = stubDocument(testDocument)
               .build()
               .documentPath(Paths.get("real/test.txt"))
               .contentType(mimeType(TEXT_HTML, UTF_8))
               .addAttribute("key", "value");

         assertThat(documentRepository().saveDocument(document, true)).isPresent();

         documentRepository.sideMetadata().deleteMetadata(testDocument);

         assertThat(documentRepository().findDocumentById(testDocument)).hasValueSatisfying(d -> {
            assertThat(d.documentId()).isEqualTo(testDocument);
            assertThat(d.metadata().documentPath()).isEqualTo(Paths.get("path/test.txt"));
            assertThat(d.metadata().creationDate()).hasValue(Instant.EPOCH);
            assertThat(d.metadata().lastUpdateDate()).hasValue(Instant.EPOCH);
            assertThat(d.metadata().contentType()).hasValue(TEXT_PLAIN);
            assertThat(d.metadata().contentSize()).isEmpty();
            assertThat(d.metadata().attributes()).isEmpty();
         });
      }

      @Test
      public void testRepositoryWhenMissingSideMetadataAndIdentityFallback() {
         SideMetadataFallback fallback = DocumentEntry::metadata;
         this.documentRepository =
               SideMetadataDocumentRepository.of(delegateRepository, sideMetadata(), fallback);

         DocumentPath testDocument = DocumentPath.of("path/test.txt");
         Document document = stubDocument(testDocument)
               .build()
               .documentPath(Paths.get("real/test.txt"))
               .contentType(mimeType(TEXT_HTML, UTF_8))
               .addAttribute("key", "value");

         assertThat(documentRepository().saveDocument(document, true)).isPresent();

         documentRepository.sideMetadata().deleteMetadata(testDocument);

         assertThat(documentRepository().findDocumentById(testDocument)).hasValueSatisfying(d -> {
            assertThat(d.documentId()).isEqualTo(testDocument);
            assertThat(d.metadata().documentPath()).isEqualTo(Paths.get("path/test.txt"));
            assertThat(d.metadata().creationDate()).isEmpty();
            assertThat(d.metadata().lastUpdateDate()).isEmpty();
            assertThat(d.metadata().contentType()).hasValue(TEXT_PLAIN);
            assertThat(d.metadata().contentSize()).isEmpty();
            assertThat(d.metadata().attributes()).isEmpty();
         });
      }

   }

   @Nested
   public class WhenSameRepositoryStorage extends CommonSideMetadataDocumentRepositoryTest {

      @Override
      protected SameRepositorySideMetadataStorage sideMetadata() {
         return SameRepositorySideMetadataStorage.of(delegateRepository());
      }

      @Test
      public void testRepositoryWhenCorruptedSideMetadataWithEmpty() throws IOException {
         DocumentPath testDocument = DocumentPath.of("path/test.txt");
         Document document = stubDocument(testDocument)
               .build()
               .documentPath(Paths.get("real/test.txt"))
               .contentType(mimeType(TEXT_HTML, UTF_8))
               .addAttribute("key", "value");

         assertThat(documentRepository().saveDocument(document, true)).isPresent();

         generateSideMetadata(new StringReader(""));

         assertThat(documentRepository().findDocumentById(testDocument)).hasValueSatisfying(d -> {
            assertThat(d.documentId()).isEqualTo(testDocument);
            assertThat(d.metadata().documentPath()).isEqualTo(Paths.get("path/test.txt"));
            assertThat(d.metadata().creationDate()).isEmpty();
            assertThat(d.metadata().lastUpdateDate()).isEmpty();
            assertThat(d.metadata().contentType()).hasValue(TEXT_PLAIN);
            assertThat(d.metadata().contentSize()).isEmpty();
            assertThat(d.metadata().attributes()).isEmpty();
         });
      }

      @Test
      public void testRepositoryWhenCorruptedSideMetadataWithUnknownKey() throws IOException {
         DocumentPath testDocument = DocumentPath.of("path/test.txt");
         Document document = stubDocument(testDocument)
               .build()
               .documentPath(Paths.get("real/test.txt"))
               .contentType(mimeType(TEXT_HTML, UTF_8))
               .addAttribute("key", "value");

         assertThat(documentRepository().saveDocument(document, true)).isPresent();

         generateSideMetadata(new StringReader("unknown-key=unknown-value"));

         assertThatExceptionOfType(DocumentAccessException.class)
               .isThrownBy(() -> documentRepository().findDocumentById(testDocument))
               .withMessage(
                     "Corrupted 'path/.test.txt.metadata' side metadata line : Unknown 'unknown-key' key");
      }

      @Test
      public void testRepositoryWhenCorruptedSideMetadataWithBadFormat() throws IOException {
         DocumentPath testDocument = DocumentPath.of("path/test.txt");
         Document document = stubDocument(testDocument)
               .build()
               .documentPath(Paths.get("real/test.txt"))
               .contentType(mimeType(TEXT_HTML, UTF_8))
               .addAttribute("key", "value");

         assertThat(documentRepository().saveDocument(document, true)).isPresent();

         generateSideMetadata(new StringReader("unknown"));

         assertThatExceptionOfType(DocumentAccessException.class)
               .isThrownBy(() -> documentRepository().findDocumentById(testDocument))
               .withMessage("Corrupted 'path/.test.txt.metadata' side metadata line : 'unknown'");
      }

      @Test
      public void testRepositoryWhenCorruptedSideMetadataWithMismatchingDocumentId() throws IOException {
         DocumentPath testDocument = DocumentPath.of("path/test.txt");
         Document document = stubDocument(testDocument)
               .build()
               .documentPath(Paths.get("real/test.txt"))
               .contentType(mimeType(TEXT_HTML, UTF_8))
               .addAttribute("key", "value");

         assertThat(documentRepository().saveDocument(document, true)).isPresent();

         generateSideMetadata(new StringReader("document-id=unknown.txt"));

         assertThatExceptionOfType(DocumentAccessException.class)
               .isThrownBy(() -> documentRepository().findDocumentById(testDocument))
               .withMessage(
                     "Corrupted 'path/.test.txt.metadata' side metadata : metadata 'unknown.txt' document-id does not match current 'path/test.txt' document id");
      }

      private void generateSideMetadata(Reader content) throws IOException {
         Document sideMetadata =
               delegateRepository.openDocument(DocumentPath.of("path/.test.txt.metadata"), true).get();
         try (Writer sideMetadataWriter = sideMetadata.content().writerContent(UTF_8)) {
            IOUtils.copy(content, sideMetadataWriter);
         }
      }

   }

   @Nested
   public class WhenMemoryRepositoryStorage extends CommonSideMetadataDocumentRepositoryTest {

      @Override
      protected SideMetadataStorage sideMetadata() {
         return MemorySideMetadataStorage.of(delegateRepository());
      }

   }

}
