/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.testsuite;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;

/**
 * Common testsuite for read-only {@link DocumentRepository} implementations.
 */
public abstract class ReadOnlyDocumentRepositoryTest extends CommonDocumentRepositoryTest {

   @Test
   @Disabled("Unsupported operation for read-only repository")
   @Override
   public void testSaveDocumentWhenNominal() { }

   @Test
   @Disabled("Unsupported operation for read-only repository")
   @Override
   public void testSaveDocumentWhenOverwrite() { }

   @Test
   @Disabled("Unsupported operation for read-only repository")
   @Override
   public void testSaveAndReturnDocumentWhenNominal() { }

   @Test
   @Disabled("Unsupported operation for read-only repository")
   @Override
   public void testSaveAndReturnDocumentWhenOverwrite() { }

   @Test
   @Disabled("Unsupported operation for read-only repository")
   @Override
   public void testDeleteDocumentByIdWhenNominal() { }

   @Test
   @Disabled("Unsupported operation for read-only repository")
   @Override
   public void testDeleteDocumentByIdWhenNotExist() { }

   @Test
   @Disabled("Unsupported operation for read-only repository")
   @Override
   public void testDeleteDocumentsBySpecificationWhenNominal() {
      super.testDeleteDocumentsBySpecificationWhenNominal();
   }

   @Test
   @Disabled("Unsupported operation for read-only repository")
   @Override
   public void testDeleteDocumentsBySpecificationWhenDocumentEntrySpecificationWithNoDirectory() {
      super.testDeleteDocumentsBySpecificationWhenDocumentEntrySpecificationWithNoDirectory();
   }

   @Test
   @Disabled("Unsupported operation for read-only repository")
   @Override
   public void testDeleteDocumentsBySpecificationWhenDocumentEntrySpecificationWithDirectoriesAndExtensions() {
      super.testDeleteDocumentsBySpecificationWhenDocumentEntrySpecificationWithDirectoriesAndExtensions();
   }

   @Test
   @Disabled("Unsupported operation for read-only repository")
   @Override
   public void testDeleteDocumentsBySpecificationWhenBaseDirectory() {
      super.testDeleteDocumentsBySpecificationWhenBaseDirectory();
   }

   @Test
   @Disabled("Unsupported operation for read-only repository")
   @Override
   public void testDeleteDocumentsBySpecificationWhenAbsoluteBaseDirectory() {
      super.testDeleteDocumentsBySpecificationWhenAbsoluteBaseDirectory();
   }

   @Test
   @Disabled("Unsupported operation for read-only repository")
   @Override
   public void testDeleteDocumentsBySpecificationWhenTraversalBaseDirectory() {
      super.testDeleteDocumentsBySpecificationWhenTraversalBaseDirectory();
   }

   @Override
   @Disabled("Not supported")
   public void testOpenDocumentWhenNominal() { }

   @Override
   @Disabled("Not supported")
   public void testOpenDocumentWhenWriteNothing() { }

   @Override
   @Disabled("Not supported")
   public void testOpenDocumentWhenOverwriteAndAppend() { }

   @Override
   @Disabled("Not supported")
   public void testOpenDocumentWhenOverwriteAndNotAppend() { }

   @Override
   @Disabled("Not supported")
   public void testOpenDocumentWhenNotOverwriteAndAppend() { }

   @Override
   @Disabled("Not supported")
   public void testOpenDocumentWhenNotOverwriteAndNotAppend() { }

   @Override
   @Disabled("Not supported")
   public void testSaveAndRestoreBigCharacterDocument() { }

   @Override
   @Disabled("Not supported")
   public void testSaveAndRestoreBigByteDocument() { }

   @Override
   @Disabled("Not supported")
   public void testOpenAndRestoreBigCharacterDocument() { }

   @Override
   @Disabled("Not supported")
   public void testOpenAndRestoreBigByteDocument() { }

   @Override
   protected DocumentEntry createDocument(DocumentRepository documentRepository, Document document) {
      System.out.println(String.format("Use '%s' test document", document.documentId().stringValue()));
      return documentRepository()
            .findDocumentEntryById(document.documentId())
            .orElseThrow(() -> new IllegalStateException(String.format("Can't find test '%s' document",
                                                                       document.documentId().stringValue())));
   }

   @Override
   protected void deleteDocument(DocumentPath documentId) {
   }

}