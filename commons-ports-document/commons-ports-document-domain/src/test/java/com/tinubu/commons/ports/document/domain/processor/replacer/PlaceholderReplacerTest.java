/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain.processor.replacer;

import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.parseMimeType;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.lang.datetime.ApplicationClock;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentPath;

public class PlaceholderReplacerTest {

   private static final ZonedDateTime FIXED_DATE =
         ZonedDateTime.of(LocalDateTime.of(2021, 11, 10, 14, 0, 0), ZoneId.of("UTC"));

   @BeforeEach
   public void initializeApplicationClock() {
      ApplicationClock.setFixedClock(FIXED_DATE);
   }

   @Test
   public void testWhenLoadedContent() {
      Document document = new DocumentBuilder()
            .documentId(DocumentPath.of("document.txt"))
            .loadedContent("This is a template with ${placeholder}", UTF_8)
            .build()
            .process(PlaceholderReplacer.of(s -> ">" + s.toUpperCase() + "<"));

      assertThat(document.content().stringContent()).isEqualTo("This is a template with >PLACEHOLDER<");

      assertThat(document.documentId()).isEqualTo(DocumentPath.of("document.txt"));
      assertThat(document.metadata().documentPath()).isEqualTo(Paths.get("document.txt"));
      assertThat(document.metadata().documentName()).isEqualTo("document.txt");
      assertThat(document.metadata().contentType()).hasValue(parseMimeType("text/plain;charset=UTF-8"));
      assertThat(document.metadata().contentEncoding()).hasValue(UTF_8);
      assertThat(document.metadata().contentSize()).isEmpty();
      assertThat(document.metadata().creationDate()).hasValue(FIXED_DATE.toInstant());
      assertThat(document.metadata().lastUpdateDate()).hasValue(FIXED_DATE.toInstant());
      assertThat(document.content().contentEncoding()).hasValue(UTF_8);
      assertThat(document.content().contentSize()).isEmpty();
   }

   @Test
   public void testWhenInputStreamContent() {
      Document document = new DocumentBuilder()
            .documentId(DocumentPath.of("document.txt"))
            .streamContent("This is a template with ${placeholder}", UTF_8)
            .build()
            .process(PlaceholderReplacer.of(s -> ">" + s.toUpperCase() + "<"));

      assertThat(document.content().stringContent()).isEqualTo("This is a template with >PLACEHOLDER<");

      assertThat(document.documentId()).isEqualTo(DocumentPath.of("document.txt"));
      assertThat(document.metadata().documentPath()).isEqualTo(Paths.get("document.txt"));
      assertThat(document.metadata().documentName()).isEqualTo("document.txt");
      assertThat(document.metadata().contentType()).hasValue(parseMimeType("text/plain;charset=UTF-8"));
      assertThat(document.metadata().contentEncoding()).hasValue(UTF_8);
      assertThat(document.metadata().contentSize()).isEmpty();
      assertThat(document.metadata().creationDate()).hasValue(FIXED_DATE.toInstant());
      assertThat(document.metadata().lastUpdateDate()).hasValue(FIXED_DATE.toInstant());
      assertThat(document.content().contentEncoding()).hasValue(UTF_8);
      assertThat(document.content().contentSize()).isEmpty();
   }

   @Test
   public void testWhenOutputStreamContent() {
      assertThatExceptionOfType(DocumentAccessException.class)
            .isThrownBy(() -> new DocumentBuilder()
                  .open(DocumentPath.of("document.txt"), UTF_8)
                  .build()
                  .process(PlaceholderReplacer.of(s -> ">" + s.toUpperCase() + "<")))
            .withMessage(
                  "Unsupported 'com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent' content type");
   }

}