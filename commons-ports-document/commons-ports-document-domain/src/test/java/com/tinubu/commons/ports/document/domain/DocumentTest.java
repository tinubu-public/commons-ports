/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.parseMimeType;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent.OutputStreamDocumentContentBuilder;
import static java.nio.charset.StandardCharsets.US_ASCII;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.assertj.core.api.Assertions.assertThatIllegalStateException;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;
import static org.assertj.core.api.Assertions.entry;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashMap;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.lang.datetime.ApplicationClock;
import com.tinubu.commons.lang.util.Pair;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.LoadedDocumentContent.LoadedDocumentContentBuilder;

public class DocumentTest {

   private static final ZonedDateTime FIXED_DATE =
         ZonedDateTime.of(LocalDateTime.of(2021, 11, 10, 14, 0, 0), ZoneId.of("UTC"));

   @BeforeEach
   public void initializeApplicationClock() {
      ApplicationClock.setFixedClock(FIXED_DATE);
   }

   @Test
   public void testDocumentWhenNominal() {
      Document document =
            new DocumentBuilder().documentId(DocumentPath.of("path/document.pdf")).loadedContent("", UTF_8)
            .build();

      assertThat(document.documentId()).isEqualTo(DocumentPath.of("path/document.pdf"));
      assertThat(document.metadata().documentPath()).isEqualTo(Paths.get("path/document.pdf"));
      assertThat(document.metadata().documentName()).isEqualTo("document.pdf");
      assertThat(document.metadata().contentType()).hasValue(parseMimeType("application/pdf;charset=UTF-8"));
      assertThat(document.metadata().contentEncoding()).hasValue(UTF_8);
      assertThat(document.metadata().contentSize()).hasValue(0L);
      assertThat(document.metadata().creationDate()).hasValue(FIXED_DATE.toInstant());
      assertThat(document.metadata().lastUpdateDate()).hasValue(FIXED_DATE.toInstant());
      assertThat(document.content().contentEncoding()).hasValue(UTF_8);
      assertThat(document.content().contentSize()).hasValue(0L);
   }

   @Test
   public void testDocumentWhenBadParameters() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new DocumentBuilder().build())
            .withMessage(
                  "Invariant validation error > Context [DocumentMetadata[documentPath=<null>,contentType=<null>,contentSize=<null>,creationDate=%1$s,lastUpdateDate=%1$s,attributes={}]] > "
                  + "{documentPath} 'documentPath' must not be null",
                  "2021-11-10T14:00:00Z");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new DocumentBuilder().documentId(DocumentPath.of("/")).build())
            .withMessage("Invariant validation error > Context [DocumentPath[value=/,newObject=false]] > "
                         + "{value} 'value=/' must have a filename component");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new DocumentBuilder().documentId(DocumentPath.of(" ")).build())
            .withMessage("Invariant validation error > Context [DocumentPath[value= ,newObject=false]] > "
                         + "{value} 'value.fileName' must not be blank");
   }

   @Test
   public void testDocumentMetadataWhenPath() {
      Document document = new DocumentBuilder()
            .documentId(DocumentPath.of("path/document.pdf"))
            .documentPath("document2.pdf")
            .loadedContent("", UTF_8)
            .build();

      assertThat(document.documentId()).isEqualTo(DocumentPath.of("path/document.pdf"));
      assertThat(document.metadata().documentPath()).isEqualTo(Paths.get("document2.pdf"));
      assertThat(document.metadata().documentName()).isEqualTo("document2.pdf");
      assertThat(document.metadata().contentType()).hasValue(parseMimeType("application/pdf;charset=UTF-8"));
   }

   @Test
   public void testDocumentWhenLoadedContentWithNoEncoding() {
      Document document = new DocumentBuilder()
            .documentId(DocumentPath.of("path/document.pdf"))
            .loadedContent(new ByteArrayInputStream("new content".getBytes(UTF_8)))
            .build();

      assertThat(document.metadata().contentType()).hasValue(parseMimeType("application/pdf"));
      assertThat(document.content().stringContent(UTF_8)).isEqualTo("new content");
      assertThat(document.content().contentEncoding()).isEmpty();

      assertThatIllegalStateException()
            .isThrownBy(() -> document.content().stringContent())
            .withMessage("No content encoding set, you should specify one to encode this content");
      assertThatIllegalStateException()
            .isThrownBy(() -> document.content().readerContent())
            .withMessage("No content encoding set, you should specify one to encode this content");
   }

   @Test
   public void testDocumentWhenStreamContentWithNoEncoding() {
      Document document = new DocumentBuilder()
            .documentId(DocumentPath.of("path/document.pdf"))
            .streamContent(new ByteArrayInputStream("new content".getBytes(UTF_8)))
            .build();

      assertThat(document.metadata().contentType()).hasValue(parseMimeType("application/pdf"));
      assertThat(document.content().stringContent(UTF_8)).isEqualTo("new content");
      assertThat(document.content().contentEncoding()).isEmpty();

      assertThatIllegalStateException()
            .isThrownBy(() -> document.content().stringContent())
            .withMessage("No content encoding set, you should specify one to encode this content");
      assertThatIllegalStateException()
            .isThrownBy(() -> document.content().readerContent())
            .withMessage("No content encoding set, you should specify one to encode this content");
   }

   @Test
   public void testDocumentName() {
      Document document =
            new DocumentBuilder().documentId(DocumentPath.of("path/document.pdf")).loadedContent("", UTF_8)
            .build();

      document = document.documentName(Paths.get("document.txt"));

      assertThat(document.documentId()).isEqualTo(DocumentPath.of("path/document.pdf"));
      assertThat(document.metadata().documentPath()).isEqualTo(Paths.get("document.txt"));
      assertThat(document.metadata().documentName()).isEqualTo("document.txt");
      assertThat(document.metadata().contentType()).hasValue(parseMimeType("application/pdf;charset=UTF-8"));
   }

   @Test
   public void testContentType() {
      Document document =
            new DocumentBuilder().documentId(DocumentPath.of("path/document.pdf")).loadedContent("", UTF_8)
            .build();

      document = document.contentType(parseMimeType("application/text"));

      assertThat(document.documentId()).isEqualTo(DocumentPath.of("path/document.pdf"));
      assertThat(document.metadata().documentPath()).isEqualTo(Paths.get("path/document.pdf"));
      assertThat(document.metadata().documentName()).isEqualTo("document.pdf");
      assertThat(document.metadata().contentType()).hasValue(parseMimeType("application/text;charset=UTF-8"));

      document = document.contentType(parseMimeType("application/text;charset=UTF-8"));

      assertThat(document.documentId()).isEqualTo(DocumentPath.of("path/document.pdf"));
      assertThat(document.metadata().documentPath()).isEqualTo(Paths.get("path/document.pdf"));
      assertThat(document.metadata().documentName()).isEqualTo("document.pdf");
      assertThat(document.metadata().contentType()).hasValue(parseMimeType("application/text;charset=UTF-8"));
   }

   @Test
   public void testContentTypeWhenUnmatchingContentType() {
      Document document =
            new DocumentBuilder().documentId(DocumentPath.of("path/document.pdf")).loadedContent("", UTF_8)
            .build();

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> document.contentType(parseMimeType("application/text;charset=US-ASCII")))
            .withMessage(
                  "Invariant validation error > Context [DocumentPath[value=path/document.pdf,newObject=false]] > "
                  + "{metadata} 'metadata.contentEncoding=US-ASCII' must be equal to 'content.contentEncoding=UTF-8'");
   }

   @Test
   public void testAttributes() {
      Document document =
            new DocumentBuilder().documentId(DocumentPath.of("path/document.pdf")).loadedContent("", UTF_8)
            .build();

      document = document.attributes(new HashMap<>() {{
         put("k1", "43");
         put("k2", "value2");
      }});

      assertThat(document.documentId()).isEqualTo(DocumentPath.of("path/document.pdf"));
      assertThat(document.metadata().attributes()).containsOnly(Pair.of("k1", "43"), Pair.of("k2", "value2"));
   }

   @Test
   public void testAttributesWhenNull() {
      Document document = new DocumentBuilder()
            .documentId(DocumentPath.of("path/document.pdf"))
            .loadedContent("", UTF_8)
            .build();

      assertThatNullPointerException()
            .isThrownBy(() -> document.attributes(null))
            .withMessage("'attributes' must not be null");
   }

   @Test
   public void testAttributesWhenBlankKey() {
      Document document = new DocumentBuilder()
            .documentId(DocumentPath.of("path/document.pdf"))
            .loadedContent("", UTF_8)
            .build();

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> document.attributes(map(entry(null, "value"))))
            .withMessage(
                  "Invariant validation error > Context [DocumentMetadata[documentPath=path/document.pdf,contentType=application/pdf;charset=UTF-8,contentSize=0,creationDate=2021-11-10T14:00:00Z,lastUpdateDate=2021-11-10T14:00:00Z,attributes={null=value}]] > {attributes} 'attributes.entrySet=[null=value]' > 'attributes.entrySet[0]' must not have blank key");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> document.attributes(map(entry(" ", "value"))))
            .withMessage(
                  "Invariant validation error > Context [DocumentMetadata[documentPath=path/document.pdf,contentType=application/pdf;charset=UTF-8,contentSize=0,creationDate=2021-11-10T14:00:00Z,lastUpdateDate=2021-11-10T14:00:00Z,attributes={ =value}]] > {attributes} 'attributes.entrySet=[ =value]' > 'attributes.entrySet[0]' must not have blank key");
   }

   @Test
   public void testAttributesWhenNullValue() {
      Document document = new DocumentBuilder()
            .documentId(DocumentPath.of("path/document.pdf"))
            .loadedContent("", UTF_8)
            .build();

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> document.attributes(map(entry("key", null))))
            .withMessage(
                  "Invariant validation error > Context [DocumentMetadata[documentPath=path/document.pdf,contentType=application/pdf;charset=UTF-8,contentSize=0,creationDate=2021-11-10T14:00:00Z,lastUpdateDate=2021-11-10T14:00:00Z,attributes={key=null}]] > {attributes} 'attributes.entrySet=[key=null]' > 'attributes.entrySet[0]' must not have null value");
   }

   @Test
   public void testAddAttributes() {
      Document document = new DocumentBuilder()
            .documentId(DocumentPath.of("path/document.pdf"))
            .loadedContent("", UTF_8)
            .build().attributes(new HashMap<>() {{
               put("k1", "43");
               put("k2", "value2");
            }});

      document = document.addAttributes(new HashMap<>() {{ put("k3", "true"); }});

      assertThat(document.documentId()).isEqualTo(DocumentPath.of("path/document.pdf"));
      assertThat(document.metadata().attributes()).containsOnly(Pair.of("k1", "43"),
                                                                Pair.of("k2", "value2"),
                                                                Pair.of("k3", "true"));
   }

   @Test
   public void testAddAttributesWhenNull() {
      Document document = new DocumentBuilder()
            .documentId(DocumentPath.of("path/document.pdf"))
            .loadedContent("", UTF_8)
            .build()
            .attributes(new HashMap<>() {{
               put("k1", "43");
               put("k2", "value2");
            }});

      assertThatNullPointerException()
            .isThrownBy(() -> document.addAttributes(null))
            .withMessage("'attributes' must not be null");
   }

   @Test
   public void testAddAttributesWhenBlankKey() {
      Document document = new DocumentBuilder()
            .documentId(DocumentPath.of("path/document.pdf"))
            .loadedContent("", UTF_8)
            .build()
            .attributes(new HashMap<>() {{
               put("k1", "43");
               put("k2", "value2");
            }});

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> document.addAttributes(map(entry(null, "value"))))
            .withMessage(
                  "Invariant validation error > Context [DocumentMetadata[documentPath=path/document.pdf,contentType=application/pdf;charset=UTF-8,contentSize=0,creationDate=2021-11-10T14:00:00Z,lastUpdateDate=2021-11-10T14:00:00Z,attributes={null=value, k1=43, k2=value2}]] > {attributes} 'attributes.entrySet=[null=value,k1=43,k2=value2]' > 'attributes.entrySet[0]' must not have blank key");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> document.addAttributes(map(entry(" ", "value"))))
            .withMessage(
                  "Invariant validation error > Context [DocumentMetadata[documentPath=path/document.pdf,contentType=application/pdf;charset=UTF-8,contentSize=0,creationDate=2021-11-10T14:00:00Z,lastUpdateDate=2021-11-10T14:00:00Z,attributes={ =value, k1=43, k2=value2}]] > {attributes} 'attributes.entrySet=[ =value,k1=43,k2=value2]' > 'attributes.entrySet[0]' must not have blank key");
   }

   @Test
   public void testAddAttributesWhenNullValue() {
      Document document = new DocumentBuilder()
            .documentId(DocumentPath.of("path/document.pdf"))
            .loadedContent("", UTF_8)
            .build()
            .attributes(new HashMap<>() {{
               put("k1", "43");
               put("k2", "value2");
            }});

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> document.addAttributes(map(entry("key", null))))
            .withMessage(
                  "Invariant validation error > Context [DocumentMetadata[documentPath=path/document.pdf,contentType=application/pdf;charset=UTF-8,contentSize=0,creationDate=2021-11-10T14:00:00Z,lastUpdateDate=2021-11-10T14:00:00Z,attributes={k1=43, key=null, k2=value2}]] > {attributes} 'attributes.entrySet=[k1=43,key=null,k2=value2]' > 'attributes.entrySet[1]' must not have null value");
   }

   @Test
   public void testDocumentAddAttribute() {
      Document document = new DocumentBuilder()
            .documentId(DocumentPath.of("path/document.pdf"))
            .loadedContent("", UTF_8)
            .build().attributes(new HashMap<>() {{
               put("k1", "43");
               put("k2", "value2");
            }});

      document = document.addAttribute("k3", "true");

      assertThat(document.documentId()).isEqualTo(DocumentPath.of("path/document.pdf"));
      assertThat(document.metadata().attributes()).containsOnly(Pair.of("k1", "43"),
                                                                Pair.of("k2", "value2"),
                                                                Pair.of("k3", "true"));

   }

   @Test
   public void testDocumentAddAttributeWhenBlankKey() {
      Document document = new DocumentBuilder()
            .documentId(DocumentPath.of("path/document.pdf"))
            .loadedContent("", UTF_8)
            .build()
            .attributes(new HashMap<>() {{
               put("k1", "43");
               put("k2", "value2");
            }});

      assertThatNullPointerException()
            .isThrownBy(() -> document.addAttribute(null, "true"))
            .withMessage("'key' must not be null");
      assertThatIllegalArgumentException()
            .isThrownBy(() -> document.addAttribute(" ", "true"))
            .withMessage("'key' must not be blank");
   }

   @Test
   public void testDocumentAddAttributeWhenNullValue() {
      Document document = new DocumentBuilder()
            .documentId(DocumentPath.of("path/document.pdf"))
            .loadedContent("", UTF_8)
            .build()
            .attributes(new HashMap<>() {{
               put("k1", "43");
               put("k2", "value2");
            }});

      assertThatNullPointerException()
            .isThrownBy(() -> document.addAttribute("key", null))
            .withMessage("'value' must not be null");
   }

   @Test
   public void testMetadataWhenUnmatchingContentSize() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new DocumentBuilder()
                  .<DocumentBuilder>reconstitute()
                  .documentId(DocumentPath.of("path/document.pdf"))
                  .loadedContent("content", UTF_8)
                  .metadata(new DocumentMetadataBuilder()
                                  .documentPath(Paths.get("document.pdf"))
                                  .contentSize(2L)
                                  .build())
                  .build())
            .withMessage(
                  "Invariant validation error > Context [DocumentPath[value=path/document.pdf,newObject=false]] > "
                  + "{metadata} 'metadata.contentSize=2' must be equal to 'content.contentSize=7'");
   }

   @Test
   public void testMetadataWhenUnmatchingContentTypeCharset() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new DocumentBuilder()
                  .documentId(DocumentPath.of("path/document.pdf"))
                  .documentPath(Paths.get("document.pdf"))
                  .loadedContent("content", UTF_8)
                  .contentType(parseMimeType("application/pdf;charset=US-ASCII"))
                  .build())
            .withMessage(
                  "Invariant validation error > Context [DocumentPath[value=path/document.pdf,newObject=false]] > "
                  + "'contentType.charset=US-ASCII' must be equal to 'contentEncoding=UTF-8'");
   }

   @Test
   public void testDocumentMetadataContentSizeWhenUnavailable() {
      Document document = new DocumentBuilder()
            .documentId(DocumentPath.of("path", "document.pdf"))
            .streamContent(new ByteArrayInputStream("content".getBytes(UTF_8)))
            .build();

      assertThat(document.metadata().contentSize()).isEmpty();
   }

   @Test
   public void testDocumentMetadataContentSizeWhenAvailable() {
      Document document = new DocumentBuilder()
            .documentId(DocumentPath.of("path/document.pdf"))
            .streamContent(new ByteArrayInputStream("content".getBytes(UTF_8)), 42L)
            .documentPath("document2.pdf")
            .build();

      assertThat(document.metadata().contentSize()).hasValue(42L);
   }

   @Test
   public void testContent() {
      Document document = new DocumentBuilder()
            .documentId(DocumentPath.of("path/document.pdf"))
            .loadedContent("content", UTF_8)
            .build();

      document = document.content(new LoadedDocumentContentBuilder().content("new content", UTF_8).build());

      assertThat(document.documentId()).isEqualTo(DocumentPath.of("path/document.pdf"));
      assertThat(document.metadata().documentPath()).isEqualTo(Paths.get("path/document.pdf"));
      assertThat(document.metadata().documentName()).isEqualTo("document.pdf");
      assertThat(document.metadata().contentType()).hasValue(parseMimeType("application/pdf;charset=UTF-8"));
      assertThat(document.metadata().contentSize()).hasValue(11L);
      assertThat(document.content().stringContent()).isEqualTo("new content");
      assertThat(document.content().contentEncoding()).hasValue(UTF_8);
   }

   @Test
   public void testContentWhenNoDocumentMetadataContentType() {
      Document document = new DocumentBuilder()
            .documentId(DocumentPath.of("path/document.unk"))
            .loadedContent("content", UTF_8)
            .build();

      document =
            document.content(new LoadedDocumentContentBuilder().content("new content", US_ASCII).build());

      assertThat(document.documentId()).isEqualTo(DocumentPath.of("path/document.unk"));
      assertThat(document.metadata().documentPath()).isEqualTo(Paths.get("path/document.unk"));
      assertThat(document.metadata().documentName()).isEqualTo("document.unk");
      assertThat(document.metadata().contentType()).isEmpty();
      assertThat(document.metadata().contentSize()).hasValue(11L);
      assertThat(document.content().stringContent()).isEqualTo("new content");
      assertThat(document.content().contentEncoding()).hasValue(US_ASCII);
   }

   @Test
   public void testContentWhenNoDocumentMetadataContentEncoding() {
      Document document = new DocumentBuilder()
            .documentId(DocumentPath.of("path/document.pdf"))
            .loadedContent("content".getBytes(UTF_8))
            .build();

      document =
            document.content(new LoadedDocumentContentBuilder().content("new content", US_ASCII).build());

      assertThat(document.documentId()).isEqualTo(DocumentPath.of("path/document.pdf"));
      assertThat(document.metadata().documentPath()).isEqualTo(Paths.get("path/document.pdf"));
      assertThat(document.metadata().documentName()).isEqualTo("document.pdf");
      assertThat(document
                       .metadata()
                       .contentType()).hasValue(parseMimeType("application/pdf;charset=US-ASCII"));
      assertThat(document.metadata().contentSize()).hasValue(11L);
      assertThat(document.content().stringContent()).isEqualTo("new content");
      assertThat(document.content().contentEncoding()).hasValue(US_ASCII);
   }

   @Test
   public void testContentWhenNoEncoding() {
      Document document = new DocumentBuilder()
            .documentId(DocumentPath.of("path/document.pdf"))
            .loadedContent("content", UTF_8)
            .build();

      document = document.content(new LoadedDocumentContentBuilder()
                                        .content("new content".getBytes(UTF_8))
                                        .build());

      assertThat(document.documentId()).isEqualTo(DocumentPath.of("path/document.pdf"));
      assertThat(document.metadata().documentPath()).isEqualTo(Paths.get("path/document.pdf"));
      assertThat(document.metadata().documentName()).isEqualTo("document.pdf");
      assertThat(document.metadata().contentType()).hasValue(parseMimeType("application/pdf;charset=UTF-8"));
      assertThat(document.metadata().contentSize()).hasValue(11L);
      assertThat(document.content().stringContent(UTF_8)).isEqualTo("new content");
      assertThat(document.content().contentEncoding()).isEmpty();
   }

   @Test
   public void testContentWhenDifferentEncoding() {
      Document document = new DocumentBuilder()
            .documentId(DocumentPath.of("path/document.pdf"))
            .loadedContent("content", UTF_8)
            .build();

      document = document.content(new LoadedDocumentContentBuilder()
                                        .content(new ByteArrayInputStream("new content".getBytes(US_ASCII)),
                                                 US_ASCII)
                                        .build());

      assertThat(document.documentId()).isEqualTo(DocumentPath.of("path/document.pdf"));
      assertThat(document.metadata().documentPath()).isEqualTo(Paths.get("path/document.pdf"));
      assertThat(document.metadata().documentName()).isEqualTo("document.pdf");
      assertThat(document
                       .metadata()
                       .contentType()).hasValue(parseMimeType("application/pdf;charset=US-ASCII"));
      assertThat(document.metadata().contentSize()).hasValue(11L);
      assertThat(document.content().contentEncoding()).hasValue(US_ASCII);
      assertThat(document.content().stringContent()).isEqualTo("new content");
   }

   @Test
   public void testDuplicateWhenNominal() {
      Document document = new DocumentBuilder()
            .documentId(DocumentPath.of("path/document.pdf"))
            .documentPath("logicalPath/document.pdf")
            .loadedContent("content1", UTF_8)
            .build();

      ZonedDateTime pit2 = FIXED_DATE.plusSeconds(1);
      ApplicationClock.setFixedClock(pit2);

      document = document.duplicate(DocumentPath.of("path/document2.pdf"));

      assertThat(document.documentId()).isEqualTo(DocumentPath.of("path/document2.pdf"));
      assertThat(document.metadata().documentPath()).isEqualTo(Paths.get("logicalPath/document.pdf"));
      assertThat(document.metadata().documentName()).isEqualTo("document.pdf");
      assertThat(document.metadata().contentType()).hasValue(parseMimeType("application/pdf;charset=UTF-8"));
      assertThat(document.metadata().contentSize()).hasValue(8L);
      assertThat(document.metadata().creationDate()).hasValue(pit2.toInstant());
      assertThat(document.metadata().lastUpdateDate()).hasValue(pit2.toInstant());
      assertThat(document.content().contentEncoding()).hasValue(UTF_8);
      assertThat(document.content().stringContent()).isEqualTo("content1");
   }

   @Test
   public void testDuplicateWhenSameDocumentId() {
      Document document = new DocumentBuilder()
            .documentId(DocumentPath.of("path/document.pdf"))
            .loadedContent("content1", UTF_8)
            .build();

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> document.duplicate(DocumentPath.of("path/document.pdf")))
            .withMessage("Invariant validation error > "
                         + "'newDocumentId=DocumentPath[value=path/document.pdf,newObject=false]' must not be equal to 'documentId=DocumentPath[value=path/document.pdf,newObject=false]'");
   }

   @Test
   public void testToStringWhenHiddenContent() {
      Document document = new DocumentBuilder()
            .documentId(DocumentPath.of("path", "document.pdf"))
            .content(new LoadedDocumentContentBuilder().content("A long or secret content", UTF_8).build())
            .build();

      assertThat(document.toString()).contains("DocumentContent[content=<hidden-value>");
   }

   @Test
   public void testInputStreamDocumentContentWhenReadMultiTimes() {
      Document document = new DocumentBuilder()
            .documentId(DocumentPath.of("path", "document.pdf"))
            .streamContent("content", UTF_8)
            .build();

      assertThat(document.content().stringContent()).isEqualTo("content");
      assertThat(document.content().inputStreamContent()).hasContent("");
      assertThat(document.content().stringContent()).isEqualTo("");
      assertThat(document.content().inputStreamContent()).hasContent("");
   }

   @Test
   public void testLoadedContentWhenReadMultiTimes() {
      Document document = new DocumentBuilder()
            .documentId(DocumentPath.of("path", "document.pdf"))
            .loadedContent("content", UTF_8)
            .build();

      assertThat(document.content().stringContent()).isEqualTo("content");
      assertThat(document.content().inputStreamContent()).hasContent("content");
      assertThat(document.content().stringContent()).isEqualTo("content");
      assertThat(document.content().inputStreamContent()).hasContent("content");
   }

   @Test
   public void testLoadContentWhenNominal() {
      Document preDocument = new DocumentBuilder()
            .documentId(DocumentPath.of("path", "document.pdf"))
            .streamContent("content", UTF_8)
            .build();
      Document document = preDocument.loadContent();

      assertThat(document).isNotSameAs(preDocument);
      assertThat(document.content().stringContent()).isEqualTo("content");
      assertThat(document.content().inputStreamContent()).hasContent("content");
      assertThat(document.content().stringContent()).isEqualTo("content");
      assertThat(document.content().inputStreamContent()).hasContent("content");
   }

   @Test
   public void testLoadContentWhenAlreadyLoaded() {
      Document preDocument = new DocumentBuilder()
            .documentId(DocumentPath.of("path", "document.pdf"))
            .loadedContent("content", UTF_8)
            .build();
      Document document = preDocument.loadContent();

      assertThat(document).isSameAs(preDocument);
      assertThat(document.content().stringContent()).isEqualTo("content");
      assertThat(document.content().inputStreamContent()).hasContent("content");
      assertThat(document.content().stringContent()).isEqualTo("content");
      assertThat(document.content().inputStreamContent()).hasContent("content");
   }

   @Test
   public void testLoadContentWhenOutputStreamContent() throws IOException {
      Document preDocument = new DocumentBuilder()
            .documentId(DocumentPath.of("path", "document.pdf"))
            .content(new OutputStreamDocumentContentBuilder()
                           .content(new ByteArrayOutputStream(), UTF_8).build()).build();

      try (OutputStream output = preDocument.content().outputStreamContent()) {
         output.write("content".getBytes(UTF_8));
      }

      Document document = preDocument.loadContent();

      assertThat(document).isNotSameAs(preDocument);
      assertThat(document.content().stringContent()).isEqualTo("content");
      assertThat(document.content().inputStreamContent()).hasContent("content");
      assertThat(document.content().stringContent()).isEqualTo("content");
      assertThat(document.content().inputStreamContent()).hasContent("content");

   }

   @Test
   public void testDocumentWhenUnDetectedContentType() {
      Document document = new DocumentBuilder()
            .documentId(DocumentPath.of("path/document.doesnotexist"))
            .loadedContent("", UTF_8)
            .build();

      assertThat(document.documentId()).isEqualTo(DocumentPath.of("path/document.doesnotexist"));
      assertThat(document.metadata().documentPath()).isEqualTo(Paths.get("path/document.doesnotexist"));
      assertThat(document.metadata().documentName()).isEqualTo("document.doesnotexist");
      assertThat(document.metadata().contentType()).isEmpty();
      assertThat(document.metadata().contentEncoding()).hasValue(UTF_8);
      assertThat(document.metadata().contentSize()).hasValue(0L);
      assertThat(document.metadata().creationDate()).hasValue(FIXED_DATE.toInstant());
      assertThat(document.metadata().lastUpdateDate()).hasValue(FIXED_DATE.toInstant());
      assertThat(document.content().contentEncoding()).hasValue(UTF_8);
      assertThat(document.content().contentSize()).hasValue(0L);
   }

   @Test
   public void testAutoCloseableContent() throws Exception {
      Document document = new DocumentBuilder()
            .documentId(DocumentPath.of("path/document.txt"))
            .streamContent("content", UTF_8)
            .build();

      try (var autoClose = document.closeableContent()) {
         assertThat(document.documentId()).isEqualTo(DocumentPath.of("path/document.txt"));
         assertThat(document.metadata().documentPath()).isEqualTo(Paths.get("path/document.txt"));
         assertThat(document.metadata().documentName()).isEqualTo("document.txt");
         assertThat(document.metadata().contentType()).hasValue(parseMimeType("text/plain;charset=UTF-8"));
         assertThat(document.metadata().contentEncoding()).hasValue(UTF_8);
         assertThat(document.metadata().contentSize()).hasValue(7L);
         assertThat(document.metadata().creationDate()).hasValue(FIXED_DATE.toInstant());
         assertThat(document.metadata().lastUpdateDate()).hasValue(FIXED_DATE.toInstant());
         assertThat(document.content().contentEncoding()).hasValue(UTF_8);
         assertThat(document.content().contentSize()).hasValue(7L);
      }

   }

}

