/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;

public class DocumentPathTest {

   @Test
   public void documentPathByComponentsWhenNominal() {
      DocumentPath documentId = DocumentPath.of("path", "name.txt");

      assertThat(documentId.value()).isEqualTo(Paths.get("path/name.txt"));
      assertThat(documentId.name()).isEqualTo("name.txt");
      assertThat(documentId.newObject()).isFalse();
      assertThat(documentId.urnValue()).isEqualTo(URI.create("urn:document:path:path/name.txt"));

      DocumentPath newDocumentId = DocumentPath.ofNewObject("path", "name.txt");

      assertThat(newDocumentId.value()).isEqualTo(Paths.get("path/name.txt"));
      assertThat(newDocumentId.name()).isEqualTo("name.txt");
      assertThat(newDocumentId.newObject()).isTrue();
      assertThat(newDocumentId.urnValue()).isEqualTo(URI.create("urn:document:path:path/name.txt"));
   }

   @Test
   public void documentPathByComponentsWhenEmpty() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> DocumentPath.of(""))
            .withMessage(
                  "Invariant validation error > Context [DocumentPath[value=,newObject=false]] > {value} 'value' must not be empty");

      DocumentPath documentId = DocumentPath.of("path", "");

      assertThat(documentId.value()).isEqualTo(Paths.get("path"));

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> DocumentPath.ofNewObject(""))
            .withMessage(
                  "Invariant validation error > Context [DocumentPath[value=,newObject=true]] > {value} 'value' must not be empty");

      DocumentPath newDocumentId = DocumentPath.ofNewObject("path", "");

      assertThat(newDocumentId.value()).isEqualTo(Paths.get("path"));
   }

   @Test
   public void documentPathByComponentsWhenBlank() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> DocumentPath.of("  "))
            .withMessage(
                  "Invariant validation error > Context [DocumentPath[value=  ,newObject=false]] > {value} 'value.fileName' must not be blank");

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> DocumentPath.of("path", "  "))
            .withMessage(
                  "Invariant validation error > Context [DocumentPath[value=path/  ,newObject=false]] > {value} 'value.fileName' must not be blank");

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> DocumentPath.ofNewObject("  "))
            .withMessage(
                  "Invariant validation error > Context [DocumentPath[value=  ,newObject=true]] > {value} 'value.fileName' must not be blank");

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> DocumentPath.ofNewObject("path", "  "))
            .withMessage(
                  "Invariant validation error > Context [DocumentPath[value=path/  ,newObject=true]] > {value} 'value.fileName' must not be blank");
   }

   @Test
   public void documentPathByComponentsWhenBadParameters() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> DocumentPath.of(null, null))
            .withMessage("Invariant validation error > 'first' must not be null | 'more' must not be null");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> DocumentPath.of(null, null, null))
            .withMessage(
                  "Invariant validation error > 'first' must not be null | 'more=[null,null]' > 'more[0]' must not be null");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> DocumentPath.of("", ""))
            .withMessage(
                  "Invariant validation error > Context [DocumentPath[value=,newObject=false]] > {value} 'value' must not be empty");
   }

   @Test
   public void documentPathByPathWhenNominal() {
      DocumentPath documentId = DocumentPath.of(Paths.get("path", "name.txt"));

      assertThat(documentId.value()).isEqualTo(Paths.get("path/name.txt"));
      assertThat(documentId.name()).isEqualTo("name.txt");
      assertThat(documentId.newObject()).isFalse();
      assertThat(documentId.urnValue()).isEqualTo(URI.create("urn:document:path:path/name.txt"));

      DocumentPath newDocumentId = DocumentPath.ofNewObject(Paths.get("path", "name.txt"));

      assertThat(newDocumentId.value()).isEqualTo(Paths.get("path/name.txt"));
      assertThat(newDocumentId.name()).isEqualTo("name.txt");
      assertThat(newDocumentId.newObject()).isTrue();
      assertThat(newDocumentId.urnValue()).isEqualTo(URI.create("urn:document:path:path/name.txt"));
   }

   @Test
   public void documentPathByPathWhenEmpty() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> DocumentPath.of(Paths.get("")))
            .withMessage(
                  "Invariant validation error > Context [DocumentPath[value=,newObject=false]] > {value} 'value' must not be empty");

      DocumentPath documentId = DocumentPath.of("path", "");

      assertThat(documentId.value()).isEqualTo(Paths.get("path"));

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> DocumentPath.ofNewObject(Paths.get("")))
            .withMessage(
                  "Invariant validation error > Context [DocumentPath[value=,newObject=true]] > {value} 'value' must not be empty");

      DocumentPath newDocumentId = DocumentPath.ofNewObject("path", "");

      assertThat(newDocumentId.value()).isEqualTo(Paths.get("path"));
   }

   @Test
   public void documentPathByPathWhenBlank() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> DocumentPath.of(Paths.get("  ")))
            .withMessage(
                  "Invariant validation error > Context [DocumentPath[value=  ,newObject=false]] > {value} 'value.fileName' must not be blank");

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> DocumentPath.of(Paths.get("path", "  ")))
            .withMessage(
                  "Invariant validation error > Context [DocumentPath[value=path/  ,newObject=false]] > {value} 'value.fileName' must not be blank");

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> DocumentPath.ofNewObject(Paths.get("  ")))
            .withMessage(
                  "Invariant validation error > Context [DocumentPath[value=  ,newObject=true]] > {value} 'value.fileName' must not be blank");

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> DocumentPath.ofNewObject(Paths.get("path", "  ")))
            .withMessage(
                  "Invariant validation error > Context [DocumentPath[value=path/  ,newObject=true]] > {value} 'value.fileName' must not be blank");
   }

   @Test
   public void documentPathByPathWhenBadParameters() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> DocumentPath.of((Path) null))
            .withMessage(
                  "Invariant validation error > Context [DocumentPath[value=<null>,newObject=false]] > {newObject} 'newObject' must be true if value is uninitialized | 'value' must not be null");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> DocumentPath.of("", ""))
            .withMessage(
                  "Invariant validation error > Context [DocumentPath[value=,newObject=false]] > {value} 'value' must not be empty");

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> DocumentPath.ofNewObject((Path) null))
            .withMessage(
                  "Invariant validation error > Context [DocumentPath[value=<null>,newObject=true]] > 'value' must not be null");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> DocumentPath.ofNewObject("", ""))
            .withMessage(
                  "Invariant validation error > Context [DocumentPath[value=,newObject=true]] > {value} 'value' must not be empty");
   }

   @Test
   public void documentPathByUrnWhenNominal() {
      DocumentPath documentId = DocumentPath.of(URI.create("urn:document:path:path/name.txt"));

      assertThat(documentId.value()).isEqualTo(Paths.get("path/name.txt"));
      assertThat(documentId.name()).isEqualTo("name.txt");
      assertThat(documentId.newObject()).isFalse();
      assertThat(documentId.urnValue()).isEqualTo(URI.create("urn:document:path:path/name.txt"));

      DocumentPath newDocumentId = DocumentPath.ofNewObject(URI.create("urn:document:path:path/name.txt"));

      assertThat(newDocumentId.value()).isEqualTo(Paths.get("path/name.txt"));
      assertThat(newDocumentId.name()).isEqualTo("name.txt");
      assertThat(newDocumentId.newObject()).isTrue();
      assertThat(newDocumentId.urnValue()).isEqualTo(URI.create("urn:document:path:path/name.txt"));
   }

   @Test
   public void documentPathByUrnWhenBadUrn() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> DocumentPath.of(URI.create("urn:document:path/name.txt")))
            .withMessage(
                  "Invariant validation error > 'urn=urn:document:path/name.txt' must be valid URN : 'urn:document:path:<id-value>'");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> DocumentPath.of(URI.create("urn:path:path/name.txt")))
            .withMessage(
                  "Invariant validation error > 'urn=urn:path:path/name.txt' must be valid URN : 'urn:document:path:<id-value>'");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> DocumentPath.of(URI.create("document:path:path/name.txt")))
            .withMessage(
                  "Invariant validation error > 'urn=document:path:path/name.txt' must be valid URN : 'urn:document:path:<id-value>'");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> DocumentPath.of(URI.create("urn:document:path:")))
            .withMessage(
                  "Invariant validation error > 'urn=urn:document:path:' must be valid URN : 'urn:document:path:<id-value>'");
   }

   @Test
   public void valueByComponentsWhenNominal() {
      DocumentPath documentId = DocumentPath.of("path", "name.txt");
      DocumentPath updatedDocumentId = documentId.value("newPath", "newName.txt");

      assertThat(updatedDocumentId.value()).isEqualTo(Paths.get("newPath/newName.txt"));
      assertThat(updatedDocumentId.newObject()).isFalse();

      DocumentPath newDocumentId = DocumentPath.ofNewObject("path", "name.txt");
      DocumentPath updatedNewDocumentId = newDocumentId.value("newPath", "newName.txt");

      assertThat(updatedNewDocumentId.value()).isEqualTo(Paths.get("newPath/newName.txt"));
      assertThat(updatedNewDocumentId.newObject()).isTrue();
   }

   @Test
   public void valueByPathWhenNominal() {
      DocumentPath documentId = DocumentPath.of("path", "name.txt");
      DocumentPath updatedDocumentId = documentId.value(Paths.get("newPath", "newName.txt"));

      assertThat(updatedDocumentId.value()).isEqualTo(Paths.get("newPath/newName.txt"));
      assertThat(updatedDocumentId.newObject()).isFalse();

      DocumentPath newDocumentId = DocumentPath.ofNewObject("path", "name.txt");
      DocumentPath updatedNewDocumentId = newDocumentId.value(Paths.get("newPath", "newName.txt"));

      assertThat(updatedNewDocumentId.value()).isEqualTo(Paths.get("newPath/newName.txt"));
      assertThat(updatedNewDocumentId.newObject()).isTrue();
   }

   @Test
   public void nameWhenNominal() {
      DocumentPath documentId = DocumentPath.of("path", "name.txt");
      DocumentPath updatedDocumentId = documentId.name("newName.txt");

      assertThat(updatedDocumentId.value()).isEqualTo(Paths.get("path/newName.txt"));
      assertThat(updatedDocumentId.newObject()).isFalse();

      DocumentPath newDocumentId = DocumentPath.ofNewObject("path", "name.txt");
      DocumentPath updatedNewDocumentId = newDocumentId.name("newName.txt");

      assertThat(updatedNewDocumentId.value()).isEqualTo(Paths.get("path/newName.txt"));
      assertThat(updatedNewDocumentId.newObject()).isTrue();
   }

   @Test
   public void nameWhenSingleComponent() {
      DocumentPath documentId = DocumentPath.of("name.txt");

      DocumentPath updatedDocumentId = documentId.name("newName.txt");

      assertThat(updatedDocumentId.value()).isEqualTo(Paths.get("newName.txt"));
   }

   @Test
   public void nameWhenBlankName() {
      DocumentPath documentId = DocumentPath.of("name.txt");

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> documentId.name(""))
            .withMessage(
                  "Invariant validation error > Context [DocumentPath[value=,newObject=false]] > {value} 'value' must not be empty");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> documentId.name("  "))
            .withMessage(
                  "Invariant validation error > Context [DocumentPath[value=  ,newObject=false]] > {value} 'value.fileName' must not be blank");
   }

   @Test
   public void extensionWhenNominal() {
      assertThat(DocumentPath.of("name.txt").extension()).isEqualTo("txt");
   }

   @Test
   public void extensionWhenCaseSensitive() {
      assertThat(DocumentPath.of("name.TxT").extension()).isEqualTo("TxT");
   }

   @Test
   public void extensionWhenNoExtension() {
      assertThat(DocumentPath.of("name").extension()).isEmpty();
   }

   @Test
   public void extensionWhenEmptyExtension() {
      assertThat(DocumentPath.of("name.").extension()).isEmpty();
   }

   @Test
   public void extensionWhenBlankExtension() {
      assertThat(DocumentPath.of("name.  ").extension()).isEqualTo("  ");
   }

   @Test
   public void hasExtensionWhenNominal() {
      assertThat(DocumentPath.of("name.txt").hasExtension("txt")).isTrue();
      assertThat(DocumentPath.of("name.txt").hasExtension("pdf")).isFalse();
   }

   @Test
   public void hasExtensionWhenCaseSensitive() {
      assertThat(DocumentPath.of("name.txt").hasExtension("txt")).isTrue();
      assertThat(DocumentPath.of("name.TxT").hasExtension("TxT")).isTrue();
      assertThat(DocumentPath.of("name.txt").hasExtension("TxT")).isFalse();
      assertThat(DocumentPath.of("name.TxT").hasExtension("txt")).isFalse();
   }

   @Test
   public void hasExtensionWhenNoExtension() {
      assertThat(DocumentPath.of("name").hasExtension("")).isTrue();
      assertThat(DocumentPath.of("name").hasExtension("txt")).isFalse();
   }

   @Test
   public void hasExtensionWhenEmptyExtension() {
      assertThat(DocumentPath.of("name.").hasExtension("")).isTrue();
      assertThat(DocumentPath.of("name.").hasExtension("txt")).isFalse();
   }

   @Test
   public void hasExtensionWhenBlankExtension() {
      assertThat(DocumentPath.of("name.  ").hasExtension("  ")).isTrue();
      assertThat(DocumentPath.of("name.  ").hasExtension("")).isFalse();
      assertThat(DocumentPath.of("name.  ").hasExtension("txt")).isFalse();
   }

   @Test
   public void hasExtensionIgnoreCaseWhenNominal() {
      assertThat(DocumentPath.of("name.txt").hasExtensionIgnoreCase("txt")).isTrue();
      assertThat(DocumentPath.of("name.txt").hasExtensionIgnoreCase("pdf")).isFalse();
   }

   @Test
   public void hasExtensionIgnoreCaseWhenCaseSensitive() {
      assertThat(DocumentPath.of("name.txt").hasExtensionIgnoreCase("txt")).isTrue();
      assertThat(DocumentPath.of("name.TxT").hasExtensionIgnoreCase("TxT")).isTrue();
      assertThat(DocumentPath.of("name.txt").hasExtensionIgnoreCase("TxT")).isTrue();
      assertThat(DocumentPath.of("name.TxT").hasExtensionIgnoreCase("txt")).isTrue();
   }

   @Test
   public void hasExtensionIgnoreCaseWhenNoExtension() {
      assertThat(DocumentPath.of("name").hasExtensionIgnoreCase("")).isTrue();
      assertThat(DocumentPath.of("name").hasExtensionIgnoreCase("txt")).isFalse();
   }

   @Test
   public void hasExtensionIgnoreCaseWhenEmptyExtension() {
      assertThat(DocumentPath.of("name.").hasExtensionIgnoreCase("")).isTrue();
      assertThat(DocumentPath.of("name.").hasExtensionIgnoreCase("txt")).isFalse();
   }

   @Test
   public void hasExtensionIgnoreCaseWhenBlankExtension() {
      assertThat(DocumentPath.of("name.  ").hasExtensionIgnoreCase("  ")).isTrue();
      assertThat(DocumentPath.of("name.  ").hasExtensionIgnoreCase("")).isFalse();
      assertThat(DocumentPath.of("name.  ").hasExtensionIgnoreCase("txt")).isFalse();
   }

   @Test
   public void subPathWhenNominal() {
      assertThat(DocumentPath.of("path/name.txt").subPath(Paths.get("path"))).hasValue(DocumentPath.of(
            "name.txt"));
   }

   @Test
   public void subPathWhenNull() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> DocumentPath.of("path/name.txt").subPath(null))
            .withMessage("Invariant validation error > 'subPath' must not be null");
   }

   @Test
   public void shouldCopyStateWhenSubPath() {
      assertThat(DocumentPath
                       .ofNewObject("path/name.txt")
                       .subPath(Paths.get("path"))).hasValue(DocumentPath.ofNewObject("name.txt"));
   }

   @Test
   public void subPathWhenDoesNotMatch() {
      assertThat(DocumentPath
                       .ofNewObject("path/name.txt")
                       .subPath(Paths.get("path"))).hasValue(DocumentPath.ofNewObject("name.txt"));
   }

   @Test
   public void subPathWhenEmpty() {
      assertThat(DocumentPath.of("path/name.txt").subPath(Paths.get(""))).hasValue(DocumentPath.of(
            "path/name.txt"));
   }

   @Test
   public void subPathWhenFullMatch() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> DocumentPath.of("path/name.txt").subPath(Paths.get("path/name.txt")))
            .withMessage(
                  "Invariant validation error > 'subPath=path/name.txt' must not be equal to 'path/name.txt'");
   }

   @Test
   public void subPathWhenPartialPathComponent() {
      assertThat(DocumentPath.of("path/name.txt").subPath(Paths.get("path/na"))).isEmpty();
   }

   @Test
   public void subPathWhenNormalized() {
      assertThat(DocumentPath.of("path/name.txt").subPath(Paths.get("./path"))).hasValue(DocumentPath.of(
            "name.txt"));
      assertThat(DocumentPath.of("path/name.txt").subPath(Paths.get("path/./"))).hasValue(DocumentPath.of(
            "name.txt"));
      assertThat(DocumentPath.of("path/name.txt").subPath(Paths.get("path/"))).hasValue(DocumentPath.of(
            "name.txt"));
   }

   @Test
   public void subPathWhenHasTraversal() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> DocumentPath.of("path/name.txt").subPath(Paths.get("../path")))
            .withMessage("Invariant validation error > 'subPath=../path' must not have traversal paths");
   }

   @Test
   public void subPathWhenAbsolute() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> DocumentPath.of("path/name.txt").subPath(Paths.get("/path")))
            .withMessage("Invariant validation error > 'subPath=/path' must not be absolute path");
   }
}
