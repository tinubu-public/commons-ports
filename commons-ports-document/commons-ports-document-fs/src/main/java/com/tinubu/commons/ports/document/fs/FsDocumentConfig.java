/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.fs;

import static com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport.checkInvariants;
import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isLessThanOrEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.NumberRules.isStrictlyPositive;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isAbsolute;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ports.document.fs.storagestrategy.DirectFsStorageStrategy;
import com.tinubu.commons.ports.document.fs.storagestrategy.FsStorageStrategy;
import com.tinubu.commons.ports.document.fs.storagestrategy.HexTreeFsStorageStrategy;

/**
 * FS document repository configuration.
 */
public class FsDocumentConfig extends AbstractValue {

   public static final int DEFAULT_HEX_TREE_DEPTH = 2;
   private static final int MAX_TREE_DEPTH = 16;

   private final Path storagePath;
   private final Path basePath;
   private final boolean createStoragePathIfMissing;
   private final boolean failFastIfMissingStoragePath;
   private final Class<? extends FsStorageStrategy> storageStrategy;
   private final HexTreeStorageStrategyConfig hexTreeStorageStrategy;
   private final boolean deleteRepositoryOnClose;

   public FsDocumentConfig(FsDocumentConfigBuilder builder) {
      this.storagePath = nullable(builder.storagePath, FsDocumentConfig::defaultStoragePath).normalize();
      this.basePath = nullable(builder.basePath, Paths.get("")).normalize();
      this.createStoragePathIfMissing = nullable(builder.createStoragePathIfMissing, false);
      this.failFastIfMissingStoragePath = nullable(builder.failFastIfMissingStoragePath, true);
      this.storageStrategy = nullable(builder.storageStrategy, DirectFsStorageStrategy.class);
      this.hexTreeStorageStrategy =
            nullable(builder.hexTreeStorageStrategy, HexTreeStorageStrategyConfig.ofDefaults());
      this.deleteRepositoryOnClose = nullable(builder.deleteRepositoryOnClose, false);
   }

   @Override
   public Fields<? extends FsDocumentConfig> defineDomainFields() {
      return Fields
            .<FsDocumentConfig>builder()
            .field("storagePath",
                   v -> v.storagePath,
                   isNotNull().andValue(isAbsolute().andValue(hasNoTraversal())))
            .field("basePath", v -> v.basePath, isNull().orValue(isNotAbsolute().andValue(hasNoTraversal())))
            .field("createStoragePathIfMissing", v -> v.createStoragePathIfMissing)
            .field("failFastIfMissingStoragePath", v -> v.failFastIfMissingStoragePath)
            .field("storageStrategy", v -> v.storageStrategy, isNotNull())
            .field("hexTreeStorageStrategy", v -> v.hexTreeStorageStrategy, isNotNull())
            .field("deleteRepositoryOnClose", v -> v.deleteRepositoryOnClose)
            .build();
   }

   /**
    * Document filesystem storage root path. This path must be absolute so that storage path does not depend
    * on current application default directory.
    */
   public Path storagePath() {
      return storagePath;
   }

   /**
    * Optional base path relative to storage path. This path must be relative. Set to {@code ""} by default.
    * If omitted, or set to {@code ""} documents will be accessed directly from storage path.
    * When {@link #createStoragePathIfMissing()} is set, base path won't be created, only the storage path is
    * created.
    */
   public Path basePath() {
      return basePath;
   }

   /**
    * Whether to automatically create missing storage path. Default to {@code false}.
    */
   public boolean createStoragePathIfMissing() {
      return createStoragePathIfMissing;
   }

   /**
    * Whether to fail fast if storage path is missing and {@link #createStoragePathIfMissing()} is set to
    * {@code false}, or if an error occurs while creating storage path. Default to {@code true}.
    */
   public boolean failFastIfMissingStoragePath() {
      return failFastIfMissingStoragePath;
   }

   /**
    * Document filesystem storage strategy to use. You must not change this configuration if documents already
    * exist, as it changes the way documents are searched.
    */
   public Class<? extends FsStorageStrategy> storageStrategy() {
      return storageStrategy;
   }

   /**
    * {@link HexTreeFsStorageStrategy} configuration.
    *
    * @return {@link HexTreeFsStorageStrategy} configuration
    */
   public HexTreeStorageStrategyConfig hexTreeStorageStrategy() {
      return hexTreeStorageStrategy;
   }

   /**
    * Whether to automatically delete repository on repository closing. Default to {@code false}.
    */
   public boolean deleteRepositoryOnClose() {
      return deleteRepositoryOnClose;
   }

   public static class HexTreeStorageStrategyConfig extends AbstractValue {

      private final int treeDepth;

      private HexTreeStorageStrategyConfig(int treeDepth) {
         this.treeDepth = treeDepth;
      }

      @Override
      public Fields<? extends HexTreeStorageStrategyConfig> defineDomainFields() {
         return Fields
               .<HexTreeStorageStrategyConfig>builder()
               .field("treeDepth",
                      v -> v.treeDepth,
                      isStrictlyPositive().andValue(isLessThanOrEqualTo(value(MAX_TREE_DEPTH))))
               .build();
      }

      public static HexTreeStorageStrategyConfig of(int treeDepth) {
         return checkInvariants(new HexTreeStorageStrategyConfig(treeDepth));
      }

      public static HexTreeStorageStrategyConfig ofDefaults() {
         return of(DEFAULT_HEX_TREE_DEPTH);
      }

      /**
       * Generated hex tree depth for files, e.g.: a depth of 3 will generate directory tree like {@code
       * 6a/fd/ac}. You must not change this configuration if documents already exist, as it changes the way
       * documents are searched.
       */
      public int treeDepth() {
         return treeDepth;
      }

   }

   private static Path defaultStoragePath() {
      try {
         return Files.createTempDirectory("document-fs");
      } catch (IOException e) {
         throw new UncheckedIOException(e);
      }
   }

   public static class FsDocumentConfigBuilder extends DomainBuilder<FsDocumentConfig> {

      private Path storagePath;
      private Path basePath;
      private Boolean createStoragePathIfMissing;
      private Boolean failFastIfMissingStoragePath;
      private Class<? extends FsStorageStrategy> storageStrategy;
      private HexTreeStorageStrategyConfig hexTreeStorageStrategy;
      private Boolean deleteRepositoryOnClose;

      public static FsDocumentConfigBuilder from(FsDocumentConfig config) {
         return new FsDocumentConfigBuilder()
               .storagePath(config.storagePath)
               .basePath(config.basePath)
               .createStoragePathIfMissing(config.createStoragePathIfMissing)
               .failFastIfMissingStoragePath(config.failFastIfMissingStoragePath)
               .storageStrategy(config.storageStrategy)
               .hexTreeStorageStrategy(config.hexTreeStorageStrategy)
               .deleteRepositoryOnClose(config.deleteRepositoryOnClose);
      }

      /**
       * Returns a pre-configured builder from specified document repository URI information, only if URI is
       * compatible with this repository.
       *
       * @param uri URI
       *
       * @return pre-configured builder or {@link Optional#empty} if URI is not compatible with this repository.
       */
      public Optional<FsDocumentConfigBuilder> fromUri(URI uri) {
         validate(uri, "uri", isNotNull()).orThrow();

         FsDocumentConfigBuilder config = null;
         if (FsDocumentRepository.isCompatibleUri(uri, false)) {
            config = new FsDocumentConfigBuilder()
                  .<FsDocumentConfigBuilder>reconstitute()
                  .storagePath(Paths.get(uri));
         }

         return nullable(config);
      }

      public FsDocumentConfigBuilder storagePath(Path storagePath) {
         this.storagePath = storagePath;
         return this;
      }

      public FsDocumentConfigBuilder basePath(Path basePath) {
         this.basePath = basePath;
         return this;
      }

      public FsDocumentConfigBuilder createStoragePathIfMissing(boolean createStoragePathIfMissing) {
         this.createStoragePathIfMissing = createStoragePathIfMissing;
         return this;
      }

      public FsDocumentConfigBuilder failFastIfMissingStoragePath(boolean failFastIfMissingStoragePath) {
         this.failFastIfMissingStoragePath = failFastIfMissingStoragePath;
         return this;
      }

      public FsDocumentConfigBuilder storageStrategy(Class<? extends FsStorageStrategy> storageStrategy) {
         this.storageStrategy = storageStrategy;
         return this;
      }

      public FsDocumentConfigBuilder hexTreeStorageStrategy(HexTreeStorageStrategyConfig hexTreeStorageStrategy) {
         this.hexTreeStorageStrategy = hexTreeStorageStrategy;
         return this;
      }

      public boolean deleteRepositoryOnClose() {
         return deleteRepositoryOnClose;
      }

      public FsDocumentConfigBuilder deleteRepositoryOnClose(boolean deleteRepositoryOnClose) {
         this.deleteRepositoryOnClose = deleteRepositoryOnClose;
         return this;
      }

      @Override
      protected FsDocumentConfig buildDomainObject() {
         return new FsDocumentConfig(this);
      }
   }
}
