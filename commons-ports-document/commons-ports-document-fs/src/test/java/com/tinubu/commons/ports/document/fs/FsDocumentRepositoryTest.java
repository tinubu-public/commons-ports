/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.fs;

import static com.tinubu.commons.lang.util.CollectionUtils.collection;
import static java.nio.file.attribute.PosixFilePermission.OWNER_EXECUTE;
import static java.nio.file.attribute.PosixFilePermission.OWNER_READ;
import static java.nio.file.attribute.PosixFilePermission.OWNER_WRITE;
import static org.apache.commons.lang3.StringUtils.stripStart;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.function.UnaryOperator;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentContent;
import com.tinubu.commons.ports.document.domain.DocumentMetadata;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.LoadedDocumentContent.LoadedDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata.OpenDocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.capability.UnsupportedCapabilityException;
import com.tinubu.commons.ports.document.domain.testsuite.CommonDocumentRepositoryTest;
import com.tinubu.commons.ports.document.fs.FsDocumentConfig.FsDocumentConfigBuilder;
import com.tinubu.commons.ports.document.fs.storagestrategy.DirectFsStorageStrategy;
import com.tinubu.commons.ports.document.fs.storagestrategy.FsStorageStrategy;
import com.tinubu.commons.ports.document.fs.storagestrategy.HexTreeFsStorageStrategy;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ZipArchiver;

public class FsDocumentRepositoryTest {

   @Nested
   @TestInstance(
         Lifecycle.PER_CLASS) /* To support non static @BeforeAll in inner classes. */ public class CommonTestsuite
         extends CommonDocumentRepositoryTest {

      private Path TEST_STORAGE_PATH;

      private DocumentRepository documentRepository;

      /**
       * {@inheritDoc}
       * At least the Linux filesystem updates the creation date on overwrite.
       */
      @Override
      public boolean isUpdatingCreationDateOnOverwrite() {
         return true;
      }

      @Override
      protected boolean isSupportingMetadataAttributes() {
         return false;
      }

      @BeforeAll
      public void createStoragePath() {
         try {
            TEST_STORAGE_PATH = Files.createTempDirectory("document-fs-test");

            System.out.printf("Creating '%s' filesystem test storage path%n", TEST_STORAGE_PATH);
         } catch (IOException e) {
            throw new IllegalStateException(e);
         }
      }

      @AfterAll
      public void deleteStoragePath() {
         try {
            System.out.printf("Deleting '%s' filesystem test storage path%n", TEST_STORAGE_PATH);

            FileUtils.deleteDirectory(TEST_STORAGE_PATH.toFile());
         } catch (IOException e) {
            throw new IllegalStateException(e);
         }
      }

      @BeforeEach
      public void configureDocumentRepository() {
         this.documentRepository = newFsDocumentRepository();
      }

      @AfterEach
      public void closeDocumentRepository() {
         this.documentRepository.close();
      }

      @Override
      protected DocumentRepository documentRepository() {
         return documentRepository;
      }

      @Override
      protected void instrumentIOException(Path documentPath, boolean enabled) {
         try {
            Path fsTarget = TEST_STORAGE_PATH.resolve(documentPath);

            if (!fsTarget.toFile().exists()) {
               throw new IllegalStateException(String.format("Can't instrument '%s' FS target for '%s'",
                                                             fsTarget,
                                                             documentPath));
            }

            if (enabled) {
               Files.setPosixFilePermissions(fsTarget, collection(HashSet::new));
            } else {
               Files.setPosixFilePermissions(fsTarget,
                                             collection(HashSet::new,
                                                        OWNER_READ,
                                                        OWNER_WRITE,
                                                        OWNER_EXECUTE));
            }
         } catch (IOException e) {
            throw new IllegalStateException(e);
         }
      }

      @Override
      protected Document zipTransformer(DocumentPath zipPath, List<Document> documents) {
         return new ZipArchiver(zipPath).compress(documents);
      }

      private FsDocumentRepository newFsDocumentRepository() {
         return new FsDocumentRepository(new FsDocumentConfigBuilder()
                                               .storagePath(TEST_STORAGE_PATH)
                                               .storageStrategy(DirectFsStorageStrategy.class)
                                               .build());
      }

      @Override
      protected UnaryOperator<DocumentMetadataBuilder> synchronizeExpectedMetadata(DocumentMetadata actual) {
         return builder -> builder
               .chain(super.synchronizeExpectedMetadata(actual))
               .attributes(actual.attributes())
               .contentType(actual.contentEncoding().orElse(null));
      }

      @Override
      protected UnaryOperator<LoadedDocumentContentBuilder> synchronizeExpectedContent(DocumentContent actual) {
         return builder -> builder
               .chain(super.synchronizeExpectedContent(actual))
               .contentEncoding(actual.contentEncoding().orElse(null));
      }

      @Nested
      public class UriAdapter {

         @Test
         public void testFsDocumentConfigBuilderFromUriWhenNominal() {
            assertThat(new FsDocumentConfigBuilder().fromUri(uri("file:/path"))).isPresent();
         }

         @Test
         public void testFsDocumentConfigBuilderFromUriWhenBadParameters() {
            assertThatExceptionOfType(InvariantValidationException.class)
                  .isThrownBy(() -> new FsDocumentConfigBuilder().fromUri(null))
                  .withMessage("Invariant validation error > 'uri' must not be null");
         }

         @Test
         public void testFsDocumentConfigBuilderFromUriWhenCompatibleUri() {
            assertThat(new FsDocumentConfigBuilder().fromUri(uri("file:/path/file.txt")))
                  .as("Here file.txt is considered as part of the storage path directory")
                  .isPresent();
            assertThat(new FsDocumentConfigBuilder().fromUri(uri("file:/"))).isPresent();
         }

         @Test
         public void testFsDocumentConfigBuilderFromUriWhenNotCompatibleUri() {
            assertThat(new FsDocumentConfigBuilder().fromUri(uri("file:path"))).isEmpty();
            assertThat(new FsDocumentConfigBuilder().fromUri(uri("other:/path"))).isEmpty();
            assertThat(new FsDocumentConfigBuilder().fromUri(uri("file:/path?query"))).isEmpty();
            assertThat(new FsDocumentConfigBuilder().fromUri(uri("file:/path#fragment"))).isEmpty();
            assertThat(new FsDocumentConfigBuilder().fromUri(uri("file://host/path"))).isEmpty();
            assertThat(new FsDocumentConfigBuilder().fromUri(uri("file://user@host/path"))).isEmpty();
         }

         @Test
         public void testToDocumentUriWhenNominal() {
            assertThat(documentRepository().toUri(DocumentPath.of("file.txt"))).isEqualTo(uri("file:"
                                                                                              + TEST_STORAGE_PATH
                                                                                              + "/file.txt"));
            assertThat(documentRepository().toUri(DocumentPath.of("path/file.txt"))).isEqualTo(uri("file:"
                                                                                                   + TEST_STORAGE_PATH
                                                                                                   + "/path/file.txt"));
         }

         @Test
         public void testSupportsDocumentUriWhenNominal() {
            assertThat(documentRepository().supportsUri(uri("file:" + TEST_STORAGE_PATH + "/path/test.txt"),
                                                        true)).isTrue();
            assertThat(documentRepository().supportsUri(uri(TEST_STORAGE_PATH + "/path/test.txt"),
                                                        true)).isTrue();
         }

         @Test
         public void testSupportsDocumentUriWhenRepositoryUri() {
            assertThat(documentRepository().supportsUri(uri("file:" + TEST_STORAGE_PATH), true)).isFalse();
            assertThat(documentRepository().supportsUri(uri(TEST_STORAGE_PATH.toString()), true)).isFalse();
         }

         @Test
         public void testSupportsDocumentUriWhenBadStoragePath() {
            assertThat(documentRepository().supportsUri(uri("file:/bad/path/test.txt"), true)).isFalse();
            assertThat(documentRepository().supportsUri(uri("/bad/path/test.txt"), true)).isFalse();
         }

         @Test
         public void testSupportsDocumentUriWhenAlternativeForms() {
            assertThat(documentRepository().supportsUri(uri("file:/" + TEST_STORAGE_PATH + "/path/test.txt"),
                                                        true)).isFalse();
            assertThat(documentRepository().supportsUri(uri("file://" + TEST_STORAGE_PATH + "/path/test.txt"),
                                                        true)).isTrue();
            assertThat(documentRepository().supportsUri(uri("file:" + stripStart(TEST_STORAGE_PATH.toString(),
                                                                                 "/") + "/path/test.txt"),
                                                        true)).isFalse();
            assertThat(documentRepository().supportsUri(uri(stripStart(TEST_STORAGE_PATH.toString(), "/")
                                                            + "/path/test.txt"), true)).isFalse();
            assertThat(documentRepository().supportsUri(uri("file:" + TEST_STORAGE_PATH
                  .toString()
                  .substring(1) + "/path/test.txt"), true)).isFalse();
            assertThat(documentRepository().supportsUri(uri("file://host"
                                                            + TEST_STORAGE_PATH
                                                            + "/path/test.txt"), true)).isFalse();
            assertThat(documentRepository().supportsUri(uri("file:"
                                                            + TEST_STORAGE_PATH
                                                            + "/path/test.txt#fragment"), true)).isFalse();
            assertThat(documentRepository().supportsUri(uri("file:"
                                                            + TEST_STORAGE_PATH
                                                            + "/path/test.txt?query"), true)).isFalse();
         }

         @Test
         public void testToRepositoryUriWhenNominal() {
            assertThat(documentRepository().toUri()).isEqualTo(uri("file:" + TEST_STORAGE_PATH));
         }

         @Test
         public void testSupportsRepositoryUriWhenNominal() {
            assertThat(documentRepository().supportsUri(uri("file:" + TEST_STORAGE_PATH), false)).isTrue();
            assertThat(documentRepository().supportsUri(uri(TEST_STORAGE_PATH.toString()), false)).isTrue();
         }

         @Test
         public void testSupportsRepositoryUriWhenDocumentUri() {
            assertThat(documentRepository().supportsUri(uri("file:" + TEST_STORAGE_PATH + "/path/test.txt"),
                                                        false)).isTrue();
            assertThat(documentRepository().supportsUri(uri(TEST_STORAGE_PATH + "/path/test.txt"),
                                                        false)).isTrue();
         }

         @Test
         public void testSupportsRepositoryUriWhenBadStoragePath() {
            assertThat(documentRepository().supportsUri(uri("file:/bad"), false)).isFalse();
            assertThat(documentRepository().supportsUri(uri("/bad"), false)).isFalse();
         }

         @Test
         public void testFindDocumentByUriWhenNominal() {
            DocumentPath documentPath = DocumentPath.of("path/pathfile2.txt");

            try {
               createDocuments(documentPath);

               assertThat(documentRepository().findDocumentByUri(uri("file:"
                                                                     + TEST_STORAGE_PATH
                                                                     + "/path/pathfile2.txt"))).hasValueSatisfying(
                     document -> {
                        assertThat(document.documentId()).isEqualTo(DocumentPath.of("path/pathfile2.txt"));
                        assertThat(document.metadata().documentPath()).isEqualTo(Paths.get(
                              "path/pathfile2.txt"));
                     });

            } finally {
               deleteDocuments(documentPath);
            }
         }

         @Test
         public void testFindDocumentByUriWhenRepositoryUri() {
            assertThatIllegalArgumentException()
                  .isThrownBy(() -> documentRepository().findDocumentByUri(uri("file:" + TEST_STORAGE_PATH)))
                  .withMessage("'documentUri' must be supported : file:" + TEST_STORAGE_PATH);
         }

      }

      @Nested
      public class WhenNotExistingStoragePath {

         @Nested
         public class WhenNotFailFastIfMissingStoragePath {

            @ParameterizedTest
            @ValueSource(classes = { DirectFsStorageStrategy.class, HexTreeFsStorageStrategy.class })
            public void testFindDocumentByIdWhenNotExistingStoragePath(Class<FsStorageStrategy> storageStrategy)
                  throws IOException {
               Path storagePath = Paths.get(System.getProperty("java.io.tmpdir", "/tmp"), "test-document-fs");

               try {
                  DocumentRepository repository = new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                                                 .storagePath(storagePath)
                                                                                 .failFastIfMissingStoragePath(
                                                                                       false)
                                                                                 .createStoragePathIfMissing(
                                                                                       false)
                                                                                 .storageStrategy(
                                                                                       storageStrategy)
                                                                                 .build());

                  assertThat(repository.findDocumentById(DocumentPath.of("file.pdf"))).isEmpty();

                  DocumentRepository createStorageIfMissingRepository =
                        new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                       .storagePath(storagePath)
                                                       .failFastIfMissingStoragePath(false)
                                                       .createStoragePathIfMissing(true)
                                                       .storageStrategy(storageStrategy)
                                                       .build());

                  assertThat(createStorageIfMissingRepository.findDocumentById(DocumentPath.of("file.pdf"))).isEmpty();
               } finally {
                  FileUtils.deleteDirectory(storagePath.toFile());
               }
            }

            @ParameterizedTest
            @ValueSource(classes = { DirectFsStorageStrategy.class, HexTreeFsStorageStrategy.class })
            public void testSaveDocumentWhenNotExistingStoragePath(Class<FsStorageStrategy> storageStrategy)
                  throws IOException {
               Path storagePath = Paths.get(System.getProperty("java.io.tmpdir", "/tmp"), "test-document-fs");

               try {
                  DocumentRepository repository = new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                                                 .storagePath(storagePath)
                                                                                 .failFastIfMissingStoragePath(
                                                                                       false)
                                                                                 .createStoragePathIfMissing(
                                                                                       false)
                                                                                 .storageStrategy(
                                                                                       storageStrategy)
                                                                                 .build());

                  assertThatExceptionOfType(DocumentAccessException.class).isThrownBy(() -> repository.saveDocument(
                        stubDocument(DocumentPath.of("file.pdf")).build(),
                        false));

                  DocumentRepository createStorageIfMissingRepository =
                        new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                       .storagePath(storagePath)
                                                       .failFastIfMissingStoragePath(false)
                                                       .createStoragePathIfMissing(true)
                                                       .storageStrategy(storageStrategy)
                                                       .build());

                  assertThat(createStorageIfMissingRepository.saveDocument(stubDocument(DocumentPath.of(
                        "file.pdf")).build(), false)).isPresent();
               } finally {
                  FileUtils.deleteDirectory(storagePath.toFile());
               }
            }

            @ParameterizedTest
            @ValueSource(classes = { DirectFsStorageStrategy.class, HexTreeFsStorageStrategy.class })
            public void testOpenDocumentWhenNotExistingStoragePath(Class<FsStorageStrategy> storageStrategy)
                  throws IOException {
               Path storagePath = Paths.get(System.getProperty("java.io.tmpdir", "/tmp"), "test-document-fs");

               try {
                  DocumentRepository repository = new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                                                 .storagePath(storagePath)
                                                                                 .failFastIfMissingStoragePath(
                                                                                       false)
                                                                                 .createStoragePathIfMissing(
                                                                                       false)
                                                                                 .storageStrategy(
                                                                                       storageStrategy)
                                                                                 .build());

                  assertThatExceptionOfType(DocumentAccessException.class).isThrownBy(() -> repository.openDocument(
                        DocumentPath.of("file.pdf"),
                        false,
                        false,
                        OpenDocumentMetadataBuilder.empty()));

                  DocumentRepository createStorageIfMissingRepository =
                        new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                       .storagePath(storagePath)
                                                       .failFastIfMissingStoragePath(false)
                                                       .createStoragePathIfMissing(true)
                                                       .storageStrategy(storageStrategy)
                                                       .build());

                  assertThat(createStorageIfMissingRepository.openDocument(DocumentPath.of("file.pdf"),
                                                                           false,
                                                                           false,
                                                                           OpenDocumentMetadataBuilder.empty())).isPresent();
               } finally {
                  FileUtils.deleteDirectory(storagePath.toFile());
               }
            }
         }

         @Nested
         public class WhenFailFastIfMissingStoragePath {

            @ParameterizedTest
            @ValueSource(classes = { DirectFsStorageStrategy.class, HexTreeFsStorageStrategy.class })
            public void testRepositoryWhenFailFastIfMissingStoragePath(Class<FsStorageStrategy> storageStrategy)
                  throws IOException {
               Path storagePath = Paths.get(System.getProperty("java.io.tmpdir", "/tmp"), "test-document-fs");

               try {
                  assertThatExceptionOfType(DocumentAccessException.class)
                        .isThrownBy(() -> new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                                         .storagePath(storagePath)
                                                                         .createStoragePathIfMissing(false)
                                                                         .failFastIfMissingStoragePath(true)
                                                                         .storageStrategy(storageStrategy)
                                                                         .build()))
                        .withMessage("Missing '%s' filesystem storage path", storagePath);

                  new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                 .storagePath(storagePath)
                                                 .createStoragePathIfMissing(true)
                                                 .failFastIfMissingStoragePath(true)
                                                 .storageStrategy(storageStrategy)
                                                 .build());
               } finally {
                  FileUtils.deleteDirectory(storagePath.toFile());
               }
            }

            @ParameterizedTest
            @ValueSource(classes = { DirectFsStorageStrategy.class, HexTreeFsStorageStrategy.class })
            public void testRepositoryWhenBasePathAndFailFastIfMissingStoragePath(Class<FsStorageStrategy> storageStrategy)
                  throws IOException {
               Path storagePath = Paths.get(System.getProperty("java.io.tmpdir", "/tmp"), "test-document-fs");

               try {
                  assertThatExceptionOfType(DocumentAccessException.class)
                        .isThrownBy(() -> new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                                         .storagePath(storagePath)
                                                                         .basePath(Paths.get("basepath"))
                                                                         .createStoragePathIfMissing(false)
                                                                         .failFastIfMissingStoragePath(true)
                                                                         .storageStrategy(storageStrategy)
                                                                         .build()))
                        .withMessage("Missing '%s' filesystem storage path", storagePath);

                  new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                 .storagePath(storagePath)
                                                 .basePath(Paths.get("basepath"))
                                                 .createStoragePathIfMissing(true)
                                                 .failFastIfMissingStoragePath(true)
                                                 .storageStrategy(storageStrategy)
                                                 .build());
               } finally {
                  FileUtils.deleteDirectory(storagePath.toFile());
               }
            }

         }
      }

      @ParameterizedTest
      @ValueSource(classes = { DirectFsStorageStrategy.class })
      public void testSubPathWhenSupportedStorageStrategies(Class<FsStorageStrategy> storageStrategy) {
         Path documentPath = path("path/subpath/subpathfile1.pdf");

         try {
            createDocuments(documentPath);

            DocumentRepository repository = new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                                           .storagePath(TEST_STORAGE_PATH)
                                                                           .storageStrategy(storageStrategy)
                                                                           .build());
            repository = repository.subPath(Paths.get("path"), false);

            assertThat(repository.findDocumentById(DocumentPath.of("subpath/subpathfile1.pdf"))).hasValueSatisfying(
                  document -> {
                     assertThat(document.documentId()).isEqualTo(DocumentPath.of("subpath/subpathfile1.pdf"));
                  });

         } finally {
            deleteDocuments(documentPath);
         }
      }

      @ParameterizedTest
      @ValueSource(classes = { HexTreeFsStorageStrategy.class })
      public void testSubPathWhenUnsupportedStorageStrategies(Class<FsStorageStrategy> storageStrategy) {
         Path documentPath = path("path/subpath/subpathfile1.pdf");

         try {
            createDocuments(documentPath);

            DocumentRepository repository = new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                                           .storagePath(TEST_STORAGE_PATH)
                                                                           .storageStrategy(storageStrategy)
                                                                           .build());
            assertThatExceptionOfType(UnsupportedCapabilityException.class)
                  .isThrownBy(() -> repository.subPath(Paths.get("path"), false))
                  .withMessage("Unsupported 'SUBPATH' (sub-path repository) capability");
         } finally {
            deleteDocuments(documentPath);
         }
      }

   }
}