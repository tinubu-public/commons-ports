/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.classpath;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.UnaryOperator;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.criterion.Criterion.CriterionBuilder;
import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentEntryCriteria.DocumentEntryCriteriaBuilder;
import com.tinubu.commons.ports.document.domain.DocumentMetadata;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.testsuite.ReadOnlyDocumentRepositoryTest;

public class ClasspathDocumentRepositoryTest extends ReadOnlyDocumentRepositoryTest {

   private static final Path TEST_CLASSPATH_PREFIX = path("/com/tinubu/commons/ports/document/classpath");

   private ClasspathDocumentRepository documentRepository;

   @Override
   protected boolean isSupportingContentLength() {
      return false;
   }

   @Override
   protected boolean isSupportingMetadataAttributes() {
      return false;
   }

   @BeforeEach
   public void configureDocumentRepository() {
      this.documentRepository = newClasspathDocumentRepository();
   }

   @AfterEach
   public void closeDocumentRepository() {
      this.documentRepository.close();
   }

   @Override
   protected DocumentRepository documentRepository() {
      return documentRepository;
   }

   @Override
   protected Document zipTransformer(DocumentPath zipPath, List<Document> documents) {
      System.out.println(String.format("Use '%s' ZIP test document", zipPath.stringValue()));
      return documentRepository()
            .findDocumentById(zipPath)
            .orElseThrow(() -> new IllegalStateException(String.format(
                  "'%s' ZIP file must exist on test classpath",
                  zipPath)));
   }

   private ClasspathDocumentRepository newClasspathDocumentRepository() {
      return newClasspathDocumentRepository(TEST_CLASSPATH_PREFIX);
   }

   private ClasspathDocumentRepository newClasspathDocumentRepository(Path classpathPrefix) {
      return new ClasspathDocumentRepository(classpathPrefix);
   }

   @Override
   protected UnaryOperator<DocumentMetadataBuilder> synchronizeExpectedMetadata(DocumentMetadata actual) {
      return builder -> builder
            .chain(super.synchronizeExpectedMetadata(actual))
            .attributes(actual.attributes())
            .contentType(actual.contentEncoding().orElse(null));
   }

   @Test
   public void testClassPathPrefixWhenNominal() {
      ClasspathDocumentRepository repository =
            newClasspathDocumentRepository(path("/com/tinubu/commons/ports/document/classpath"));

      assertThat(repository.findDocumentById(DocumentPath.of("path/pathfile1.pdf"))).isPresent();
   }

   @Test
   public void testClassPathPrefixWhenNull() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newClasspathDocumentRepository(null))
            .withMessage("Invariant validation error > 'classpathPrefix' must not be null");
   }

   @Test
   public void testClassPathPrefixWhenRelative() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newClasspathDocumentRepository(Paths.get(
                  "com/tinubu/commons/ports/document/classpath")))
            .withMessage(
                  "Invariant validation error > 'classpathPrefix=com/tinubu/commons/ports/document/classpath' must be absolute path");
   }

   @Test
   public void testClassPathPrefixWhenEmpty() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> newClasspathDocumentRepository(Paths.get("")))
            .withMessage("Invariant validation error > 'classpathPrefix=' must be absolute path");
   }

   @Test
   public void testClassPathPrefixWhenRoot() {
      ClasspathDocumentRepository repository = newClasspathDocumentRepository(path("/"));

      assertThat(repository.findDocumentById(DocumentPath.of(
            "com/tinubu/commons/ports/document/classpath/path/pathfile1.pdf"))).isPresent();
   }

   @Test
   public void testFindDocumentEntriesBySpecificationWhenPathContain() {
      List<Document> documents = documentRepository()
            .findDocumentsBySpecification(new DocumentEntryCriteriaBuilder()
                                                .documentPath(CriterionBuilder.contain(path("path")))
                                                .build())
            .collect(toList());

      assertThat(documents)
            .extracting(entry -> entry.documentId().stringValue())
            .containsExactlyInAnyOrder("path/pathfile1.pdf",
                                       "path/pathfile2.txt",
                                       "path/subpath/subpathfile1.pdf",
                                       "path/subpath/subpathfile2.txt");
   }

   @Test
   public void testFindDocumentEntriesBySpecificationWhenPathNotContain() {
      List<Document> documents = documentRepository()
            .findDocumentsBySpecification(path("path"),
                                          new DocumentEntryCriteriaBuilder()
                                                .documentPath(CriterionBuilder.notContain(path("subpath")))
                                                .build())
            .collect(toList());

      assertThat(documents)
            .extracting(entry -> entry.documentId().stringValue())
            .containsExactlyInAnyOrder("path/pathfile1.pdf", "path/pathfile2.txt");
   }

   @Test
   public void testFindDocumentEntriesBySpecificationWhenPathMatch() {
      List<Document> documents = documentRepository()
            .findDocumentsBySpecification(new DocumentEntryCriteriaBuilder()
                                                .documentPath(CriterionBuilder.match(path("path/**/*.pdf")))
                                                .build())
            .collect(toList());

      assertThat(documents)
            .extracting(entry -> entry.documentId().stringValue())
            .containsExactlyInAnyOrder("path/subpath/subpathfile1.pdf");
   }

   @Nested
   public class UriAdapter {

      @Test
      public void testFromUriWhenNominal() {
         assertThat(ClasspathDocumentRepository.fromUri(uri("classpath:/path"))).isPresent();
      }

      @Test
      public void testFromUriWhenBadParameters() {
         assertThatExceptionOfType(InvariantValidationException.class)
               .isThrownBy(() -> ClasspathDocumentRepository.fromUri(null))
               .withMessage("Invariant validation error > 'uri' must not be null");
      }

      @Test
      public void testFromUriWhenCompatibleUri() {
         assertThat(ClasspathDocumentRepository.fromUri(uri("classpath:/path/file.txt")))
               .as("Here file.txt is considered as part of the classpath prefix directory")
               .isPresent();
         assertThat(ClasspathDocumentRepository.fromUri(uri("classpath:/"))).isPresent();
      }

      @Test
      public void testFromUriWhenNotCompatibleUri() {
         assertThat(ClasspathDocumentRepository.fromUri(uri("classpath:path"))).isEmpty();
         assertThat(ClasspathDocumentRepository.fromUri(uri("other:/path"))).isEmpty();
         assertThat(ClasspathDocumentRepository.fromUri(uri("classpath:/path?query"))).isEmpty();
         assertThat(ClasspathDocumentRepository.fromUri(uri("classpath:/path#fragment"))).isEmpty();
         assertThat(ClasspathDocumentRepository.fromUri(uri("classpath://host/path"))).isEmpty();
         assertThat(ClasspathDocumentRepository.fromUri(uri("classpath://user@host/path"))).isEmpty();
      }

      @Test
      public void testToUriWhenNominal() {
         assertThat(documentRepository().toUri(DocumentPath.of("file.txt"))).isEqualTo(uri(
               "classpath:/com/tinubu/commons/ports/document/classpath/file.txt"));
         assertThat(documentRepository().toUri(DocumentPath.of("path/file.txt"))).isEqualTo(uri(
               "classpath:/com/tinubu/commons/ports/document/classpath/path/file.txt"));
      }

      @Test
      public void testSupportsDocumentUriWhenNominal() {
         assertThat(documentRepository().supportsUri(uri(
               "classpath:/com/tinubu/commons/ports/document/classpath/path/test.txt"), true)).isTrue();
      }

      @Test
      public void testSupportsDocumentUriWhenRepositoryUri() {
         assertThat(documentRepository().supportsUri(uri(
               "classpath:/com/tinubu/commons/ports/document/classpath"), true)).isFalse();
      }

      @Test
      public void testSupportsDocumentUriWhenBadPrefix() {
         assertThat(documentRepository().supportsUri(uri("classpath:/bad/path/test.txt"), true)).isFalse();
      }

      @Test
      public void testSupportsDocumentRepositoryUriWhenAlternativeForms() {
         assertThat(documentRepository().supportsUri(uri(
               "classpath:///com/tinubu/commons/ports/document/classpath/path/test.txt"), true)).isTrue();
         assertThat(documentRepository().supportsUri(uri(
               "classpath:com/tinubu/commons/ports/document/classpath/path/test.txt"), true)).isFalse();
         assertThat(documentRepository().supportsUri(uri(
                                                           "classpath://host/com/tinubu/commons/ports/document/classpath/path/test.txt"),
                                                     true)).isFalse();
         assertThat(documentRepository().supportsUri(uri(
                                                           "classpath:/com/tinubu/commons/ports/document/classpath/path/test.txt#fragment"),
                                                     true)).isFalse();
         assertThat(documentRepository().supportsUri(uri(
                                                           "classpath:/com/tinubu/commons/ports/document/classpath/path/test.txt?query"),
                                                     true)).isFalse();
      }

      @Test
      public void testToRepositoryUriWhenNominal() {
         assertThat(documentRepository().toUri()).isEqualTo(uri(
               "classpath:/com/tinubu/commons/ports/document/classpath"));
      }

      @Test
      public void testSupportsRepositoryUriWhenNominal() {
         assertThat(documentRepository().supportsUri(uri(
               "classpath:/com/tinubu/commons/ports/document/classpath"), false)).isTrue();
      }

      @Test
      public void testSupportsRepositoryUriWhenBadPrefix() {
         assertThat(documentRepository().supportsUri(uri("classpath:/bad"), false)).isFalse();
      }

      @Test
      public void testSupportsRepositoryUriWhenDocumentUri() {
         assertThat(documentRepository().supportsUri(uri(
               "classpath:/com/tinubu/commons/ports/document/classpath/path/test.txt"), false)).isTrue();
      }

      @Test
      public void testFindDocumentByUriWhenNominal() {
         DocumentPath documentPath = DocumentPath.of("path/pathfile2.txt");

         try {
            createDocuments(documentPath);

            assertThat(documentRepository().findDocumentByUri(uri(
                  "classpath:/com/tinubu/commons/ports/document/classpath/path/pathfile2.txt"))).hasValueSatisfying(
                  document -> {
                     assertThat(document.documentId()).isEqualTo(DocumentPath.of("path/pathfile2.txt"));
                     assertThat(document
                                      .metadata()
                                      .documentPath()).isEqualTo(Paths.get("path/pathfile2.txt"));
                  });

         } finally {
            deleteDocuments(documentPath);
         }
      }

      @Test
      public void testFindDocumentByUriWhenRepositoryUri() {
         assertThatIllegalArgumentException()
               .isThrownBy(() -> documentRepository().findDocumentByUri(uri(
                     "classpath:/com/tinubu/commons/ports/document/classpath")))
               .withMessage(
                     "'documentUri' must be supported : classpath:/com/tinubu/commons/ports/document/classpath");
      }

   }
}