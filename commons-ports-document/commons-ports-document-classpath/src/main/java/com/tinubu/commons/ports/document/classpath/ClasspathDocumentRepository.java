/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.classpath;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isAbsolute;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.ddd2.invariant.rules.PredicateRules.satisfies;
import static com.tinubu.commons.lang.util.CollectionUtils.collection;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.DOCUMENT_URI;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.ITERABLE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.QUERYABLE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.READABLE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.REPOSITORY_URI;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.WRITABLE;
import static java.util.Objects.requireNonNull;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.function.BiFunction;
import java.util.stream.Stream;

import org.apache.commons.lang3.time.StopWatch;

import com.tinubu.commons.ddd2.domain.event.RegistrableDomainEventService;
import com.tinubu.commons.ddd2.domain.event.SynchronousDomainEventService;
import com.tinubu.commons.ddd2.domain.repository.Repository;
import com.tinubu.commons.ddd2.domain.specification.Specification;
import com.tinubu.commons.ddd2.invariant.rules.UriRules;
import com.tinubu.commons.lang.validation.CheckReturnValue;
import com.tinubu.commons.ports.document.classpath.resourceloader.ClasspathResource;
import com.tinubu.commons.ports.document.classpath.resourceloader.ClasspathResourceLoader;
import com.tinubu.commons.ports.document.classpath.resourceloader.ClasspathResourceLoaderFactory;
import com.tinubu.commons.ports.document.domain.AbstractDocumentRepository;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentEntry.DocumentEntryBuilder;
import com.tinubu.commons.ports.document.domain.DocumentEntrySpecification;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata;
import com.tinubu.commons.ports.document.domain.UriAdapter;
import com.tinubu.commons.ports.document.domain.capability.RepositoryCapability;
import com.tinubu.commons.ports.document.domain.capability.UnsupportedCapabilityException;
import com.tinubu.commons.ports.document.transformer.DocumentTransformerUriAdapter;

/**
 * Classpath {@link DocumentRepository} adapter implementation.
 * Limitations :
 * <ul>
 *    <li>No write operation supported</li>
 *    <li>the following Metadata are not persisted: documentPath, creationDate, lastUpdateDate, contentEncoding, contentType, contentSize, attributes</li>
 * </ul>
 *
 * @implSpec Immutable class implementation
 */
public class ClasspathDocumentRepository extends AbstractDocumentRepository {
   private static final String URI_SCHEME = "classpath";
   private static final HashSet<RepositoryCapability> CAPABILITIES =
         collection(HashSet::new, READABLE, ITERABLE, QUERYABLE, REPOSITORY_URI, DOCUMENT_URI);

   private final Path classpathPrefix;
   private final DocumentTransformerUriAdapter transformerUriAdapter;
   private final ClasspathResourceLoader resourceLoader;

   public ClasspathDocumentRepository(Path classpathPrefix, RegistrableDomainEventService eventService) {
      super(eventService);

      this.transformerUriAdapter = new DocumentTransformerUriAdapter(new ClasspathUriAdapter());
      this.classpathPrefix = validate(classpathPrefix,
                                      "classpathPrefix",
                                      isNotNull().andValue(isAbsolute().andValue(hasNoTraversal())))
            .orThrow()
            .normalize();
      resourceLoader = ClasspathResourceLoaderFactory.resourceLoader();
   }

   public ClasspathDocumentRepository(Path classpathPrefix) {
      this(classpathPrefix, new SynchronousDomainEventService());
   }

   public ClasspathDocumentRepository() {
      this(Paths.get("/"));
   }

   /**
    * Returns an instance from document repository URI information, only if URI is
    * compatible with this repository.
    *
    * @param uri repository URI, without document
    *
    * @return pre-configured repository or {@link Optional#empty} if URI is not compatible with this repository.
    */
   public static Optional<ClasspathDocumentRepository> fromUri(URI uri) {
      validate(uri, "uri", isNotNull()).orThrow();

      return optional(uri)
            .filter(u -> isCompatibleUri(u, false))
            .map(u -> new ClasspathDocumentRepository(Paths.get(u.getPath())));
   }

   @Override
   public HashSet<RepositoryCapability> capabilities() {
      return CAPABILITIES;
   }

   @Override
   public boolean sameRepositoryAs(Repository<Document, DocumentPath> documentRepository) {
      validate(documentRepository, "documentRepository", isNotNull()).orThrow();

      return documentRepository instanceof ClasspathDocumentRepository
             && Objects.equals(((ClasspathDocumentRepository) documentRepository).classpathPrefix,
                               classpathPrefix);
   }

   @Override
   public ClasspathDocumentRepository subPath(Path subPath, boolean shareContext) {
      validate(subPath, "subPath", isNotAbsolute().andValue(hasNoTraversal())).orThrow();

      return new ClasspathDocumentRepository(classpathPrefix.resolve(subPath), eventService);
   }

   @Override
   @CheckReturnValue
   public Optional<Document> openDocument(DocumentPath documentId,
                                          boolean overwrite,
                                          boolean append,
                                          OpenDocumentMetadata metadata) {
      throw new UnsupportedCapabilityException(WRITABLE);
   }

   @Override
   public Optional<Document> findDocumentById(DocumentPath documentId) {
      validate(documentId, "documentId", isNotNull()).orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEvent(() -> resource(documentId).map(r -> document(documentEntry(documentId), r)),
                                 d -> documentAccessed(d.documentEntry(), watch));
   }

   @Override
   public Stream<Document> findDocumentsBySpecification(Path basePath,
                                                        Specification<DocumentEntry> specification) {
      validate(basePath, "basePath", isNotAbsolute().andValue(hasNoTraversal()))
            .and(validate(specification, "documentId", isNotNull()))
            .orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentStreamEvent(() -> documentContentAutoCloseOnStreamClose(listResources(basePath,
                                                                                                 specification,
                                                                                                 (resource, documentEntry) -> document(
                                                                                                       documentEntry,
                                                                                                       resource))),
                                       d -> documentAccessed(d.documentEntry(), watch));
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryById(DocumentPath documentId) {
      validate(documentId, "documentId", isNotNull()).orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEntryEvent(() -> resource(documentId).map(r -> documentEntry(documentId)),
                                      d -> documentAccessed(d, watch));
   }

   @Override
   public Stream<DocumentEntry> findDocumentEntriesBySpecification(Path basePath,
                                                                   Specification<DocumentEntry> specification) {
      validate(basePath, "basePath", isNotAbsolute().andValue(hasNoTraversal()))
            .and(validate(specification, "specification", isNotNull()))
            .orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEntryStreamEvent(() -> listResources(basePath,
                                                                specification,
                                                                (__, documentEntry) -> documentEntry),
                                            d -> documentAccessed(d, watch));
   }

   @Override
   @CheckReturnValue
   public Optional<DocumentEntry> saveDocument(Document document, boolean overwrite) {
      throw new UnsupportedCapabilityException(WRITABLE);
   }

   @Override
   @CheckReturnValue
   public Optional<Document> saveAndReturnDocument(Document document, boolean overwrite) {
      throw new UnsupportedCapabilityException(WRITABLE);
   }

   @Override
   @CheckReturnValue
   public Optional<DocumentEntry> deleteDocumentById(DocumentPath documentId) {
      throw new UnsupportedCapabilityException(WRITABLE);
   }

   @Override
   public List<DocumentEntry> deleteDocumentsBySpecification(Path basePath,
                                                             Specification<DocumentEntry> specification) {
      throw new UnsupportedCapabilityException(WRITABLE);
   }

   /**
    * Checks if specified URI is compatible with this repository, independently to current repository
    * configuration
    *
    * @param uri URI
    * @param documentUri whether specified URI is a document URI or any supported URI
    *
    * @return {@code true} if specified URI is compatible with this repository
    *
    * @see #supportsUri(URI, boolean)
    */
   public static boolean isCompatibleUri(URI uri, boolean documentUri) {
      validate(uri, "uri", isNotNull()).orThrow();

      if (!(uri.isAbsolute()
            && uri.getScheme().equals(URI_SCHEME)
            && uri.getPath() != null
            && uri.getAuthority() == null
            && uri.getFragment() == null
            && uri.getQuery() == null)) {
         return false;
      }

      return !documentUri || !Paths.get(uri.getPath()).equals(Paths.get("/"));
   }

   @Override
   public boolean supportsUri(URI uri, boolean documentUri) {
      return transformerUriAdapter.supportsUri(uri, documentUri);
   }

   @Override
   public URI toUri() {
      return transformerUriAdapter.toUri();
   }

   @Override
   public URI toUri(DocumentPath documentId) {
      return transformerUriAdapter.toUri(documentId);
   }

   @Override
   public Optional<Document> findDocumentByUri(URI documentUri) {
      return transformerUriAdapter.findDocumentByUri(documentUri);
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryByUri(URI documentUri) {
      return transformerUriAdapter.findDocumentEntryByUri(documentUri);
   }

   /**
    * Real classpath URI adapter to be used by {@link DocumentTransformerUriAdapter}.
    */
   public class ClasspathUriAdapter implements UriAdapter {
      @Override
      public boolean supportsUri(URI uri, boolean documentUri) {
         validate(uri, "uri", isNotNull()).orThrow();

         if (!isCompatibleUri(uri, documentUri)) {
            return false;
         }

         if (!Paths.get(uri.getPath()).startsWith(classpathPrefix)) {
            return false;
         }

         return !documentUri || !classpathPrefix.equals(Paths.get(uri.getPath()));
      }

      @Override
      public URI toUri(DocumentPath documentId) {
         validate(documentId, "documentId", isNotNull()).orThrow();

         try {
            return new URI(URI_SCHEME, null, classpathPrefix.resolve(documentId.value()).toString(), null);
         } catch (URISyntaxException e) {
            throw new IllegalStateException(e);
         }
      }

      @Override
      public URI toUri() {
         try {
            return new URI(URI_SCHEME, null, classpathPrefix.toString(), null);
         } catch (URISyntaxException e) {
            throw new IllegalStateException(e);
         }
      }

      @Override
      public Optional<Document> findDocumentByUri(URI documentUri) {
         validate(documentUri,
                  "documentUri",
                  UriRules
                        .hasNoTraversal()
                        .andValue(satisfies(this::supportsDocumentUri,
                                            "'%s' must be supported",
                                            validatingObject()))).orThrow();

         return findDocumentById(documentId(documentUri));
      }

      @Override
      public Optional<DocumentEntry> findDocumentEntryByUri(URI documentUri) {
         validate(documentUri,
                  "documentUri",
                  UriRules
                        .hasNoTraversal()
                        .andValue(satisfies(this::supportsDocumentUri,
                                            "'%s' must be supported",
                                            validatingObject()))).orThrow();

         return findDocumentEntryById(documentId(documentUri));
      }

      /**
       * Extracts document identifier from specified document URI.
       *
       * @param documentUri document URI
       *
       * @return document identifier
       */
      protected DocumentPath documentId(URI documentUri) {
         return DocumentPath.of(classpathPrefix.relativize(Paths.get(documentUri.getPath())));
      }
   }

   private DocumentEntry documentEntry(DocumentPath documentId) {
      return new DocumentEntryBuilder()
            .<DocumentEntryBuilder>reconstitute()
            .documentId(documentId)
            .metadata(new DocumentMetadataBuilder().documentPath(documentId.value()).build())
            .build();
   }

   private Document document(DocumentEntry documentEntry, ClasspathResource resource) {
      try {
         return new DocumentBuilder()
               .<DocumentBuilder>reconstitute()
               .documentEntry(documentEntry)
               .streamContent(resource.content())
               .build();
      } catch (IOException e) {
         throw new DocumentAccessException(String.format("Can't read '%s'",
                                                         documentEntry.documentId().stringValue()), e);
      }
   }

   private Optional<ClasspathResource> resource(DocumentPath documentId) {
      try {
         return resourceLoader.resource(classpathPrefix.resolve(documentId.value()));
      } catch (IOException e) {
         throw new DocumentAccessException(e);
      }

   }

   /**
    * Returns {@link ClasspathResource} directly present in specified path.
    *
    * @param path path to list, or {@code null}
    *
    * @return resource stream
    */
   private Stream<ClasspathResource> directResources(Path path) {
      try {
         return resourceLoader.resources(classpathPrefix.resolve(nullable(path, () -> Paths.get(""))));
      } catch (IOException e) {
         throw new DocumentAccessException(e);
      }
   }

   /**
    * Searches for resources in specified base path and matching specification.
    *
    * @param basePath search base path
    * @param specification search specification, should be a {@link DocumentEntrySpecification} for
    *       search optimizations
    * @param mapper mapper for found resources
    *
    * @return found resources as document entries
    */
   private <T> Stream<T> listResources(Path basePath,
                                       Specification<DocumentEntry> specification,
                                       BiFunction<ClasspathResource, DocumentEntry, T> mapper) {
      if (specification instanceof DocumentEntrySpecification) {
         if (!((DocumentEntrySpecification) specification).satisfiedBySubPath(basePath)) {
            return stream();
         }
      }

      return directResources(basePath).flatMap(resource -> {
         Path currentPath = basePath.resolve(requireNonNull(resource.name()));

         try {
            if (resource.isDirectory()) {
               return listResources(currentPath, specification, mapper);
            } else {
               return stream(documentEntry(DocumentPath.of(currentPath)))
                     .filter(specification::satisfiedBy)
                     .map(documentEntry -> mapper.apply(resource, documentEntry));
            }
         } catch (IOException e) {
            throw new DocumentAccessException(e);
         }
      });
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", ClasspathDocumentRepository.class.getSimpleName() + "[", "]")
            .add("classPathPrefix=" + classpathPrefix)
            .toString();
   }
}
