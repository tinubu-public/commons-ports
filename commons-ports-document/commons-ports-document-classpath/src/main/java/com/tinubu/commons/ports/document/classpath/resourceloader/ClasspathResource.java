/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.classpath.resourceloader;

import java.io.IOException;
import java.io.InputStream;

/**
 * Classpath resource abstraction.
 */
public interface ClasspathResource {

   /**
    * Resource name, without path.
    *
    * @return resource name
    */
   String name();

   /**
    * Returns {@code true} if resource is a "directory".
    * "Directory" here represents a logical path directory, i.e.: an intermediate path element, whatever the
    * underlying classpath implementation.
    *
    * @return {@code true} if resource is a "directory"
    *
    * @throws IOException if an I/O error occurs
    */
   boolean isDirectory() throws IOException;

   /**
    * Returns resource content as a stream.
    *
    * @return resource content
    *
    * @throws IOException if an I/O error occurs
    */
   InputStream content() throws IOException;

}
