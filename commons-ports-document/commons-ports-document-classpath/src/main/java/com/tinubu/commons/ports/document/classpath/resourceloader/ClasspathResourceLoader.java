/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.classpath.resourceloader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Resource loader abstraction.
 */
public interface ClasspathResourceLoader {

   /**
    * Returns a single resource identified by specified path.
    *
    * @param resourcePath resource absolute path
    *
    * @return resource or {@link Optional#empty} if no resource found
    *
    * @throws IOException if I/O error occurs
    */
   Optional<ClasspathResource> resource(Path resourcePath) throws IOException;

   /**
    * Returns all resources in specified "directory" path.
    * "Directory" here represents a logical path directory, i.e.: an intermediate path element, whatever the
    * underlying classpath implementation.
    *
    * @param directoryPath classpath "directory" absolute path
    *
    * @return resources stream, never {@code null}
    *
    * @throws IOException if I/O error occurs
    */
   Stream<ClasspathResource> resources(Path directoryPath) throws IOException;

}
