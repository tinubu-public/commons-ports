/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.awss3;

import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.mimeType;
import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.parseMimeType;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_OCTET_STREAM;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_PDF;
import static com.tinubu.commons.lang.util.CollectionUtils.entry;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.OptionalUtils.peek;
import static com.tinubu.commons.ports.document.awss3.S3Utils.tryCreateBucket;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.io.IOException;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.UnaryOperator;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.event.Level;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.output.Slf4jLogConsumer;
import org.testcontainers.images.PullPolicy;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.AwsS3DocumentConfigBuilder;
import com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.HttpClientConfig;
import com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.ListingContentMode;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentMetadata;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata.OpenDocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.testsuite.CommonDocumentRepositoryTest;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ZipArchiver;

import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.metrics.LoggingMetricPublisher;
import software.amazon.awssdk.metrics.LoggingMetricPublisher.Format;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.S3ClientBuilder;
import software.amazon.awssdk.services.s3.S3Utilities;
import software.amazon.awssdk.services.s3.model.GetUrlRequest;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import software.amazon.awssdk.services.s3.model.Tag;
import software.amazon.awssdk.services.s3.model.Tagging;

@Testcontainers(disabledWithoutDocker = true)
public class AwsS3DocumentRepositoryTest {
   private static final Logger log = LoggerFactory.getLogger(AwsS3DocumentRepositoryTest.class);

   public static final DockerImageName LOCALSTACK_DOCKER_IMAGE = DockerImageName
         .parse(
               "localstack/localstack@sha256:b5c082a6d78d49fc4a102841648a8adeab4895f7d9a4ad042d7d485aed2da10d")
         .withTag("3.3.0");
   public static final String TEST_BUCKET = "test-bucket";
   /** @implNote Region must be set to {@code us-east-1} when using custom endpoint with S3. */
   private static final String TEST_REGION = "us-east-1";
   private static final int CHUNK_SIZE = 5 * 1024 * 1024;
   /** AWS S3 Metric publisher. Configure logback-test.xml to enable metric logging. */
   private static final LoggingMetricPublisher METRIC_PUBLISHER =
         LoggingMetricPublisher.create(Level.DEBUG, Format.PRETTY);

   @Container
   private static final GenericContainer<?> localstack = new GenericContainer<>(LOCALSTACK_DOCKER_IMAGE)
         .withImagePullPolicy(PullPolicy.alwaysPull())
         .withExposedPorts(4566)
         .withLogConsumer(new Slf4jLogConsumer(log));

   @AfterAll
   public static void afterAll() {
      localstack.close();
   }

   private static URL s3Url() {
      try {
         return URI
               .create("http://" + localstack.getContainerIpAddress() + ":" + localstack.getMappedPort(4566))
               .toURL();
      } catch (MalformedURLException e) {
         throw new IllegalStateException(e);
      }
   }

   private static AwsCredentialsProvider credentialsProvider() {
      return StaticCredentialsProvider.create(AwsBasicCredentials.create("test", "test"));
   }

   private abstract class AbstractAwsS3DocumentRepositoryTest extends CommonDocumentRepositoryTest {

      private AwsS3DocumentRepository documentRepository;

      @BeforeEach
      public void configureDocumentRepository() {
         assertThat(localstack.isRunning()).isTrue();

         documentRepository = new AwsS3DocumentRepository(awsS3Config().build(),
                                                          metricPublisher().andThen(awsS3ClientConfigurer()));
         documentRepository.deleteBucket(TEST_BUCKET);
      }

      protected Consumer<S3ClientBuilder> awsS3ClientConfigurer() {
         return __ -> { };
      }

      protected Consumer<S3ClientBuilder> metricPublisher() {
         return builder -> builder.overrideConfiguration(b -> b.addMetricPublisher(METRIC_PUBLISHER));
      }

      @AfterEach
      public void closeDocumentRepository() {
         this.documentRepository.close();
      }

      protected AwsS3DocumentConfigBuilder awsS3Config() {
         return new AwsS3DocumentConfigBuilder()
               .region(TEST_REGION)
               .pathStyleAccess(true)
               .endpoint(s3Url())
               .bucketName(TEST_BUCKET)
               .createIfMissingBucket(true)
               .failFastIfMissingBucket(false)
               .uploadChunkSize(CHUNK_SIZE)
               .credentialsProvider(credentialsProvider())
               .listingContentMode(ListingContentMode.FILTERING_COMPLETE);
      }

      protected S3Client buildS3Client(AwsS3DocumentConfig s3Config, boolean createBucket) {
         S3ClientBuilder s3ClientBuilder = S3Client
               .builder()
               .forcePathStyle(s3Config.pathStyleAccess())
               .region(Region.of(s3Config.region()))
               .credentialsProvider(s3Config.credentialsProvider());

         s3Config.endpoint().ifPresent(endpoint -> {
            try {
               s3ClientBuilder.endpointOverride(endpoint.toURI());
            } catch (URISyntaxException e) {
               throw new IllegalStateException(e);
            }
         });

         S3Client s3Client = s3ClientBuilder.build();

         if (createBucket) {
            tryCreateBucket(s3Client, s3Config.bucketName(), true);
         }

         return s3Client;
      }

      protected Tag tag(String key, String value) {
         return Tag.builder().key(key).value(value).build();
      }

      @Override
      protected long chunkSize() {
         return CHUNK_SIZE;
      }

      @Override
      protected DocumentRepository documentRepository() {
         return documentRepository;
      }

      @Override
      protected Document zipTransformer(DocumentPath zipPath, List<Document> documents) {
         return new ZipArchiver(zipPath).compress(documents);
      }

      @Override
      protected Optional<MimeType> defaultContentType() {
         return optional(APPLICATION_OCTET_STREAM);
      }

      @Override
      @Disabled("Not supported")
      public void testOpenDocumentWhenOverwriteAndAppend() { }

      @Override
      @Disabled("Not supported")
      public void testOpenDocumentWhenNotOverwriteAndAppend() { }

      @Override
      protected UnaryOperator<DocumentMetadataBuilder> synchronizeExpectedMetadata(DocumentMetadata actual) {
         return builder -> builder.chain(super.synchronizeExpectedMetadata(actual));
      }

      protected URI testS3CliUri(String bucket) {
         return URI.create("s3://" + bucket + "/");
      }

      protected URI testS3CliUri() {
         return testS3CliUri(TEST_BUCKET);
      }

      protected URI testS3StandardUri(String region, String bucket) {
         S3Utilities utils = S3Utilities.builder().region(Region.of(region)).build();
         return URI.create(utils
                                 .getUrl(GetUrlRequest
                                               .builder()
                                               .region(Region.of(region))
                                               .bucket(bucket)
                                               .key("/")
                                               .build())
                                 .toString());
      }

      protected URI testS3StandardUri() {
         return testS3StandardUri(TEST_REGION, TEST_BUCKET);
      }

      protected URI testS3StandardGlobalUri(String bucket) {
         return URI.create(testS3StandardUri(TEST_REGION, bucket)
                                 .toString()
                                 .replaceAll("s3\\.[^.]+\\.amazonaws.com", "s3.amazonaws.com"));
      }

      protected URI testS3StandardGlobalUri() {
         return testS3StandardGlobalUri(TEST_BUCKET);
      }

   }

   @Nested
   public class WhenCreateIfMissingBucket extends AbstractAwsS3DocumentRepositoryTest {

      /**
       * Bucket is deleted after each test to test missing bucket lazy creation in all operations.
       * failFastIfMissingBucket should be set to false for tests to be complete.
       */
      protected AwsS3DocumentConfigBuilder awsS3Config() {
         return super.awsS3Config().createIfMissingBucket(true).failFastIfMissingBucket(false);
      }

      /**
       * Document must be > chunk size for multipart upload to be enabled.
       */
      @Test
      public void testOpenDocumentWhenNoMultipart() {
         Path documentPath = path("path/pathfile1.pdf");
         DocumentPath testDocument = DocumentPath.of(documentPath);

         try {
            OpenDocumentMetadata metadata = new OpenDocumentMetadataBuilder()
                  .documentPath("path/alternative.pdf")
                  .contentType(mimeType(APPLICATION_PDF, UTF_8))
                  .build();

            String content = StringUtils.repeat('0', 10);
            Optional<Document> newDocument =
                  peek(documentRepository().openDocument(testDocument, true, false, metadata),
                       writeContent(content));

            assertThat(newDocument).isPresent();

            Optional<Document> document = documentRepository().findDocumentById(testDocument);

            assertThat(document)
                  .usingValueComparator(contentAgnosticDocumentComparator())
                  .hasValue(new DocumentBuilder()
                                  .<DocumentBuilder>reconstitute()
                                  .documentId(testDocument)
                                  .metadata(new DocumentMetadataBuilder()
                                                  .<DocumentMetadataBuilder>reconstitute()
                                                  .documentPath(Paths.get("path/alternative.pdf"))
                                                  .contentSize(isSupportingContentLength() ? 10L : null)
                                                  .contentType(parseMimeType("application/pdf;charset=UTF-8"))
                                                  .chain(synchronizeExpectedMetadata(document
                                                                                           .map(Document::metadata)
                                                                                           .orElse(null)))
                                                  .build())
                                  .loadedContent(content, UTF_8)
                                  .build());
         } finally {
            deleteDocuments(documentPath);
         }
      }

      /**
       * Document must be > chunk size for multipart upload to be enabled.
       */
      @Test
      public void testOpenDocumentWhenMultipart() {
         Path documentPath = path("path/pathfile1.pdf");
         DocumentPath testDocument = DocumentPath.of(documentPath);

         try {
            OpenDocumentMetadata metadata = new OpenDocumentMetadataBuilder()
                  .documentPath("path/alternative.pdf")
                  .contentType(mimeType(APPLICATION_PDF, UTF_8))
                  .build();

            String content = StringUtils.repeat('0', 2 * CHUNK_SIZE + 1);
            Optional<Document> newDocument =
                  peek(documentRepository().openDocument(testDocument, true, false, metadata),
                       writeContent(content));

            assertThat(newDocument).isPresent();

            Optional<Document> document = documentRepository().findDocumentById(testDocument);

            assertThat(document)
                  .usingValueComparator(contentAgnosticDocumentComparator())
                  .hasValue(new DocumentBuilder()
                                  .<DocumentBuilder>reconstitute()
                                  .documentId(testDocument)
                                  .metadata(new DocumentMetadataBuilder()
                                                  .<DocumentMetadataBuilder>reconstitute()
                                                  .documentPath(Paths.get("path/alternative.pdf"))
                                                  .contentSize(isSupportingContentLength() ? 2 * CHUNK_SIZE
                                                                                             + 1L : null)
                                                  .contentType(parseMimeType("application/pdf;charset=UTF-8"))
                                                  .chain(synchronizeExpectedMetadata(document
                                                                                           .map(Document::metadata)
                                                                                           .orElse(null)))
                                                  .build())
                                  .loadedContent(content, UTF_8)
                                  .build());
         } finally {
            deleteDocuments(documentPath);
         }
      }
   }

   @Nested
   public class WhenNominalBucketBasePath extends AbstractAwsS3DocumentRepositoryTest {
      protected AwsS3DocumentConfigBuilder awsS3Config() {
         return super.awsS3Config().bucketBasePath(Paths.get("basePath"));
      }

      @Nested
      public class UriAdapter {

         @Test
         public void testToDocumentUriWhenNominal() {
            assertThat(documentRepository().toUri(DocumentPath.of("file.txt"))).isEqualTo(testS3CliUri().resolve(
                  "basePath/file.txt"));
            assertThat(documentRepository().toUri(DocumentPath.of("path/file.txt"))).isEqualTo(testS3CliUri().resolve(
                  "basePath/path/file.txt"));
         }

         @Test
         public void testSupportsDocumentUriWhenNominal() {
            assertThat(documentRepository().supportsUri(testS3CliUri().resolve("basePath/path/test.txt"),
                                                        true)).isTrue();

            assertThat(documentRepository().supportsUri(testS3StandardUri().resolve("basePath/path/test.txt"),
                                                        true)).isTrue();
            assertThat(documentRepository().supportsUri(testS3StandardGlobalUri().resolve(
                  "basePath/path/test.txt"), true)).isTrue();
         }

         @Test
         public void testSupportsDocumentUriWhenBadRegion() {
            assertThat(documentRepository().supportsUri(testS3StandardUri("eu-west-2", TEST_BUCKET).resolve(
                  "basePath/path/test.txt"), true)).isFalse();
         }

         @Test
         public void testSupportsDocumentUriWhenBadBucket() {
            assertThat(documentRepository().supportsUri(testS3CliUri("other").resolve("basePath/path/test.txt"),
                                                        true)).isFalse();

            assertThat(documentRepository().supportsUri(testS3StandardUri(TEST_REGION, "other").resolve(
                  "basePath/path/test.txt"), true)).isFalse();
            assertThat(documentRepository().supportsUri(testS3StandardGlobalUri("other").resolve(
                  "basePath/path/test.txt"), true)).isFalse();
         }

         @Test
         public void testSupportsDocumentUriWhenRepositoryUri() {
            assertThat(documentRepository().supportsUri(testS3CliUri().resolve("basePath"), true)).isFalse();
            assertThat(documentRepository().supportsUri(testS3StandardUri().resolve("basePath"),
                                                        true)).isFalse();
            assertThat(documentRepository().supportsUri(testS3StandardGlobalUri().resolve("basePath"),
                                                        true)).isFalse();
         }

         @Test
         public void testSupportsDocumentRepositoryUriWhenAlternativeForms() {
            assertThat(documentRepository().supportsUri(testS3CliUri().resolve(
                  "basePath/path/test.txt#fragment"), true)).isTrue();
            assertThat(documentRepository().supportsUri(testS3CliUri().resolve("basePath/path/test.txt?query"),
                                                        true)).isTrue();
            assertThat(documentRepository().supportsUri(uri("s3://user@"
                                                            + TEST_BUCKET
                                                            + "/basePath/path/test.txt"), true)).isFalse();

            assertThat(documentRepository().supportsUri(testS3StandardUri().resolve(
                  "basePath/path/test.txt#fragment"), true)).isTrue();
            assertThat(documentRepository().supportsUri(testS3StandardUri().resolve(
                  "basePath/path/test.txt?query"), true)).isTrue();
            assertThat(documentRepository().supportsUri(uri("http://user@"
                                                            + TEST_BUCKET
                                                            + "/basePath/path/test.txt"), true)).isFalse();
            assertThat(documentRepository().supportsUri(uri("https://"
                                                            + TEST_BUCKET
                                                            + "/basePath/path/test.txt"), true)).isFalse();
         }

         @Test
         public void testToRepositoryUriWhenNominal() {
            assertThat(documentRepository().toUri()).isEqualTo(testS3CliUri().resolve("basePath"));
         }

         @Test
         public void testSupportsRepositoryUriWhenNominal() {
            assertThat(documentRepository().supportsUri(testS3CliUri().resolve("basePath"), false)).isTrue();
            assertThat(documentRepository().supportsUri(testS3StandardUri().resolve("basePath"),
                                                        false)).isTrue();
            assertThat(documentRepository().supportsUri(testS3StandardGlobalUri().resolve("basePath"),
                                                        false)).isTrue();
         }

         @Test
         public void testSupportsRepositoryUriWhenBadRegion() {
            assertThat(documentRepository().supportsUri(testS3StandardUri("eu-west-2", TEST_BUCKET).resolve(
                  "basePath"), false)).isFalse();
         }

         @Test
         public void testSupportsRepositoryUriWhenBadBucket() {
            assertThat(documentRepository().supportsUri(testS3CliUri("other").resolve("basePath"),
                                                        false)).isFalse();

            assertThat(documentRepository().supportsUri(testS3StandardUri(TEST_REGION, "other").resolve(
                  "basePath"), false)).isFalse();
            assertThat(documentRepository().supportsUri(testS3StandardGlobalUri("other").resolve("basePath"),
                                                        false)).isFalse();
         }

         @Test
         public void testSupportsRepositoryUriWhenDocumentUri() {
            assertThat(documentRepository().supportsUri(testS3CliUri().resolve("basePath/path/test.txt"),
                                                        false)).isTrue();
            assertThat(documentRepository().supportsUri(testS3StandardUri().resolve("basePath/path/test.txt"),
                                                        false)).isTrue();
            assertThat(documentRepository().supportsUri(testS3StandardGlobalUri().resolve(
                  "basePath/path/test.txt"), false)).isTrue();
         }
      }
   }

   @Nested
   public class WhenEmptyBucketBasePath extends AbstractAwsS3DocumentRepositoryTest {
      protected AwsS3DocumentConfigBuilder awsS3Config() {
         return super.awsS3Config().bucketBasePath(Paths.get(""));
      }

      @Nested
      public class UriAdapter {

         @Test
         public void testToDocumentUriWhenNominal() {
            assertThat(documentRepository().toUri(DocumentPath.of("file.txt"))).isEqualTo(testS3CliUri().resolve(
                  "file.txt"));
            assertThat(documentRepository().toUri(DocumentPath.of("path/file.txt"))).isEqualTo(testS3CliUri().resolve(
                  "path/file.txt"));
         }

         @Test
         public void testSupportsDocumentUriWhenNominal() {
            assertThat(documentRepository().supportsUri(testS3CliUri().resolve("path/test.txt"),
                                                        true)).isTrue();

            assertThat(documentRepository().supportsUri(testS3StandardUri().resolve("path/test.txt"),
                                                        true)).isTrue();
            assertThat(documentRepository().supportsUri(testS3StandardGlobalUri().resolve("path/test.txt"),
                                                        true)).isTrue();
         }

         @Test
         public void testSupportsDocumentUriWhenBadRegion() {
            assertThat(documentRepository().supportsUri(testS3StandardUri("eu-west-2", TEST_BUCKET).resolve(
                  "path/test.txt"), true)).isFalse();
         }

         @Test
         public void testSupportsDocumentUriWhenBadBucket() {
            assertThat(documentRepository().supportsUri(testS3CliUri("other").resolve("path/test.txt"),
                                                        true)).isFalse();

            assertThat(documentRepository().supportsUri(testS3StandardUri(TEST_REGION, "other").resolve(
                  "path/test.txt"), true)).isFalse();
            assertThat(documentRepository().supportsUri(testS3StandardGlobalUri("other").resolve(
                  "path/test.txt"), true)).isFalse();
         }

         @Test
         public void testSupportsDocumentUriWhenRepositoryUri() {
            assertThat(documentRepository().supportsUri(testS3CliUri(), true)).isFalse();
            assertThat(documentRepository().supportsUri(testS3StandardUri(), true)).isFalse();
            assertThat(documentRepository().supportsUri(testS3StandardGlobalUri(), true)).isFalse();
         }

         @Test
         public void testSupportsDocumentRepositoryUriWhenAlternativeForms() {
            assertThat(documentRepository().supportsUri(testS3CliUri().resolve("path/test.txt#fragment"),
                                                        true)).isTrue();
            assertThat(documentRepository().supportsUri(testS3CliUri().resolve("path/test.txt?query"),
                                                        true)).isTrue();
            assertThat(documentRepository().supportsUri(uri("s3://user@" + TEST_BUCKET + "/path/test.txt"),
                                                        true)).isFalse();

            assertThat(documentRepository().supportsUri(testS3StandardUri().resolve("path/test.txt#fragment"),
                                                        true)).isTrue();
            assertThat(documentRepository().supportsUri(testS3StandardUri().resolve("path/test.txt?query"),
                                                        true)).isTrue();
            assertThat(documentRepository().supportsUri(uri("http://user@" + TEST_BUCKET + "/path/test.txt"),
                                                        true)).isFalse();
            assertThat(documentRepository().supportsUri(uri("https://" + TEST_BUCKET + "/path/test.txt"),
                                                        true)).isFalse();
         }

         @Test
         public void testToRepositoryUriWhenNominal() {
            assertThat(documentRepository().toUri()).isEqualTo(testS3CliUri());
         }

         @Test
         public void testSupportsRepositoryUriWhenNominal() {
            assertThat(documentRepository().supportsUri(testS3CliUri(), false)).isTrue();
            assertThat(documentRepository().supportsUri(testS3StandardUri(), false)).isTrue();
            assertThat(documentRepository().supportsUri(testS3StandardGlobalUri(), false)).isTrue();
         }

         @Test
         public void testSupportsRepositoryUriWhenBadRegion() {
            assertThat(documentRepository().supportsUri(testS3StandardUri("eu-west-2", TEST_BUCKET),
                                                        false)).isFalse();
         }

         @Test
         public void testSupportsRepositoryUriWhenBadBucket() {
            assertThat(documentRepository().supportsUri(testS3CliUri("other"), false)).isFalse();

            assertThat(documentRepository().supportsUri(testS3StandardUri(TEST_REGION, "other"),
                                                        false)).isFalse();
            assertThat(documentRepository().supportsUri(testS3StandardGlobalUri("other"), false)).isFalse();
         }

         @Test
         public void testSupportsRepositoryUriWhenDocumentUri() {
            assertThat(documentRepository().supportsUri(testS3CliUri().resolve("path/test.txt"),
                                                        false)).isTrue();
            assertThat(documentRepository().supportsUri(testS3StandardUri().resolve("path/test.txt"),
                                                        false)).isTrue();
            assertThat(documentRepository().supportsUri(testS3StandardGlobalUri().resolve("path/test.txt"),
                                                        false)).isTrue();
         }
      }

   }

   @Nested
   public class WhenSlashTerminatedBucketBasePath extends AbstractAwsS3DocumentRepositoryTest {
      protected AwsS3DocumentConfigBuilder awsS3Config() {
         return super.awsS3Config().bucketBasePath(Paths.get("basePath/"));
      }
   }

   @Nested
   public class WhenBucketBasePathAndSubPath extends AbstractAwsS3DocumentRepositoryTest {
      protected AwsS3DocumentConfigBuilder awsS3Config() {
         return super.awsS3Config().bucketBasePath(Paths.get("basePath/subBasePath"));
      }
   }

   @Nested
   public class WhenMatchTags extends AbstractAwsS3DocumentRepositoryTest {
      protected AwsS3DocumentConfigBuilder awsS3Config() {
         return super.awsS3Config().matchTags(Pattern.compile("tagkey.*"), Pattern.compile("otherkey"));
      }

      @Test
      public void testFindDocumentByIdWhenMatchTags() {
         Path documentPath = path("path/pathfile1.pdf");
         DocumentPath testDocument = DocumentPath.of(documentPath);

         try {

            AwsS3DocumentConfig s3Config = awsS3Config().build();
            S3Client s3Client = buildS3Client(s3Config, true);
            s3Client.putObject(PutObjectRequest
                                     .builder()
                                     .bucket(s3Config.bucketName())
                                     .key(documentPath.toString())
                                     .contentLength(0L)
                                     .metadata(map(entry("tag0", "value0"),
                                                   entry("tagkey1", "metadatavalue1"),
                                                   entry("tagkey2", "metadatavalue2")))
                                     .tagging(Tagging
                                                    .builder()
                                                    .tagSet(tag("tag0", "tagvalue0"),
                                                            tag("tagkey1", "tagvalue1"),
                                                            tag("tagkey3", "tagvalue3"),
                                                            tag("extratag1", "extravalue1"))
                                                    .build())
                                     .build(), RequestBody.empty());

            assertThat(documentRepository().findDocumentById(testDocument)).hasValueSatisfying(document -> {
               assertThat(document.metadata().attributes()).isEqualTo(map(entry("tag0", "value0"),
                                                                          entry("tagkey1", "tagvalue1"),
                                                                          entry("tagkey2", "metadatavalue2"),
                                                                          entry("tagkey3", "tagvalue3")));
            });

         } finally {
            deleteDocuments(documentPath);
         }
      }

   }

   @Nested
   public class WhenLoadAllTags extends AbstractAwsS3DocumentRepositoryTest {
      protected AwsS3DocumentConfigBuilder awsS3Config() {
         return super
               .awsS3Config()
               .loadAllTags()
               .matchTags(Pattern.compile("tagkey.*"), Pattern.compile("otherkey"));
      }

      @Test
      public void testFindDocumentByIdWhenMatchTags() {
         Path documentPath = path("path/pathfile1.pdf");
         DocumentPath testDocument = DocumentPath.of(documentPath);

         try {

            AwsS3DocumentConfig s3Config = awsS3Config().build();
            S3Client s3Client = buildS3Client(s3Config, true);
            s3Client.putObject(PutObjectRequest
                                     .builder()
                                     .bucket(s3Config.bucketName())
                                     .key(documentPath.toString())
                                     .contentLength(0L)
                                     .metadata(map(entry("tag0", "value0"),
                                                   entry("tagkey1", "metadatavalue1"),
                                                   entry("tagkey2", "metadatavalue2")))
                                     .tagging(Tagging
                                                    .builder()
                                                    .tagSet(tag("tag0", "tagvalue0"),
                                                            tag("tagkey1", "tagvalue1"),
                                                            tag("tagkey3", "tagvalue3"),
                                                            tag("extratag1", "extravalue1"))
                                                    .build())
                                     .build(), RequestBody.empty());

            assertThat(documentRepository().findDocumentById(testDocument)).hasValueSatisfying(document -> {
               assertThat(document.metadata().attributes()).isEqualTo(map(entry("tag0", "tagvalue0"),
                                                                          entry("tagkey1", "tagvalue1"),
                                                                          entry("tagkey2", "metadatavalue2"),
                                                                          entry("tagkey3", "tagvalue3"),
                                                                          entry("extratag1", "extravalue1")));
            });

         } finally {
            deleteDocuments(documentPath);
         }
      }

   }

   @Nested
   public class WhenFailFast {

      private AwsS3DocumentRepository documentRepository;

      @BeforeEach
      public void configureDocumentRepository() {
         assertThat(localstack.isRunning()).isTrue();

         AwsS3DocumentConfig config = new AwsS3DocumentConfigBuilder()
               .region(TEST_REGION)
               .endpoint(s3Url())
               .bucketName(TEST_BUCKET)
               .pathStyleAccess(true)
               .createIfMissingBucket(true)
               .failFastIfMissingBucket(true)
               .credentialsProvider(credentialsProvider())
               .build();

         documentRepository = new AwsS3DocumentRepository(config);
         documentRepository.deleteBucket(TEST_BUCKET);
      }

      @Test
      public void testFailFastWhenMissingBucket() {
         AwsS3DocumentConfigBuilder config = new AwsS3DocumentConfigBuilder()
               .region(TEST_REGION)
               .endpoint(s3Url())
               .bucketName(TEST_BUCKET)
               .pathStyleAccess(true)
               .failFastIfMissingBucket(true)
               .uploadChunkSize(CHUNK_SIZE)
               .credentialsProvider(credentialsProvider());

         assertThatExceptionOfType(DocumentAccessException.class)
               .isThrownBy(() -> new AwsS3DocumentRepository(config.createIfMissingBucket(false).build()))
               .withMessage("Can't create 'test-bucket' missing bucket");

         new AwsS3DocumentRepository(config.createIfMissingBucket(true).build());
      }

   }

   @Nested
   public class WhenMissingBucket {

      private AwsS3DocumentRepository documentRepository;

      @BeforeEach
      public void configureDocumentRepository() {
         assertThat(localstack.isRunning()).isTrue();

         documentRepository = new AwsS3DocumentRepository(awsS3Config());
         documentRepository.deleteBucket(TEST_BUCKET);
      }

      private AwsS3DocumentConfig awsS3Config() {
         return new AwsS3DocumentConfigBuilder()
               .region(TEST_REGION)
               .endpoint(s3Url())
               .bucketName(TEST_BUCKET)
               .pathStyleAccess(true)
               .createIfMissingBucket(false)
               .failFastIfMissingBucket(false)
               .uploadChunkSize(CHUNK_SIZE)
               .credentialsProvider(credentialsProvider())
               .build();
      }

      @Test
      public void testOpenDocumentWithNoMultipartWhenMissingBucket() {
         assertThatExceptionOfType(DocumentAccessException.class)
               .isThrownBy(() -> documentRepository
                     .openDocument(DocumentPath.of("file.txt"),
                                   false,
                                   false,
                                   new OpenDocumentMetadataBuilder().contentEncoding(UTF_8).build())
                     .ifPresent(document -> {
                        try (Writer writer = document.content().writerContent()) {
                           writer.write("content");
                        } catch (IOException e) {
                           throw new IllegalStateException(e);
                        }
                     }))
               .withMessage("Can't create 'test-bucket' missing bucket");
      }

      /**
       * Document must be > chunk size for multipart upload to be enabled.
       */
      @Test
      public void testOpenDocumentWithMultipartWhenMissingBucket() {
         assertThatExceptionOfType(DocumentAccessException.class)
               .isThrownBy(() -> documentRepository
                     .openDocument(DocumentPath.of("file.txt"),
                                   false,
                                   false,
                                   new OpenDocumentMetadataBuilder().contentEncoding(UTF_8).build())
                     .ifPresent(document -> {
                        try (Writer writer = document.content().writerContent()) {
                           String content = StringUtils.repeat('0', 2 * CHUNK_SIZE + 1);
                           writer.write(content);
                        } catch (IOException e) {
                           throw new IllegalStateException(e);
                        }
                     }))
               .withMessage("Can't create 'test-bucket' missing bucket");
      }

      @Test
      public void testSaveDocumentWithNoMultipartWhenMissingBucket() {
         assertThatExceptionOfType(DocumentAccessException.class)
               .isThrownBy(() -> documentRepository.saveDocument(new DocumentBuilder()
                                                                       .documentId(DocumentPath.of("file.txt"))
                                                                       .loadedContent("content", UTF_8)
                                                                       .build(), false))
               .withMessage("Can't create 'test-bucket' missing bucket");
      }

      /**
       * Document must be > chunk size for multipart upload to be enabled.
       */
      @Test
      public void testSaveDocumentWithMultipartWhenMissingBucket() {
         String content = StringUtils.repeat('0', 2 * CHUNK_SIZE + 1);
         assertThatExceptionOfType(DocumentAccessException.class)
               .isThrownBy(() -> documentRepository.saveDocument(new DocumentBuilder()
                                                                       .documentId(DocumentPath.of("file.txt"))
                                                                       .streamContent(content, UTF_8)
                                                                       .build(), false))
               .withMessage("Can't create 'test-bucket' missing bucket");
      }
   }

   @Nested
   public class WhenOneMaxConnections extends AbstractAwsS3DocumentRepositoryTest {
      protected AwsS3DocumentConfigBuilder awsS3Config() {
         return super
               .awsS3Config()
               .httpClientConfig(new HttpClientConfig.Builder().maxConnections(1).build());
      }
   }

}
