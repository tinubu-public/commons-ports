/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.awss3;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.ddd2.invariant.rules.PredicateRules.satisfies;
import static com.tinubu.commons.lang.util.CollectionUtils.collectionConcat;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.util.StreamUtils.streamConcat;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.ListingContentMode.FILTERING_COMPLETE;
import static com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.ListingContentMode.RETURN_COMPLETE;
import static com.tinubu.commons.ports.document.awss3.S3Utils.documentPath;
import static com.tinubu.commons.ports.document.awss3.S3Utils.tryCreateBucket;
import static com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.DOCUMENT_URI;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.ITERABLE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.METADATA_ATTRIBUTES;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.METADATA_CONTENT_ENCODING;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.METADATA_CONTENT_SIZE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.METADATA_CONTENT_TYPE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.METADATA_DOCUMENT_PATH;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.METADATA_LAST_UPDATE_DATE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.OPEN;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.OPEN_APPEND;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.QUERYABLE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.READABLE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.REPOSITORY_URI;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.WRITABLE;
import static org.apache.commons.lang3.StringUtils.removeStart;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.time.StopWatch;

import com.tinubu.commons.ddd2.domain.event.RegistrableDomainEventService;
import com.tinubu.commons.ddd2.domain.event.SynchronousDomainEventService;
import com.tinubu.commons.ddd2.domain.repository.Repository;
import com.tinubu.commons.ddd2.domain.specification.Specification;
import com.tinubu.commons.ddd2.invariant.rules.UriRules;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.lang.mimetype.MimeTypeFactory;
import com.tinubu.commons.lang.validation.CheckReturnValue;
import com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.AwsS3DocumentConfigBuilder;
import com.tinubu.commons.ports.document.awss3.AwsS3DocumentConfig.ListingContentMode;
import com.tinubu.commons.ports.document.domain.AbstractDocumentRepository;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentContent;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentEntry.DocumentEntryBuilder;
import com.tinubu.commons.ports.document.domain.DocumentEntrySpecification;
import com.tinubu.commons.ports.document.domain.DocumentMetadata;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent.InputStreamDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata.OpenDocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent.OutputStreamDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.UriAdapter;
import com.tinubu.commons.ports.document.domain.capability.RepositoryCapability;
import com.tinubu.commons.ports.document.domain.capability.UnsupportedCapabilityException;
import com.tinubu.commons.ports.document.transformer.DocumentTransformerUriAdapter;

import software.amazon.awssdk.core.ResponseInputStream;
import software.amazon.awssdk.core.client.config.ClientOverrideConfiguration;
import software.amazon.awssdk.core.exception.SdkException;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.http.apache.ApacheHttpClient;
import software.amazon.awssdk.metrics.MetricPublisher;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.S3ClientBuilder;
import software.amazon.awssdk.services.s3.S3Uri;
import software.amazon.awssdk.services.s3.model.DeleteBucketRequest;
import software.amazon.awssdk.services.s3.model.DeleteObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectResponse;
import software.amazon.awssdk.services.s3.model.GetObjectTaggingRequest;
import software.amazon.awssdk.services.s3.model.GetObjectTaggingResponse;
import software.amazon.awssdk.services.s3.model.HeadObjectRequest;
import software.amazon.awssdk.services.s3.model.HeadObjectResponse;
import software.amazon.awssdk.services.s3.model.InvalidObjectStateException;
import software.amazon.awssdk.services.s3.model.ListObjectsV2Request;
import software.amazon.awssdk.services.s3.model.NoSuchBucketException;
import software.amazon.awssdk.services.s3.model.NoSuchKeyException;
import software.amazon.awssdk.services.s3.model.S3Object;
import software.amazon.awssdk.services.s3.model.StorageClass;
import software.amazon.awssdk.services.s3.model.Tag;

/**
 * Azure Blob {@link DocumentRepository} adapter implementation.
 * <p>
 * If configured bucket is missing, the bucket will be created only at write time, or the write
 * operation will fail if repository configuration does not instruct to do so.
 * <p>
 * Limitations :
 * <ul>
 *    <li>the following Metadata are not persisted: creationDate</li>
 *    <li>the contentType Metadata is forced by S3 to {@code application/octet-stream} at save if not provided</li>
 *    <li>"append mode" is not supported in {@link #openDocument}</li>
 *    <li>Overwrite is not atomic in open and save operations</li>
 * </ul>
 *
 * @implSpec Immutable class implementation
 */
// FIXME Create bucket ACLs
public class AwsS3DocumentRepository extends AbstractDocumentRepository {

   private static final String URI_SCHEME = "s3";
   private static final HashSet<RepositoryCapability> CAPABILITIES = collectionConcat(HashSet::new,
                                                                                      list(WRITABLE,
                                                                                           READABLE,
                                                                                           ITERABLE,
                                                                                           QUERYABLE,
                                                                                           OPEN),
                                                                                      list(REPOSITORY_URI,
                                                                                           DOCUMENT_URI),
                                                                                      list(METADATA_DOCUMENT_PATH,
                                                                                           METADATA_LAST_UPDATE_DATE,
                                                                                           METADATA_CONTENT_TYPE,
                                                                                           METADATA_CONTENT_ENCODING,
                                                                                           METADATA_CONTENT_SIZE,
                                                                                           METADATA_ATTRIBUTES));

   private final AwsS3DocumentConfig awsS3DocumentConfig;
   private final Consumer<S3ClientBuilder> clientConfigurer;
   private final S3Client client;
   private final boolean sharedClient;
   private final int sameRepositoryHash;
   private final DocumentTransformerUriAdapter transformerUriAdapter;
   private final String bucketName;
   private final Path bucketBasePath;
   private final int listingMaxKeys;
   private final ListingContentMode listingContentMode;
   private final int uploadChunkSize;
   private final boolean createIfMissingBucket;
   private final List<Pattern> matchTags;
   private final boolean loadAllTags;
   private final StorageClass storageClass;

   private boolean closed = false;

   private AwsS3DocumentRepository(AwsS3DocumentConfig awsS3DocumentConfig,
                                   RegistrableDomainEventService eventService,
                                   Consumer<S3ClientBuilder> clientConfigurer,
                                   S3Client client) {
      super(eventService);

      this.awsS3DocumentConfig = validate(awsS3DocumentConfig, "awsS3DocumentConfig", isNotNull()).orThrow();

      this.sharedClient = client != null;
      this.clientConfigurer = validate(clientConfigurer, "clientConfigurer", isNotNull()).orThrow();
      this.client = nullable(client, buildS3Client(awsS3DocumentConfig, clientConfigurer));
      this.sameRepositoryHash = sameRepositoryHash(awsS3DocumentConfig);
      this.transformerUriAdapter = new DocumentTransformerUriAdapter(new AwsS3UriAdapter());
      this.bucketName = awsS3DocumentConfig.bucketName();
      this.bucketBasePath = awsS3DocumentConfig.bucketBasePath();
      this.listingMaxKeys = awsS3DocumentConfig.listingMaxKeys();
      this.listingContentMode = awsS3DocumentConfig.listingContentMode();
      this.uploadChunkSize = awsS3DocumentConfig.uploadChunkSize();
      this.createIfMissingBucket = awsS3DocumentConfig.createIfMissingBucket();
      this.matchTags = awsS3DocumentConfig.matchTags();
      this.loadAllTags = awsS3DocumentConfig.loadAllTags();
      this.storageClass = awsS3DocumentConfig.storageClass();

      if (awsS3DocumentConfig.failFastIfMissingBucket()) {
         tryCreateBucket(this.client, bucketName, createIfMissingBucket);
      }
   }

   public AwsS3DocumentRepository(AwsS3DocumentConfig awsS3DocumentConfig,
                                  RegistrableDomainEventService eventService) {
      this(awsS3DocumentConfig, eventService, __ -> { }, null);
   }

   public AwsS3DocumentRepository(AwsS3DocumentConfig awsS3DocumentConfig) {
      this(awsS3DocumentConfig, new SynchronousDomainEventService());
   }

   public AwsS3DocumentRepository(AwsS3DocumentConfig awsS3DocumentConfig,
                                  RegistrableDomainEventService eventService,
                                  Consumer<S3ClientBuilder> clientConfigurer) {
      this(awsS3DocumentConfig, eventService, clientConfigurer, null);
   }

   public AwsS3DocumentRepository(AwsS3DocumentConfig awsS3DocumentConfig,
                                  Consumer<S3ClientBuilder> clientConfigurer) {
      this(awsS3DocumentConfig, new SynchronousDomainEventService(), clientConfigurer);
   }

   /**
    * Helper to configure S3 HTTP client when configuring this repository.
    * This configurer is {@link Consumer#andThen(Consumer) composable}.
    *
    * @param httpClientConfigurer HTTP client configurer
    *
    * @return client configurer
    */
   public static Consumer<S3ClientBuilder> configureHttpClient(Consumer<ClientOverrideConfiguration.Builder> httpClientConfigurer) {
      return s3Client -> s3Client.overrideConfiguration(httpClientConfigurer);
   }

   /**
    * Helper to configure a {@link MetricPublisher} for the S3 HTTP client when configuring this repository.
    * This configurer is {@link Consumer#andThen(Consumer) composable}.
    *
    * @param metricPublisher metric publisher to add
    *
    * @return client configurer
    */
   public static Consumer<S3ClientBuilder> configureHttpClientMetricPublisher(MetricPublisher metricPublisher) {
      return configureHttpClient(httpClient -> httpClient.addMetricPublisher(metricPublisher));
   }

   @Override
   public HashSet<RepositoryCapability> capabilities() {
      return CAPABILITIES;
   }

   @Override
   public boolean sameRepositoryAs(Repository<Document, DocumentPath> documentRepository) {
      validate(documentRepository, "documentRepository", isNotNull()).orThrow();

      return documentRepository instanceof AwsS3DocumentRepository
             && Objects.equals(((AwsS3DocumentRepository) documentRepository).sameRepositoryHash,
                               sameRepositoryHash);
   }

   @Override
   public AwsS3DocumentRepository subPath(Path subPath, boolean shareContext) {
      validate(subPath, "subPath", isNotAbsolute().andValue(hasNoTraversal())).orThrow();

      AwsS3DocumentConfig subPathConfig = AwsS3DocumentConfigBuilder
            .from(awsS3DocumentConfig)
            .bucketBasePath(awsS3DocumentConfig.bucketBasePath().resolve(subPath))
            .build();

      if (shareContext) {
         return new AwsS3DocumentRepository(subPathConfig, eventService, clientConfigurer, client);
      } else {
         return new AwsS3DocumentRepository(subPathConfig, eventService, clientConfigurer);
      }
   }

   /**
    * {@inheritDoc}
    *
    * @implNote Overwrite check is not atomic, this can lead to overwriting a document created
    *       concurrently by another client, even if overwrite flag is set to {@code false}.
    */
   @Override
   @CheckReturnValue
   public Optional<Document> openDocument(DocumentPath documentId,
                                          boolean overwrite,
                                          boolean append,
                                          OpenDocumentMetadata metadata) {
      validate(documentId, "documentId", isNotNull())
            .and(validate(metadata, "metadata", isNotNull()))
            .orThrow();

      if (append) {
         throw new UnsupportedCapabilityException(OPEN_APPEND);
      }

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEvent(() -> {
         if (!(overwrite || append) && s3DocumentEntry(documentId).isPresent()) {
            return optional();
         }

         return optional(new DocumentBuilder()
                               .documentId(documentId)
                               .content(new OutputStreamDocumentContentBuilder()
                                              .content(new AwsS3MultipartOutputStream(client,
                                                                                      bucketName,
                                                                                      createIfMissingBucket,
                                                                                      key(documentId),
                                                                                      metadata,
                                                                                      uploadChunkSize,
                                                                                      matchTags,
                                                                                      storageClass),
                                                       metadata.contentEncoding().orElse(null))
                                              .build())
                               .chain(metadata.chainDocumentBuilder())
                               .build());
      }, d -> documentSaved(d.documentEntry(), watch));
   }

   @Override
   public Optional<Document> findDocumentById(DocumentPath documentId) {
      validate(documentId, "documentId", isNotNull()).orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEvent(() -> s3Document(documentId),
                                 d -> documentAccessed(d.documentEntry(), watch));
   }

   @Override
   public Stream<Document> findDocumentsBySpecification(Path basePath,
                                                        Specification<DocumentEntry> specification) {
      StopWatch watch = StopWatch.createStarted();

      return handleDocumentStreamEvent(() -> documentContentAutoCloseOnStreamClose(s3DocumentEntries(basePath,
                                                                                                     specification).flatMap(
            entry -> {
               Optional<DocumentContent> documentContent = s3DocumentContent(entry.documentId());
               return stream(documentContent.map(content -> new DocumentBuilder()
                     .<DocumentBuilder>reconstitute()
                     .documentEntry(entry)
                     .content(content)
                     .build()));
            })), d -> documentAccessed(d.documentEntry(), watch));
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryById(DocumentPath documentId) {
      validate(documentId, "documentId", isNotNull()).orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEntryEvent(() -> s3DocumentEntry(documentId), d -> documentAccessed(d, watch));
   }

   /**
    * {@inheritDoc}
    *
    * @implSpec Metadata returned by S3 are very limited, and can't be completed because of the
    *       performance impact. The following Metadata are not managed by default in returned document entry
    *       and specification filtering :
    *       <ul>
    *          <li>creation date : unset</li>
    *          <li>custom attributes : unset</li>
    *          <li>content type : unset </li>
    *          <li>document path/name : replaced with {@link DocumentPath document identifier} value</li>
    *       </ul>
    */
   @Override
   public Stream<DocumentEntry> findDocumentEntriesBySpecification(Path basePath,
                                                                   Specification<DocumentEntry> specification) {
      validate(basePath, "basePath", isNotAbsolute().andValue(hasNoTraversal()))
            .and(validate(specification, "specification", isNotNull()))
            .orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEntryStreamEvent(() -> s3DocumentEntries(basePath, specification),
                                            d -> documentAccessed(d, watch));
   }

   /**
    * {@inheritDoc}
    *
    * @implNote Overwrite check is not atomic, this can lead to overwriting a document created
    *       concurrently by another client, even if overwrite flag is set to {@code false}.
    */
   @Override
   @CheckReturnValue
   public Optional<DocumentEntry> saveDocument(Document document, boolean overwrite) {
      validate(document, "document", isNotNull()).orThrow();

      if (document.content() instanceof OutputStreamDocumentContent) {
         throw new IllegalArgumentException(
               "Unsupported operation for this document content implementation : " + document
                     .content()
                     .getClass()
                     .getName());
      }

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEntryEvent(() -> {
         if (!rawSaveDocument(document, overwrite)) {
            return optional();
         }
         return s3DocumentEntry(document.documentId());
      }, d -> documentSaved(d, watch));
   }

   /**
    * {@inheritDoc}
    *
    * @implNote Overwrite check is not atomic, this can lead to overwriting a document created
    *       concurrently by another client, even if overwrite flag is set to {@code false}.
    */
   @Override
   @CheckReturnValue
   public Optional<Document> saveAndReturnDocument(Document document, boolean overwrite) {
      validate(document, "document", isNotNull()).orThrow();

      if (document.content() instanceof OutputStreamDocumentContent) {
         throw new IllegalArgumentException(
               "Unsupported operation for this document content implementation : " + document
                     .content()
                     .getClass()
                     .getName());
      }

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEvent(() -> {
         if (!rawSaveDocument(document, overwrite)) {
            return optional();
         }
         return s3Document(document.documentId());
      }, d -> documentSaved(d.documentEntry(), watch));
   }

   private boolean rawSaveDocument(Document document, boolean overwrite) {
      String key = key(document.documentId());

      if (overwrite || !s3DocumentEntry(document.documentId()).isPresent()) {
         try (AwsS3MultipartOutputStream outputStream = new AwsS3MultipartOutputStream(client,
                                                                                       bucketName,
                                                                                       createIfMissingBucket,
                                                                                       key,
                                                                                       openDocumentMetadata(
                                                                                             document.metadata()),
                                                                                       uploadChunkSize,
                                                                                       matchTags,
                                                                                       storageClass)) {
            try {
               IOUtils.copy(document.content().inputStreamContent(), outputStream);
            } catch (IOException e) {
               throw new DocumentAccessException(e);
            }
         }
         return true;
      }

      return false;
   }

   @Override
   @CheckReturnValue
   public Optional<DocumentEntry> deleteDocumentById(DocumentPath documentId) {
      validate(documentId, "documentId", isNotNull()).orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEntryEvent(() -> {

         String key = key(documentId);

         return s3DocumentEntry(documentId).filter(entry -> {
            try {
               client.deleteObject(DeleteObjectRequest.builder().bucket(bucketName).key(key).build());
            } catch (NoSuchBucketException e) {
               return false;
            } catch (SdkException e) {
               throw new DocumentAccessException(e.getMessage(), e);
            }
            return true;
         });
      }, d -> documentDeleted(d, watch));
   }

   @Override
   @CheckReturnValue
   public List<DocumentEntry> deleteDocumentsBySpecification(Path basePath,
                                                             Specification<DocumentEntry> specification) {
      validate(basePath, "basePath", isNotAbsolute().andValue(hasNoTraversal()))
            .and(validate(specification, "specification", isNotNull()))
            .orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEntriesEvent(() -> list(s3DocumentEntries(basePath, specification).filter(
            documentEntry -> {
               String key = key(documentEntry.documentId());

               try {
                  client.deleteObject(DeleteObjectRequest.builder().bucket(bucketName).key(key).build());
               } catch (NoSuchBucketException e) {
                  return false;
               } catch (SdkException e) {
                  throw new DocumentAccessException(e.getMessage(), e);
               }
               return true;
            })), d -> documentDeleted(d, watch));
   }

   @Override
   public void close() {
      if (!closed) {
         closed = true;
         if (!sharedClient) {
            client.close();
         }
      }
   }

   /**
    * Checks if specified URI is compatible with this repository, independently to current repository
    * configuration
    *
    * @param uri URI
    * @param documentUri whether specified URI is a document URI or any supported URI
    *
    * @return {@code true} if specified URI is compatible with this repository
    *
    * @see #supportsUri(URI, boolean)
    */
   public boolean isCompatibleUri(URI uri, boolean documentUri) {
      validate(uri, "uri", isNotNull()).orThrow();

      try {
         client.utilities().parseUri(uri);
      } catch (Exception e) {
         return false;
      }

      /*if (!(uri.isAbsolute()
            && uri.getScheme().equals(URI_SCHEME)
            && uri.getAuthority() != null
            && uri.getHost() != null
            && uri.getFragment() == null
            && uri.getQuery() == null
            && uri.getPath() != null)) {
         return false;
      }*/

      return !documentUri || !Paths.get(uri.getPath()).equals(Paths.get("/"));
   }

   @Override
   public boolean supportsUri(URI uri, boolean documentUri) {
      return transformerUriAdapter.supportsUri(uri, documentUri);
   }

   @Override
   public URI toUri() {
      return transformerUriAdapter.toUri();
   }

   @Override
   public URI toUri(DocumentPath documentId) {
      return transformerUriAdapter.toUri(documentId);
   }

   @Override
   public Optional<Document> findDocumentByUri(URI documentUri) {
      return transformerUriAdapter.findDocumentByUri(documentUri);
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryByUri(URI documentUri) {
      return transformerUriAdapter.findDocumentEntryByUri(documentUri);
   }

   /**
    * Real Azure Blob URI adapter to be used by {@link DocumentTransformerUriAdapter}.
    */
   public class AwsS3UriAdapter implements UriAdapter {
      @Override
      public boolean supportsUri(URI uri, boolean documentUri) {
         validate(uri, "uri", isNotNull()).orThrow();

         if (!isCompatibleUri(uri, documentUri)) {
            return false;
         }

         S3Uri s3Uri = client.utilities().parseUri(uri);

         if (!s3Uri.bucket().map(bucket -> bucket.equals(bucketName)).orElse(true)) {
            return false;
         }

         if (!s3Uri
               .region()
               .map(region -> region.equals(client.serviceClientConfiguration().region()))
               .orElse(true)) {
            return false;
         }

         if (!bucketBasePath.toString().isEmpty() && !uriPath(uri).startsWith(bucketBasePath)) {
            return false;
         }

         return !documentUri || !uriPath(uri).equals(bucketBasePath);
      }

      @Override
      public URI toUri() {
         try {
            return new URI(URI_SCHEME, bucketName, "/" + bucketBasePath.toString(), null, null);
         } catch (URISyntaxException e) {
            throw new IllegalStateException(e);
         }
      }

      @Override
      public URI toUri(DocumentPath documentId) {
         validate(documentId, "documentId", isNotNull()).orThrow();

         try {
            return new URI(URI_SCHEME,
                           bucketName,
                           "/" + bucketBasePath.resolve(documentId.value()).toString(),
                           null,
                           null);
         } catch (URISyntaxException e) {
            throw new IllegalStateException(e);
         }
      }

      @Override
      public Optional<Document> findDocumentByUri(URI documentUri) {
         validate(documentUri,
                  "documentUri",
                  UriRules
                        .hasNoTraversal()
                        .andValue(satisfies(this::supportsDocumentUri,
                                            "'%s' must be supported",
                                            validatingObject()))).orThrow();

         return findDocumentById(documentId(documentUri));
      }

      @Override
      public Optional<DocumentEntry> findDocumentEntryByUri(URI documentUri) {
         validate(documentUri,
                  "documentUri",
                  UriRules
                        .hasNoTraversal()
                        .andValue(satisfies(this::supportsDocumentUri,
                                            "'%s' must be supported",
                                            validatingObject()))).orThrow();

         return findDocumentEntryById(documentId(documentUri));
      }

      /**
       * Returns the base path from the specified URI.
       *
       * @param uri uri to parse
       *
       * @return base path from URI
       */
      protected Path uriPath(URI uri) {
         return Paths.get(removeStart(uri.getPath(), "/"));
      }

      /**
       * Extracts document identifier from specified document URI.
       *
       * @param documentUri document URI
       *
       * @return document identifier
       */
      protected DocumentPath documentId(URI documentUri) {
         return DocumentPath.of(bucketBasePath.relativize(uriPath(documentUri)));
      }

   }

   /** Generates a hash unique for a given server storage path to identify a similar repository. */
   private int sameRepositoryHash(AwsS3DocumentConfig awsS3DocumentConfig) {
      return Objects.hash(awsS3DocumentConfig.region(),
                          awsS3DocumentConfig.bucketName(),
                          awsS3DocumentConfig.bucketBasePath());
   }

   private static S3Client buildS3Client(AwsS3DocumentConfig awsS3DocumentConfig,
                                         Consumer<S3ClientBuilder> clientConfigurer) {
      S3ClientBuilder s3ClientBuilder = S3Client
            .builder()
            .forcePathStyle(awsS3DocumentConfig.pathStyleAccess())
            .region(Region.of(awsS3DocumentConfig.region()))
            .credentialsProvider(awsS3DocumentConfig.credentialsProvider());

      if (awsS3DocumentConfig.httpClientConfig() != null) {
         s3ClientBuilder.httpClientBuilder(ApacheHttpClient
                                                 .builder()
                                                 .maxConnections(awsS3DocumentConfig
                                                                       .httpClientConfig()
                                                                       .maxConnections()));
      }

      awsS3DocumentConfig.endpoint().ifPresent(endpoint -> {
         try {
            s3ClientBuilder.endpointOverride(endpoint.toURI());
         } catch (URISyntaxException e) {
            throw new IllegalStateException(e);
         }
      });

      clientConfigurer.accept(s3ClientBuilder);

      return s3ClientBuilder.build();
   }

   private OpenDocumentMetadata openDocumentMetadata(DocumentMetadata metadata) {
      return new OpenDocumentMetadataBuilder()
            .documentPath(metadata.documentPath())
            .contentType(metadata.contentType().orElse(null))
            .attributes(metadata.attributes())
            .build();
   }

   private Optional<DocumentEntry> s3DocumentEntry(DocumentPath documentId) {
      try {
         HeadObjectResponse headObjectResponse =
               client.headObject(HeadObjectRequest.builder().bucket(bucketName).key(key(documentId)).build());

         return optional(new DocumentEntryBuilder()
                               .<DocumentEntryBuilder>reconstitute()
                               .documentId(documentId)
                               .metadata(documentMetadata(documentId, headObjectResponse))
                               .build());
      } catch (NoSuchKeyException | NoSuchBucketException e) {
         return optional();
      } catch (SdkException e) {
         throw new DocumentAccessException(e.getMessage(), e);
      }
   }

   private Stream<DocumentEntry> s3DocumentEntries(Path basePath,
                                                   Specification<DocumentEntry> specification) {
      Stream<DocumentEntry> documentEntries =
            s3ListPrefixDocumentEntries(bucketBasePath.resolve(basePath), directoryFilter(specification));

      if (listingContentMode == FILTERING_COMPLETE) {
         documentEntries = documentEntries.flatMap(e -> stream(s3DocumentEntry(e.documentId())));
      }

      documentEntries = documentEntries.filter(specification::satisfiedBy);

      if (listingContentMode == RETURN_COMPLETE) {
         documentEntries = documentEntries.flatMap(e -> stream(s3DocumentEntry(e.documentId())));
      }

      return documentEntries;
   }

   /**
    * Delete bucket for testing purpose.
    */
   void deleteBucket(String bucketName) {
      try {
         client.deleteBucket(DeleteBucketRequest.builder().bucket(bucketName).build());
      } catch (NoSuchBucketException e) {
         /* Does nothing. */
      } catch (SdkException e) {
         throw new DocumentAccessException(e.getMessage(), e);
      }
   }

   private Optional<Document> s3Document(DocumentPath documentId) {
      try {
         ResponseInputStream<GetObjectResponse> object =
               client.getObject(GetObjectRequest.builder().bucket(bucketName).key(key(documentId)).build());

         return optional(new DocumentBuilder()
                               .<DocumentBuilder>reconstitute()
                               .documentId(documentId)
                               .metadata(documentMetadata(documentId, object.response()))
                               .content(inputStreamContent(object))
                               .build());
      } catch (NoSuchKeyException | NoSuchBucketException | InvalidObjectStateException e) {
         return optional();
      } catch (SdkException e) {
         throw new DocumentAccessException(e.getMessage(), e);
      }
   }

   private Optional<DocumentContent> s3DocumentContent(DocumentPath documentId) {
      try {
         ResponseInputStream<GetObjectResponse> object =
               client.getObject(GetObjectRequest.builder().bucket(bucketName).key(key(documentId)).build());

         return optional(inputStreamContent(object));
      } catch (NoSuchKeyException | InvalidObjectStateException e) {
         return optional();
      } catch (SdkException e) {
         throw new DocumentAccessException(e.getMessage(), e);
      }
   }

   private RequestBody requestBody(Document document) {
      return RequestBody.fromInputStream(document.content().inputStreamContent(),
                                         document
                                               .content()
                                               .contentSize()
                                               .orElseThrow(() -> new IllegalArgumentException(String.format(
                                                     "Unknown '%s' content size",
                                                     document.documentId()))));
   }

   private DocumentEntry s3DocumentEntry(S3Object object) {
      DocumentPath documentId = DocumentPath.of(bucketBasePath.relativize(Paths.get(object.key())));

      return new DocumentEntryBuilder()
            .<DocumentEntryBuilder>reconstitute()
            .documentId(documentId)
            .metadata(documentMetadata(documentId, object))
            .build();
   }

   private Predicate<Path> directoryFilter(Specification<DocumentEntry> specification) {
      if (specification instanceof DocumentEntrySpecification) {
         DocumentEntrySpecification documentEntrySpecification = (DocumentEntrySpecification) specification;
         return documentEntrySpecification::satisfiedBySubPath;
      } else {
         return __ -> true;
      }
   }

   private Stream<DocumentEntry> s3ListPrefixDocumentEntries(Path prefix, Predicate<Path> prefixFilter) {
      notNull(prefix, "prefix");
      notNull(prefixFilter, "prefixFilter");

      String s3Prefix = prefix.toString().isEmpty() ? "" : prefix + "/";

      try {
         return client
               .listObjectsV2Paginator(ListObjectsV2Request
                                             .builder()
                                             .bucket(bucketName)
                                             .delimiter("/")
                                             .prefix(s3Prefix)
                                             .maxKeys(listingMaxKeys)
                                             .build())
               .stream()
               .flatMap(response -> {
                  Stream<DocumentEntry> entries = stream();
                  if (response.hasContents()) {
                     entries = streamConcat(entries, response.contents().stream().map(this::s3DocumentEntry));
                  }

                  if (response.hasCommonPrefixes()) {
                     entries = streamConcat(entries,
                                            response
                                                  .commonPrefixes()
                                                  .stream()
                                                  .map(commonPrefix -> Paths.get(commonPrefix.prefix()))
                                                  .filter(prefixFilter)
                                                  .flatMap(commonPrefix -> s3ListPrefixDocumentEntries(
                                                        commonPrefix,
                                                        prefixFilter)));
                  }

                  return entries;
               });
      } catch (NoSuchBucketException e) {
         return stream();
      } catch (SdkException e) {
         throw new DocumentAccessException(e.getMessage(), e);
      }
   }

   /**
    * @implSpec incomplete Metadata.
    */
   private DocumentMetadata documentMetadata(DocumentPath documentId, S3Object object) {
      Instant lastUpdateDate = object.lastModified();
      Long contentSize = object.size();

      return new DocumentMetadataBuilder()
            .<DocumentMetadataBuilder>reconstitute()
            .documentPath(documentId.value())
            .lastUpdateDate(lastUpdateDate)
            .contentSize(contentSize)
            .build();
   }

   private DocumentMetadata documentMetadata(DocumentPath documentId, GetObjectResponse objectMetadata) {
      Instant lastUpdateDate = objectMetadata.lastModified();
      MimeType contentType =
            nullable(objectMetadata.contentType()).map(MimeTypeFactory::parseMimeType).orElse(null);
      Path documentPath = documentPath(objectMetadata.contentDisposition()).orElse(documentId.value());
      Long contentSize = objectMetadata.contentLength();

      Map<String, String> attributes = map(objectMetadata.metadata());
      s3ObjectTags(documentId, loadAllTags ? null : matchTags, null).forEach(tag -> attributes.put(tag.key(),
                                                                                                   tag.value()));

      return new DocumentMetadataBuilder()
            .<DocumentMetadataBuilder>reconstitute()
            .documentPath(documentPath)
            .contentType(contentType)
            .lastUpdateDate(lastUpdateDate)
            .contentSize(contentSize)
            .attributes(attributes)
            .build();
   }

   private Stream<Tag> s3ObjectTags(DocumentPath documentId, List<Pattern> includes, List<Pattern> excludes) {
      if (includes != null && includes.isEmpty()) {
         return stream();
      }

      try {
         GetObjectTaggingResponse tagResponse = client.getObjectTagging(GetObjectTaggingRequest
                                                                              .builder()
                                                                              .bucket(bucketName)
                                                                              .key(key(documentId))
                                                                              .build());
         return stream(tagResponse.tagSet())
               .filter(tag -> excludes == null || !matchTag(excludes, tag))
               .filter(tag -> includes == null || matchTag(includes, tag));
      } catch (NoSuchKeyException | NoSuchBucketException e) {
         return stream();
      } catch (SdkException e) {
         throw new DocumentAccessException(e.getMessage(), e);
      }
   }

   private static boolean matchTag(List<Pattern> patterns, Tag tag) {
      return patterns.stream().anyMatch(pattern -> pattern.matcher(tag.key()).matches());
   }

   private DocumentMetadata documentMetadata(DocumentPath documentId, HeadObjectResponse objectMetadata) {
      Instant lastUpdateDate = objectMetadata.lastModified();
      MimeType contentType =
            nullable(objectMetadata.contentType()).map(MimeTypeFactory::parseMimeType).orElse(null);
      Path documentPath = documentPath(objectMetadata.contentDisposition()).orElse(documentId.value());
      Long contentSize = objectMetadata.contentLength();
      Map<String, String> attributes = objectMetadata.metadata();

      return new DocumentMetadataBuilder()
            .<DocumentMetadataBuilder>reconstitute()
            .documentPath(documentPath)
            .contentType(contentType)
            .lastUpdateDate(lastUpdateDate)
            .contentSize(contentSize)
            .attributes(attributes)
            .build();
   }

   private InputStreamDocumentContent inputStreamContent(ResponseInputStream<GetObjectResponse> object) {
      Long contentSize = object.response().contentLength();
      Charset contentEncoding = nullable(object.response().contentEncoding())
            .map(Charset::forName)
            .orElseGet(() -> nullable(object.response().contentType())
                  .map(MimeTypeFactory::parseMimeType)
                  .flatMap(MimeType::charset)
                  .orElse(null));

      return new InputStreamDocumentContentBuilder().content(object, contentEncoding, contentSize).build();
   }

   private String key(DocumentPath documentId) {
      return bucketBasePath.resolve(documentId.value()).toString();
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", AwsS3DocumentRepository.class.getSimpleName() + "[", "]").toString();
   }
}
