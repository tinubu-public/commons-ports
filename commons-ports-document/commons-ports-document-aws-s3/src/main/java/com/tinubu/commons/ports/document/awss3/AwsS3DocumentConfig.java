/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.awss3;

import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.hasNoNullElements;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isGreaterThanOrEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isLessThanOrEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.NumberRules.isStrictlyPositive;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.lang.util.CollectionUtils.immutable;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.ports.document.domain.DocumentEntry;

import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.auth.credentials.DefaultCredentialsProvider;
import software.amazon.awssdk.services.s3.model.StorageClass;

/**
 * Azure Blob document repository configuration.
 */
public class AwsS3DocumentConfig extends AbstractValue {
   /** Absolute maximum number of keys that can be retrieved in a single call when listing objects. */
   private static final int MAX_KEYS_MAX_VALUE = 1000;
   /** Minimum supported part upload chunk size value. */
   private static final int MIN_UPLOAD_CHUNK_SIZE = 5 * 1024 * 1024;
   /** Default part upload chunk size value. */
   private static final int DEFAULT_UPLOAD_CHUNK_SIZE = MIN_UPLOAD_CHUNK_SIZE;
   /**
    * Default AWS S3 credentials provider.
    *
    * @implNote Do not use {@link DefaultCredentialsProvider#create()} here, as it's returning a
    *       singleton instance, and HTTP connection pool used for token retrieval could be closed by another
    *       AWS SDK instance.
    */
   public static final DefaultCredentialsProvider DEFAULT_CREDENTIALS_PROVIDER =
         DefaultCredentialsProvider.builder().build();

   private final String region;
   private final URL endpoint;
   private final String bucketName;
   private final Path bucketBasePath;
   private final boolean createIfMissingBucket;
   private final boolean failFastIfMissingBucket;
   private final boolean pathStyleAccess;
   private final int listingMaxKeys;
   private final ListingContentMode listingContentMode;
   private final int uploadChunkSize;
   private final AwsCredentialsProvider credentialsProvider;
   private final List<Pattern> matchTags;
   private final boolean loadAllTags;
   private final HttpClientConfig httpClientConfig;
   private final StorageClass storageClass;

   private AwsS3DocumentConfig(AwsS3DocumentConfigBuilder builder) {
      this.region = builder.region;
      this.endpoint = builder.endpoint;
      this.bucketName = builder.bucketName;
      this.bucketBasePath = nullable(builder.bucketBasePath, Paths.get(""));
      this.createIfMissingBucket = nullable(builder.createIfMissingBucket, false);
      this.failFastIfMissingBucket = nullable(builder.failFastIfMissingBucket, true);
      this.pathStyleAccess = nullable(builder.pathStyleAccess, false);
      this.listingMaxKeys = nullable(builder.listingMaxKeys, MAX_KEYS_MAX_VALUE);
      this.listingContentMode = nullable(builder.listingContentMode, ListingContentMode.FAST);
      this.uploadChunkSize = nullable(builder.uploadChunkSize, DEFAULT_UPLOAD_CHUNK_SIZE);
      this.credentialsProvider = nullable(builder.credentialsProvider, DEFAULT_CREDENTIALS_PROVIDER);
      this.matchTags = immutable(list(builder.matchTags));
      this.loadAllTags = nullable(builder.loadAllTags, false);
      this.httpClientConfig = builder.httpClientConfig;
      this.storageClass = nullable(builder.storageClass, StorageClass.STANDARD);

   }

   @Override
   public Fields<? extends AwsS3DocumentConfig> defineDomainFields() {
      return Fields
            .<AwsS3DocumentConfig>builder()
            .field("region", v -> v.region, isNotBlank())
            .field("endpoint", v -> v.endpoint)
            .field("bucketName", v -> v.bucketName, isNotBlank())
            .field("bucketBasePath",
                   v -> v.bucketBasePath,
                   isNotNull().andValue(isNotAbsolute().andValue(hasNoTraversal())))
            .field("createIfMissingBucket", v -> v.createIfMissingBucket)
            .field("failFastIfMissingBucket", v -> v.failFastIfMissingBucket)
            .field("pathStyleAccess", v -> v.pathStyleAccess)
            .field("listingMaxKeys",
                   v -> v.listingMaxKeys,
                   isStrictlyPositive().andValue(isLessThanOrEqualTo(value(MAX_KEYS_MAX_VALUE))))
            .field("listingContentMode", v -> v.listingContentMode, isNotNull())
            .field("uploadChunkSize",
                   v -> v.uploadChunkSize,
                   isGreaterThanOrEqualTo(value(MIN_UPLOAD_CHUNK_SIZE)))
            .field("credentialsProvider", v -> v.credentialsProvider)
            .field("matchTags", v -> v.matchTags, hasNoNullElements())
            .field("loadAllTags", v -> v.loadAllTags)
            .field("httpClientConfig", v -> v.httpClientConfig)
            .build();
   }

   /**
    * S3 region.
    */
   @Getter
   public String region() {
      return region;
   }

   /**
    * Optional S3 endpoint override.
    */
   @Getter
   public Optional<URL> endpoint() {
      return nullable(endpoint);
   }

   /**
    * S3 bucket name.
    */
   @Getter
   public String bucketName() {
      return bucketName;
   }

   /**
    * Optional base path, relative to bucket, to store all documents for the configured repository.
    * Path is never {@code null}, but can be empty to disable it. Traversal paths are not supported so that
    * this base path can be considered a "sandbox" for configured repository. Note that this base path is a
    * backend configuration detail and will never appear in retrieved/saved document identifiers and paths.
    */
   @Getter
   public Path bucketBasePath() {
      return bucketBasePath;
   }

   /**
    * Optional flag to automatically create missing bucket. Default to {@code false}.
    */
   @Getter
   public boolean createIfMissingBucket() {
      return createIfMissingBucket;
   }

   /**
    * Optional flag to fail-fast if missing bucket. Default to {@code true}.
    */
   @Getter
   public boolean failFastIfMissingBucket() {
      return failFastIfMissingBucket;
   }

   /**
    * Optional flag to use path-style access.
    */
   @Getter
   public boolean pathStyleAccess() {
      return pathStyleAccess;
   }

   /**
    * Optional max keys (i.e. page size) in objects listing operations. Default to
    * {@link #MAX_KEYS_MAX_VALUE}.
    */
   @Getter
   public int listingMaxKeys() {
      return listingMaxKeys;
   }

   /**
    * Optional listing content mode to workaround S3 limitations.
    * Default to {@link ListingContentMode#FAST} : S3 does not return all metadata for objects, so that
    * {@link DocumentEntry} metadata are  incomplete for both filtering and returned entries. The library can
    * complete the metadata at the cost of one extra API call per object.
    */
   @Getter
   public ListingContentMode listingContentMode() {
      return listingContentMode;
   }

   /**
    * Optional multipart upload chunk size. Default to {@link #DEFAULT_UPLOAD_CHUNK_SIZE}.
    */
   @Getter
   public int uploadChunkSize() {
      return uploadChunkSize;
   }

   /**
    * Credentials provider. Default to {@link DefaultCredentialsProvider}.
    *
    * @return credentials provider
    */
   @Getter
   public AwsCredentialsProvider credentialsProvider() {
      return credentialsProvider;
   }

   /**
    * Document metadata attributes are loaded from S3 object metadata by default.
    * Returns patterns matching object tag keys to load as document metadata attributes, and matching
    * document metadata attribute keys to save as object tags. By default, no tags are matched.
    * If an object metadata is already existing with the same key, its value will always be overridden with
    * tag value.
    * A tag/attribute matches if any pattern in the list matches the tag/attribute key.
    *
    * @return patterns to match tag keys to load/save
    */
   @Getter
   public List<Pattern> matchTags() {
      return matchTags;
   }

   /**
    * Returns whether to always load all available tags as document metadata attributes, and not just those
    * matching {@link #matchTags()}. Default to {@code false}.
    * If an object metadata is already existing with the same key, its value will always be overridden with
    * tag value.
    *
    * @return whether to always load all available tags, and not just those matching configured patterns
    */
   @Getter
   public boolean loadAllTags() {
      return loadAllTags;
   }

   /**
    * Returns HTTP client configuration.
    *
    * @return HTTP client configuration or {@code null} if no custom client configuration is set
    */
   @Getter
   public HttpClientConfig httpClientConfig() {
      return httpClientConfig;
   }

   /**
    * S3 listing operation metadata completing mode.
    */
   public enum ListingContentMode {
      /** Fast mode : no metadata are completed for both filtering and returned entries. */
      FAST,
      /**
       * Low performance mode : metadata are completed after filtering, for returned entries. Use this
       * mode only if you do not need complete metadata for specification filtering, but only for returned
       * entries, and there is a small number of returned objects after filtering.
       */
      RETURN_COMPLETE,
      /**
       * Very low performance mode : metadata are completed for both filtering and returned entries. Use this
       * mode only if you need complete metadata and there is a small number of objects in the bucket.
       */
      FILTERING_COMPLETE
   }

   /**
    * Each object in Amazon S3 has a storage class associated with it.
    * By default, objects in S3 are stored in the S3 Standard storage class, however Amazon S3 offers a range
    * of other storage classes for the objects that you store. You choose a class depending on your use case
    * scenario and performance access requirements. Choosing a storage class designed for your use case lets
    * you optimize storage costs, performance, and availability for your objects. All of these storage classes
    * offer high durability.
    *
    * @return the defined storage class. Default value is equal to {@code StorageClass.STANDARD}
    */
   @Getter
   public StorageClass storageClass() {
      return storageClass;
   }

   public static class HttpClientConfig extends AbstractValue {

      /**
       * Max concurrent HTTP connections hold in pool.
       */
      private final Integer maxConnections;

      private HttpClientConfig(Builder builder) {
         this.maxConnections = builder.maxConnections;
      }

      @Override
      public Fields<? extends HttpClientConfig> defineDomainFields() {
         return Fields
               .<HttpClientConfig>builder()
               .field("maxConnections", v -> v.maxConnections, isNull().orValue(isStrictlyPositive()))
               .build();
      }

      /**
       * Optional max concurrent HTTP connections hold in pool.
       *
       * @return max concurrent HTTP connections hold in pool, or {@code null} if default AWS SDK value should
       *       be used
       */
      @Getter
      public Integer maxConnections() {
         return maxConnections;
      }

      public static class Builder extends DomainBuilder<HttpClientConfig> {

         private Integer maxConnections;

         public static Builder from(HttpClientConfig config) {
            notNull(config, "config");

            return new Builder().maxConnections(config.maxConnections);
         }

         @Setter
         public Builder maxConnections(Integer maxConnections) {
            this.maxConnections = maxConnections;
            return this;
         }

         @Override
         protected HttpClientConfig buildDomainObject() {
            return new HttpClientConfig(this);
         }
      }

   }

   public static class AwsS3DocumentConfigBuilder extends DomainBuilder<AwsS3DocumentConfig> {

      private String region;
      private URL endpoint;
      private String bucketName;
      private Path bucketBasePath;
      private Boolean createIfMissingBucket;
      private Boolean failFastIfMissingBucket;
      private Boolean pathStyleAccess;
      private Integer listingMaxKeys;
      private ListingContentMode listingContentMode;
      private Integer uploadChunkSize;
      private AwsCredentialsProvider credentialsProvider;
      private List<Pattern> matchTags = list();
      private Boolean loadAllTags;
      private HttpClientConfig httpClientConfig;
      private StorageClass storageClass;

      public static AwsS3DocumentConfigBuilder from(AwsS3DocumentConfig config) {
         notNull(config, "config");

         return new AwsS3DocumentConfigBuilder()
               .region(config.region)
               .endpoint(config.endpoint)
               .bucketName(config.bucketName)
               .bucketBasePath(config.bucketBasePath)
               .createIfMissingBucket(config.createIfMissingBucket)
               .failFastIfMissingBucket(config.failFastIfMissingBucket)
               .pathStyleAccess(config.pathStyleAccess)
               .listingMaxKeys(config.listingMaxKeys)
               .listingContentMode(config.listingContentMode)
               .uploadChunkSize(config.uploadChunkSize)
               .credentialsProvider(config.credentialsProvider)
               .matchTags(config.matchTags)
               .loadAllTags(config.loadAllTags)
               .httpClientConfig(config.httpClientConfig)
               .storageClass(config.storageClass);
      }

      @Setter
      public AwsS3DocumentConfigBuilder region(String region) {
         this.region = region;
         return this;
      }

      @Setter
      public AwsS3DocumentConfigBuilder endpoint(URL endpoint) {
         this.endpoint = endpoint;
         return this;
      }

      @Setter
      public AwsS3DocumentConfigBuilder bucketName(String bucketName) {
         this.bucketName = bucketName;
         return this;
      }

      @Setter
      public AwsS3DocumentConfigBuilder bucketBasePath(Path bucketBasePath) {
         this.bucketBasePath = bucketBasePath;
         return this;
      }

      @Setter
      public AwsS3DocumentConfigBuilder createIfMissingBucket(Boolean createIfMissingBucket) {
         this.createIfMissingBucket = createIfMissingBucket;
         return this;
      }

      @Setter
      public AwsS3DocumentConfigBuilder failFastIfMissingBucket(Boolean failFastIfMissingBucket) {
         this.failFastIfMissingBucket = failFastIfMissingBucket;
         return this;
      }

      @Setter
      public AwsS3DocumentConfigBuilder pathStyleAccess(Boolean pathStyleAccess) {
         this.pathStyleAccess = pathStyleAccess;
         return this;
      }

      @Setter
      public AwsS3DocumentConfigBuilder listingMaxKeys(int listingMaxKeys) {
         this.listingMaxKeys = listingMaxKeys;
         return this;
      }

      @Setter
      public AwsS3DocumentConfigBuilder listingContentMode(ListingContentMode listingContentMode) {
         this.listingContentMode = listingContentMode;
         return this;
      }

      @Setter
      public AwsS3DocumentConfigBuilder uploadChunkSize(Integer uploadChunkSize) {
         this.uploadChunkSize = uploadChunkSize;
         return this;
      }

      @Setter
      public AwsS3DocumentConfigBuilder credentialsProvider(AwsCredentialsProvider credentialsProvider) {
         this.credentialsProvider = credentialsProvider;
         return this;
      }

      @Setter
      public AwsS3DocumentConfigBuilder matchTags(List<Pattern> matchTags) {
         this.matchTags = list(matchTags);
         return this;
      }

      public AwsS3DocumentConfigBuilder matchTags(Pattern... matchTags) {
         return matchTags(list(matchTags));
      }

      public AwsS3DocumentConfigBuilder addMatchTag(Pattern matchTag) {
         matchTags.add(matchTag);
         return this;
      }

      @Setter
      public AwsS3DocumentConfigBuilder loadAllTags(Boolean loadAllTags) {
         this.loadAllTags = loadAllTags;
         return this;
      }

      @Setter
      public AwsS3DocumentConfigBuilder loadAllTags() {
         return loadAllTags(true);
      }

      @Setter
      public AwsS3DocumentConfigBuilder httpClientConfig(HttpClientConfig httpClientConfig) {
         this.httpClientConfig = httpClientConfig;
         return this;
      }

      @Setter
      public AwsS3DocumentConfigBuilder storageClass(StorageClass storageClass) {
         this.storageClass = storageClass;
         return this;
      }

      @Override
      protected AwsS3DocumentConfig buildDomainObject() {
         return new AwsS3DocumentConfig(this);
      }
   }
}
