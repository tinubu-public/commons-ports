/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.git;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.net.URI;
import java.util.List;
import java.util.function.UnaryOperator;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentContent;
import com.tinubu.commons.ports.document.domain.DocumentMetadata;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.LoadedDocumentContent.LoadedDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.testsuite.CommonDocumentRepositoryTest;
import com.tinubu.commons.ports.document.fs.FsDocumentConfig.FsDocumentConfigBuilder;
import com.tinubu.commons.ports.document.fs.FsDocumentRepository;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.GitDocumentConfigBuilder;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ZipArchiver;

public class GitDocumentRepositoryTest {

   private static final URI REMOTE_REPOSITORY_URI = URI.create("https://github.com/github/testrepo.git");
   private static final String REMOTE_BRANCH = "email";

   @Nested
   public class CommonTestSuite extends CommonDocumentRepositoryTest {
      private FsDocumentRepository localRepository;
      private GitDocumentRepository documentRepository;

      /**
       * {@inheritDoc}
       * At least the Linux filesystem updates the creation date on overwrite.
       */
      @Override
      public boolean isUpdatingCreationDateOnOverwrite() {
         return true;
      }

      @Override
      protected boolean isSupportingDocumentUri() {
         return false;
      }

      @Override
      protected boolean isSupportingSubPath() {
         return false;
      }

      @Override
      protected boolean isSupportingMetadataAttributes() {
         return false;
      }

      @BeforeEach
      public void configureDocumentRepository() {
         this.localRepository =
               new FsDocumentRepository(new FsDocumentConfigBuilder().deleteRepositoryOnClose(true).build());
         this.documentRepository = new GitDocumentRepository(localRepository,
                                                             new GitDocumentConfigBuilder()
                                                                   .remoteUri(REMOTE_REPOSITORY_URI)
                                                                   .branch(REMOTE_BRANCH)
                                                                   .build());
      }

      @AfterEach
      public void closeDocumentRepository() {
         try {
            this.documentRepository.close();
         } finally {
            this.localRepository.close();
         }
      }

      @Override
      protected DocumentRepository documentRepository() {
         return documentRepository;
      }

      @Override
      protected Document zipTransformer(DocumentPath zipPath, List<Document> documents) {
         return new ZipArchiver(zipPath).compress(documents);
      }

      @Override
      protected UnaryOperator<DocumentMetadataBuilder> synchronizeExpectedMetadata(DocumentMetadata actual) {
         return builder -> builder
               .chain(super.synchronizeExpectedMetadata(actual))
               .attributes(actual.attributes())
               .contentType(actual.contentEncoding().orElse(null));
      }

      @Override
      protected UnaryOperator<LoadedDocumentContentBuilder> synchronizeExpectedContent(DocumentContent actual) {
         return builder -> builder
               .chain(super.synchronizeExpectedContent(actual))
               .contentEncoding(actual.contentEncoding().orElse(null));
      }

      @Nested
      public class UriAdapter {

         @Test
         public void testGitDocumentConfigBuilderFromUriWhenNominal() {
            assertThat(new GitDocumentConfigBuilder().fromUri(URI.create("https://localhost/path.git"))).isPresent();
         }

         @Test
         public void testGitDocumentConfigBuilderFromUriWhenBadParameters() {
            assertThatExceptionOfType(InvariantValidationException.class)
                  .isThrownBy(() -> new GitDocumentConfigBuilder().fromUri(null))
                  .withMessage("Invariant validation error > 'uri' must not be null");
         }

         @Test
         public void testGitDocumentConfigBuilderFromUriWhenCompatibleUri() {
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("https://localhost/path.git"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("https://localhost/path"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("https://localhost/path#branch"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("https://user@localhost/path.git"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("https://user:password@localhost/path.git"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("https://localhost/path.git#branch"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("git:https://localhost/path.git"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("git:https://localhost/path.git#branch"))).isPresent();

            assertThat(new GitDocumentConfigBuilder().fromUri(uri("http://localhost/path.git"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("http://localhost/path"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("http://localhost/path#branch"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("http://user@localhost/path.git"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("http://user:password@localhost/path.git"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("http://localhost/path.git#branch"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("git:http://localhost/path.git"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("git:http://localhost/path.git#branch"))).isPresent();

            assertThat(new GitDocumentConfigBuilder().fromUri(uri("git://localhost/path.git"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("git://localhost/path"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("git://localhost/path#branch"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("git://user@localhost/path.git"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("git://user:password@localhost/path.git"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("git://localhost/path.git#branch"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("git:git://localhost/path.git"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("git:git://localhost/path.git#branch"))).isPresent();

            assertThat(new GitDocumentConfigBuilder().fromUri(uri("ssh://localhost/path.git"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("ssh://localhost/path"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("ssh://localhost/path#branch"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("ssh://user@localhost/path.git"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("ssh://user:password@localhost/path.git"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("ssh://localhost/path.git#branch"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("git:ssh://localhost/path.git"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("git:ssh://localhost/path.git#branch"))).isPresent();

            assertThat(new GitDocumentConfigBuilder().fromUri(uri("file:/path.git"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("file:/path"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("file:///path.git"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("file:/path.git#branch"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("file:///path.git#branch"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("/path.git"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("/path"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("/path.git#branch"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("git:file:/path.git"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("git:file:/path"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("git:file:/path.git#branch"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("git:/path.git"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("git:/path"))).isPresent();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("git:/path.git#branch"))).isPresent();
         }

         @Test
         public void testGitDocumentConfigBuilderFromUriWhenNotCompatibleUri() {
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("other://localhost/path.git"))).isEmpty();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("file:/path.git?query"))).isEmpty();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("file://path.git"))).isEmpty();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("path.git"))).isEmpty();
            assertThat(new GitDocumentConfigBuilder().fromUri(uri("path.git#branch"))).isEmpty();
         }

         @Test
         public void testToRepositoryUriWhenNominal() {
            assertThat(documentRepository().toUri()).isEqualTo(uri(
                  "https://github.com/github/testrepo.git#email"));
         }

         @Test
         public void testSupportsRepositoryUriWhenNominal() {
            assertThat(documentRepository().supportsUri(uri("https://github.com/github/testrepo.git"),
                                                        false)).isTrue();
            assertThat(documentRepository().supportsUri(uri("https://github.com:443/github/testrepo.git"),
                                                        false)).isTrue();
         }

         @Test
         public void testSupportsRepositoryUriWhenHasBranch() {
            assertThat(documentRepository().supportsUri(uri("https://github.com/github/testrepo.git"),
                                                        false)).isTrue();
            assertThat(documentRepository().supportsUri(uri("https://github.com/github/testrepo.git#email"),
                                                        false)).isTrue();
         }

         @Test
         public void testSupportsRepositoryUriWhenHasNotMatchingBranch() {
            assertThat(documentRepository().supportsUri(uri("https://github.com/github/testrepo.git#bad"),
                                                        false)).isFalse();
         }

         @Test
         public void testSupportsRepositoryUriWhenHasDisambiguationWrapping() {
            assertThat(documentRepository().supportsUri(uri("git:https://github.com/github/testrepo.git"),
                                                        false)).isTrue();
         }

         @Test
         public void testSupportsRepositoryUriWhenHasNotMatchingUri() {
            assertThat(documentRepository().supportsUri(uri("http://github.com/github/testrepo.git"),
                                                        false)).isFalse();
            assertThat(documentRepository().supportsUri(uri("git://github.com/github/testrepo.git"),
                                                        false)).isFalse();
            assertThat(documentRepository().supportsUri(uri("ssh://github.com/github/testrepo.git"),
                                                        false)).isFalse();

            assertThat(documentRepository().supportsUri(uri("https://github.fr/github/testrepo.git"),
                                                        false)).isFalse();
            assertThat(documentRepository().supportsUri(uri("https://github.com/bad/testrepo.git"),
                                                        false)).isFalse();
            assertThat(documentRepository().supportsUri(uri("https://github.com/github/bad.git"),
                                                        false)).isFalse();
            assertThat(documentRepository().supportsUri(uri("https://github.com:444/github/testrepo.git"),
                                                        false)).isFalse();
         }

         @Test
         public void testSupportsRepositoryUriWhenHasNotCompatibleUri() {
            assertThat(documentRepository().supportsUri(uri("scheme://repository"), false))
                  .as("Scheme is not supported")
                  .isFalse();
            assertThat(documentRepository().supportsUri(uri("https://github.com/github/testrepo"), false))
                  .as("Path must finish with .git")
                  .isFalse();
         }

         @Test
         public void testSupportsRepositoryUriWhenHasDifferentButSupportedParts() {
            assertThat(documentRepository().supportsUri(uri("https://otheruser@github.com/github/testrepo.git"),
                                                        false)).isTrue();
            assertThat(documentRepository().supportsUri(uri(
                  "https://otheruser:otherpassword@github.com/github/testrepo.git"), false)).isTrue();
         }

      }

   }

   @Nested
   public class WhenAlternativeUriSchemes {

      @Test
      public void testGitRepositoryWhenWrappedUri() {
         try (FsDocumentRepository localRepository = new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                                                    .deleteRepositoryOnClose(
                                                                                          true)
                                                                                    .build());
              DocumentRepository repository = new GitDocumentRepository(localRepository,
                                                                        new GitDocumentConfigBuilder()
                                                                              .remoteUri(URI.create(
                                                                                    "git:https://github.com/github/testrepo.git"))
                                                                              .branch(REMOTE_BRANCH)
                                                                              .cloneRepositoryIfMissing(true)
                                                                              .build())) {
            assertThat(repository).isNotNull();
         }
      }

      @Test
      public void testGitRepositoryWhenWrappedUriAndBranch() {
         try (FsDocumentRepository localRepository = new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                                                    .deleteRepositoryOnClose(
                                                                                          true)
                                                                                    .build());
              DocumentRepository repository = new GitDocumentRepository(localRepository,
                                                                        new GitDocumentConfigBuilder()
                                                                              .remoteUri(URI.create(
                                                                                    "git:https://github.com/github/testrepo.git#"
                                                                                    + REMOTE_BRANCH))
                                                                              .cloneRepositoryIfMissing(true)
                                                                              .build())) {
            assertThat(repository).isNotNull();
         }
      }

      @Test
      @Disabled("GitHub authentication issues")
      public void testGitRepositoryWhenGitScheme() {
         try (FsDocumentRepository localRepository = new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                                                    .deleteRepositoryOnClose(
                                                                                          true)
                                                                                    .build());
              DocumentRepository repository = new GitDocumentRepository(localRepository,
                                                                        new GitDocumentConfigBuilder()
                                                                              .remoteUri(URI.create(
                                                                                    "git://github.com/github/testrepo.git"))
                                                                              .branch(REMOTE_BRANCH)
                                                                              .cloneRepositoryIfMissing(true)
                                                                              .build())) {
            assertThat(repository).isNotNull();
         }
      }

      @Test
      @Disabled("GitHub authentication issues")
      public void testGitRepositoryWhenSshScheme() {
         try (FsDocumentRepository localRepository = new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                                                    .deleteRepositoryOnClose(
                                                                                          true)
                                                                                    .build());
              DocumentRepository repository = new GitDocumentRepository(localRepository,
                                                                        new GitDocumentConfigBuilder()
                                                                              .remoteUri(URI.create(
                                                                                    "ssh://git@github.com/github/testrepo.git"))
                                                                              .branch(REMOTE_BRANCH)
                                                                              .cloneRepositoryIfMissing(true)
                                                                              .build())) {
            assertThat(repository).isNotNull();
         }

      }

      @Test
      @Disabled("Not supported")
      public void testGitRepositoryWhenSshWithoutScheme() {
         try (FsDocumentRepository localRepository = new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                                                    .deleteRepositoryOnClose(
                                                                                          true)
                                                                                    .build());
              DocumentRepository repository = new GitDocumentRepository(localRepository,
                                                                        new GitDocumentConfigBuilder()
                                                                              .remoteUri(URI.create(
                                                                                    "git@github.com:github/testrepo.git"))
                                                                              .branch(REMOTE_BRANCH)
                                                                              .cloneRepositoryIfMissing(true)
                                                                              .build())) {
            assertThat(repository).isNotNull();
         }
      }

      @Test
      public void testGitRepositoryWhenLocalPath() {
         try (FsDocumentRepository firstLocalRepository = new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                                                         .deleteRepositoryOnClose(
                                                                                               true)
                                                                                         .build());
              GitDocumentRepository firstRepository = new GitDocumentRepository(firstLocalRepository,
                                                                                new GitDocumentConfigBuilder()
                                                                                      .remoteUri(
                                                                                            REMOTE_REPOSITORY_URI)
                                                                                      .branch(REMOTE_BRANCH)
                                                                                      .cloneRepositoryIfMissing(
                                                                                            true)
                                                                                      .build());
              FsDocumentRepository localRepository = new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                                                    .deleteRepositoryOnClose(
                                                                                          true)
                                                                                    .build());
              DocumentRepository repository = new GitDocumentRepository(localRepository,
                                                                        new GitDocumentConfigBuilder()
                                                                              .remoteUri(URI.create(
                                                                                    firstLocalRepository
                                                                                          .storagePath()
                                                                                          .toString()))
                                                                              .branch(REMOTE_BRANCH)
                                                                              .cloneRepositoryIfMissing(true)
                                                                              .build())) {
            assertThat(repository).isNotNull();
         }
      }

   }

   @Nested
   public class WhenUnitializedGitRepository {

      @Test
      public void testGitRepositoryWhenInitializedRepository() {
         try (FsDocumentRepository localRepository = new FsDocumentRepository(new FsDocumentConfigBuilder()
                                                                                    .deleteRepositoryOnClose(
                                                                                          true)
                                                                                    .build())) {
            try (DocumentRepository initializeRepository = new GitDocumentRepository(localRepository,
                                                                                     new GitDocumentConfigBuilder()
                                                                                           .remoteUri(
                                                                                                 REMOTE_REPOSITORY_URI)
                                                                                           .branch(
                                                                                                 REMOTE_BRANCH)
                                                                                           .cloneRepositoryIfMissing(
                                                                                                 false)
                                                                                           .build())) {
               assertThat(initializeRepository).isNotNull();

            }

            try (DocumentRepository repository = new GitDocumentRepository(localRepository,
                                                                           new GitDocumentConfigBuilder()
                                                                                 .remoteUri(
                                                                                       REMOTE_REPOSITORY_URI)
                                                                                 .branch(REMOTE_BRANCH)
                                                                                 .cloneRepositoryIfMissing(
                                                                                       false)
                                                                                 .build())) {
               assertThat(repository).isNotNull();
            }

            try (DocumentRepository repository = new GitDocumentRepository(localRepository,
                                                                           new GitDocumentConfigBuilder()
                                                                                 .remoteUri(
                                                                                       REMOTE_REPOSITORY_URI)
                                                                                 .branch(REMOTE_BRANCH)
                                                                                 .cloneRepositoryIfMissing(
                                                                                       true)
                                                                                 .build())) {
               assertThat(repository).isNotNull();
            }
         }

      }
   }

}