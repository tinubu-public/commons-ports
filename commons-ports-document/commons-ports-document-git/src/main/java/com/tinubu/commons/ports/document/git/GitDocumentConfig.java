/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.git;

import static com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport.checkInvariants;
import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObjectName;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.satisfiesValue;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isGreaterThan;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.ports.document.git.FastForward.FAST_FORWARD;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Duration;
import java.util.Optional;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.Invariant;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.MessageValue;
import com.tinubu.commons.ddd2.valueformatter.HiddenValueFormatter;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.PullStrategy.PullStrategyBuilder;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.PushStrategy.PushStrategyBuilder;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.Transport.TransportBuilder;
import com.tinubu.commons.ports.document.git.gitops.GitUtils;

/**
 * GIT document repository configuration.
 */
public class GitDocumentConfig extends AbstractValue {

   private static final String DEFAULT_BRANCH = "master";
   private static final String DEFAULT_REMOTE = "origin";

   private final URI remoteUri;
   private final String branch;
   private final String remote;
   private final boolean cloneRepositoryIfMissing;
   private final boolean initRepositoryIfMissing;
   private final Authentication authentication;
   private final PullStrategy pullStrategy;
   private final PushStrategy pushStrategy;
   private final Transport transport;

   public GitDocumentConfig(GitDocumentConfigBuilder builder) {
      this.branch = nullable(builder.remoteUri)
            .flatMap(GitDocumentConfig::branchFromRepositoryUri)
            .or(() -> nullable(builder.branch))
            .orElse(DEFAULT_BRANCH);
      this.remoteUri = stripBranchFromRepositoryUri(builder.remoteUri);
      this.remote = nullable(builder.remote, DEFAULT_REMOTE);
      this.cloneRepositoryIfMissing = nullable(builder.cloneRepositoryIfMissing, remoteUri != null);
      this.initRepositoryIfMissing = nullable(builder.initRepositoryIfMissing, true);
      this.authentication = builder.authentication;
      this.pullStrategy = nullable(builder.pullStrategy, new PullStrategyBuilder().build());
      this.pushStrategy = nullable(builder.pushStrategy, new PushStrategyBuilder().build());
      this.transport = nullable(builder.transport, new TransportBuilder().build());
   }

   @Override
   public Fields<? extends GitDocumentConfig> defineDomainFields() {
      return Fields
            .<GitDocumentConfig>builder()
            .field("remoteUri",
                   v -> v.remoteUri,
                   isNotNull("'%s' must be set if 'cloneRepositoryIfMissing' is set", validatingObjectName())
                         .ifIsSatisfied(() -> cloneRepositoryIfMissing)
                         .andValue(isNull().orValue(isGitCompatibleUri())))
            .field("branch", v -> v.branch, isNotBlank())
            .field("remote", v -> v.remote, isNotBlank())
            .field("createRepositoryIfMissing", v -> v.cloneRepositoryIfMissing)
            .field("initRepositoryIfMissing", v -> v.initRepositoryIfMissing)
            .field("authentication", v -> v.authentication)
            .field("pullStrategy", v -> v.pullStrategy, isNotNull())
            .field("pushStrategy", v -> v.pushStrategy, isNotNull())
            .field("transport", v -> v.transport, isNotNull())
            .build();
   }

   /**
    * GIT remote URI.
    */
   public URI remoteUri() {
      return remoteUri;
   }

   /**
    * Branch name. Default to {@link #DEFAULT_BRANCH}.
    * If a branch is set in URI in the fragment part, it will take precedence over explicit branch
    * configuration.
    */
   public String branch() {
      return branch;
   }

   /**
    * Remote name for this repository implementation in GIT repository configuration. Default to
    * {@link #DEFAULT_REMOTE}
    */
   public String remote() {
      return remote;
   }

   /**
    * Whether to automatically clone an uninitialized GIT repository. Default to {@code true} if
    * {@link #remoteUri} is set.
    */
   public boolean cloneRepositoryIfMissing() {
      return cloneRepositoryIfMissing;
   }

   /**
    * Whether to automatically initialize an uninitialized GIT repository. Default to {@code true}.
    */
   public boolean initRepositoryIfMissing() {
      return initRepositoryIfMissing;
   }

   /**
    * GIT authentication information if any.
    *
    * @return authentication information or {@link Optional#empty()} if no authentication required
    */
   public Optional<Authentication> authentication() {
      return nullable(authentication);
   }

   /**
    * GIT pull strategy configuration.
    *
    * @return GIT pull strategy configuration
    */
   public PullStrategy pullStrategy() {
      return pullStrategy;
   }

   /**
    * GIT push strategy configuration.
    *
    * @return GIT push strategy configuration
    */
   public PushStrategy pushStrategy() {
      return pushStrategy;
   }

   /**
    * GIT server transport configuration.
    *
    * @return GIT server transport configuration
    */
   public Transport transport() {
      return transport;
   }

   private static Optional<String> branchFromRepositoryUri(URI remoteUri) {
      validate(remoteUri, "remoteUri", isNotNull()).orThrow();

      return nullable(remoteUri.getFragment());
   }

   private static URI stripBranchFromRepositoryUri(URI remoteUri) {
      if (remoteUri == null) {
         return null;
      }
      try {
         return new URI(remoteUri.getScheme(), remoteUri.getSchemeSpecificPart(), null);
      } catch (URISyntaxException e) {
         throw new IllegalStateException(e);
      }
   }

   private InvariantRule<URI> isGitCompatibleUri() {
      return satisfiesValue(GitUtils::isCompatibleRepositoryUri,
                            "'%s' URI must be GIT-compatible",
                            validatingObject());
   }

   /**
    * GIT authentication information.
    * Known platforms authentication :
    * <ul>
    *    <li>GitHub : username={@code token}, password={@code }</li>
    *    <li>Gitlab : username={@code PRIVATE-TOKEN}, password={@code token}</li>
    *    <li>Gitlab : username={@code oauth2}, password={@code token}</li>
    * </ul>
    * <p>
    * Note that authentication information is also supported from repository URI user infos.
    */
   public static class Authentication extends AbstractValue {
      /**
       * GIT user name.
       */
      private final String username;

      /**
       * GIT user password.
       */
      private final String password;

      private Authentication(String username, String password) {
         this.username = username;
         this.password = password;
      }

      public static Authentication of(String username, String password) {
         return checkInvariants(new Authentication(username, password));
      }

      @Override
      public Fields<? extends Authentication> defineDomainFields() {
         return Fields
               .<Authentication>builder()
               .field("username",
                      v -> v.username,
                      v -> new HiddenValueFormatter().compose(Authentication::username),
                      isNotBlank())
               .field("password",
                      v -> v.password,
                      v -> new HiddenValueFormatter().compose(Authentication::password),
                      isNotNull())
               .build();
      }

      public String username() {
         return username;
      }

      public String password() {
         return password;
      }

   }

   /**
    * GIT pull strategy configuration.
    */
   public static class PullStrategy extends AbstractValue {
      private static final boolean DEFAULT_REBASE = false;
      private static final FastForward DEFAULT_FAST_FORWARD = FAST_FORWARD;
      private static final PullMode DEFAULT_UPDATE_MODE = PullMode.NEVER;

      private final boolean rebase;
      private final FastForward fastForward;
      private final PullMode pullMode;
      private final Integer shallowDepth;

      public enum PullMode {
         /** Pull the repository once at document repository initialization. */
         ON_CREATE,
         /** Pull the repository before each document repository operation call. */
         ALWAYS,
         /** Never pull the repository. */
         NEVER;
      }

      private PullStrategy(PullStrategyBuilder builder) {
         this.rebase = nullable(builder.rebase, DEFAULT_REBASE);
         this.fastForward = nullable(builder.fastForward, DEFAULT_FAST_FORWARD);
         this.pullMode = nullable(builder.pullMode, DEFAULT_UPDATE_MODE);
         this.shallowDepth = builder.shallowDepth;
      }

      @Override
      public Fields<? extends PullStrategy> defineDomainFields() {
         return Fields
               .<PullStrategy>builder()
               .field("rebase", v -> v.rebase)
               .field("fastForward", v -> v.fastForward, isNotNull())
               .field("pullMode", v -> v.pullMode, isNotNull())
               .field("shallowDepth", v -> v.shallowDepth, isNull().orValue(isGreaterThan(value(0))))
               .build();
      }

      /**
       * Rebase when merging. Default to {@value #DEFAULT_REBASE}.
       */
      public boolean rebase() {
         return rebase;
      }

      /**
       * Fast-forward when merging. Default to {@link #DEFAULT_FAST_FORWARD}
       */
      public FastForward fastForward() {
         return fastForward;
      }

      /**
       * Pull strategy when using document repository. By default, repository is never updated.
       */
      public PullMode pullMode() {
         return pullMode;
      }

      /**
       * Shallow fetch depth. Can be {@link Optional#empty()} if fetch is not shallow, otherwise, strictly
       * positive number of commits to fetch.
       *
       * @return shallow fetch depth
       */
      public Optional<Integer> shallowDepth() {
         return nullable(shallowDepth);
      }

      public static class PullStrategyBuilder extends DomainBuilder<PullStrategy> {
         private Boolean rebase;
         private FastForward fastForward;
         private PullMode pullMode;
         private Integer shallowDepth;

         public static PullStrategy never() {
            return new PullStrategyBuilder().pullMode(PullMode.NEVER).build();
         }

         public static PullStrategy always() {
            return new PullStrategyBuilder().pullMode(PullMode.ALWAYS).build();
         }

         public static PullStrategy onCreate() {
            return new PullStrategyBuilder().pullMode(PullMode.ON_CREATE).build();
         }

         public PullStrategyBuilder rebase(Boolean rebase) {
            this.rebase = rebase;
            return this;
         }

         public PullStrategyBuilder fastForward(FastForward fastForward) {
            this.fastForward = fastForward;
            return this;
         }

         public PullStrategyBuilder pullMode(PullMode pullMode) {
            this.pullMode = pullMode;
            return this;
         }

         public PullStrategyBuilder shallowDepth(Integer shallowDepth) {
            this.shallowDepth = shallowDepth;
            return this;
         }

         @Override
         protected PullStrategy buildDomainObject() {
            return new PullStrategy(this);
         }
      }
   }

   /**
    * GIT push strategy configuration.
    */
   public static class PushStrategy extends AbstractValue {
      private static final PushMode DEFAULT_COMMIT_MODE = PushMode.NEVER;
      private static final PushMode DEFAULT_PUSH_MODE = PushMode.NEVER;

      private final PushMode commitMode;
      private final PushMode pushMode;

      public enum PushMode {
         /** Never commit/push. */
         NEVER,
         /** Commit/push the repository after each document repository operation call. */
         ALWAYS,
         /** Commit/push the repository once on repository close. */
         ON_CLOSE
      }

      private PushStrategy(PushStrategyBuilder builder) {
         this.commitMode = nullable(builder.commitMode, DEFAULT_COMMIT_MODE);
         this.pushMode = nullable(builder.pushMode, DEFAULT_PUSH_MODE);
      }

      @Override
      public Fields<? extends PushStrategy> defineDomainFields() {
         return Fields
               .<PushStrategy>builder()
               .field("commitMode", v -> v.commitMode, isNotNull())
               .field("pushMode", v -> v.pushMode, isNotNull())
               .invariants(Invariant.of(() -> this, areCompatibleModes()))
               .build();
      }

      private InvariantRule<PushStrategy> areCompatibleModes() {
         return satisfiesValue(strategy -> {
                                  switch (pushMode) {
                                     case NEVER:
                                        return true;
                                     case ALWAYS:
                                        return commitMode == PushMode.ALWAYS;
                                     case ON_CLOSE:
                                        return commitMode == PushMode.ALWAYS || commitMode == PushMode.ON_CLOSE;
                                     default:
                                        throw new IllegalStateException();
                                  }
                               },
                               "'%s' commit mode must be compatible with '%s' push mode",
                               MessageValue.value(commitMode),
                               MessageValue.value(pushMode));
      }

      /**
       * Commit strategy when using document repository. By default, repository is never committed.
       */
      public PushMode commitMode() {
         return commitMode;
      }

      /**
       * Push strategy when using document repository. By default, repository is never pushed.
       */
      public PushMode pushMode() {
         return pushMode;
      }

      public static class PushStrategyBuilder extends DomainBuilder<PushStrategy> {
         private PushMode commitMode;
         private PushMode pushMode;

         public static PushStrategy never() {
            return new PushStrategyBuilder().pushMode(PushMode.NEVER).build();
         }

         public static PushStrategy always() {
            return new PushStrategyBuilder().pushMode(PushMode.ALWAYS).build();
         }

         public static PushStrategy onClose() {
            return new PushStrategyBuilder().pushMode(PushMode.ON_CLOSE).build();
         }

         public PushMode commitMode() {
            return commitMode;
         }

         public PushStrategyBuilder commitMode(PushMode commitMode) {
            this.commitMode = commitMode;
            return this;
         }

         public PushMode pushMode() {
            return pushMode;
         }

         public PushStrategyBuilder pushMode(PushMode pushMode) {
            this.pushMode = pushMode;
            if (commitMode == null) {
               commitMode = pushMode;
            }
            return this;
         }

         @Override
         protected PushStrategy buildDomainObject() {
            return new PushStrategy(this);
         }
      }
   }

   /**
    * GIT server transport configuration.
    */
   public static class Transport extends AbstractValue {
      private static final Duration DEFAULT_CONNECT_TIMEOUT = Duration.ofSeconds(10);
      private static final boolean DEFAULT_DISABLE_SSL_VERIFY = false;
      private static final boolean DEFAULT_ALLOW_SSH_HOSTS = false;

      private final Duration connectTimeout;
      private final boolean disableSslVerify;
      private final boolean allowSshHosts;

      private Transport(TransportBuilder builder) {
         this.connectTimeout = nullable(builder.connectTimeout, DEFAULT_CONNECT_TIMEOUT);
         this.disableSslVerify = nullable(builder.disableSslVerify, DEFAULT_DISABLE_SSL_VERIFY);
         this.allowSshHosts = nullable(builder.allowSshHosts, DEFAULT_ALLOW_SSH_HOSTS);
      }

      @Override
      public Fields<? extends Transport> defineDomainFields() {
         return Fields
               .<Transport>builder()
               .field("connectTimeout", v -> v.connectTimeout, isNotNull())
               .field("disableSslVerify", v -> v.disableSslVerify)
               .field("allowSshHosts", v -> v.allowSshHosts)
               .build();
      }

      /**
       * GIT server connection timeout.
       *
       * @return GIT server connection timeout
       */
      public Duration connectTimeout() {
         return connectTimeout;
      }

      /**
       * Disables SSL certificate verification (auto-signed certificates, ...).
       *
       * @return {@code true} if SSL certificate verification is disabled
       */
      public boolean disableSslVerify() {
         return disableSslVerify;
      }

      /**
       * Allows unknown SSH hosts.
       *
       * @return {@code true} if unknown SSH hosts are allowed
       */
      public boolean allowSshHosts() {
         return allowSshHosts;
      }

      public static class TransportBuilder extends DomainBuilder<Transport> {
         private Duration connectTimeout;
         private Boolean disableSslVerify;
         private Boolean allowSshHosts;

         public Duration connectTimeout() {
            return connectTimeout;
         }

         public TransportBuilder connectTimeout(Duration connectTimeout) {
            this.connectTimeout = connectTimeout;
            return this;
         }

         public Boolean disableSslVerify() {
            return disableSslVerify;
         }

         public TransportBuilder disableSslVerify(Boolean disableSslVerify) {
            this.disableSslVerify = disableSslVerify;
            return this;
         }

         public Boolean allowSshHosts() {
            return allowSshHosts;
         }

         public TransportBuilder allowSshHosts(Boolean allowSshHosts) {
            this.allowSshHosts = allowSshHosts;
            return this;
         }

         @Override
         protected Transport buildDomainObject() {
            return new Transport(this);
         }
      }

   }

   public static class GitDocumentConfigBuilder extends DomainBuilder<GitDocumentConfig> {

      private URI remoteUri;
      private String branch;
      private String remote;
      private Boolean cloneRepositoryIfMissing;
      private Boolean initRepositoryIfMissing;
      private Authentication authentication;
      private PullStrategy pullStrategy;
      private PushStrategy pushStrategy;
      private Transport transport;

      /**
       * Returns a pre-configured builder from specified document repository URI information, only if URI is
       * compatible with this repository.
       *
       * @param uri URI
       *
       * @return pre-configured builder or {@link Optional#empty} if URI is not compatible with this repository.
       */
      public Optional<GitDocumentConfigBuilder> fromUri(URI uri) {
         validate(uri, "uri", isNotNull()).orThrow();

         GitDocumentConfigBuilder config = null;
         if (GitUtils.isCompatibleRepositoryUri(uri)) {
            config = new GitDocumentConfigBuilder().<GitDocumentConfigBuilder>reconstitute().remoteUri(uri);
         }

         return nullable(config);
      }

      public URI remoteUri() {
         return remoteUri;
      }

      public GitDocumentConfigBuilder remoteUri(URI remoteUri) {
         this.remoteUri = remoteUri;
         return this;
      }

      public String branch() {
         return branch;
      }

      public GitDocumentConfigBuilder branch(String branch) {
         this.branch = branch;
         return this;
      }

      public String remote() {
         return remote;
      }

      public GitDocumentConfigBuilder remote(String remote) {
         this.remote = remote;
         return this;
      }

      public Boolean cloneRepositoryIfMissing() {
         return cloneRepositoryIfMissing;
      }

      public GitDocumentConfigBuilder cloneRepositoryIfMissing(Boolean cloneRepositoryIfMissing) {
         this.cloneRepositoryIfMissing = cloneRepositoryIfMissing;
         return this;
      }

      public Boolean initRepositoryIfMissing() {
         return initRepositoryIfMissing;
      }

      public GitDocumentConfigBuilder initRepositoryIfMissing(Boolean initRepositoryIfMissing) {
         this.initRepositoryIfMissing = initRepositoryIfMissing;
         return this;
      }

      public Authentication authentication() {
         return authentication;
      }

      public GitDocumentConfigBuilder authentication(Authentication authentication) {
         this.authentication = authentication;
         return this;
      }

      public PullStrategy pullStrategy() {
         return pullStrategy;
      }

      public GitDocumentConfigBuilder pullStrategy(PullStrategy pullStrategy) {
         this.pullStrategy = pullStrategy;
         return this;
      }

      public PushStrategy pushStrategy() {
         return pushStrategy;
      }

      public GitDocumentConfigBuilder pushStrategy(PushStrategy pushStrategy) {
         this.pushStrategy = pushStrategy;
         return this;
      }

      public Transport transport() {
         return transport;
      }

      public GitDocumentConfigBuilder transport(Transport transport) {
         this.transport = transport;
         return this;
      }

      @Override
      protected GitDocumentConfig buildDomainObject() {
         return new GitDocumentConfig(this);
      }
   }
}
