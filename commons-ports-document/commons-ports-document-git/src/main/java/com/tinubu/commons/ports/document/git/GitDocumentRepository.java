/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.git;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.hasNoBlankElements;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.hasNoNullElements;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.size;
import static com.tinubu.commons.ddd2.invariant.rules.NumberRules.isStrictlyPositive;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.lang.util.CollectionUtils.collectionConcat;
import static com.tinubu.commons.lang.util.CollectionUtils.collectionIntersection;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.OptionalUtils.peek;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.util.StreamUtils.streamConcat;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.DOCUMENT_URI;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.ITERABLE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.OPEN;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.OPEN_APPEND;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.QUERYABLE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.READABLE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.REPOSITORY_URI;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.SUBPATH;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.WRITABLE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.allMetadataCapabilities;
import static com.tinubu.commons.ports.document.git.gitops.GitUtils.branchExists;
import static com.tinubu.commons.ports.document.git.gitops.GitUtils.checkPushResults;
import static com.tinubu.commons.ports.document.git.gitops.GitUtils.fileSystemRepository;
import static com.tinubu.commons.ports.document.git.gitops.GitUtils.isCompatibleRepositoryUri;
import static com.tinubu.commons.ports.document.git.gitops.GitUtils.isWrappedRepositoryUri;
import static com.tinubu.commons.ports.document.git.gitops.GitUtils.repositoryUriPort;
import static com.tinubu.commons.ports.document.git.gitops.GitUtils.skipPromptCredentialsProvider;
import static com.tinubu.commons.ports.document.git.gitops.GitUtils.unwrapRepositoryUri;
import static java.util.stream.Collectors.joining;
import static org.eclipse.jgit.api.MergeCommand.FastForwardMode.FF;
import static org.eclipse.jgit.api.MergeCommand.FastForwardMode.FF_ONLY;
import static org.eclipse.jgit.api.MergeCommand.FastForwardMode.NO_FF;
import static org.eclipse.jgit.api.RemoteSetUrlCommand.UriType.FETCH;
import static org.eclipse.jgit.lib.CommitConfig.CleanupMode.DEFAULT;
import static org.eclipse.jgit.merge.ContentMergeStrategy.CONFLICT;
import static org.eclipse.jgit.merge.ContentMergeStrategy.OURS;
import static org.eclipse.jgit.merge.ContentMergeStrategy.THEIRS;
import static org.eclipse.jgit.merge.MergeStrategy.RECURSIVE;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.StringJoiner;
import java.util.function.Consumer;
import java.util.stream.Stream;

import org.apache.commons.lang3.time.StopWatch;
import org.eclipse.jgit.api.AddCommand;
import org.eclipse.jgit.api.CheckoutCommand;
import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.CommitCommand;
import org.eclipse.jgit.api.FetchCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.MergeCommand;
import org.eclipse.jgit.api.MergeCommand.FastForwardMode;
import org.eclipse.jgit.api.MergeResult;
import org.eclipse.jgit.api.PullCommand;
import org.eclipse.jgit.api.PushCommand;
import org.eclipse.jgit.api.RemoteSetUrlCommand.UriType;
import org.eclipse.jgit.api.RmCommand;
import org.eclipse.jgit.api.Status;
import org.eclipse.jgit.api.TransportCommand;
import org.eclipse.jgit.api.errors.CheckoutConflictException;
import org.eclipse.jgit.api.errors.EmptyCommitException;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidConfigurationException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.JGitInternalException;
import org.eclipse.jgit.api.errors.NoHeadException;
import org.eclipse.jgit.api.errors.RefNotAdvertisedException;
import org.eclipse.jgit.api.errors.RefNotFoundException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.api.errors.WrongRepositoryStateException;
import org.eclipse.jgit.errors.LockFailedException;
import org.eclipse.jgit.errors.MissingObjectException;
import org.eclipse.jgit.lib.ConfigConstants;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectLoader;
import org.eclipse.jgit.lib.ProgressMonitor;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.transport.ChainingCredentialsProvider;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.NetRCCredentialsProvider;
import org.eclipse.jgit.transport.RefSpec;
import org.eclipse.jgit.transport.TagOpt;
import org.eclipse.jgit.transport.URIish;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinubu.commons.ddd2.domain.event.DomainEvent;
import com.tinubu.commons.ddd2.domain.event.DomainEventListener;
import com.tinubu.commons.ddd2.domain.event.RegistrableDomainEventService;
import com.tinubu.commons.ddd2.domain.event.SynchronousDomainEventService;
import com.tinubu.commons.ddd2.domain.specification.Specification;
import com.tinubu.commons.ddd2.invariant.rules.ArrayRules;
import com.tinubu.commons.ddd2.invariant.rules.CollectionRules;
import com.tinubu.commons.lang.validation.CheckReturnValue;
import com.tinubu.commons.ports.document.domain.AbstractDocumentRepository;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentEntry.DocumentEntryBuilder;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata;
import com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent.OutputStreamDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.capability.RepositoryCapability;
import com.tinubu.commons.ports.document.domain.capability.UnsupportedCapabilityException;
import com.tinubu.commons.ports.document.fs.FsDocumentRepository;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.Authentication;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.PullStrategy;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.PullStrategy.PullMode;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.PushStrategy;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.PushStrategy.PushMode;
import com.tinubu.commons.ports.document.git.GitDocumentConfig.Transport;
import com.tinubu.commons.ports.document.git.exception.CheckoutConflictGitException;
import com.tinubu.commons.ports.document.git.exception.EmptyCommitGitException;
import com.tinubu.commons.ports.document.git.exception.GitException;
import com.tinubu.commons.ports.document.git.exception.InvalidConfigurationGitException;
import com.tinubu.commons.ports.document.git.exception.InvalidRemoteGitException;
import com.tinubu.commons.ports.document.git.exception.IoGitException;
import com.tinubu.commons.ports.document.git.exception.LockFailedGitException;
import com.tinubu.commons.ports.document.git.exception.MergeFailedGitException;
import com.tinubu.commons.ports.document.git.exception.NoHeadGitException;
import com.tinubu.commons.ports.document.git.exception.NotCleanGitException;
import com.tinubu.commons.ports.document.git.exception.PushFailedGitException;
import com.tinubu.commons.ports.document.git.exception.RefNotAdvertisedGitException;
import com.tinubu.commons.ports.document.git.exception.RefNotFoundGitException;
import com.tinubu.commons.ports.document.git.exception.TransportGitException;
import com.tinubu.commons.ports.document.git.exception.UnknownRevisionGitException;
import com.tinubu.commons.ports.document.git.exception.WrongRepositoryStateGitException;
import com.tinubu.commons.ports.document.git.gitops.GitUtils;

/**
 * GIT {@link DocumentRepository} adapter implementation.
 * <p>
 * Limitations :
 * <ul>
 *    <li>intermediate {@link FsDocumentRepository} storage :<ul>
 *      <li>the following Metadata are not persisted: documentPath, contentEncoding, contentType, attributes</li>
 *      <li>depending on OS/FS driver: Metadata creationDate, lastUpdateDate best precision is generally second</li>
 *      <li>depending on OS/FS driver: creationDate can be reset when overriding an existing document</li>
 *   </ul></li>
 * </ul>
 *
 * @implSpec Immutable class implementation
 * @apiNote This repository is not thread-safe by design, because of GIT operations side effects
 *       (indexed documents, etc...)
 */
public class GitDocumentRepository extends AbstractDocumentRepository {

   private static final Logger log = LoggerFactory.getLogger(GitDocumentRepository.class);

   private final Git git;
   private final FsDocumentRepository localRepository;
   private URI remoteUri;
   private final String branch;
   private final String remote;
   private final boolean cloneRepositoryIfMissing;
   private final boolean initRepositoryIfMissing;
   private final CredentialsProvider credentialsProvider;
   private final PullStrategy pullStrategy;
   private final PushStrategy pushStrategy;
   private final Transport transport;
   private boolean closed = false;

   /**
    * Creates a new Git document repository wrapping specified local file-system repository.
    * Wrapped document repository won't be automatically closed on {@link #close}, it has to be closed
    * externally.
    *
    * @param localRepository local file-system document repository to apply GIT onto
    * @param gitDocumentConfig repository configuration
    */
   // FIXME returns and supports current repo branch in URIs and other places instead of hard-coded branch field ?
   public GitDocumentRepository(FsDocumentRepository localRepository,
                                GitDocumentConfig gitDocumentConfig,
                                RegistrableDomainEventService eventService) {
      super(eventService);

      validate(gitDocumentConfig, "gitDocumentConfig", isNotNull()).orThrow();

      this.remoteUri = unwrapRepositoryUri(gitDocumentConfig.remoteUri());
      this.branch = gitDocumentConfig.branch();
      this.remote = gitDocumentConfig.remote();
      this.cloneRepositoryIfMissing = gitDocumentConfig.cloneRepositoryIfMissing();
      this.initRepositoryIfMissing = gitDocumentConfig.initRepositoryIfMissing();
      this.credentialsProvider = credentialsProvider(gitDocumentConfig.authentication().orElse(null),
                                                     gitDocumentConfig.transport());
      this.pullStrategy = gitDocumentConfig.pullStrategy();
      this.pushStrategy = gitDocumentConfig.pushStrategy();
      this.transport = gitDocumentConfig.transport();

      this.localRepository = localRepository;
      this.git = initializeRepository();

      if (remoteUri == null) {
         remoteUri = remoteGetUrl(git, remote);
      }
   }

   public GitDocumentRepository(FsDocumentRepository localRepository, GitDocumentConfig gitDocumentConfig) {
      this(localRepository, gitDocumentConfig, new SynchronousDomainEventService());
   }

   @Override
   public HashSet<RepositoryCapability> capabilities() {
      HashSet<RepositoryCapability> localRepositoryMetadataCapabilities =
            collectionIntersection(HashSet::new, localRepository.capabilities(), allMetadataCapabilities());

      return collectionConcat(HashSet::new,
                              list(WRITABLE,
                                   READABLE,
                                   ITERABLE,
                                   QUERYABLE,
                                   OPEN,
                                   OPEN_APPEND,
                                   REPOSITORY_URI),
                              localRepositoryMetadataCapabilities);
   }

   @Override
   public boolean sameRepositoryAs(com.tinubu.commons.ddd2.domain.repository.Repository<Document, DocumentPath> documentRepository) {
      validate(documentRepository, "documentRepository", isNotNull()).orThrow();

      return documentRepository instanceof GitDocumentRepository
             && ((GitDocumentRepository) documentRepository).localRepository.sameRepositoryAs(localRepository);
   }

   @Override
   public GitDocumentRepository subPath(Path subPath, boolean shareContext) {
      throw new UnsupportedCapabilityException(SUBPATH);
   }

   @Override
   @CheckReturnValue
   public Optional<Document> openDocument(DocumentPath documentId,
                                          boolean overwrite,
                                          boolean append,
                                          OpenDocumentMetadata metadata) {
      applyPullStrategy(false);

      return localRepository
            .openDocument(documentId, overwrite, append, metadata)
            .map(document -> finalizeOpenDocument(document, __ -> {
               if (pushStrategy.commitMode() == PushMode.ALWAYS) {
                  addDocumentToIndex(document.documentId());
               }
               applyPushStrategy(String.format("Save '%s' document (overwrite=%s, append=%s)",
                                               document.documentId().stringValue(),
                                               overwrite,
                                               append), false);
            }));
   }

   @Override
   public Optional<Document> findDocumentById(DocumentPath documentId) {
      applyPullStrategy(false);

      return localRepository.findDocumentById(documentId);
   }

   /**
    * Finds GIT document by id from any GIT branch/commit/... using specified GIT revision string.
    *
    * @param documentId document id
    * @param revision GIT revision string
    *
    * @return found document or {@link Optional#empty()} if revision string not found, or document not found
    *
    * @throws UnknownRevisionGitException if specified revision can't be resolved
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   public Optional<Document> findDocumentById(String revision, DocumentPath documentId) {
      applyPullStrategy(false);

      try {
         return loadDocument(git, revision, documentId);
      } catch (IoGitException e) {
         throw new DocumentAccessException(e.getMessage(), e);
      }
   }

   /**
    * Finds GIT document entry by id from any GIT branch/commit/... using specified GIT revision string. This
    * enables to only retrieve metadata and not document content.
    * If document exists, a document metadata is always returned with available data.
    *
    * @param documentId document id
    * @param revision GIT revision string
    *
    * @return found document or {@link Optional#empty()} if revision string not found, or document not found
    *
    * @throws UnknownRevisionGitException if specified revision can't be resolved
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   public Optional<DocumentEntry> findDocumentEntryById(String revision, DocumentPath documentId) {
      applyPullStrategy(false);

      try {
         return loadDocumentEntry(git, revision, documentId);
      } catch (IoGitException e) {
         throw new DocumentAccessException(e.getMessage(), e);
      }
   }

   @Override
   public Stream<Document> findDocumentsBySpecification(Path basePath,
                                                        Specification<DocumentEntry> specification) {
      applyPullStrategy(false);

      return localRepository.findDocumentsBySpecification(basePath, specification);
   }

   @Override
   public Stream<Document> findDocumentsBySpecification(Specification<DocumentEntry> specification) {
      applyPullStrategy(false);

      return localRepository.findDocumentsBySpecification(specification);
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryById(DocumentPath documentId) {
      applyPullStrategy(false);

      return localRepository.findDocumentEntryById(documentId);
   }

   @Override
   public Stream<DocumentEntry> findDocumentEntriesBySpecification(Path basePath,
                                                                   Specification<DocumentEntry> specification) {
      applyPullStrategy(false);

      return localRepository.findDocumentEntriesBySpecification(basePath, specification);
   }

   @Override
   public Stream<DocumentEntry> findDocumentEntriesBySpecification(Specification<DocumentEntry> specification) {
      applyPullStrategy(false);

      return localRepository.findDocumentEntriesBySpecification(specification);
   }

   @Override
   @CheckReturnValue
   public Optional<DocumentEntry> saveDocument(Document document, boolean overwrite) {
      applyPullStrategy(false);

      return peek(localRepository.saveDocument(document, overwrite), documentEntry -> {
         if (pushStrategy.commitMode() == PushMode.ALWAYS) {
            addDocumentToIndex(documentEntry.documentId());
         }
         applyPushStrategy(String.format("Save '%s' document (overwrite=%s)",
                                         document.documentId().stringValue(),
                                         overwrite), false);
      });
   }

   @Override
   @CheckReturnValue
   public Optional<Document> saveAndReturnDocument(Document document, boolean overwrite) {
      applyPullStrategy(false);

      return peek(localRepository.saveAndReturnDocument(document, overwrite), savedDocument -> {
         if (pushStrategy.commitMode() == PushMode.ALWAYS) {
            addDocumentToIndex(savedDocument.documentId());
         }
         applyPushStrategy(String.format("Save '%s' document (overwrite=%s)",
                                         savedDocument.documentId().stringValue(),
                                         overwrite), false);
      });
   }

   @Override
   @CheckReturnValue
   public Optional<DocumentEntry> deleteDocumentById(DocumentPath documentId) {
      applyPullStrategy(false);

      return peek(localRepository.deleteDocumentById(documentId), documentEntry -> {
         if (pushStrategy.commitMode() == PushMode.ALWAYS) {
            removeDocumentFromIndex(true, documentEntry.documentId());
         }
         applyPushStrategy(String.format("Delete '%s' document", documentEntry.documentId().stringValue()),
                           false);
      });
   }

   @Override
   @CheckReturnValue
   public List<DocumentEntry> deleteDocumentsBySpecification(Path basePath,
                                                             Specification<DocumentEntry> specification) {
      applyPullStrategy(false);

      List<DocumentEntry> deletedDocuments =
            localRepository.deleteDocumentsBySpecification(basePath, specification);

      if (pushStrategy.commitMode() == PushMode.ALWAYS) {
         deletedDocuments.forEach(documentEntry -> removeDocumentFromIndex(true, documentEntry.documentId()));
      }

      applyPushStrategy(String.format("Delete specified documents from '%s' base path", basePath), false);

      return deletedDocuments;
   }

   @Override
   public void close() {
      if (!closed) {
         closed = true;

         if (git != null) {
            try {
               if (pushStrategy.commitMode() == PushMode.ON_CLOSE) {
                  addAllDocumentsToIndex();
               }
               applyPushStrategy(onCloseCommitMessage(), true);
            } finally {
               git.getRepository().close();
            }
         }
      }
   }

   @Override
   public <E extends DomainEvent> void registerEventListener(DomainEventListener<? super E> eventListener,
                                                             Class<? extends E>... eventClasses) {
      localRepository.registerEventListener(eventListener, eventClasses);
   }

   @Override
   public void unregisterEventListeners() {
      localRepository.unregisterEventListeners();
   }

   @Override
   public URI toUri() {
      if (remoteUri == null) {
         throw new UnsupportedCapabilityException(REPOSITORY_URI,
                                                  String.format("Repository has no URI set for '%s' remote",
                                                                remote));
      } else {
         try {
            return new URI(remoteUri.getScheme(),
                           remoteUri.getAuthority(),
                           remoteUri.getPath(),
                           remoteUri.getQuery(),
                           branch);
         } catch (URISyntaxException e) {
            throw new IllegalStateException(e);
         }
      }
   }

   @Override
   public URI toUri(DocumentPath documentId) {
      throw new UnsupportedCapabilityException(DOCUMENT_URI);
   }

   @Override
   public boolean supportsUri(URI uri, boolean documentUri) {
      validate(uri, "uri", isNotNull()).orThrow();

      if (documentUri) {
         return false;
      }

      if (remoteUri == null) {
         throw new UnsupportedCapabilityException(REPOSITORY_URI,
                                                  String.format("Repository has no URI set for '%s' remote",
                                                                remote));
      } else {
         if (isWrappedRepositoryUri(uri)) {
            return supportsUri(unwrapRepositoryUri(uri), documentUri);
         }

         return isCompatibleRepositoryUri(uri)
                && (Objects.equals(uri.getScheme(), remoteUri.getScheme())
                    && Objects.equals(uri.getHost(), remoteUri.getHost())
                    && repositoryUriPort(uri) == repositoryUriPort(remoteUri))
                && uri.getPath().equals(remoteUri.getPath())
                && (uri.getFragment() == null || uri.getFragment().equals(branch));
      }
   }

   @Override
   public Optional<Document> findDocumentByUri(URI documentUri) {
      throw new UnsupportedCapabilityException(DOCUMENT_URI);
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryByUri(URI documentUri) {
      throw new UnsupportedCapabilityException(DOCUMENT_URI);
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", GitDocumentRepository.class.getSimpleName() + "[", "]")
            .add("localRepository=" + localRepository)
            .add("remote=" + remote)
            .add("remoteUri=" + remoteUri)
            .add("branch=" + branch)
            .toString();
   }

   /**
    * Returns access to internal porcelain GIT client directed at this repository location. .
    *
    * @return GIT porcelain client
    *
    * @apiNote You can close the {@link Git} client as it does nothing, but you must not close the
    *       underlying {@link Git#getRepository()}.
    */
   public Git git() {
      return git;
   }

   /**
    * Checks if working tree is clean
    *
    * @return {@code true} if working tree is clean
    *
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    */
   public boolean isClean() {
      try {
         return GitUtils.isClean(git);
      } catch (Exception e) {
         throw wrapGitException(e, "Can't check if repository is clean");
      }
   }

   /**
    * Returns current branch name.
    *
    * @return current branch name
    *
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    */
   public String branch() {
      try {
         return git.getRepository().getBranch();
      } catch (Exception e) {
         throw wrapGitException(e, "Can't detect current branch");
      }
   }

   /**
    * Resolve GIT revision to GIT object id.
    *
    * @param revision GIT revision
    *
    * @return GIT object id or {@link Optional#empty} if revision can't be found
    *
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    */
   public Optional<String> resolveObjectId(String revision) {
      validate(revision, "revision", isNotBlank()).orThrow();

      return resolve(git, revision).map(ObjectId::getName);
   }

   /**
    * Returns whether current branch has {@code HEAD}.
    *
    * @return {@code true} if current branch has {@code HEAD}
    *
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    */
   public boolean branchHasHead() {
      try {
         return git.getRepository().resolve("HEAD") != null;
      } catch (Exception e) {
         throw wrapGitException(e, "Can't check if branch has head");
      }
   }

   /**
    * Update repository's current branch using configured remote. Repository must be clean if configured pull
    * strategy use rebase.
    *
    * @param fastForward fast-forward mode
    * @param rebase whether to rebase
    *
    * @throws NotCleanGitException if repository is not clean
    * @throws NoHeadGitException if required {@code HEAD} ref not found
    * @throws RefNotAdvertisedGitException if required ref not advertised
    * @throws RefNotFoundGitException if ref can't be resolved
    * @throws WrongRepositoryStateGitException if repository is not in a correct state for the operation
    * @throws InvalidRemoteGitException if remote is invalid
    * @throws TransportGitException if an error occurs in transport protocol
    * @throws InvalidConfigurationGitException if configuration is malformed
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    */
   public void pull(FastForward fastForward, boolean rebase) {
      pull(remote, fastForward, rebase);
   }

   /**
    * Update repository's current branch using specified remote. Repository must be clean if configured pull
    * strategy use rebase.
    *
    * @param remote remote name
    * @param fastForward fast-forward mode
    * @param rebase whether to rebase
    *
    * @throws NotCleanGitException if repository is not clean
    * @throws NoHeadGitException if required {@code HEAD} ref not found
    * @throws RefNotAdvertisedGitException if required ref not advertised
    * @throws RefNotFoundGitException if ref can't be resolved
    * @throws WrongRepositoryStateGitException if repository is not in a correct state for the operation
    * @throws InvalidRemoteGitException if remote is invalid
    * @throws TransportGitException if an error occurs in transport protocol
    * @throws InvalidConfigurationGitException if configuration is malformed
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    */
   public void pull(String remote, FastForward fastForward, boolean rebase) {
      validate(remote, "remote", isNotBlank())
            .and(validate(fastForward, "fastForward", isNotNull()))
            .orThrow();

      StopWatch watch = StopWatch.createStarted();

      pull(git, null, remote, fastForward, rebase);

      log.info("Pull from '{}' remote on current branch (fastforward={}, rebase={}) in {} ms",
               remote,
               fastForward,
               rebase,
               duration(watch).toMillis());
   }

   /**
    * Push repository's current branch using configured remote.
    * Tags are not pushed.
    *
    * @throws InvalidRemoteGitException if remote is invalid
    * @throws TransportGitException if an error occurs in transport protocol
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    * @throws PushFailedGitException if push has errors
    * @see #push(boolean, boolean, boolean, String...)
    */
   public void push() {
      push(remote);
   }

   /**
    * Push repository's current branch using specified remote.
    * Tags are not pushed.
    *
    * @param remote remote name
    *
    * @throws InvalidRemoteGitException if remote is invalid
    * @throws TransportGitException if an error occurs in transport protocol
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    * @throws PushFailedGitException if push has errors
    * @see #push(boolean, boolean, boolean, String...)
    */
   public void push(String remote) {
      StopWatch watch = StopWatch.createStarted();

      push(git, null, remote, false, false, false, false);

      log.info("Push current branch to '{}' remote in {} ms", remote, duration(watch).toMillis());
   }

   /**
    * Pushes specified objects to configured remote.
    *
    * @param force whether to force push
    * @param pushTags whether to also push tags
    * @param pushAllBranches whether to push all branches, not only the current branch
    * @param refSpecs extra RefSpecs to push
    *
    * @throws InvalidRemoteGitException if remote is invalid
    * @throws TransportGitException if an error occurs in transport protocol
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    * @throws PushFailedGitException if push has errors
    */
   public void push(boolean force, boolean pushTags, boolean pushAllBranches, String... refSpecs) {
      push(remote, force, pushTags, pushAllBranches, refSpecs);
   }

   /**
    * Pushes current branch using specified remote.
    *
    * @param remote remote name
    * @param force whether to force push
    * @param pushTags whether to also push tags
    * @param pushAllBranches whether to push all branches, not only the current branch
    * @param refSpecs extra RefSpecs to push
    *
    * @throws InvalidRemoteGitException if remote is invalid
    * @throws TransportGitException if an error occurs in transport protocol
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    * @throws PushFailedGitException if push has errors
    */
   public void push(String remote,
                    boolean force,
                    boolean pushTags,
                    boolean pushAllBranches,
                    String... refSpecs) {
      validate(remote, "remote", isNotBlank())
            .and(validate(refSpecs, "refSpecs", ArrayRules.list(hasNoBlankElements())))
            .orThrow();

      StopWatch watch = StopWatch.createStarted();

      push(git,
           null,
           remote,
           false,
           force,
           pushTags,
           pushAllBranches,
           stream(refSpecs).map(RefSpec::new).toArray(RefSpec[]::new));

      log.info("Push [{}] to '{}' remote (force={}) in {} ms",
               streamConcat(stream(pushTags ? "<tags>" : null,
                                   pushAllBranches ? "<all branches>" : "<current branch>"), stream(refSpecs))
                     .filter(Objects::nonNull)
                     .collect(joining(",")),
               remote,
               force,
               duration(watch).toMillis());
   }

   /**
    * Fetch specified objects from configured remote.
    *
    * @param force whether to force update references
    * @param shallowDepth shallow repository depth, can be {@code null} or &gt; 0
    * @param shallowSince deepens or shortens the history of a shallow repository to include all
    *       reachable commits after a specified time
    * @param unshallow unshallow repository. Can't be set if other shallow options are also set
    * @param checkFetchedObjects whether to check fetched objects validity
    * @param removeDeletedRefs whether to remove references deleted from source
    * @param fetchTags fetch tags behavior
    * @param refSpecs RefSpecs to fetch. E.g. : {@code refs/heads/master:refs/heads/master},
    *       {@code refs/heads/remote-branch:local-branch}, {@code refs/heads/remote-branch}
    *
    * @throws InvalidRemoteGitException if remote is invalid
    * @throws TransportGitException if an error occurs in transport protocol
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    */
   public void fetch(boolean force,
                     Integer shallowDepth,
                     Instant shallowSince,
                     boolean unshallow,
                     boolean checkFetchedObjects,
                     Boolean removeDeletedRefs,
                     FetchTag fetchTags,
                     String... refSpecs) {
      fetch(remote,
            force,
            shallowDepth,
            shallowSince,
            unshallow,
            checkFetchedObjects,
            removeDeletedRefs,
            fetchTags,
            refSpecs);
   }

   /**
    * Fetch specified objects from specified remote.
    *
    * @param remote remote name
    * @param force whether to force update references
    * @param shallowDepth shallow repository depth, can be {@code null} or &gt; 0
    * @param shallowSince deepens or shortens the history of a shallow repository to include all
    *       reachable commits after a specified time
    * @param unshallow unshallow repository. Can't be set if other shallow options are also set
    * @param checkFetchedObjects whether to check fetched objects validity
    * @param removeDeletedRefs whether to remove references deleted from source
    * @param fetchTags fetch tags behavior
    * @param refSpecs RefSpecs to fetch. E.g. : {@code refs/heads/master:refs/heads/master},
    *       {@code refs/heads/remote-branch:local-branch}, {@code refs/heads/remote-branch}
    *
    * @throws InvalidRemoteGitException if remote is invalid
    * @throws TransportGitException if an error occurs in transport protocol
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    */
   public void fetch(String remote,
                     boolean force,
                     Integer shallowDepth,
                     Instant shallowSince,
                     boolean unshallow,
                     boolean checkFetchedObjects,
                     Boolean removeDeletedRefs,
                     FetchTag fetchTags,
                     String... refSpecs) {
      validate(remote, "remote", isNotBlank())
            .and(validate(refSpecs,
                          "refSpecs",
                          ArrayRules.list(CollectionRules
                                                .<List<String>, String>hasNoBlankElements()
                                                .andValue(size(isStrictlyPositive())))))
            .and(validate(fetchTags, "fetchTags", isNotNull()))
            .orThrow();

      StopWatch watch = StopWatch.createStarted();

      fetch(git,
            null,
            remote,
            force,
            shallowDepth,
            shallowSince,
            unshallow,
            checkFetchedObjects,
            removeDeletedRefs,
            fetchTags,
            stream(refSpecs).map(RefSpec::new).toArray(RefSpec[]::new));

      log.info("Fetch [{}] from '{}' remote in {} ms",
               stream(refSpecs).collect(joining(",")),
               remote,
               duration(watch).toMillis());
   }

   /**
    * Commits indexed documents.
    *
    * @param message commit message, can be empty
    * @param amend whether to amend previous commit
    *
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    * @throws EmptyCommitGitException if empty commit is not allowed
    */
   public void commitIndexedDocuments(String message, boolean amend, boolean allowEmptyCommit) {
      validate(message, "message", isNotNull()).orThrow();

      StopWatch watch = StopWatch.createStarted();

      commit(git, message, amend, false, allowEmptyCommit);

      log.info("Commit indexed documents (amend={}) in {} ms", amend, duration(watch).toMillis());
   }

   /**
    * Commits all documents, including all added, changed or deleted tracked files, but not new
    * un-indexed files.
    *
    * @param message commit message, can be empty
    * @param amend whether to amend previous commit
    *
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    * @throws EmptyCommitGitException if empty commit is not allowed
    */
   public void commitAllDocuments(String message, boolean amend, boolean allowEmptyCommit) {
      validate(message, "message", isNotNull()).orThrow();

      StopWatch watch = StopWatch.createStarted();

      commit(git, message, amend, true, allowEmptyCommit);

      log.info("Commit all documents (amend={}) in {} ms", amend, duration(watch).toMillis());
   }

   /**
    * Adds specified documents to index.
    *
    * @param documents list of documents identifiers to remove, relatively to repository
    *
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    */
   public void addDocumentToIndex(DocumentPath... documents) {
      validate(documents, "documents", ArrayRules.list(hasNoNullElements())).orThrow();

      addDocumentToIndex(git, false, documents);
   }

   /**
    * Adds all documents to index.
    *
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    */
   public void addAllDocumentsToIndex() {
      addDocumentToIndex(git, true);
   }

   /**
    * Removes a document from index.
    *
    * @param cached whether to remove file from index only, but not physically
    * @param documents list of documents identifiers to remove, relatively to repository
    *
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    */
   public void removeDocumentFromIndex(boolean cached, DocumentPath... documents) {
      validate(documents, "documents", ArrayRules.list(hasNoNullElements())).orThrow();

      removeDocumentFromIndex(git, cached, documents);
   }

   /**
    * Set configured remote's URI in GIT configuration.
    *
    * @param remoteUri URI to set
    *
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    */
   public void remoteSetUrl(URI remoteUri) {
      validate(remoteUri, "remoteUri", isNotNull()).orThrow();

      StopWatch watch = StopWatch.createStarted();

      remoteSetUrl(git, null, remoteUri, FETCH);

      log.info("Set default remote URL to '{}' in {} ms", remoteUri, duration(watch).toMillis());
   }

   /**
    * Set specified remote's URI in GIT configuration.
    *
    * @param remote remote name
    * @param remoteUri URI to set
    *
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    */
   public void remoteSetUrl(String remote, URI remoteUri) {
      validate(remote, "remote", isNotBlank()).and(validate(remoteUri, "remoteUri", isNotNull())).orThrow();

      StopWatch watch = StopWatch.createStarted();

      remoteSetUrl(git, remote, remoteUri, FETCH);

      log.info("Set '{}' remote URL to '{}' in {} ms", remote, remoteUri, duration(watch).toMillis());
   }

   /**
    * Get specified remote's URI from GIT configuration.
    *
    * @param remote remote name
    *
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    */
   public void remoteGetUrl(String remote) {
      validate(remote, "remote", isNotBlank()).orThrow();

      StopWatch watch = StopWatch.createStarted();

      remoteGetUrl(git, remote);

      log.info("Get remote URL from '{}' in {} ms", remote, duration(watch).toMillis());
   }

   /**
    * Checkout specified branch.
    *
    * @param branch branch name to checkout
    * @param createIfMissing create branch if missing
    * @param createStartRevision use this GIT revision to create branch from, if needed
    *
    * @throws RefNotFoundGitException if specified ref can't be resolved
    * @throws CheckoutConflictGitException if a conflict occurs
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    */
   public void checkout(String branch, boolean createIfMissing, String createStartRevision) {
      validate(branch, "branch", isNotBlank())
            .and(validate(createStartRevision, "createStartRevision", isNotBlank()))
            .orThrow();

      StopWatch watch = StopWatch.createStarted();

      checkout(git, null, branch, createIfMissing, createStartRevision);

      if (log.isInfoEnabled()) {
         String parameters = "createIfMissing=" + createIfMissing;
         if (createIfMissing) {
            parameters += ", createStartRevision=" + createStartRevision;
         }

         log.info("Checkout '{}' branch ({}) in {} ms", branch, parameters, duration(watch).toMillis());
      }
   }

   /**
    * Checkout specified branch.
    *
    * @param branch branch name to checkout
    * @param createIfMissing create branch from {@code HEAD} if missing
    *
    * @throws RefNotFoundGitException if specified ref can't be resolved
    * @throws CheckoutConflictGitException if a conflict occurs
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    */
   public void checkout(String branch, boolean createIfMissing) {
      checkout(branch, createIfMissing, "HEAD");
   }

   /**
    * Merge specified revision to current branch.
    *
    * @param revision revision name to merge to current branch (e.g.: {@code main},
    *       {@code refs/heads/main}, ...)
    * @param fastForward fast-forward mode to use
    * @param contentMergeStrategy content merge strategy to use
    * @param message merge message
    *
    * @return merge status if merge is successful
    *
    * @throws NoHeadGitException if required {@code HEAD} ref not found
    * @throws CheckoutConflictGitException if a conflict occurs
    * @throws WrongRepositoryStateGitException if repository is not in a correct state for the operation
    * @throws MergeFailedGitException if merge failed
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    */
   public MergeStatus merge(String revision,
                            FastForward fastForward,
                            ContentMergeStrategy contentMergeStrategy,
                            String message) {
      validate(revision, "revision", isNotBlank()).and(validate(branch, "message", isNotNull())).orThrow();

      StopWatch watch = StopWatch.createStarted();

      MergeStatus mergeStatus = merge(git, null, revision, fastForward, contentMergeStrategy, message);

      log.info("Merge '{}' revision (fastForward={}) in {} ms",
               revision,
               fastForward,
               duration(watch).toMillis());

      return mergeStatus;
   }

   /**
    * Initializes document repository by cloning GIT repository or initializing GIT. Otherwise, and if
    * configured, repository is updated.
    *
    * @return GIT repository
    *
    * @throws GitException if an error occurs while operating GIT repository
    * @implNote Caller has the responsibility to close the returned {@link Git#getRepository()}
    *       ({@link Git#close()}
    *       close nothing in most cases).
    */
   private Git initializeRepository() {
      try {
         Repository repository = fileSystemRepository(localRepository.storagePath()).orElse(null);
         Git git;

         try {
            if (repository != null) {
               git = new Git(repository);
               if (pullStrategy.pullMode() == PullMode.ON_CREATE) {
                  pull(git, null, null, FastForward.FAST_FORWARD, false);
               }
            } else {
               if (cloneRepositoryIfMissing && remoteUri != null) {
                  git = new Git(cloneRepository(null,
                                                remote,
                                                remoteUri,
                                                branch,
                                                pullStrategy.shallowDepth().orElse(null)));
               } else {
                  git = new Git(fileSystemRepository(localRepository.storagePath(), initRepositoryIfMissing));

                  if (remoteUri != null) {
                     remoteSetUrl(git, remote, remoteUri, FETCH);
                  }
               }
            }
         } catch (Exception e) {
            if (repository != null) {
               repository.close();
            }
            throw e;
         }
         return git;

      } catch (Exception e) {
         throw wrapGitException(e, "Can't initialize repository");
      }
   }

   /** Generates a general commit message for commit on close mode. */
   private String onCloseCommitMessage() {
      Set<String> addedDocuments;
      Set<String> changedDocuments;
      Set<String> removedDocuments;

      try {
         Status status = git.status().call();
         addedDocuments = status.getAdded();
         changedDocuments = status.getChanged();
         removedDocuments = status.getRemoved();
      } catch (Exception e) {
         throw wrapGitException(e, "Can't get repository status");
      }

      return String.format("Add %d, change %d, remove %d documents",
                           addedDocuments.size(),
                           changedDocuments.size(),
                           removedDocuments.size());
   }

   /**
    * Apply GIT pull strategy in repository document operations.
    *
    * @param onCreate whether context is repository initialization
    *
    * @throws GitException if an error occurs while operating GIT repository
    */
   private void applyPullStrategy(boolean onCreate) {
      if ((onCreate && pullStrategy.pullMode() == PullMode.ON_CREATE) || (!onCreate
                                                                          && pullStrategy.pullMode()
                                                                             == PullMode.ALWAYS)) {
         pull(git, null, remote, pullStrategy.fastForward(), pullStrategy.rebase());
      }
   }

   /**
    * Apply GIT push strategy in repository document operations.
    *
    * @param message commit message
    * @param onClose whether context is repository closing
    *
    * @throws GitException if an error occurs while operating GIT repository
    */
   private void applyPushStrategy(String message, boolean onClose) {
      if ((onClose && pushStrategy.commitMode() == PushMode.ON_CLOSE) || (!onClose
                                                                          && pushStrategy.commitMode()
                                                                             == PushMode.ALWAYS)) {
         try {
            commitIndexedDocuments(message, false, false);
         } catch (EmptyCommitGitException e) {
            /* do nothing */
         }
      }
      if ((onClose && pushStrategy.pushMode() == PushMode.ON_CLOSE) || (!onClose
                                                                        && pushStrategy.pushMode()
                                                                           == PushMode.ALWAYS)) {
         push(git, null, remote, false, false, false, false);
      }
   }

   /**
    * Set configured remote's repository URI.
    *
    * @param git GIT API
    * @param remote remote name, or {@code null} to use configured remote
    * @param remoteUri remote URI to set
    * @param uriType URI type to set
    *
    * @throws GitException if an error occurs while operating GIT repository
    */
   private void remoteSetUrl(Git git, String remote, URI remoteUri, UriType uriType) {
      try {
         git
               .remoteSetUrl()
               .setRemoteName(nullable(remote).orElse(this.remote))
               .setRemoteUri(new URIish(remoteUri.toString()))
               .setUriType(uriType)
               .call();
      } catch (Exception e) {
         throw wrapGitException(e, "Can't set '%s' remote URL to '%s'", remote, remoteUri);
      }
   }

   /**
    * Get configured remote's repository URI.
    *
    * @param git GIT API
    * @param remote remote name, or {@code null} to use configured remote
    *
    * @throws GitException if an error occurs while operating GIT repository
    */
   private URI remoteGetUrl(Git git, String remote) {
      String remoteUrl = git
            .getRepository()
            .getConfig()
            .getString(ConfigConstants.CONFIG_REMOTE_SECTION, remote, ConfigConstants.CONFIG_KEY_URL);
      try {
         if (remoteUrl != null) {
            return new URI(remoteUrl);
         } else {
            return null;
         }
      } catch (URISyntaxException e) {
         log.warn("GIT '{}' URL configuration for '{}' remote is not a supported URI", remoteUrl, remote);
         return null;
      }
   }

   /**
    * Commit files.
    *
    * @param git GIT API
    * @param message commit message, can be empty
    * @param amend whether to amend commit
    * @param all commit all changes on tracked files
    * @param allowEmptyCommit whether to allow empty commits
    *
    * @throws GitException if an error occurs while operating GIT repository
    * @throws EmptyCommitGitException if empty commit is not allowed
    */
   private void commit(Git git, String message, boolean amend, boolean all, boolean allowEmptyCommit) {
      notNull(message, "message");

      try {
         CommitCommand commitCommand = git
               .commit()
               .setCleanupMode(DEFAULT)
               .setAllowEmpty(allowEmptyCommit)
               .setAmend(amend)
               .setMessage(message)
               .setAll(all);

         commitCommand.call();
      } catch (Exception e) {
         throw wrapGitException(e,
                                "Can't commit documents (amend=%s, all=%s, allowEmptyCommit=%s",
                                amend,
                                all,
                                allowEmptyCommit);
      }
   }

   /**
    * Clones the repository from specified repository URI.
    *
    * @param monitor optional progress monitor
    * @param remoteBranch remote branch name to clone
    * @param remoteUri remote URI to clone
    * @param shallowDepth optional shallow clone depth, can be set to {@code null} to disable shallow
    *       clone
    *
    * @return GIT repository
    *
    * @throws InvalidRemoteGitException if remote is invalid
    * @throws TransportGitException if an error occurs in transport protocol
    * @throws GitException if an error occurs while operating GIT repository
    * @implNote Caller has the responsibility to close the returned repository.
    */
   @SuppressWarnings("resource")
   private Repository cloneRepository(ProgressMonitor monitor,
                                      String remote,
                                      URI remoteUri,
                                      String remoteBranch,
                                      Integer shallowDepth) {
      try {
         CloneCommand cloneCommand = configureTransport(Git
                                                              .cloneRepository()
                                                              .setRemote(remote)
                                                              .setURI(remoteUri.toString())
                                                              .setBranch(remoteBranch)
                                                              .setDirectory(localRepository
                                                                                  .storagePath()
                                                                                  .toFile())
                                                              .setProgressMonitor(monitor));

         nullable(shallowDepth).ifPresent(cloneCommand::setDepth);

         Git call = cloneCommand.call();

         return call.getRepository();
      } catch (Exception e) {
         throw wrapGitException(e, "Can't clone '%s' repository", remoteUri);
      }
   }

   /**
    * Update repository's current branch using configured remote. Repository must be clean if configured pull
    * strategy use rebase.
    *
    * @param git GIT API
    * @param monitor optional progress monitor
    * @param remote remote name, or {@code null} to use configured remote
    * @param fastForward fast-forward mode
    * @param rebase whether to rebase
    *
    * @throws NotCleanGitException if repository is not clean
    * @throws NoHeadGitException if required {@code HEAD} ref not found
    * @throws RefNotAdvertisedGitException if required ref not advertised
    * @throws RefNotFoundGitException if ref can't be resolved
    * @throws WrongRepositoryStateGitException if repository is not in a correct state for the operation
    * @throws InvalidRemoteGitException if remote is invalid
    * @throws TransportGitException if an error occurs in transport protocol
    * @throws InvalidConfigurationGitException if configuration is malformed
    * @throws GitException if an error occurs while operating GIT repository
    */
   private void pull(Git git,
                     ProgressMonitor monitor,
                     String remote,
                     FastForward fastForward,
                     boolean rebase) {
      try {
         if (pullStrategy.rebase()) {
            checkClean();
         }

         PullCommand pullCommand = configureTransport(git
                                                            .pull()
                                                            .setFastForward(fastForward(fastForward))
                                                            .setRebase(rebase)
                                                            .setRemote(nullable(remote).orElse(this.remote))
                                                            .setProgressMonitor(monitor));

         pullCommand.call();
      } catch (Exception e) {
         throw wrapGitException(e, "Can't pull repository");
      }
   }

   /**
    * Pushes specified objects to remote.
    *
    * @param git GIT API
    * @param monitor optional progress monitor
    * @param remote remote name
    * @param atomic whether to use atomic push
    * @param force whether to force push
    * @param pushTags whether to also push tags
    * @param pushAllBranches whether to push all branches, not only the current branch
    * @param refSpecs extra RefSpecs to push
    *
    * @throws InvalidRemoteGitException if remote is invalid
    * @throws TransportGitException if an error occurs in transport protocol
    * @throws PushFailedGitException if push failed
    * @throws GitException if an error occurs while operating GIT repository
    */
   private void push(Git git,
                     ProgressMonitor monitor,
                     String remote,
                     boolean atomic,
                     boolean force,
                     boolean pushTags,
                     boolean pushAllBranches,
                     RefSpec... refSpecs) {
      try {
         PushCommand pushCommand = configureTransport(git
                                                            .push()
                                                            .setAtomic(atomic)
                                                            .setRemote(remote)
                                                            .setForce(force)
                                                            .setProgressMonitor(monitor));

         List<RefSpec> pushRefSpecs = list(refSpecs);
         if (pushTags) {
            pushRefSpecs.add(org.eclipse.jgit.transport.Transport.REFSPEC_TAGS);
         }
         if (pushAllBranches) {
            pushRefSpecs.add(org.eclipse.jgit.transport.Transport.REFSPEC_PUSH_ALL);
         }

         pushCommand.setRefSpecs(pushRefSpecs);

         checkPushResults(pushCommand.call());
      } catch (PushFailedGitException e) {
         throw e;
      } catch (Exception e) {
         String objects = streamConcat(stream(pushTags ? "<tags>" : null,
                                              pushAllBranches ? "<all branches>" : "<current branch>"),
                                       stream(refSpecs).map(RefSpec::toString))
               .filter(Objects::nonNull)
               .collect(joining(","));

         throw wrapGitException(e, String.format("Can't push [%s] to '%s' remote", objects, remote));
      }
   }

   /**
    * Fetch specified RefSpecs.
    *
    * @param git GIT API
    * @param monitor optional progress monitor
    * @param remote remote name
    * @param force whether to force update references
    * @param shallowDepth shallow repository depth, can be {@code null} to ignore, otherwise must be
    *       &gt; 0
    * @param shallowSince deepens or shortens the history of a shallow repository to include all
    *       reachable commits after a specified time, can be {@code null} to ignore
    * @param unshallow unshallow repository
    * @param checkFetchedObjects whether to check fetched objects validity
    * @param removeDeletedRefs whether to remove references deleted from source, set to {@code null} to
    *       use GIT configuration
    * @param fetchTags fetch tags behavior
    * @param refSpecs RefSpecs to fetch. E.g. : {@code refs/heads/master:refs/heads/master},
    *       {@code refs/heads/remote-branch:local-branch}, {@code refs/heads/remote-branch}
    *
    * @throws InvalidRemoteGitException if remote is invalid
    * @throws TransportGitException if an error occurs in transport protocol
    * @throws GitException if an error occurs while operating GIT repository
    */
   private void fetch(Git git,
                      ProgressMonitor monitor,
                      String remote,
                      boolean force,
                      Integer shallowDepth,
                      Instant shallowSince,
                      boolean unshallow,
                      boolean checkFetchedObjects,
                      Boolean removeDeletedRefs,
                      FetchTag fetchTags,
                      RefSpec... refSpecs) {
      try {
         FetchCommand fetchCommand = configureTransport(git
                                                              .fetch()
                                                              .setUnshallow(unshallow)
                                                              .setCheckFetchedObjects(checkFetchedObjects)
                                                              .setForceUpdate(force)
                                                              .setProgressMonitor(monitor)
                                                              .setRemote(remote)
                                                              .setRefSpecs(refSpecs)
                                                              .setTagOpt(tagOpt(fetchTags)));

         nullable(shallowDepth).ifPresent(fetchCommand::setDepth);
         nullable(shallowSince).ifPresent(fetchCommand::setShallowSince);
         nullable(removeDeletedRefs).ifPresent(fetchCommand::setRemoveDeletedRefs);

         fetchCommand.call();
      } catch (Exception e) {
         String objects = stream(refSpecs).map(RefSpec::toString).collect(joining(","));

         throw wrapGitException(e, String.format("Can't fetch [%s] from '%s' remote", objects, remote));
      }
   }

   /**
    * Checkout specified branch.
    *
    * @param git GIT API
    * @param monitor optional progress monitor
    * @param branch branch name to checkout
    * @param createIfMissing create branch if missing
    * @param createStartRevision use this GIT revision to create branch from, if neede,, or {@code null}
    *       to use {@code HEAD} default start point
    *
    * @throws RefNotFoundGitException if specified ref can't be resolved
    * @throws CheckoutConflictGitException if a conflict occurs
    * @throws GitException if an error occurs while operating GIT repository
    */
   private void checkout(Git git,
                         ProgressMonitor monitor,
                         String branch,
                         boolean createIfMissing,
                         String createStartRevision) {
      try {
         boolean correctBranch = branch.equals(git.getRepository().getBranch());

         if (!correctBranch) {
            CheckoutCommand checkoutCommand = git
                  .checkout()
                  .setName(branch)
                  .setCreateBranch(createIfMissing && !branchExists(git.getRepository(), branch))
                  .setStartPoint(nullable(createStartRevision).orElse("HEAD"))
                  //FIXME .setUpstreamMode(SetupUpstreamMode.SET_UPSTREAM|TRACK)
                  .setProgressMonitor(monitor);

            checkoutCommand.call();
         }
      } catch (Exception e) {
         throw wrapGitException(e, "Can't checkout '%s' branch", branch);
      }
   }

   /**
    * Merge specified ref to current branch
    *
    * @param git GIT API
    * @param monitor optional progress monitor
    * @param reference reference to merge to current branch
    * @param fastForward fast-forward mode to use
    * @param contentMergeStrategy content merge strategy to use
    * @param message merge message, can be empty
    *
    * @return merge status if merge is successful
    *
    * @throws NoHeadGitException if required {@code HEAD} ref not found
    * @throws CheckoutConflictGitException if a conflict occurs
    * @throws WrongRepositoryStateGitException if repository is not in a correct state for the operation
    * @throws MergeFailedGitException if merge failed
    * @throws GitException if an error occurs while operating GIT repository
    */
   private MergeStatus merge(Git git,
                             ProgressMonitor monitor,
                             Ref reference,
                             FastForward fastForward,
                             ContentMergeStrategy contentMergeStrategy,
                             String message) {
      try {
         MergeCommand mergeCommand = git
               .merge()
               .setFastForward(fastForward(fastForward))
               .setStrategy(RECURSIVE)
               .setContentMergeStrategy(contentMergeStrategy(contentMergeStrategy))
               .setMessage(message)
               .include(reference)
               .setProgressMonitor(monitor);

         return checkMergeResults(reference, mergeCommand.call());
      } catch (MergeFailedGitException e) {
         throw e;
      } catch (Exception e) {
         throw wrapGitException(e, "Can't merge '%s' reference", reference);
      }
   }

   /**
    * Merge specified branch to current branch.
    *
    * @param git GIT API
    * @param monitor optional progress monitor
    * @param revision revision to merge to current branch (e.g.: {@code main},
    *       {@code refs/heads/main}, ...)
    * @param fastForward fast-forward mode to use
    * @param contentMergeStrategy content merge strategy to use
    * @param message merge message, can be empty
    *
    * @return merge status if merge is successful
    *
    * @throws NoHeadGitException if required {@code HEAD} ref not found
    * @throws CheckoutConflictGitException if a conflict occurs
    * @throws WrongRepositoryStateGitException if repository is not in a correct state for the operation
    * @throws MergeFailedGitException if merge failed
    * @throws GitException if an error occurs while operating GIT repository
    */
   private MergeStatus merge(Git git,
                             ProgressMonitor monitor,
                             String revision,
                             FastForward fastForward,
                             ContentMergeStrategy contentMergeStrategy,
                             String message) {
      try {
         return merge(git,
                      monitor,
                      git.getRepository().findRef(revision),
                      fastForward,
                      contentMergeStrategy,
                      message);
      } catch (IOException e) {
         throw wrapGitException(e, "Can't merge '%s' revision", revision);
      }
   }

   /**
    * Check merge results and throw an exception with information if merge is not successful.
    *
    * @param reference merge from reference
    * @param mergeResults merge results
    *
    * @return merge status if merge is successful
    *
    * @throws MergeFailedGitException if merge is not successful
    */
   private MergeStatus checkMergeResults(Ref reference, MergeResult mergeResults) {
      MergeStatus mergeStatus = mergeStatus(mergeResults.getMergeStatus());

      if (!mergeStatus.successful()) {
         Map<String, int[][]> conflicts = mergeResults.getConflicts();

         StringBuilder documentsInConflicts = new StringBuilder();
         if (conflicts != null) {
            documentsInConflicts.append(System.lineSeparator());
            for (String path : conflicts.keySet()) {
               documentsInConflicts.append("Conflict: ").append(path).append(System.lineSeparator());
            }
         }

         throw new MergeFailedGitException(mergeStatus,
                                           String.format(
                                                 "Merge '%s (%s)' reference failed with '%s' status\n%s%s",
                                                 reference.getName(),
                                                 reference.getObjectId(),
                                                 mergeStatus.name(),
                                                 mergeResults,
                                                 documentsInConflicts));
      } else {
         return mergeStatus;
      }
   }

   /**
    * Adds specified documents to index.
    *
    * @param git GIT API
    * @param documents list of documents identifiers to add, relatively to repository
    */
   private void addDocumentToIndex(Git git, boolean all, DocumentPath... documents) {
      try {
         AddCommand addCommand = git.add();

         if (all) {
            addCommand.addFilepattern(".");
         }
         stream(documents).map(DocumentPath::stringValue).forEach(addCommand::addFilepattern);

         addCommand.call();
      } catch (Exception e) {
         throw wrapGitException(e,
                                "Can't add %s documents to index",
                                streamConcat(stream(optional(".").filter(__ -> all)),
                                             stream(documents).map(DocumentPath::stringValue)).collect(joining(
                                      ",",
                                      "[",
                                      "]")));
      }
   }

   /**
    * Removes specified documents from index.
    *
    * @param git GIT API
    * @param cached whether to remove file from index only, but not physically
    * @param documents list of documents identifiers to remove, relatively to repository
    */
   private void removeDocumentFromIndex(Git git, boolean cached, DocumentPath... documents) {
      try {
         RmCommand rmCommand = git.rm();

         rmCommand.setCached(cached);
         stream(documents).map(DocumentPath::stringValue).forEach(rmCommand::addFilepattern);

         rmCommand.call();
      } catch (Exception e) {
         throw wrapGitException(e,
                                "Can't remove %s documents from index (cached=%s)",
                                stream(documents)
                                      .map(DocumentPath::stringValue)
                                      .collect(joining(",", "[", "]")),
                                cached);
      }
   }

   /**
    * Resolve the specified revision to GIT object id.
    *
    * @param revision GIT revision string
    * @param git GIT API
    *
    * @return object id
    *
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    * @see Repository#resolve(String)
    */
   private Optional<ObjectId> resolve(Git git, String revision) {
      try {
         return nullable(git.getRepository().resolve(revision));
      } catch (Exception e) {
         throw wrapGitException(e, "Can't resolve '%s' revision", revision);
      }
   }

   /**
    * Loads the specified document from any GIT branch/commit/... using specified GIT revision string.
    *
    * @param git GIT API
    * @param revision GIT revision string
    * @param documentId document path to load
    *
    * @return document or {@link Optional#empty} if document not found
    *
    * @throws UnknownRevisionGitException if specified revision can't be resolved
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    * @see Repository#resolve(String)
    */
   private Optional<Document> loadDocument(Git git, String revision, DocumentPath documentId) {
      try {
         ObjectId revisionObjectId =
               resolve(git, revision).orElseThrow(() -> new UnknownRevisionGitException(String.format(
                     "Can't resolve '%s' revision",
                     revision)));

         try (RevWalk walk = new RevWalk(git.getRepository())) {
            RevCommit commit = walk.parseCommit(revisionObjectId);

            try (TreeWalk treeWalk = TreeWalk.forPath(git.getRepository(),
                                                      documentId.stringValue(),
                                                      commit.getTree())) {
               if (treeWalk == null) {
                  return optional();
               }

               ObjectId documentObjectId = treeWalk.getObjectId(0);

               ObjectLoader loader = git.getRepository().open(documentObjectId);
               return optional(new DocumentBuilder()
                                     .<DocumentBuilder>reconstitute()
                                     .documentId(documentId)
                                     .metadata(new DocumentMetadataBuilder()
                                                     .<DocumentMetadataBuilder>reconstitute()
                                                     .documentPath(documentId.value())
                                                     .contentSize(loader.getSize())
                                                     .build())
                                     .streamContent(loader.openStream(), loader.getSize())
                                     .build());
            } catch (MissingObjectException e) {
               return optional();
            }
         }
      } catch (Exception e) {
         throw wrapGitException(e,
                                "Can't load '%s' document from '%s' revision",
                                documentId.stringValue(),
                                revision);
      }
   }

   /**
    * Loads the specified document entry from any GIT branch/commit/... using specified GIT revision string.
    *
    * @param git GIT API
    * @param revision GIT revision string
    * @param documentId document path to load
    *
    * @return document entry or {@link Optional#empty} if document not found
    *
    * @throws UnknownRevisionGitException if specified revision can't be resolved
    * @throws IoGitException if an IO error occurs while operating GIT repository
    * @throws GitException if an error occurs while operating GIT repository
    * @see Repository#resolve(String)
    */
   private Optional<DocumentEntry> loadDocumentEntry(Git git, String revision, DocumentPath documentId) {
      try {
         ObjectId revisionObjectId =
               resolve(git, revision).orElseThrow(() -> new UnknownRevisionGitException(String.format(
                     "Can't resolve '%s' revision",
                     revision)));

         try (RevWalk walk = new RevWalk(git.getRepository())) {
            RevCommit commit = walk.parseCommit(revisionObjectId);

            try (TreeWalk treeWalk = TreeWalk.forPath(git.getRepository(),
                                                      documentId.stringValue(),
                                                      commit.getTree())) {
               if (treeWalk == null) {
                  return optional();
               }

               ObjectId documentObjectId = treeWalk.getObjectId(0);

               ObjectLoader loader = git.getRepository().open(documentObjectId);
               return optional(new DocumentEntryBuilder()
                                     .<DocumentEntryBuilder>reconstitute()
                                     .documentId(documentId)
                                     .metadata(new DocumentMetadataBuilder()
                                                     .<DocumentMetadataBuilder>reconstitute()
                                                     .documentPath(documentId.value())
                                                     .contentSize(loader.getSize())
                                                     .build())
                                     .build());
            } catch (MissingObjectException e) {
               return optional();
            }
         }
      } catch (Exception t) {
         throw wrapGitException(t,
                                "Can't load '%s' document from '%s' revision",
                                documentId.stringValue(),
                                revision);
      }
   }

   /**
    * Git operations error management and exception wrapping.
    *
    * @param exception original exception
    * @param errorMessage operation error message
    * @param args operation error message arguments
    *
    * @return wrapped {@link GitException}
    */
   private static GitException wrapGitException(Exception exception, String errorMessage, Object... args) {
      String message = String.format(errorMessage, args) + " > " + exception.getMessage();

      if (exception instanceof NoHeadException) {
         return new NoHeadGitException(message, exception.getCause());
      } else if (exception instanceof LockFailedException) {
         return new LockFailedGitException(message, exception.getCause());
      } else if (exception instanceof RefNotAdvertisedException) {
         return new RefNotAdvertisedGitException(message, exception.getCause());
      } else if (exception instanceof RefNotFoundException) {
         return new RefNotFoundGitException(message, exception.getCause());
      } else if (exception instanceof WrongRepositoryStateException) {
         return new WrongRepositoryStateGitException(message, exception.getCause());
      } else if (exception instanceof InvalidRemoteException) {
         return new InvalidRemoteGitException(message, exception.getCause());
      } else if (exception instanceof TransportException) {
         return new TransportGitException(message, exception.getCause());
      } else if (exception instanceof InvalidConfigurationException) {
         return new InvalidConfigurationGitException(message, exception.getCause());
      } else if (exception instanceof CheckoutConflictException) {
         return new CheckoutConflictGitException(message, exception.getCause());
      } else if (exception instanceof EmptyCommitException) {
         return new EmptyCommitGitException(errorMessage, exception.getCause());
      } else if (exception instanceof JGitInternalException) {
         if (exception.getCause() != null) {
            if (exception.getCause() instanceof Error) {
               throw (Error) exception.getCause();
            } else {
               return wrapGitException((Exception) exception.getCause(), message);
            }
         } else {
            return new GitException(message, exception);
         }
      } else if (exception instanceof GitAPIException) {
         return new GitException(message, exception);
      } else if (exception instanceof IOException || exception instanceof UncheckedIOException) {
         return new IoGitException(errorMessage, exception);
      } else {
         return new GitException(message, exception);
      }
   }

   /**
    * Checks if GIT repository is "clean", otherwise throw an exception
    *
    * @throws NotCleanGitException if repository is not clean
    */
   private void checkClean() {
      if (!isClean()) {
         throw new NotCleanGitException("Working tree is not clean");
      }
   }

   /**
    * Generic configuration for transport commands.
    *
    * @param transportCommand transport command to configure
    * @param <C> command type
    * @param <T> command return type
    */
   private <C extends TransportCommand<C, T>, T> C configureTransport(C transportCommand) {
      return transportCommand
            .setCredentialsProvider(credentialsProvider)
            .setTimeout((int) transport.connectTimeout().getSeconds());
   }

   private static TagOpt tagOpt(FetchTag fetchTags) {
      TagOpt tagOpt;

      switch (fetchTags) {
         case AUTO:
            tagOpt = TagOpt.AUTO_FOLLOW;
            break;
         case NO_TAGS:
            tagOpt = TagOpt.NO_TAGS;
            break;
         case TAGS:
            tagOpt = TagOpt.FETCH_TAGS;
            break;
         default:
            throw new IllegalStateException(String.format("Unknown '%s' fetch tag option", fetchTags.name()));
      }

      return tagOpt;
   }

   private static FastForwardMode fastForward(FastForward fastForward) {
      FastForwardMode ff;

      switch (fastForward) {
         case FAST_FORWARD:
            ff = FF;
            break;
         case NO_FAST_FORWARD:
            ff = NO_FF;
            break;
         case FAST_FORWARD_ONLY:
            ff = FF_ONLY;
            break;
         default:
            throw new IllegalStateException(String.format("Unknown '%s' fast-forward mode",
                                                          fastForward.name()));
      }

      return ff;
   }

   private static org.eclipse.jgit.merge.ContentMergeStrategy contentMergeStrategy(ContentMergeStrategy contentMergeStrategy) {
      org.eclipse.jgit.merge.ContentMergeStrategy cms;

      switch (contentMergeStrategy) {
         case CONFLICT:
            cms = CONFLICT;
            break;
         case OURS:
            cms = OURS;
            break;
         case THEIRS:
            cms = THEIRS;
            break;
         default:
            throw new IllegalStateException(String.format("Unknown '%s' content merge strategy",
                                                          contentMergeStrategy.name()));
      }

      return cms;
   }

   private MergeStatus mergeStatus(MergeResult.MergeStatus status) {
      MergeStatus ret;

      switch (status) {
         case FAST_FORWARD:
            ret = MergeStatus.FAST_FORWARD;
            break;
         case FAST_FORWARD_SQUASHED:
            ret = MergeStatus.FAST_FORWARD_SQUASHED;
            break;
         case ALREADY_UP_TO_DATE:
            ret = MergeStatus.ALREADY_UP_TO_DATE;
            break;
         case FAILED:
            ret = MergeStatus.FAILED;
            break;
         case MERGED:
            ret = MergeStatus.MERGED;
            break;
         case MERGED_SQUASHED:
            ret = MergeStatus.MERGED_SQUASHED;
            break;
         case MERGED_SQUASHED_NOT_COMMITTED:
            ret = MergeStatus.MERGED_SQUASHED_NOT_COMMITTED;
            break;
         case CONFLICTING:
            ret = MergeStatus.CONFLICTING;
            break;
         case ABORTED:
            ret = MergeStatus.ABORTED;
            break;
         case MERGED_NOT_COMMITTED:
            ret = MergeStatus.MERGED_NOT_COMMITTED;
            break;
         case NOT_SUPPORTED:
            ret = MergeStatus.NOT_SUPPORTED;
            break;
         case CHECKOUT_CONFLICT:
            ret = MergeStatus.CHECKOUT_CONFLICT;
            break;
         default:
            throw new IllegalStateException(String.format("Unknown '%s' status", status.name()));
      }

      return ret;
   }

   /**
    * Applies finalization operation to a document in open-mode.
    *
    * @param document document to instrument
    * @param finalize finalize operation
    *
    * @return new document copy with finalization operation set
    */
   private Document finalizeOpenDocument(Document document, Consumer<OutputStream> finalize) {
      return DocumentBuilder
            .from(document)
            .content(new OutputStreamDocumentContentBuilder()
                           .content(document.content().outputStreamContent(),
                                    document.content().contentEncoding().orElse(null))
                           .finalize(finalize)
                           .build())
            .build();
   }

   /**
    * Generates {@link CredentialsProvider} chain for configured authentication and security options.
    *
    * @param authentication authentication configuration
    * @param transport transport configuration
    *
    * @return credentials provider chain
    */
   private static CredentialsProvider credentialsProvider(Authentication authentication,
                                                          Transport transport) {
      List<CredentialsProvider> credentialsProvider = list();

      if (authentication != null) {
         credentialsProvider.add(new UsernamePasswordCredentialsProvider(authentication.username(),
                                                                         authentication.password()));
      }

      credentialsProvider.add(new NetRCCredentialsProvider());

      if (transport.disableSslVerify()) {
         credentialsProvider.add(skipPromptCredentialsProvider("SSL verification"));
      }

      if (transport.allowSshHosts()) {
         credentialsProvider.add(skipPromptCredentialsProvider(null /* FIXME find a matching text */));
      }

      return new ChainingCredentialsProvider(credentialsProvider.toArray(new CredentialsProvider[0]));
   }

}
