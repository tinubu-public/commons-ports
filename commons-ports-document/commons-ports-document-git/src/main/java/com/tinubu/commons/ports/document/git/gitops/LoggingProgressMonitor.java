/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.git.gitops;

import org.eclipse.jgit.lib.ProgressMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggingProgressMonitor implements ProgressMonitor {

   private static final Logger log = LoggerFactory.getLogger(LoggingProgressMonitor.class);

   @Override
   public void start(int totalTasks) {
      log.debug("{start} totalTasks={}", totalTasks);
   }

   @Override
   public void beginTask(String title, int totalWork) {
      log.debug("{beginTask} '{}' totalWork={}", title, totalWork);
   }

   @Override
   public void update(int completed) {
      log.debug("{update} completed={}", completed);
   }

   @Override
   public void endTask() {
      log.debug("{endTask}");
   }

   @Override
   public boolean isCancelled() {
      return false;
   }

   @Override
   public void showDuration(boolean enabled) {
   }
}
