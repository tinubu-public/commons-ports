/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.transformer.transformers.base64;

import static com.tinubu.commons.ports.document.domain.LoadedDocumentContent.LoadedDocumentContentBuilder;
import static java.nio.charset.StandardCharsets.US_ASCII;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentContent;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent.InputStreamDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent.OutputStreamDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.processor.DocumentContentProcessor;

class Base64EncoderTest {

   @Test
   public void testRawEncoderWhenNominal() {
      Document document = stubDocument("test",
                                       "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua");

      Document encodedDocument = Base64Encoder.encoder().process(document);

      assertThat(encodedDocument.content().contentSize()).hasValue(164L);
      assertThat(encodedDocument.content().contentEncoding()).hasValue(UTF_8);
      assertThat(encodedDocument.content().stringContent()).isEqualTo(
            "TG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdCwgc2VkIGRvIGVpdXNtb2QgdGVtcG9yIGluY2lkaWR1bnQgdXQgbGFib3JlIGV0IGRvbG9yZSBtYWduYSBhbGlxdWE=");
   }

   @Test
   public void testRawEncoderWhenLoadedContent() {
      Document document = stubDocument(new LoadedDocumentContentBuilder()
                                             .content(
                                                   "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua",
                                                   UTF_8)
                                             .build());

      Document encodedDocument = Base64Encoder.encoder().process(document);

      assertThat(encodedDocument.content().contentSize()).hasValue(164L);
      assertThat(encodedDocument.content().contentEncoding()).hasValue(UTF_8);
      assertThat(encodedDocument.content().stringContent()).isEqualTo(
            "TG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdCwgc2VkIGRvIGVpdXNtb2QgdGVtcG9yIGluY2lkaWR1bnQgdXQgbGFib3JlIGV0IGRvbG9yZSBtYWduYSBhbGlxdWE=");
   }

   @Test
   public void testRawEncoderWhenEmpty() {
      Document document = stubDocument("test", "");

      Document encodedDocument = Base64Encoder.encoder().process(document);

      assertThat(encodedDocument.content().contentSize()).hasValue(0L);
      assertThat(encodedDocument.content().stringContent()).isEqualTo("");
   }

   @Test
   public void testRawEncoderWhenOneByte() {
      Document document = stubDocument("test", "a");

      Document encodedDocument = Base64Encoder.encoder().process(document);

      assertThat(encodedDocument.content().contentSize()).hasValue(4L);
      assertThat(encodedDocument.content().stringContent()).isEqualTo("YQ==");
   }

   @Test
   public void testRawEncoderWhenCustomEncoding() {
      Document document = stubDocument("test",
                                       "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua");
      document =
            document.process((DocumentContentProcessor) documentContent -> new InputStreamDocumentContentBuilder()
                  .content(documentContent.inputStreamContent(),
                           US_ASCII,
                           documentContent.contentSize().orElse(null))
                  .build());

      Document encodedDocument = Base64Encoder.encoder().process(document);

      assertThat(encodedDocument.content().contentSize()).hasValue(164L);
      assertThat(encodedDocument.content().contentEncoding()).hasValue(UTF_8);
      assertThat(encodedDocument.content().stringContent()).isEqualTo(
            "TG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdCwgc2VkIGRvIGVpdXNtb2QgdGVtcG9yIGluY2lkaWR1bnQgdXQgbGFib3JlIGV0IGRvbG9yZSBtYWduYSBhbGlxdWE=");
   }

   @Test
   public void testRawEncoderWhenLineLength() {
      Document document = stubDocument("test",
                                       "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua");

      Document encodedDocument = Base64Encoder.encoder(100).process(document);

      assertThat(encodedDocument.content().contentSize()).hasValue(168L);
      assertThat(encodedDocument.content().stringContent()).isEqualTo(
            "TG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdCwgc2VkIGRvIGVpdXNtb2QgdGVt\r\n"
            + "cG9yIGluY2lkaWR1bnQgdXQgbGFib3JlIGV0IGRvbG9yZSBtYWduYSBhbGlxdWE=\r\n");
   }

   @Test
   public void testRawEncoderWhenZeroLineLength() {
      Document document = stubDocument("test",
                                       "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua");

      Document encodedDocument = Base64Encoder.encoder(0).process(document);

      assertThat(encodedDocument.content().contentSize()).hasValue(164L);
      assertThat(encodedDocument.content().stringContent()).isEqualTo(
            "TG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdCwgc2VkIGRvIGVpdXNtb2QgdGVtcG9yIGluY2lkaWR1bnQgdXQgbGFib3JlIGV0IGRvbG9yZSBtYWduYSBhbGlxdWE=");
   }

   @Test
   public void testRawEncoderWhenGreaterLineLength() {
      Document document = stubDocument("test",
                                       "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua");

      Document encodedDocument = Base64Encoder.encoder(200).process(document);

      assertThat(encodedDocument.content().contentSize()).hasValue(166L);
      assertThat(encodedDocument.content().stringContent()).isEqualTo(
            "TG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdCwgc2VkIGRvIGVpdXNtb2QgdGVtcG9yIGluY2lkaWR1bnQgdXQgbGFib3JlIGV0IGRvbG9yZSBtYWduYSBhbGlxdWE=\r\n");
   }

   @Test
   public void testRawEncoderWhenExactMatchAndMultipleOfFourLineLength() {
      Document document = stubDocument("test",
                                       "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua+");

      Document encodedDocument = Base64Encoder.encoder(164).process(document);

      assertThat(encodedDocument.content().contentSize()).hasValue(166L);
      assertThat(encodedDocument.content().stringContent()).isEqualTo(
            "TG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdCwgc2VkIGRvIGVpdXNtb2QgdGVtcG9yIGluY2lkaWR1bnQgdXQgbGFib3JlIGV0IGRvbG9yZSBtYWduYSBhbGlxdWEr\r\n");
   }

   @Test
   public void testRawEncoderWhenExactMatchAndNotMultipleOfFourLineLength() {
      Document document = stubDocument("test",
                                       "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua++");

      Document encodedDocument = Base64Encoder.encoder(164).process(document);

      assertThat(encodedDocument.content().contentSize()).hasValue(172L);
      assertThat(encodedDocument.content().stringContent()).isEqualTo(
            "TG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdCwgc2VkIGRvIGVpdXNtb2QgdGVtcG9yIGluY2lkaWR1bnQgdXQgbGFib3JlIGV0IGRvbG9yZSBtYWduYSBhbGlxdWEr\r\nKw==\r\n");
   }

   @Test
   public void testRawEncoderWhenNegativeLineLength() {
      Document document = stubDocument("test",
                                       "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua");

      Document encodedDocument = Base64Encoder.encoder(-1).process(document);

      assertThat(encodedDocument.content().contentSize()).hasValue(164L);
      assertThat(encodedDocument.content().stringContent()).isEqualTo(
            "TG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdCwgc2VkIGRvIGVpdXNtb2QgdGVtcG9yIGluY2lkaWR1bnQgdXQgbGFib3JlIGV0IGRvbG9yZSBtYWduYSBhbGlxdWE=");
   }

   @Test
   public void testRawEncoderWhenLineLengthAndLineSeparator() {
      Document document = stubDocument("test",
                                       "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua");

      Document encodedDocument = Base64Encoder.encoder(100, new byte[] { '\n' }).process(document);

      assertThat(encodedDocument.content().contentSize()).hasValue(166L);
      assertThat(encodedDocument.content().stringContent()).isEqualTo(
            "TG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdCwgc2VkIGRvIGVpdXNtb2QgdGVt\n"
            + "cG9yIGluY2lkaWR1bnQgdXQgbGFib3JlIGV0IGRvbG9yZSBtYWduYSBhbGlxdWE=\n");
   }

   @Test
   public void testRawEncoderWhenLineLengthAndNullLineSeparator() {
      Document document = stubDocument("test",
                                       "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua");

      Document encodedDocument = Base64Encoder.encoder(100, null).process(document);

      assertThat(encodedDocument.content().contentSize()).hasValue(168L);
      assertThat(encodedDocument.content().stringContent()).isEqualTo(
            "TG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdCwgc2VkIGRvIGVpdXNtb2QgdGVt\r\n"
            + "cG9yIGluY2lkaWR1bnQgdXQgbGFib3JlIGV0IGRvbG9yZSBtYWduYSBhbGlxdWE=\r\n");
   }

   @Test
   public void testMimeEncoderWhenNominal() {
      Document document = stubDocument("test",
                                       "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua");

      Document encodedDocument = Base64Encoder.mimeEncoder().process(document);

      assertThat(encodedDocument.content().contentSize()).hasValue(170L);
      assertThat(encodedDocument.content().stringContent()).isEqualTo(
            "TG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdCwg\r\n"
            + "c2VkIGRvIGVpdXNtb2QgdGVtcG9yIGluY2lkaWR1bnQgdXQgbGFib3JlIGV0IGRvbG9yZSBtYWdu\r\n"
            + "YSBhbGlxdWE=\r\n");
   }

   @Test
   public void testPemEncoderWhenNominal() {
      Document document = stubDocument("test",
                                       "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua");

      Document encodedDocument = Base64Encoder.pemEncoder().process(document);

      assertThat(encodedDocument.content().contentSize()).hasValue(170L);
      assertThat(encodedDocument.content().stringContent()).isEqualTo(
            "TG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2Np\r\n"
            + "bmcgZWxpdCwgc2VkIGRvIGVpdXNtb2QgdGVtcG9yIGluY2lkaWR1bnQgdXQgbGFi\r\n"
            + "b3JlIGV0IGRvbG9yZSBtYWduYSBhbGlxdWE=\r\n");
   }

   @Test
   public void testEncoderWithOutputStreamContentWhenNominal() {
      try (OutputStream result = new ByteArrayOutputStream()) {
         Document document = stubOutputDocument(result);
         document = Base64Encoder.encoder().process(document);

         try (Writer os = new OutputStreamWriter(document.content().outputStreamContent())) {
            os.write(
                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua");
         } catch (IOException e) {
            throw new IllegalStateException(e);
         }

         assertThat(document.content().contentSize()).isEmpty();
         assertThat(document.content().contentEncoding()).hasValue(UTF_8);
         assertThat(result.toString()).isEqualTo(
               "TG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdCwgc2VkIGRvIGVpdXNtb2QgdGVtcG9yIGluY2lkaWR1bnQgdXQgbGFib3JlIGV0IGRvbG9yZSBtYWduYSBhbGlxdWE=");
      } catch (IOException e) {
         throw new IllegalStateException(e);
      }

   }

   private static Document stubDocument(DocumentPath documentId, String content) {
      return new DocumentBuilder()
            .documentId(documentId)
            .streamContent(content, StandardCharsets.UTF_8)
            .build();
   }

   private static Document stubDocument(String documentId, String content) {
      return stubDocument(DocumentPath.of(documentId), content);
   }

   private static Document stubDocument(DocumentContent content) {
      return new DocumentBuilder().documentId(DocumentPath.of("test")).content(content).build();
   }

   private static Document stubOutputDocument(OutputStream outputStream) {
      return stubOutputDocument(new OutputStreamDocumentContentBuilder()
                                      .content(outputStream, StandardCharsets.UTF_8)
                                      .build());
   }

   private static Document stubOutputDocument(DocumentContent content) {
      return new DocumentBuilder().documentId(DocumentPath.of("test")).content(content).build();
   }

}