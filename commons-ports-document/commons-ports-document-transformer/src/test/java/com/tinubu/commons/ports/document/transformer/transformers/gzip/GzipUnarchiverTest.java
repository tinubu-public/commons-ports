/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.transformer.transformers.gzip;

import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.mimeType;
import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.parseMimeType;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_GZIP;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.time.ZonedDateTime;

import org.apache.commons.codec.binary.Base64OutputStream;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.lang.datetime.ApplicationClock;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent.OutputStreamDocumentContentBuilder;

class GzipUnarchiverTest {

   private static final ZonedDateTime now = ZonedDateTime.now();

   @BeforeEach
   public void initializeApplicationClock() {
      ApplicationClock.setFixedClock(now);
   }

   @Test
   public void testSupportsWhenNominal() {
      GzipUnarchiver gzipUnarchiver = new GzipUnarchiver();

      assertThat(gzipUnarchiver.supports(stubDocument(DocumentPath.of("test.gz")).build())).isTrue();
      assertThat(gzipUnarchiver.supports(stubDocument(DocumentPath.of("test.gzip")).build())).isTrue();
      assertThat(gzipUnarchiver.supports(stubDocument(DocumentPath.of("test.txt"))
                                               .<DocumentBuilder>chain(m -> m.contentType(mimeType(
                                                     "application",
                                                     "gzip")))
                                               .build())).isTrue();
      assertThat(gzipUnarchiver.supports(stubDocument(DocumentPath.of("test.txt"))
                                               .<DocumentBuilder>chain(m -> m.contentType(mimeType(
                                                     "application",
                                                     "x-gzip")))
                                               .build())).isTrue();
   }

   @Test
   public void testSupportsWhenBadContentType() {
      GzipUnarchiver gzipUnarchiver = new GzipUnarchiver();

      assertThat(gzipUnarchiver.supports(stubDocument(DocumentPath.of("test.txt")).build())).isFalse();
      assertThat(gzipUnarchiver.supports(stubDocument(DocumentPath.of("test.zip"))
                                               .<DocumentBuilder>chain(m -> m.contentType(mimeType("text",
                                                                                                   "plain")))
                                               .build())).isFalse();
   }

   @Test
   public void testTransformWhenNominal() {
      Document document = new DocumentBuilder()
            .documentId(DocumentPath.of("test.txt"))
            .loadedContent("content", StandardCharsets.UTF_8)
            .contentType(parseMimeType("text/plain"))
            .build();

      Document gzip = new GzipArchiver().transform(document);

      assertThat(gzip.documentId()).isEqualTo(DocumentPath.of("test.txt.gz"));

      Document gunzip = new GzipUnarchiver().transform(gzip);

      assertThat(gunzip.documentId()).isEqualTo(DocumentPath.of("test.txt"));
      assertThat(gunzip.metadata().simpleContentType()).hasValue(parseMimeType("text/plain"));
      assertThat(gunzip.metadata().documentPath()).isEqualTo(Paths.get("test.txt"));
      // assertThat(gunzip.content().contentEncoding()) Do not enforce content encoding check in implementations
      assertThat(gunzip.content().stringContent(UTF_8)).isEqualTo("content");
   }

   @Test
   public void testTransformWhenInvalidParameters() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new GzipUnarchiver().transform(null))
            .withMessage("Invariant validation error > 'document' must not be null");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new GzipUnarchiver().transform(stubDocument(DocumentPath.of("test.txt")).build()))
            .withMessage(
                  "Invariant validation error > 'document.contentType=text/plain' must be in [application/gzip]");
   }

   @Test
   public void testTransformWhenEmptyDocuments() {
      Document document = new DocumentBuilder()
            .documentId(DocumentPath.of("test.txt"))
            .loadedContent("", StandardCharsets.UTF_8)
            .contentType(parseMimeType("text/plain"))
            .build();

      Document gzip = new GzipArchiver().transform(document);

      assertThat(gzip.documentId()).isEqualTo(DocumentPath.of("test.txt.gz"));

      Document gunzip = new GzipUnarchiver().transform(gzip);

      assertThat(gunzip.documentId()).isEqualTo(DocumentPath.of("test.txt"));
      assertThat(gunzip.metadata().simpleContentType()).hasValue(parseMimeType("text/plain"));
      assertThat(gunzip.metadata().documentPath()).isEqualTo(Paths.get("test.txt"));
      // assertThat(gunzip.content().contentEncoding()) Do not enforce content encoding check in implementations
      assertThat(gunzip.content().stringContent(UTF_8)).isEqualTo("");
   }

   @Test
   public void testTransformWithOutputStreamContentWhenNominal() {
      Document gzip = stubDocument(DocumentPath.of("test.txt"),
                                   "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua")
            .build()
            .process(new GzipArchiver());

      try (ByteArrayOutputStream result = new ByteArrayOutputStream()) {

         Document document = stubOutputDocument(DocumentPath.of("test.txt.gz"), result).build();
         Document gunzip = new GzipUnarchiver().transform(document);

         try (OutputStream output = gunzip.content().outputStreamContent()) {
            IOUtils.copy(gzip.content().inputStreamContent(), output);
         }

         assertThat(gunzip.documentId()).isEqualTo(DocumentPath.of("test.txt"));
         assertThat(gunzip.metadata().simpleContentType()).hasValue(parseMimeType("text/plain"));
         assertThat(gunzip.metadata().documentPath()).isEqualTo(Paths.get("test.txt"));
         assertThat(gunzip.content().contentSize()).isEmpty();
         // assertThat(gunzip.content().contentEncoding()) Do not enforce content encoding check in implementations
         assertThat(result.toString()).isEqualTo(
               "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua");

      } catch (IOException e) {
         throw new IllegalStateException(e);
      }
   }

   private String base64(byte[] content) {
      try (ByteArrayOutputStream result = new ByteArrayOutputStream();
           Base64OutputStream bOs = new Base64OutputStream(result, true, -1, null)) {
         IOUtils.write(content, bOs);
         return result.toString();
      } catch (IOException e) {
         throw new IllegalStateException(e);
      }
   }

   private DocumentBuilder stubDocument(DocumentPath documentId, String content) {
      DocumentBuilder documentBuilder =
            new DocumentBuilder().documentId(documentId).loadedContent(content, StandardCharsets.UTF_8);

      if (documentId.stringValue().endsWith(".gz") || documentId.stringValue().endsWith(".gzip")) {
         documentBuilder = documentBuilder.contentType(APPLICATION_GZIP);
      }

      return documentBuilder;
   }

   private DocumentBuilder stubDocument(DocumentPath documentId) {
      return stubDocument(documentId, documentId.stringValue());
   }

   private DocumentBuilder stubOutputDocument(DocumentPath documentId, OutputStream outputStream) {
      DocumentBuilder documentBuilder = new DocumentBuilder()
            .documentId(documentId)
            .content(new OutputStreamDocumentContentBuilder()
                           .content(outputStream, StandardCharsets.UTF_8)
                           .build());

      if (documentId.stringValue().endsWith(".gz") || documentId.stringValue().endsWith(".gzip")) {
         documentBuilder = documentBuilder.contentType(APPLICATION_GZIP);
      }

      return documentBuilder;
   }

}