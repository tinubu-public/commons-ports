/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.jdk;

import static com.tinubu.commons.lang.util.ExceptionUtils.runtimeThrow;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;
import java.time.Instant;
import java.util.Iterator;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Stream;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.IOUtils;

import com.tinubu.commons.lang.util.MechanicalSympathy;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipArchive;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipArchiveEntry;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipConfiguration;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipException;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipOptions;

/**
 * JDK-native {@link ZipArchive} implementation.
 */
public class JdkZipArchive implements ZipArchive {

   /** Read/write buffer size. */
   private static final int BUFFER_SIZE = MechanicalSympathy.generalBufferSize();

   private final InputStream content;
   private final Long contentSize;
   private final ZipOptions options;

   private JdkZipArchive(InputStream content, Long contentSize, ZipOptions options) {
      this.content = notNull(content, "content");
      this.contentSize = contentSize;
      this.options = notNull(options, "options");

      if (options.password() != null) {
         throw new IllegalArgumentException("Password not supported");
      }
   }

   /**
    * Creates a low-level zip archive from raw zip content.
    *
    * @param content raw content
    * @param options zip options
    *
    * @return zip archive
    */
   public static JdkZipArchive fromContent(InputStream content, Long contentSize, ZipOptions options) {
      return new JdkZipArchive(content, contentSize, options);
   }

   /**
    * Creates a low-level zip archive from specified entries.
    *
    * @param entries entries to add to zip archive
    * @param options zip options
    * @param config zip configuration
    *
    * @return zip archive
    *
    * @throws UncheckedIOException if an I/O error occurs
    * @throws ZipException if an error occurs
    */
   public static JdkZipArchive fromEntries(Stream<ZipArchiveEntry> entries,
                                           ZipOptions options, ZipConfiguration config) {
      notNull(entries, "entries");
      notNull(options, "options");
      notNull(config, "config");

      try (ByteArrayOutputStream byteOutput = new ByteArrayOutputStream();
           ZipOutputStream zipOutput = new ZipOutputStream(byteOutput, options.metadataEncoding())) {

         configCompressionLevel(zipOutput, config);

         byte[] readBuffer = new byte[BUFFER_SIZE];
         for (ZipArchiveEntry entry : (Iterable<ZipArchiveEntry>) (entries.filter(Objects::nonNull))::iterator) {
            try (InputStream bis = entry.content()) {
               ZipEntry zipEntry = new ZipEntry(entry.name().toString());

               if (entry.creationDate() != null) {
                  zipEntry.setCreationTime(FileTime.from(entry.creationDate()));
               }
               if (entry.lastUpdateDate() != null) {
                  zipEntry.setLastModifiedTime(FileTime.from(entry.lastUpdateDate()));
               }
               zipOutput.putNextEntry(zipEntry);
               int bytesRead;
               while ((bytesRead = bis.read(readBuffer)) != -1) {
                  zipOutput.write(readBuffer, 0, bytesRead);
               }
            } finally {
               zipOutput.closeEntry();
            }
         }
         zipOutput.finish();

         byte[] zipContent = byteOutput.toByteArray();
         return new JdkZipArchive(new ByteArrayInputStream(zipContent), (long) zipContent.length, options);
      } catch (IOException e) {
         throw runtimeThrow(e);
      } catch (Exception e) {
         throw new ZipException(e);
      }
   }

   private static void configCompressionLevel(ZipOutputStream zipOutput, ZipConfiguration config) {
      switch (config.compressionLevel()) {
         case NONE:
            zipOutput.setLevel(Deflater.NO_COMPRESSION);
            break;
         case MINIMUM:
            zipOutput.setLevel(Deflater.BEST_SPEED);
            break;
         case LOW:
            zipOutput.setLevel(3);
            break;
         case NORMAL:
            zipOutput.setLevel(5);
            break;
         case HIGH:
            zipOutput.setLevel(7);
            break;
         case MAXIMUM:
            zipOutput.setLevel(Deflater.BEST_COMPRESSION);
            break;
      }
   }

   @Override
   public InputStream content() {
      return content;
   }

   @Override
   public Optional<Long> contentSize() {
      return nullable(contentSize);
   }

   @Override
   public ZipOptions options() {
      return options;
   }

   @Override
   public Stream<ZipArchiveEntry> unzipEntries() {
      return unzipEntries(__ -> true);
   }

   @Override
   public Stream<ZipArchiveEntry> unzipEntries(Predicate<? super Path> entryFilter) {
      UnzipIterator iterator = new UnzipIterator(content, options.metadataEncoding(), entryFilter);

      return stream(() -> iterator).onClose(iterator::close);
   }

   static class UnzipIterator implements Iterator<ZipArchiveEntry>, AutoCloseable {

      private final ZipInputStream zis;
      private final Predicate<? super Path> entryFilter;
      private ZipEntry zipEntry;

      public UnzipIterator(InputStream content, Charset charset, Predicate<? super Path> entryFilter) {
         this.zis = new ZipInputStream(content, charset);
         this.entryFilter = entryFilter;
      }

      @Override
      public boolean hasNext() {
         do {
            try {
               zipEntry = zis.getNextEntry();
            } catch (IOException e) {
               throw runtimeThrow(e);
            }
         } while (zipEntry != null && !entryFilter.test(Path.of(zipEntry.getName())));

         return zipEntry != null;
      }

      @Override
      public ZipArchiveEntry next() {
         if (zipEntry == null) {
            throw new IllegalStateException("No more entries available");
         }

         try {
            try (ByteArrayOutputStream fos = new ByteArrayOutputStream()) {
               IOUtils.copy(zis, fos, BUFFER_SIZE);

               Instant creationDate =
                     nullable(zipEntry.getCreationTime()).map(FileTime::toInstant).orElse(null);
               Instant lastUpdateDate =
                     nullable(zipEntry.getLastModifiedTime()).map(FileTime::toInstant).orElse(null);

               return new ZipArchiveEntry() {
                  @Override
                  public Path name() {
                     return Paths.get(zipEntry.getName());
                  }

                  @Override
                  public InputStream content() {
                     return new ByteArrayInputStream(fos.toByteArray());
                  }

                  @Override
                  public Long contentSize() {
                     return (long) fos.size();
                  }

                  @Override
                  public Instant creationDate() {
                     return creationDate;
                  }

                  @Override
                  public Instant lastUpdateDate() {
                     return lastUpdateDate;
                  }
               };
            }
         } catch (IOException e) {
            throw runtimeThrow(e);
         } catch (Exception e) {
            throw new ZipException(e);
         } finally {
            try {
               zis.closeEntry();
            } catch (IOException e) {
               throw runtimeThrow(e);
            }
         }
      }

      @Override
      public void close() {
         try {
            zis.close();
         } catch (IOException e) {
            throw runtimeThrow(e);
         }
      }
   }
}
