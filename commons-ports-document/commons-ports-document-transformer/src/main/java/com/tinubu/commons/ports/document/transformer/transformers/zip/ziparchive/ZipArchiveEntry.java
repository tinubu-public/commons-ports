/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive;

import java.io.InputStream;
import java.nio.file.Path;
import java.time.Instant;

import com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.jdk.JdkZipArchive;

/**
 * Defines a {@link JdkZipArchive} compatible entry.
 */
public interface ZipArchiveEntry {

   /**
    * Zip entry name.
    *
    * @return zip entry name
    */
   Path name();

   /**
    * Zip entry content.
    *
    * @return zip entry content
    */
   InputStream content();

   /**
    * Zip entry content size.
    *
    * @return zip entry content size or {@code null} if not set
    */
   Long contentSize();

   /**
    * Zip creation date.
    *
    * @return zip creation date or {@code null} if not set
    */
   Instant creationDate();

   /**
    * Zip last update date.
    *
    * @return zip last update date or {@code null} if not set
    */
   Instant lastUpdateDate();

}
