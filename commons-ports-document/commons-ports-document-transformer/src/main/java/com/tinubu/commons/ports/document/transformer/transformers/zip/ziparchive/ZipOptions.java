/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * ZIP archive options.
 */
public class ZipOptions {

   /** Default encoding used to encode zip metadata : entry names, comments. */
   private static final Charset DEFAULT_ZIP_METADATA_ENCODING = StandardCharsets.UTF_8;

   /**
    * Optional ZIP password. Password must not be empty. Password must be UTF-8 encoded.
    */
   private String password;
   /**
    * ZIP entry name and comments encoding (not the file content encoding).
    */
   private Charset metadataEncoding = DEFAULT_ZIP_METADATA_ENCODING;

   public String password() {
      return password;
   }

   public ZipOptions password(String password) {
      this.password = nullable(password, p -> notBlank(p, "password"));
      return this;
   }

   public Charset metadataEncoding() {
      return metadataEncoding;
   }

   public ZipOptions metadataEncoding(Charset metadataEncoding) {
      this.metadataEncoding = notNull(metadataEncoding, "metadataEncoding");
      return this;
   }
}
