/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.zip4j;

import static com.tinubu.commons.lang.util.ExceptionUtils.runtimeThrow;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static net.lingala.zip4j.model.enums.CompressionLevel.FAST;
import static net.lingala.zip4j.model.enums.CompressionLevel.FASTEST;
import static net.lingala.zip4j.model.enums.CompressionLevel.MAXIMUM;
import static net.lingala.zip4j.model.enums.CompressionLevel.NORMAL;
import static net.lingala.zip4j.model.enums.CompressionLevel.NO_COMPRESSION;
import static net.lingala.zip4j.model.enums.CompressionLevel.ULTRA;
import static net.lingala.zip4j.model.enums.EncryptionMethod.AES;
import static net.lingala.zip4j.model.enums.EncryptionMethod.ZIP_STANDARD;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.Iterator;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.apache.commons.io.IOUtils;

import com.tinubu.commons.lang.util.MechanicalSympathy;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipArchive;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipArchiveEntry;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipConfiguration;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipException;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ziparchive.ZipOptions;

import net.lingala.zip4j.io.inputstream.ZipInputStream;
import net.lingala.zip4j.io.outputstream.ZipOutputStream;
import net.lingala.zip4j.model.LocalFileHeader;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.model.enums.AesKeyStrength;

/**
 * JDK-native {@link ZipArchive} implementation.
 */
public class Zip4jZipArchive implements ZipArchive {

   /** Read/write buffer size. */
   private static final int BUFFER_SIZE = MechanicalSympathy.generalBufferSize();

   private final InputStream content;
   private final Long contentSize;
   private final ZipOptions options;

   private Zip4jZipArchive(InputStream content, Long contentSize, ZipOptions options) {
      this.content = notNull(content, "content");
      this.contentSize = contentSize;
      this.options = notNull(options, "options");
   }

   /**
    * Creates a low-level zip archive from raw zip content.
    *
    * @param content raw content
    * @param contentSize raw content size
    * @param options zip options
    *
    * @return zip archive
    */
   public static Zip4jZipArchive fromContent(InputStream content, Long contentSize, ZipOptions options) {
      return new Zip4jZipArchive(content, contentSize, options);
   }

   /**
    * Creates a low-level zip archive from specified entries.
    *
    * @param entries entries to add to zip archive
    * @param options zip options
    * @param config zip configuration
    *
    * @return zip archive
    *
    * @throws UncheckedIOException if an I/O error occurs
    * @throws ZipException if an error occurs
    */
   public static Zip4jZipArchive fromEntries(Stream<ZipArchiveEntry> entries,
                                             ZipOptions options, ZipConfiguration config) {
      notNull(entries, "entries");
      notNull(options, "options");
      notNull(config, "config");

      ZipParameters zipParameters =
            configEncryptionMethod(configCompressionLevel(new ZipParameters(), config), config);
      zipParameters.setEncryptFiles(options.password() != null);

      try (ByteArrayOutputStream byteOutput = new ByteArrayOutputStream();
           ZipOutputStream zipOutput = new ZipOutputStream(byteOutput,
                                                           nullable(options.password())
                                                                 .map(String::toCharArray)
                                                                 .orElse(null),
                                                           options.metadataEncoding())) {
         byte[] readBuffer = new byte[BUFFER_SIZE];
         for (ZipArchiveEntry entry : (Iterable<ZipArchiveEntry>) (entries.filter(Objects::nonNull))::iterator) {
            try (InputStream bis = entry.content()) {
               zipParameters.setEntrySize(entry.contentSize());
               zipParameters.setFileNameInZip(entry.name().toString());
               zipParameters.setLastModifiedFileTime(entry.lastUpdateDate().toEpochMilli());

               zipOutput.putNextEntry(zipParameters);

               int bytesRead;
               while ((bytesRead = bis.read(readBuffer)) != -1) {
                  zipOutput.write(readBuffer, 0, bytesRead);
               }
            } finally {
               zipOutput.closeEntry();
            }
         }

         zipOutput.close();

         byte[] zipContent = byteOutput.toByteArray();
         return new Zip4jZipArchive(new ByteArrayInputStream(zipContent), (long) zipContent.length, options);
      } catch (net.lingala.zip4j.exception.ZipException e) {
         throw new ZipException(e.getMessage(), e);
      } catch (IOException e) {
         throw runtimeThrow(e);
      } catch (Exception e) {
         throw new ZipException(e);
      }

   }

   @Override
   public InputStream content() {
      return content;
   }

   @Override
   public Optional<Long> contentSize() {
      return nullable(contentSize);
   }

   @Override
   public ZipOptions options() {
      return options;
   }

   @Override
   public Stream<ZipArchiveEntry> unzipEntries() {
      return unzipEntries(__ -> true);
   }

   @Override
   public Stream<ZipArchiveEntry> unzipEntries(Predicate<? super Path> entryFilter) {
      UnzipIterator iterator = new UnzipIterator(content,
                                                 options.metadataEncoding(),
                                                 nullable(options.password())
                                                       .map(String::toCharArray)
                                                       .orElse(null),
                                                 entryFilter);

      return stream(() -> iterator).onClose(iterator::close);
   }

   static class UnzipIterator implements Iterator<ZipArchiveEntry>, AutoCloseable {

      private final ZipInputStream zis;
      private final Predicate<? super Path> entryFilter;
      private LocalFileHeader zipEntry;

      public UnzipIterator(InputStream content,
                           Charset charset,
                           char[] password,
                           Predicate<? super Path> entryFilter) {
         this.zis = new ZipInputStream(content, password, charset);
         this.entryFilter = entryFilter;
      }

      @Override
      public boolean hasNext() {
         do {
            try {
               zipEntry = zis.getNextEntry();
            } catch (IOException e) {
               throw runtimeThrow(e);
            }
         } while (zipEntry != null && !entryFilter.test(Path.of(zipEntry.getFileName())));

         return zipEntry != null;
      }

      @Override
      public ZipArchiveEntry next() {
         if (zipEntry == null) {
            throw new IllegalStateException("No more entries available");
         }

         try {
            try (ByteArrayOutputStream fos = new ByteArrayOutputStream()) {
               IOUtils.copy(zis, fos, BUFFER_SIZE);

               Instant lastUpdateDate =
                     nullable(zipEntry.getLastModifiedTimeEpoch()).map(Instant::ofEpochMilli).orElse(null);

               return new ZipArchiveEntry() {
                  @Override
                  public Path name() {
                     return Paths.get(zipEntry.getFileName());
                  }

                  @Override
                  public InputStream content() {
                     return new ByteArrayInputStream(fos.toByteArray());
                  }

                  @Override
                  public Long contentSize() {
                     return (long) fos.size();
                  }

                  @Override
                  public Instant creationDate() {
                     return null;
                  }

                  @Override
                  public Instant lastUpdateDate() {
                     return lastUpdateDate;
                  }
               };
            }
         } catch (net.lingala.zip4j.exception.ZipException e) {
            throw new ZipException(e.getMessage(), e);
         } catch (IOException e) {
            throw runtimeThrow(e);
         } catch (Exception e) {
            throw new ZipException(e);
         }
      }

      @Override
      public void close() {
         try {
            zis.close();
         } catch (IOException e) {
            throw runtimeThrow(e);
         }
      }

   }

   private static ZipParameters configCompressionLevel(ZipParameters zipParameters, ZipConfiguration config) {
      switch (config.compressionLevel()) {
         case NONE:
            zipParameters.setCompressionLevel(NO_COMPRESSION);
            break;
         case MINIMUM:
            zipParameters.setCompressionLevel(FASTEST);
            break;
         case LOW:
            zipParameters.setCompressionLevel(FAST);
            break;
         case NORMAL:
            zipParameters.setCompressionLevel(NORMAL);
            break;
         case HIGH:
            zipParameters.setCompressionLevel(MAXIMUM);
            break;
         case MAXIMUM:
            zipParameters.setCompressionLevel(ULTRA);
            break;
      }
      return zipParameters;
   }

   private static ZipParameters configEncryptionMethod(ZipParameters zipParameters, ZipConfiguration config) {
      switch (config.encryptionMethod()) {
         case ZIP_STANDARD:
            zipParameters.setEncryptionMethod(ZIP_STANDARD);
            break;
         case AES_128:
            zipParameters.setEncryptionMethod(AES);
            zipParameters.setAesKeyStrength(AesKeyStrength.KEY_STRENGTH_128);
            break;
         case AES_256:
            zipParameters.setEncryptionMethod(AES);
            zipParameters.setAesKeyStrength(AesKeyStrength.KEY_STRENGTH_256);
            break;
      }
      return zipParameters;
   }

}
