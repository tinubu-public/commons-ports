/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.transformer;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.hasNoNullElements;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.validation.Validate.noNullElements;

import java.util.Arrays;
import java.util.List;

import com.tinubu.commons.ports.document.domain.DocumentAccessException;

/**
 * Responsibility chain pattern implementation for {@link DocumentTransformer}.
 * The first transformer specified in the chain that supports the source object will be applied.
 *
 * @param <S> source object type
 * @param <T> target object type
 */
public class DocumentTransformerChain<S, T> implements DocumentTransformer<S, T> {

   private final List<DocumentTransformer<S, T>> transformers;

   protected DocumentTransformerChain(DocumentTransformerChainBuilder<S, T> builder) {
      this.transformers = validate(builder.transformers, "transformers", hasNoNullElements()).orThrow();
   }

   @Override
   public boolean supports(S source) {
      return transformers.stream().anyMatch(t -> t.supports(source));
   }

   public T transform(S source) throws DocumentAccessException {

      for (DocumentTransformer<S, T> transformer : transformers) {
         if (transformer.supports(source)) {
            return transformer.transform(source);
         }
      }

      throw new IllegalStateException(String.format("Unsupported '%s' source", source));
   }

   public static class DocumentTransformerChainBuilder<S, T> {

      private final List<DocumentTransformer<S, T>> transformers = list();

      @SafeVarargs
      public final DocumentTransformerChainBuilder<S, T> transformers(DocumentTransformer<S, T>... transformers) {
         this.transformers.addAll(Arrays.asList(noNullElements(transformers, "transformers")));
         return this;
      }

      public final DocumentTransformerChainBuilder<S, T> transformers(List<DocumentTransformer<S, T>> transformers) {
         this.transformers.addAll(list(noNullElements(transformers, "transformers")));
         return this;
      }

      public DocumentTransformerChain<S, T> build() {
         return new DocumentTransformerChain<>(this);
      }
   }
}
