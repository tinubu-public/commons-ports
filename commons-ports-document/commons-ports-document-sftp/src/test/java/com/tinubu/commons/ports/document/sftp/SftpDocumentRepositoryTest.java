/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.sftp;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatIllegalStateException;

import java.net.URI;
import java.nio.file.Path;
import java.time.Duration;

import org.apache.sshd.server.SshServer;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.sftp.SftpDocumentConfig.SftpDocumentConfigBuilder;
import com.tinubu.commons.ports.document.sftp.strategy.DefaultChunkedWriteStrategy;
import com.tinubu.commons.ports.document.sftp.strategy.FileSystemWriteStrategy;
import com.tinubu.commons.ports.document.sftp.strategy.NonTransactionalChunkedServerStrategy;

public class SftpDocumentRepositoryTest {

   /**
    * Test suite for general tests.
    */
   @Nested
   public class WhenNominal extends AbstractSftpDocumentRepositoryTest {

      @Override
      public SftpDocumentConfigBuilder sftpDocumentConfig(SshServer server) {
         return super.sftpDocumentConfig(server).sessionPoolSize(4).timeout(Duration.ofSeconds(1));
      }

      @Test
      public void testBadLibraryUsageWhenUnclosedDocumentContent() {
         Path documentPath = path("path/pathfile1.pdf");
         DocumentPath testDocument = DocumentPath.of(documentPath);

         try {
            createDocuments(documentPath);

            documentRepository().findDocumentById(testDocument);
            documentRepository().findDocumentById(testDocument);

         } finally {
            deleteDocuments(documentPath);
         }

         assertThatIllegalStateException()
               .isThrownBy(this::closeDocumentRepository)
               .withMessage(
                     "2 active sessions not released in caching pool on repository close, reasons can be : bad library usage => you MUST close (try-with-resources) EVERY DocumentRepository instances, and EVERY returned (Stream<Document|DocumentEntry> streams, or Document content's inputStream/outputStream)");
      }

      @Test
      public void testBadLibraryUsageWhenUnclosedDocumentStream() {
         Path documentPath = path("path/pathfile1.pdf");

         try {
            createDocuments(documentPath);

            documentRepository().findDocumentsBySpecification(__ -> true);
            documentRepository().findDocumentEntriesBySpecification(__ -> true);
         } finally {
            deleteDocuments(documentPath);
         }

         assertThatIllegalStateException()
               .isThrownBy(this::closeDocumentRepository)
               .withMessage(
                     "2 active sessions not released in caching pool on repository close, reasons can be : bad library usage => you MUST close (try-with-resources) EVERY DocumentRepository instances, and EVERY returned (Stream<Document|DocumentEntry> streams, or Document content's inputStream/outputStream)");
      }

      @Test
      public void testBadLibraryUsageWhenUnclosedDocumentAndFullPool() {
         Path documentPath = path("path/pathfile1.pdf");
         DocumentPath testDocument = DocumentPath.of(documentPath);

         try {
            createDocuments(documentPath);

            Document d1 =
                  documentRepository().findDocumentById(testDocument).orElseThrow(IllegalStateException::new);
            Document d2 =
                  documentRepository().findDocumentById(testDocument).orElseThrow(IllegalStateException::new);
            Document d3 =
                  documentRepository().findDocumentById(testDocument).orElseThrow(IllegalStateException::new);
            Document d4 =
                  documentRepository().findDocumentById(testDocument).orElseThrow(IllegalStateException::new);

            try {
               assertThatExceptionOfType(DocumentAccessException.class)
                     .isThrownBy(() -> documentRepository().findDocumentById(testDocument))
                     .withMessage(
                           "Timed out while waiting to acquire a pool entry, reasons can be : too much concurrent SFTP operations on the same repository => increase pool size in configuration | bad library usage => you MUST close (try-with-resources) EVERY DocumentRepository instances, and EVERY returned (Stream<Document|DocumentEntry> streams, or Document content's inputStream/outputStream)");
            } finally { // ensure deleteDocuments won't override failure if assertion fails
               d1.content().close();
               d2.content().close();
               d3.content().close();
               d4.content().close();
            }

         } finally {
            deleteDocuments(documentPath);
         }
      }

   }

   /**
    * Test suite with a non-caching session factory.
    */
   @Nested
   public class WhenNonCachingSessionFactory extends AbstractSftpDocumentRepositoryTest {
      @Override
      public SftpDocumentConfigBuilder sftpDocumentConfig(SshServer server) {
         return super.sftpDocumentConfig(server).sessionCaching(false);
      }
   }

   /**
    * Test suite with a relative base path.
    */
   @Nested
   public class WhenRelativeBasePath extends AbstractSftpDocumentRepositoryTest {

      @Override
      public SftpDocumentConfigBuilder sftpDocumentConfig(SshServer server) {
         return super.sftpDocumentConfig(server).basePath(path("basePath"));
      }

      @Nested
      public class UriAdapter {

         @Test
         public void testToDocumentUriWhenNominal() {
            assertThat(documentRepository().toUri(DocumentPath.of("file.txt"))).isEqualTo(testSftpUri().resolve(
                  "basePath/file.txt"));
            assertThat(documentRepository().toUri(DocumentPath.of("path/file.txt"))).isEqualTo(testSftpUri().resolve(
                  "basePath/path/file.txt"));
         }

         @Test
         public void testSupportsDocumentUriWhenNominal() {
            assertThat(documentRepository().supportsUri(testSftpUri().resolve("basePath/path/test.txt"),
                                                        true)).isTrue();
         }

         @Test
         public void testSupportsDocumentUriWhenRepositoryUri() {
            assertThat(documentRepository().supportsUri(testSftpUri().resolve("basePath"), true)).isFalse();
         }

         @Test
         public void testSupportsDocumentRepositoryUriWhenAlternativeForms() {
            assertThat(documentRepository().supportsUri(testSftpUri().resolve(
                  "basePath/path/test.txt#fragment"), true)).isFalse();
            assertThat(documentRepository().supportsUri(testSftpUri().resolve("basePath/path/test.txt?query"),
                                                        true)).isFalse();
            assertThat(documentRepository().supportsUri(uri("sftp://"
                                                            + testSftpUsername()
                                                            + "@"
                                                            + testSftpHost()
                                                            + "/basePath/path/test.txt"), true))
                  .as("Test service does not use default port")
                  .isFalse();
         }

         @Test
         public void testToRepositoryUriWhenNominal() {
            assertThat(documentRepository().toUri()).isEqualTo(testSftpUri().resolve("basePath"));
         }

         @Test
         public void testSupportsRepositoryUriWhenNominal() {
            assertThat(documentRepository().supportsUri(testSftpUri().resolve("basePath"), false)).isTrue();
         }

         @Test
         public void testSupportsRepositoryUriWhenDocumentUri() {
            assertThat(documentRepository().supportsUri(testSftpUri().resolve("basePath/path/test.txt"),
                                                        false)).isTrue();
         }

      }

   }

   /**
    * Test suite with an absolute base path.
    */
   @Nested
   public class WhenAbsoluteBasePath extends AbstractSftpDocumentRepositoryTest {
      @Override
      public SftpDocumentConfigBuilder sftpDocumentConfig(SshServer server) {
         return super.sftpDocumentConfig(server).basePath(path("/basePath"));
      }

      @Nested
      public class UriAdapter {

         @Test
         public void testToDocumentUriWhenNominal() {
            assertThat(documentRepository().toUri(DocumentPath.of("file.txt"))).isEqualTo(uri(testSftpUri()
                                                                                              + "/basePath/file.txt"));
            assertThat(documentRepository().toUri(DocumentPath.of("path/file.txt"))).isEqualTo(uri(testSftpUri()
                                                                                                   + "/basePath/path/file.txt"));
         }

         @Test
         public void testSupportsDocumentUriWhenNominal() {
            assertThat(documentRepository().supportsUri(uri(testSftpUri() + "/basePath/path/test.txt"),
                                                        true)).isTrue();
         }

         @Test
         public void testSupportsDocumentUriWhenRepositoryUri() {
            assertThat(documentRepository().supportsUri(uri(testSftpUri() + "/basePath"), true)).isFalse();
         }

         @Test
         public void testSupportsDocumentRepositoryUriWhenAlternativeForms() {
            assertThat(documentRepository().supportsUri(uri(testSftpUri()
                                                            + "/basePath/path/test.txt#fragment"),
                                                        true)).isFalse();
            assertThat(documentRepository().supportsUri(uri(testSftpUri() + "/basePath/path/test.txt?query"),
                                                        true)).isFalse();
            assertThat(documentRepository().supportsUri(uri("sftp://"
                                                            + testSftpUsername()
                                                            + "@"
                                                            + testSftpHost()
                                                            + "//basePath/path/test.txt"), true))
                  .as("Test service does not use default port")
                  .isFalse();
         }

         @Test
         public void testToRepositoryUriWhenNominal() {
            assertThat(documentRepository().toUri()).isEqualTo(uri(testSftpUri() + "/basePath"));
         }

         @Test
         public void testSupportsRepositoryUriWhenNominal() {
            assertThat(documentRepository().supportsUri(uri(testSftpUri() + "/basePath"), false)).isTrue();
         }

         @Test
         public void testSupportsRepositoryUriWhenDocumentUri() {
            assertThat(documentRepository().supportsUri(uri(testSftpUri() + "/basePath/path/test.txt"),
                                                        false)).isTrue();
         }

      }

   }

   /**
    * Test suite with an empty base path.
    */
   @Nested
   public class WhenEmptyBasePath extends AbstractSftpDocumentRepositoryTest {
      @Override
      public SftpDocumentConfigBuilder sftpDocumentConfig(SshServer server) {
         return super.sftpDocumentConfig(server).basePath(path(""));
      }

      @Nested
      public class UriAdapter {

         @Test
         public void testToDocumentUriWhenNominal() {
            assertThat(documentRepository().toUri(DocumentPath.of("file.txt"))).isEqualTo(testSftpUri().resolve(
                  "/file.txt"));
            assertThat(documentRepository().toUri(DocumentPath.of("path/file.txt"))).isEqualTo(testSftpUri().resolve(
                  "/path/file.txt"));
         }

         @Test
         public void testSupportsDocumentUriWhenNominal() {
            assertThat(documentRepository().supportsUri(testSftpUri().resolve("/path/test.txt"),
                                                        true)).isTrue();
         }

         @Test
         public void testSupportsDocumentUriWhenRepositoryUri() {
            assertThat(documentRepository().supportsUri(testSftpUri().resolve("/"), true)).isFalse();
         }

         @Test
         public void testSupportsDocumentRepositoryUriWhenAlternativeForms() {
            assertThat(documentRepository().supportsUri(testSftpUri().resolve("/path/test.txt#fragment"),
                                                        true)).isFalse();
            assertThat(documentRepository().supportsUri(testSftpUri().resolve("/path/test.txt?query"),
                                                        true)).isFalse();
            assertThat(documentRepository().supportsUri(uri("sftp://"
                                                            + testSftpUsername()
                                                            + "@"
                                                            + testSftpHost()
                                                            + "/basePath/path/test.txt"), true))
                  .as("Test service does not use default port")
                  .isFalse();
         }

         @Test
         public void testToRepositoryUriWhenNominal() {
            assertThat(documentRepository().toUri()).isEqualTo(testSftpUri().resolve("/"));
         }

         @Test
         public void testSupportsRepositoryUriWhenNominal() {
            assertThat(documentRepository().supportsUri(testSftpUri().resolve("/"), false)).isTrue();
         }

         @Test
         public void testSupportsRepositoryUriWhenDocumentUri() {
            assertThat(documentRepository().supportsUri(testSftpUri().resolve("/path/test.txt"),
                                                        false)).isTrue();
         }

      }

   }

   /**
    * Test suite with a relative base path, including a sub path.
    */
   @Nested
   public class WhenServerBasePathAndSubPath extends AbstractSftpDocumentRepositoryTest {
      @Override
      public SftpDocumentConfigBuilder sftpDocumentConfig(SshServer server) {
         return super.sftpDocumentConfig(server).basePath(path("basePath/subPathPath"));
      }
   }

   /**
    * Test suite with a root absolute base path.
    */
   @Nested
   public class WhenRootBasePath extends AbstractSftpDocumentRepositoryTest {
      @Override
      public SftpDocumentConfigBuilder sftpDocumentConfig(SshServer server) {
         return super.sftpDocumentConfig(server).basePath(path("/"));
      }
   }

   /**
    * Test URI matching when port is not specified. No SFTP server is instantiated in this test branch.
    */
   @Nested
   public class WhenDefaultPort {

      @Test
      public void testSupportsDocumentUriWhenAlternativeForms() {
         try (SftpDocumentRepository documentRepository = new SftpDocumentRepository(new SftpDocumentConfigBuilder()
                                                                                           .host("localhost")
                                                                                           .username("user")
                                                                                           .password(
                                                                                                 "password")
                                                                                           .build())) {

            assertThat(documentRepository.supportsUri(URI.create("sftp://user@localhost/path/test.txt"),
                                                      true)).isTrue();
         }
      }

      @Test
      public void testSupportsRepositoryUriWhenAlternativeForms() {
         try (SftpDocumentRepository documentRepository = new SftpDocumentRepository(new SftpDocumentConfigBuilder()
                                                                                           .host("localhost")
                                                                                           .username("user")
                                                                                           .password(
                                                                                                 "password")
                                                                                           .build())) {

            assertThat(documentRepository.supportsUri(URI.create("sftp://user@localhost/path"),
                                                      true)).isTrue();
         }
      }

   }

   /**
    * Test suite when SFTP session pool size is 1.
    */
   @Nested
   public class WhenSessionPoolSizeOne extends AbstractSftpDocumentRepositoryTest {
      @Override
      public SftpDocumentConfigBuilder sftpDocumentConfig(SshServer server) {
         return super.sftpDocumentConfig(server).sessionPoolSize(1).keepAliveInterval(Duration.ofSeconds(60));
      }
   }

   /**
    * Test suite sing a {@link DefaultChunkedWriteStrategy} write strategy.
    */
   @Nested
   public class WhenDefaultChunkedWriteStrategy extends AbstractSftpDocumentRepositoryTest {
      @Override
      public SftpDocumentConfigBuilder sftpDocumentConfig(SshServer server) {
         return super.sftpDocumentConfig(server).writeStrategyClass(DefaultChunkedWriteStrategy.class);
      }
   }

   /**
    * Test suite sing a {@link NonTransactionalChunkedServerStrategy} write strategy.
    */
   @Nested
   public class WhenNonTransactionalChunkedWriteStrategy extends AbstractSftpDocumentRepositoryTest {
      @Override
      public SftpDocumentConfigBuilder sftpDocumentConfig(SshServer server) {
         return super
               .sftpDocumentConfig(server)
               .writeStrategyClass(NonTransactionalChunkedServerStrategy.class);
      }
   }

   /**
    * Test suite sing a {@link FileSystemWriteStrategy} write strategy.
    */
   @Nested
   public class WhenFileSystemWriteStrategy extends AbstractSftpDocumentRepositoryTest {
      @Override
      public SftpDocumentConfigBuilder sftpDocumentConfig(SshServer server) {
         return super.sftpDocumentConfig(server).writeStrategyClass(FileSystemWriteStrategy.class);
      }
   }

}