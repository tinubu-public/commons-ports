
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.sftp.strategy;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinubu.commons.ports.document.sftp.SftpOperations;

/**
 * Write strategy to simulate transactional operations on non-transactional SFTP servers, using temporary
 * files.
 * The workaround does not work in append mode, in this case the operation will be non-transactional.
 * The strategy also works for transactional SFTP servers, with same result, but with some extra complexity.
 * The strategy will append multiple chunks of data to the target SFTP file.
 */
public class NonTransactionalChunkedServerStrategy extends AbstractChunkedWriteStrategy {
   private static final Logger log = LoggerFactory.getLogger(NonTransactionalChunkedServerStrategy.class);

   /**
    * Temporary file suffix used for consistent transfer, this suffix will be appended to real transferred
    * name.
    */
   private static final String DEFAULT_TEMPORARY_FILE_SUFFIX = ".writing";
   /**
    * Temporary file random suffix used to isolate concurrent operations on the same file on
    * non-transactional SFTP servers. Set to {@code null} to disable the feature.
    */
   private final Random USE_TEMPORARY_FILE_RANDOM_GENERATOR = null;

   private final Path targetFile;
   private final WriteContext writeContext;

   public NonTransactionalChunkedServerStrategy(WriteContext writeContext,
                                                int chunkSize,
                                                String temporaryFileSuffix) {
      super(chunkSize);
      this.writeContext = notNull(writeContext, "writeContext");
      this.targetFile = targetFile(temporaryFileSuffix);
   }

   public NonTransactionalChunkedServerStrategy(WriteContext writeContext, String temporaryFileSuffix) {
      super();
      this.writeContext = notNull(writeContext, "writeContext");
      this.targetFile = targetFile(temporaryFileSuffix);
   }

   private Path targetFile(String temporaryFileSuffix) {
      String validTemporaryFileSuffix = nullable(temporaryFileSuffix,
                                                 tps -> notBlank(temporaryFileSuffix, "temporaryFileSuffix"),
                                                 DEFAULT_TEMPORARY_FILE_SUFFIX);
      return useTemporaryFile()
             ? temporaryFile(writeContext.sftpFile(), validTemporaryFileSuffix)
             : writeContext.sftpFile();
   }

   public NonTransactionalChunkedServerStrategy(WriteContext writeContext) {
      this(writeContext, null);
   }

   @Override
   public void start() throws IOException {
      if (writeContext.overwrite() && writeContext.exists() && !writeContext.append()) {
         writeContext.session().remove(writeContext.sftpFile().toString());
      }
      if (!(writeContext.append() && writeContext.exists())) {
         createTarget();
      }
   }

   @Override
   public void rawWrite(byte[] byteArray, int position, int length) throws IOException {
      writeContext
            .session()
            .append(new ByteArrayInputStream(byteArray, position, length), targetFile.toString());
   }

   @Override
   public void finish() throws IOException {
      try {
         super.finish();
      } finally {
         if (useTemporaryFile()) {
            try {
               commitTemporaryFile();
            } catch (IOException | UncheckedIOException e) {
               cleanTemporaryFile();
            }
         }
      }
   }

   private boolean useTemporaryFile() {
      return !writeContext.append();
   }

   private void createTarget() throws IOException {
      createParentFolders(targetFile);
      writeContext.session().write(new ByteArrayInputStream(new byte[0], 0, 0), targetFile.toString());
   }

   /**
    * Computes target file name for specified SFTP file to be transferred.
    */
   private Path temporaryFile(Path sftpFile, String temporaryFileSuffix) {
      String random = nullable(USE_TEMPORARY_FILE_RANDOM_GENERATOR)
            .map(generator -> String.valueOf(generator.nextInt() % 1000))
            .orElse("");
      return Paths.get(sftpFile.toString() + temporaryFileSuffix + random);
   }

   private void commitTemporaryFile() throws IOException {
      writeContext.session().rename(targetFile.toString(), writeContext.sftpFile().toString());
   }

   /**
    * @apiNote IOException should be ignored.
    */
   private void cleanTemporaryFile() {
      try {
         writeContext.session().remove(targetFile.toString());
      } catch (IOException | UncheckedIOException e) {
         log.warn("Failed to remove '{}' temporary file", targetFile, e);
      }
   }

   /**
    * Creates missing parent directories recursively for specified sftp file.
    */
   private void createParentFolders(Path sftpFile) throws IOException {
      if (sftpFile.getParent() != null) {
         SftpOperations.sftpMkdirs(writeContext.session(), sftpFile.getParent());
      }
   }
}
