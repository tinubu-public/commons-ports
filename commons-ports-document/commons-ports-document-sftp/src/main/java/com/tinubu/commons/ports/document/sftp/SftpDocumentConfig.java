/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.sftp;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.ArrayRules.isNotEmptyBytes;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isGreaterThanOrEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.NumberRules.isStrictlyPositive;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.Optional;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.valueformatter.HiddenValueFormatter;
import com.tinubu.commons.lang.util.Pair;
import com.tinubu.commons.ports.document.sftp.strategy.DefaultChunkedWriteStrategy;
import com.tinubu.commons.ports.document.sftp.strategy.WriteStrategy;

/**
 * SFTP document repository configuration.
 */
// TODO createIfMissingBasePath + failFastIfMissingBasePath ?
public class SftpDocumentConfig extends AbstractValue {
   private static final String DEFAULT_HOST = "localhost";
   private static final int DEFAULT_PORT = 22;
   /** Whether to allow unknown server keys by default. */
   private static final boolean DEFAULT_ALLOW_UNKNOWN_KEYS = true;
   private static final String DEFAULT_USERNAME = System.getProperty("user.name");
   private static final byte[] DEFAULT_PRIVATE_KEY =
         fileContent(Paths.get(System.getProperty("user.home")).resolve(".ssh/id_rsa"), true);
   /**
    * Whether to enable SFTP caching by default.
    */
   private static final boolean DEFAULT_SESSION_CACHING = true;
   /**
    * Default SFTP caching session pool size. Value must be > 0.
    */
   private static final int DEFAULT_SESSION_POOL_SIZE = 10;
   /**
    * Default session wait timeout for available connection in pool. Session wait timeout disabled if
    * set to 0.
    */
   private static final Duration DEFAULT_SESSION_POOL_WAIT_TIMEOUT = Duration.ZERO;
   /** Keep-alive interval, before an alive message is sent to the server. */
   private static final Duration DEFAULT_KEEP_ALIVE_INTERVAL = Duration.ofMinutes(1);
   /** Default write strategy. */
   private static final Class<? extends WriteStrategy> DEFAULT_WRITE_STRATEGY =
         DefaultChunkedWriteStrategy.class;
   /**
    * Default connection timeout. Connection timeout disabled if set to 0.
    */
   private static final Duration DEFAULT_CONNECT_TIMEOUT = Duration.ZERO;
   /**
    * Default socket (I/O) timeout. Socket timeout disabled if set to 0.
    */
   private static final Duration DEFAULT_SOCKET_TIMEOUT = Duration.ZERO;

   private final String host;
   private final int port;
   private final boolean allowUnknownKeys;
   private final String username;
   private final String password;
   private final byte[] privateKey;
   private final String privateKeyPassphrase;
   private final Path basePath;
   private final boolean sessionCaching;
   private final int sessionPoolSize;
   private final Duration sessionPoolWaitTimeout;
   private final Duration keepAliveInterval;
   private final Class<? extends WriteStrategy> writeStrategyClass;
   private final Duration connectTimeout;
   private final Duration socketTimeout;

   public SftpDocumentConfig(SftpDocumentConfigBuilder builder) {
      this.host = nullable(builder.host, DEFAULT_HOST);
      this.port = nullable(builder.port, DEFAULT_PORT);
      this.allowUnknownKeys = nullable(builder.allowUnknownKeys, DEFAULT_ALLOW_UNKNOWN_KEYS);
      this.username = nullable(builder.username, DEFAULT_USERNAME);
      this.password = builder.password;
      this.privateKey = nullable(builder.privateKey, DEFAULT_PRIVATE_KEY);
      this.privateKeyPassphrase = builder.privateKeyPassphrase;
      this.basePath = nullable(builder.basePath, Paths.get("")).normalize();
      this.sessionCaching = nullable(builder.sessionCaching, DEFAULT_SESSION_CACHING);
      this.sessionPoolSize = nullable(builder.sessionPoolSize, DEFAULT_SESSION_POOL_SIZE);
      this.sessionPoolWaitTimeout =
            nullable(builder.sessionPoolWaitTimeout, DEFAULT_SESSION_POOL_WAIT_TIMEOUT);
      this.keepAliveInterval = nullable(builder.keepAliveInterval, DEFAULT_KEEP_ALIVE_INTERVAL);
      this.writeStrategyClass = nullable(builder.writeStrategyClass, DEFAULT_WRITE_STRATEGY);
      this.connectTimeout = nullable(builder.connectTimeout, DEFAULT_CONNECT_TIMEOUT);
      this.socketTimeout = nullable(builder.socketTimeout, DEFAULT_SOCKET_TIMEOUT);
   }

   @Override
   public Fields<? extends SftpDocumentConfig> defineDomainFields() {
      return Fields
            .<SftpDocumentConfig>builder()
            .field("host", v -> v.host, isNotBlank())
            .field("port", v -> v.port, isStrictlyPositive())
            .field("allowUnknownKeys", v -> v.allowUnknownKeys)
            .field("username", v -> v.username, isNotBlank())
            .field("password",
                   v -> v.password,
                   new HiddenValueFormatter(),
                   isNotBlank().ifIsSatisfied(() -> privateKey == null))
            .field("privateKey",
                   v -> v.privateKey,
                   new HiddenValueFormatter(),
                   isNull().orValue(isNotEmptyBytes()))
            .field("privateKeyPassphrase",
                   v -> v.privateKeyPassphrase,
                   new HiddenValueFormatter(),
                   isNull().orValue(isNotBlank()))
            .field("basePath",
                   v -> v.basePath,
                   isNotNull() /* TODO : (must always support "" + support both absolute and not absolute). should I support traversals in relative base path ? -> this can have a functional interest, because basepath can be relative to user account that change dynamically, but this can be more secure to be more restrictive here : .andValue(hasNoTraversal())*/)
            .field("sessionCaching", v -> v.sessionCaching)
            .field("sessionPoolSize", v -> v.sessionPoolSize, isStrictlyPositive())
            .field("sessionWaitTimeout", v -> v.sessionPoolWaitTimeout,
                   isGreaterThanOrEqualTo(value(Duration.ZERO)))
            .field("keepAliveInterval",
                   v -> v.keepAliveInterval,
                   isGreaterThanOrEqualTo(value(Duration.ZERO)))
            .field("writeStrategyClass", v -> v.writeStrategyClass, isNotNull())
            .field("connectTimeout", v -> v.connectTimeout, isGreaterThanOrEqualTo(value(Duration.ZERO)))
            .field("socketTimeout", v -> v.socketTimeout, isGreaterThanOrEqualTo(value(Duration.ZERO)))
            .build();
   }

   /**
    * SFTP server host. Default to {@link #DEFAULT_HOST}.
    */
   public String host() {
      return host;
   }

   /**
    * SFTP server port. Default to {@link #DEFAULT_PORT}.
    */
   public Integer port() {
      return port;
   }

   /**
    * Whether to allow unknown server keys. Default to {@link #DEFAULT_ALLOW_UNKNOWN_KEYS}.
    */
   public boolean allowUnknownKeys() {
      return this.allowUnknownKeys;
   }

   /**
    * SFTP server authentication username.
    * Default to {@code ${user.name}}.
    */
   public String username() {
      return username;
   }

   /**
    * Optional SFTP server authentication password.
    * Use either privateKey or password for authentication. Use password authentication if both are set.
    */
   public String password() {
      return password;
   }

   /**
    * Optional SFTP server authentication private key.
    * Use either privateKey or password for authentication. Use password authentication if both set.
    * Default to {@code ${user.home}/.ssh/id_rsa}.
    */
   public byte[] privateKey() {
      return privateKey;
   }

   /**
    * Optional SFTP server authentication private key passphrase.
    */
   public String privateKeyPassphrase() {
      return privateKeyPassphrase;
   }

   /**
    * Optional base path to store documents in SFTP server. If omitted, or set to {@code ""}, the SFTP server
    * default directory will be used.
    */
   public Path basePath() {
      return basePath;
   }

   /**
    * Whether to use session caching. Enabled by default.
    *
    * @return whether to use session caching
    */
   public boolean sessionCaching() {
      return sessionCaching;
   }

   /**
    * Optional SFTP caching session pool size. Value must be > 0. Default to
    * {@link #DEFAULT_SESSION_POOL_SIZE}.
    *
    * @return SFTP caching session pool size
    */
   public int sessionPoolSize() {
      return sessionPoolSize;
   }

   /**
    * SFTP session wait timeout for available connection in pool. Timeout is disabled if set
    * to 0. Default to {@link #DEFAULT_SESSION_POOL_WAIT_TIMEOUT}.
    *
    * @return SFTP caching session pool size
    */
   public Duration sessionPoolWaitTimeout() {
      return sessionPoolWaitTimeout;
   }

   /**
    * Keep-alive interval. Value must be greater than or equal to 0.
    * If interval duration is set to 0, keep-alive will be disabled.
    * Default interval is {@link #DEFAULT_KEEP_ALIVE_INTERVAL}.
    *
    * @return keep-alive interval, never {@code null}
    */
   public Duration keepAliveInterval() {
      return keepAliveInterval;
   }

   /**
    * Write strategy to use.
    * Default strategy is {@link #DEFAULT_WRITE_STRATEGY}.
    *
    * @return write strategy, never {@code null}
    */
   public Class<? extends WriteStrategy> writeStrategyClass() {
      return writeStrategyClass;
   }

   /**
    * SFTP server connection timeout. If timeout is set to 0, no connection timeout will be applied. Default
    * timeout is {@link #DEFAULT_CONNECT_TIMEOUT}.
    *
    * @return SFTP server socket timeout
    */
   public Duration connectTimeout() {
      return connectTimeout;
   }

   /**
    * SFTP server socket (I/O) timeout. If timeout is set to 0, no socket timeout will be applied. Default
    * timeout is {@link #DEFAULT_SOCKET_TIMEOUT}.
    *
    * @return SFTP server socket timeout
    */
   public Duration socketTimeout() {
      return socketTimeout;
   }

   private static byte[] fileContent(Path path, boolean ignoreIfNotFound) {
      try (FileInputStream fis = new FileInputStream(path.toString())) {
         return IOUtils.toByteArray(fis);
      } catch (FileNotFoundException e) {
         if (!ignoreIfNotFound) {
            throw new IllegalStateException(String.format("Unknown '%s' path", path));
         } else {
            return null;
         }
      } catch (IOException e) {
         throw new IllegalStateException(e);
      }
   }

   public static class SftpDocumentConfigBuilder extends DomainBuilder<SftpDocumentConfig> {
      private String host;
      private Integer port;
      private Boolean allowUnknownKeys;
      private String username;
      private String password;
      private byte[] privateKey;
      private String privateKeyPassphrase;
      private Path basePath;
      private Boolean sessionCaching;
      private Integer sessionPoolSize;
      private Duration sessionPoolWaitTimeout;
      private Duration keepAliveInterval;
      private Class<? extends WriteStrategy> writeStrategyClass;
      private Duration connectTimeout;
      private Duration socketTimeout;

      public static SftpDocumentConfigBuilder from(SftpDocumentConfig config) {
         return new SftpDocumentConfigBuilder()
               .host(config.host)
               .port(config.port)
               .allowUnknownKeys(config.allowUnknownKeys)
               .username(config.username)
               .password(config.password)
               .privateKey(config.privateKey)
               .privateKeyPassphrase(config.privateKeyPassphrase)
               .basePath(config.basePath)
               .sessionCaching(config.sessionCaching)
               .sessionPoolSize(config.sessionPoolSize).sessionPoolWaitTimeout(config.sessionPoolWaitTimeout)
               .keepAliveInterval(config.keepAliveInterval)
               .writeStrategyClass(config.writeStrategyClass)
               .connectTimeout(config.connectTimeout)
               .socketTimeout(config.socketTimeout);
      }

      /**
       * Returns a pre-configured builder from specified document repository URI information, only if URI is
       * compatible with this repository.
       *
       * @param uri URI
       *
       * @return pre-configured builder or {@link Optional#empty} if URI is not compatible with this repository.
       */
      public Optional<SftpDocumentConfigBuilder> fromUri(URI uri) {
         validate(uri, "uri", isNotNull()).orThrow();

         SftpDocumentConfigBuilder config = null;
         if (SftpDocumentRepository.isCompatibleUri(uri, false)) {
            Pair<String, String> userInfos = uriUserInfos(uri);
            config = new SftpDocumentConfigBuilder()
                  .<SftpDocumentConfigBuilder>reconstitute()
                  .host(uri.getHost())
                  .port(uri.getPort() == -1 ? null : uri.getPort())
                  .username(userInfos.getKey())
                  .password(userInfos.getValue())
                  .basePath(Paths.get(StringUtils.removeStart(uri.getPath(), "/")));
         }

         return nullable(config);
      }

      /**
       * Returns username and password from URI if any.
       *
       * @param uri URI
       *
       * @return pair of username and password, each one can be {@code null} if missing
       */
      private static Pair<String, String> uriUserInfos(URI uri) {
         return nullable(uri.getUserInfo()).map(ui -> {
            String[] uiParts = StringUtils.split(ui, ":", 2);
            return Pair.of(uiParts[0], uiParts.length > 1 ? uiParts[1] : null);
         }).orElse(Pair.of(null, null));
      }

      public SftpDocumentConfigBuilder host(String host) {
         this.host = host;
         return this;
      }

      public SftpDocumentConfigBuilder port(Integer port) {
         this.port = port;
         return this;
      }

      public SftpDocumentConfigBuilder allowUnknownKeys(Boolean allowUnknownKeys) {
         this.allowUnknownKeys = allowUnknownKeys;
         return this;
      }

      public SftpDocumentConfigBuilder username(String username) {
         this.username = username;
         return this;
      }

      public SftpDocumentConfigBuilder password(String password) {
         this.password = password;
         return this;
      }

      public SftpDocumentConfigBuilder privateKey(byte[] privateKey) {
         this.privateKey = privateKey;
         return this;
      }

      public SftpDocumentConfigBuilder privateKeyPassphrase(String privateKeyPassphrase) {
         this.privateKeyPassphrase = privateKeyPassphrase;
         return this;
      }

      public SftpDocumentConfigBuilder basePath(Path basePath) {
         this.basePath = basePath;
         return this;
      }

      public SftpDocumentConfigBuilder sessionCaching(Boolean sessionCaching) {
         this.sessionCaching = sessionCaching;
         return this;
      }

      public SftpDocumentConfigBuilder sessionPoolSize(Integer sessionPoolSize) {
         this.sessionPoolSize = sessionPoolSize;
         return this;
      }

      public SftpDocumentConfigBuilder sessionPoolWaitTimeout(Duration sessionPoolWaitTimeout) {
         this.sessionPoolWaitTimeout = sessionPoolWaitTimeout;
         return this;
      }

      public SftpDocumentConfigBuilder keepAliveInterval(Duration keepAliveInterval) {
         this.keepAliveInterval = keepAliveInterval;
         return this;
      }

      public SftpDocumentConfigBuilder writeStrategyClass(Class<? extends WriteStrategy> writeStrategyClass) {
         this.writeStrategyClass = writeStrategyClass;
         return this;
      }

      /**
       * @deprecated Use {@link #connectTimeout(Duration)} and/or {@link #socketTimeout(Duration)} instead.
       */
      @Deprecated
      public SftpDocumentConfigBuilder timeout(Duration timeout) {
         this.socketTimeout = timeout;
         this.connectTimeout = timeout;
         this.sessionPoolWaitTimeout = timeout;
         return this;
      }

      public SftpDocumentConfigBuilder connectTimeout(Duration connectTimeout) {
         this.connectTimeout = connectTimeout;
         return this;
      }

      public SftpDocumentConfigBuilder socketTimeout(Duration socketTimeout) {
         this.socketTimeout = socketTimeout;
         return this;
      }

      @Override
      protected SftpDocumentConfig buildDomainObject() {
         return new SftpDocumentConfig(this);
      }
   }
}
