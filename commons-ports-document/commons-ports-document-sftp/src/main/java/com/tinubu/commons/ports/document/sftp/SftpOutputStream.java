/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.sftp;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import java.io.IOException;
import java.io.OutputStream;

import com.tinubu.commons.ports.document.sftp.strategy.DefaultChunkedWriteStrategy;
import com.tinubu.commons.ports.document.sftp.strategy.WriteContext;
import com.tinubu.commons.ports.document.sftp.strategy.WriteStrategy;
import com.tinubu.commons.ports.document.sftp.strategy.WriteStrategyFactory;

/**
 * {@link OutputStream} implementation using SFTP append under the hood.
 * <p>
 * Maximum memory consumption for this stream has the size of a chunk.
 *
 * @implNote Stream copy transaction is implemented using a temporary file renamed after full stream
 *       have been correctly copied to the SFTP.
 */
public class SftpOutputStream extends OutputStream {

   /** Write context to use. */
   private final WriteContext writeContext;
   /** Write strategy to use. */
   private final WriteStrategy writeStrategy;
   /** Indicates whether the stream is closed. */
   private boolean closed;

   public SftpOutputStream(WriteContext writeContext, WriteStrategyFactory writeStrategyFactory)
         throws IOException {
      this.writeContext = writeContext;
      this.closed = false;

      this.writeStrategy = nullable(writeStrategyFactory)
            .orElse(DefaultChunkedWriteStrategy::new)
            .createWriteStrategy(writeContext);
      this.writeStrategy.start();
   }

   public SftpOutputStream(WriteContext writeContext) throws IOException {
      this(writeContext, null);
   }

   /**
    * Write a byte to the stream.
    *
    * @param b the {@code byte}.
    *
    * @throws IOException triggered when data cannot be send to sftp server.
    */
   @Override
   public void write(int b) throws IOException {
      write(new byte[] { (byte) b }, 0, 1);
   }

   /**
    * Write an array to the stream.
    *
    * @param byteArray the byte-array to append.
    *
    * @throws IOException triggered when data cannot be send to sftp server.
    */
   @Override
   public void write(byte[] byteArray) throws IOException {
      write(byteArray, 0, byteArray.length);
   }

   /**
    * Writes an array to the stream.
    *
    * @param byteArray the array to write
    * @param position the offset into the array
    * @param length the number of bytes to write
    *
    * @throws IOException triggered when data cannot be send to sftp server.
    */
   @Override
   public void write(byte[] byteArray, int position, int length) throws IOException {
      assertOpen();
      writeStrategy.write(byteArray, position, length);
   }

   @Override
   public void close() throws IOException {
      if (!closed) {
         closed = true;

         writeStrategy.finish();
      }
   }

   private void assertOpen() {
      if (closed) {
         throw new IllegalStateException("Stream is closed");
      }
   }

}
