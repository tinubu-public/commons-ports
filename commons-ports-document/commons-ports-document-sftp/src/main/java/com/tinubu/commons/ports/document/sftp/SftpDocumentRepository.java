/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.sftp;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.ddd2.invariant.rules.PredicateRules.satisfies;
import static com.tinubu.commons.lang.util.CheckedRunnable.uncheckedRunnable;
import static com.tinubu.commons.lang.util.CollectionUtils.collectionConcat;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.ExceptionUtils.runtimeThrow;
import static com.tinubu.commons.lang.util.ExceptionUtils.sneakyThrow;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent.OutputStreamDocumentContentBuilder;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.METADATA_CONTENT_SIZE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.METADATA_CREATION_DATE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.METADATA_LAST_UPDATE_DATE;
import static com.tinubu.commons.ports.document.domain.capability.RepositoryCapability.allFeatureCapabilities;
import static org.apache.commons.lang3.StringUtils.removeStart;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.integration.file.remote.session.CachingSessionFactory;
import org.springframework.integration.file.remote.session.Session;
import org.springframework.integration.file.remote.session.SessionFactory;
import org.springframework.integration.sftp.session.DefaultSftpSessionFactory;
import org.springframework.integration.util.PoolItemNotAvailableException;
import org.springframework.integration.util.SimplePool;

import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.tinubu.commons.ddd2.domain.event.RegistrableDomainEventService;
import com.tinubu.commons.ddd2.domain.event.SynchronousDomainEventService;
import com.tinubu.commons.ddd2.domain.repository.Repository;
import com.tinubu.commons.ddd2.domain.specification.Specification;
import com.tinubu.commons.ddd2.invariant.rules.UriRules;
import com.tinubu.commons.lang.util.MechanicalSympathy;
import com.tinubu.commons.lang.validation.CheckReturnValue;
import com.tinubu.commons.ports.document.domain.AbstractDocumentRepository;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentEntry.DocumentEntryBuilder;
import com.tinubu.commons.ports.document.domain.DocumentEntrySpecification;
import com.tinubu.commons.ports.document.domain.DocumentMetadata;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent.InputStreamDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata;
import com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.UriAdapter;
import com.tinubu.commons.ports.document.domain.capability.RepositoryCapability;
import com.tinubu.commons.ports.document.sftp.SftpDocumentConfig.SftpDocumentConfigBuilder;
import com.tinubu.commons.ports.document.sftp.strategy.WriteContext;
import com.tinubu.commons.ports.document.sftp.strategy.WriteStrategy;
import com.tinubu.commons.ports.document.sftp.strategy.WriteStrategyFactory;
import com.tinubu.commons.ports.document.transformer.DocumentTransformerUriAdapter;

/**
 * SFTP {@link DocumentRepository} adapter implementation.
 * <p>
 * This implementation uses a {@link CachingSessionFactory SFTP connection pool}, that must be closed before
 * leaving the application. This is done transparently by {@link AutoCloseable#close()} method
 * implementation.
 * <p>
 * Limitations :
 * <ul>
 *    <li>the following Metadata are not persisted: documentPath, contentEncoding, contentType, attributes</li>
 *    <li>Metadata lastUpdateDate best precision is second</li>
 * </ul>
 *
 * @implSpec Immutable class implementation
 */
// FIXME revoir ds tous les adapters le pattern pour le throw des DocumentAccessException (seulement sur IO ou pour toute erreur ?)
// FIXME les exceptions dans les try (SessionInstance.get(..) sont pas encapsulées
// FIXME support defaultFactory.setKnownHostsResource(config);
public class SftpDocumentRepository extends AbstractDocumentRepository {
   private static final Logger log = LoggerFactory.getLogger(SftpDocumentRepository.class);

   private static final String URI_SCHEME = "sftp";
   private static final int DEFAULT_PORT = 22;

   /**
    * Whether to use daemon threads for SFTP sessions.
    */
   private static final boolean SESSION_DAEMON_THREAD = false;
   /**
    * Whether to test connections in caching pool.
    */
   private static final boolean CACHING_SESSION_TEST_SESSION = true;

   /** SFTP client configurer. */
   private final Consumer<DefaultSftpSessionFactory> clientConfigurer;
   /** SFTP client. */
   private final SessionFactory<LsEntry> sftpSessionFactory;
   /** Whether SFTP session is shared. */
   private final boolean sharedSession;
   private final Path basePath;
   private final SftpDocumentConfig sftpDocumentConfig;

   /**
    * Hash of a set of server connection parameters identifying a similar repository.
    */
   private final int sameRepositoryHash;
   private final DocumentTransformerUriAdapter transformerUriAdapter;
   private boolean closed = false;

   private SftpDocumentRepository(SftpDocumentConfig sftpDocumentConfig,
                                  RegistrableDomainEventService eventService,
                                  Consumer<DefaultSftpSessionFactory> clientConfigurer,
                                  SessionFactory<LsEntry> sftpSessionFactory) {
      super(eventService);

      this.sftpDocumentConfig = validate(sftpDocumentConfig, "sftpDocumentConfig", isNotNull()).orThrow();
      this.sharedSession = sftpSessionFactory != null;
      this.clientConfigurer = validate(clientConfigurer, "clientConfigurer", isNotNull()).orThrow();
      this.sftpSessionFactory =
            nullable(sftpSessionFactory, sftpSessionFactory(sftpDocumentConfig, clientConfigurer));
      this.basePath = sftpDocumentConfig.basePath();
      this.sameRepositoryHash = sameRepositoryHash(sftpDocumentConfig);
      this.transformerUriAdapter = new DocumentTransformerUriAdapter(new SftpUriAdapter());
   }

   public SftpDocumentRepository(SftpDocumentConfig sftpDocumentConfig,
                                 RegistrableDomainEventService eventService) {
      this(sftpDocumentConfig, eventService, __ -> { }, null);
   }

   public SftpDocumentRepository(SftpDocumentConfig sftpDocumentConfig) {
      this(sftpDocumentConfig, new SynchronousDomainEventService());
   }

   public SftpDocumentRepository(SftpDocumentConfig sftpDocumentConfig,
                                 RegistrableDomainEventService eventService,
                                 Consumer<DefaultSftpSessionFactory> clientConfigurer) {
      this(sftpDocumentConfig, eventService, clientConfigurer, null);
   }

   public SftpDocumentRepository(SftpDocumentConfig sftpDocumentConfig,
                                 Consumer<DefaultSftpSessionFactory> clientConfigurer) {
      this(sftpDocumentConfig, new SynchronousDomainEventService(), clientConfigurer);
   }

   @Override
   public HashSet<RepositoryCapability> capabilities() {
      return collectionConcat(HashSet::new,
                              allFeatureCapabilities(),
                              list(METADATA_CREATION_DATE, METADATA_LAST_UPDATE_DATE, METADATA_CONTENT_SIZE));
   }

   @Override
   public boolean sameRepositoryAs(Repository<Document, DocumentPath> documentRepository) {
      validate(documentRepository, "documentRepository", isNotNull()).orThrow();

      return documentRepository instanceof SftpDocumentRepository
             && Objects.equals(((SftpDocumentRepository) documentRepository).sameRepositoryHash,
                               sameRepositoryHash);
   }

   @Override
   public SftpDocumentRepository subPath(Path subPath, boolean shareContext) {
      validate(subPath, "subPath", isNotAbsolute().andValue(hasNoTraversal())).orThrow();

      SftpDocumentConfig subPathConfig = SftpDocumentConfigBuilder
            .from(sftpDocumentConfig)
            .basePath(sftpDocumentConfig.basePath().resolve(subPath))
            .build();
      if (shareContext) {
         return new SftpDocumentRepository(subPathConfig, eventService, clientConfigurer, sftpSessionFactory);
      } else {
         return new SftpDocumentRepository(subPathConfig, eventService, clientConfigurer);
      }
   }

   @Override
   @CheckReturnValue
   public Optional<Document> openDocument(DocumentPath documentId,
                                          boolean overwrite,
                                          boolean append,
                                          OpenDocumentMetadata metadata) {
      validate(documentId, "documentId", isNotNull())
            .and(validate(metadata, "metadata", isNotNull()))
            .orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEvent(() -> {

         try (SessionInstance sessionInstance = new SessionInstance(sftpSessionFactory)) {
            Path sftpFile = sftpFile(documentId);
            boolean exists = sessionInstance.session().exists(sftpFile.toString());
            if (!(overwrite || append) && exists) {
               return optional();
            }

            return sessionInstance.borrowsSession(si -> optional(new DocumentBuilder()
                                                                       .documentId(documentId)
                                                                       .content(new OutputStreamDocumentContentBuilder()
                                                                                      .content(new SftpOutputStream(
                                                                                                     new WriteContext(
                                                                                                           si.session(),
                                                                                                           sftpFile,
                                                                                                           exists,
                                                                                                           overwrite,
                                                                                                           append),
                                                                                                     writeStrategyFactory()),
                                                                                               metadata
                                                                                                     .contentEncoding()
                                                                                                     .orElse(
                                                                                                           null))
                                                                                      .finalize(__ -> si.close())
                                                                                      .build())
                                                                       .chain(metadata.chainDocumentBuilder())
                                                                       .build()));
         } catch (IOException e) {
            throw new DocumentAccessException(e);
         } catch (UncheckedIOException e) {
            throw new DocumentAccessException(e);
         }

      }, d -> documentSaved(d.documentEntry(), watch));
   }

   @Override
   public Optional<Document> findDocumentById(DocumentPath documentId) {
      validate(documentId, "documentId", isNotNull()).orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEvent(() -> {
         try (SessionInstance sessionInstance = new SessionInstance(sftpSessionFactory)) {
            Path sftpFile = sftpFile(documentId);

            return documentEntry(sessionInstance.session(),
                                 sftpFile).map(entry -> sessionInstance.borrowsSession(si -> new DocumentBuilder()
                  .<DocumentBuilder>reconstitute()
                  .documentEntry(entry)
                  .content(sftpDocumentContent(si, sftpFile, entry.metadata().contentSize().orElse(null)))
                  .build()));
         } catch (UncheckedIOException e) {
            throw new DocumentAccessException(e);
         }
      }, d -> documentAccessed(d.documentEntry(), watch));
   }

   @Override
   public Stream<Document> findDocumentsBySpecification(Path basePath,
                                                        Specification<DocumentEntry> specification) {
      validate(basePath, "basePath", isNotAbsolute().andValue(hasNoTraversal()))
            .and(validate(specification, "specification", isNotNull()))
            .orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentStreamEvent(() -> {
         try (SessionInstance sessionInstance = new SessionInstance(sftpSessionFactory)) {
            return sessionInstance.borrowsSession(si -> listDocumentEntries(si.session(),
                                                                            sftpDirectory(basePath),
                                                                            directoryFilter(specification))
                  .filter(specification)
                  .map(entry -> new DocumentBuilder()
                        .<DocumentBuilder>reconstitute()
                        .documentEntry(entry)
                        .content(sftpDocumentContent(si.session(),
                                                     sftpFile(entry.documentId()),
                                                     entry.metadata().contentSize().orElse(null)))
                        .build())
                  .onClose(si::close));
         } catch (UncheckedIOException e) {
            throw new DocumentAccessException(e);
         }
      }, d -> documentAccessed(d.documentEntry(), watch));
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryById(DocumentPath documentId) {
      validate(documentId, "documentId", isNotNull()).orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEntryEvent(() -> {
         try (SessionInstance sessionInstance = new SessionInstance(sftpSessionFactory)) {
            Path sftpFile = sftpFile(documentId);

            return documentEntry(sessionInstance.session(), sftpFile);
         } catch (UncheckedIOException e) {
            throw new DocumentAccessException(e);
         }
      }, d -> documentAccessed(d, watch));
   }

   @Override
   public Stream<DocumentEntry> findDocumentEntriesBySpecification(Path basePath,
                                                                   Specification<DocumentEntry> specification) {
      validate(basePath, "basePath", isNotAbsolute().andValue(hasNoTraversal()))
            .and(validate(specification, "specification", isNotNull()))
            .orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEntryStreamEvent(() -> {
         try (SessionInstance sessionInstance = new SessionInstance(sftpSessionFactory)) {
            return sessionInstance.borrowsSession(si -> listDocumentEntries(si.session(),
                                                                            sftpDirectory(basePath),
                                                                            directoryFilter(specification))
                  .filter(specification)
                  .onClose(si::close));
         } catch (UncheckedIOException e) {
            throw new DocumentAccessException(e);
         }
      }, d -> documentAccessed(d, watch));
   }

   @Override
   @CheckReturnValue
   public Optional<DocumentEntry> saveDocument(Document document, boolean overwrite) {
      validate(document, "document", isNotNull()).orThrow();

      if (document.content() instanceof OutputStreamDocumentContent) {
         throw new IllegalArgumentException(
               "Unsupported operation for this document content implementation : " + document
                     .content()
                     .getClass()
                     .getName());
      }

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEntryEvent(() -> {

         try (SessionInstance sessionInstance = new SessionInstance(sftpSessionFactory)) {
            if (!rawSaveDocument(sessionInstance, document, overwrite)) {
               return optional();
            }
            Path sftpFile = sftpFile(document.documentId());
            return documentEntry(sessionInstance.session(), sftpFile);
         }
      }, d -> documentSaved(d, watch));
   }

   @Override
   @CheckReturnValue
   public Optional<Document> saveAndReturnDocument(Document document, boolean overwrite) {
      validate(document, "document", isNotNull()).orThrow();

      if (document.content() instanceof OutputStreamDocumentContent) {
         throw new IllegalArgumentException(
               "Unsupported operation for this document content implementation : " + document
                     .content()
                     .getClass()
                     .getName());
      }

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEvent(() -> {
         try (SessionInstance sessionInstance = new SessionInstance(sftpSessionFactory)) {
            if (!rawSaveDocument(sessionInstance, document, overwrite)) {
               return optional();
            }
            Path sftpFile = sftpFile(document.documentId());
            return documentEntry(sessionInstance.session(),
                                 sftpFile).map(entry -> sessionInstance.borrowsSession(si -> new DocumentBuilder()
                  .<DocumentBuilder>reconstitute()
                  .documentEntry(entry)
                  .content(sftpDocumentContent(si, sftpFile, entry.metadata().contentSize().orElse(null)))
                  .build()));
         } catch (UncheckedIOException e) {
            throw new DocumentAccessException(e);
         }
      }, d -> documentSaved(d.documentEntry(), watch));
   }

   private boolean rawSaveDocument(SessionInstance sessionInstance, Document document, boolean overwrite) {
      try {
         Path sftpFile = sftpFile(document.documentId());

         boolean exists = sessionInstance.session().exists(sftpFile.toString());
         if (overwrite || !exists) {
            try (SftpOutputStream outputStream = new SftpOutputStream(new WriteContext(sessionInstance.session(),
                                                                                       sftpFile,
                                                                                       exists,
                                                                                       overwrite,
                                                                                       false),
                                                                      writeStrategyFactory())) {
               try (InputStream documentInputStream = document.content().inputStreamContent()) {
                  IOUtils.copy(documentInputStream, outputStream);
               }
            }
            return true;
         }

         return false;
      } catch (IOException e) {
         throw new DocumentAccessException(e);
      } catch (UncheckedIOException e) {
         throw new DocumentAccessException(e);
      }
   }

   @Override
   @CheckReturnValue
   public Optional<DocumentEntry> deleteDocumentById(DocumentPath documentId) {
      validate(documentId, "documentId", isNotNull()).orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEntryEvent(() -> {

         try (SessionInstance sessionInstance = new SessionInstance(sftpSessionFactory)) {
            Path sftpFile = sftpFile(documentId);
            Optional<DocumentEntry> documentEntry = documentEntry(sessionInstance.session(), sftpFile);

            boolean removed =
                  documentEntry.isPresent() && sessionInstance.session().remove(sftpFile.toString());

            return documentEntry.filter(__ -> removed);
         } catch (IOException e) {
            throw new DocumentAccessException(e);
         } catch (UncheckedIOException e) {
            throw new DocumentAccessException(e);
         }
      }, d -> documentDeleted(d, watch));
   }

   @Override
   public List<DocumentEntry> deleteDocumentsBySpecification(Path basePath,
                                                             Specification<DocumentEntry> specification) {
      validate(basePath, "basePath", isNotAbsolute().andValue(hasNoTraversal()))
            .and(validate(specification, "specification", isNotNull()))
            .orThrow();

      StopWatch watch = StopWatch.createStarted();

      return handleDocumentEntriesEvent(() -> {
         try (SessionInstance sessionInstance = new SessionInstance(sftpSessionFactory)) {
            return list(listDocumentEntries(sessionInstance.session(),
                                            sftpDirectory(basePath),
                                            directoryFilter(specification))
                              .filter(specification)
                              .filter(documentEntry -> {
                                 Path sftpFile = sftpFile(documentEntry.documentId());

                                 try {
                                    return sessionInstance.session().remove(sftpFile.toString());
                                 } catch (IOException e) {
                                    throw runtimeThrow(e);
                                 }
                              }));
         } catch (UncheckedIOException e) {
            throw new DocumentAccessException(e);
         }
      }, d -> documentDeleted(d, watch));
   }

   /**
    * Checks if specified URI is compatible with this repository, independently to current repository
    * configuration
    *
    * @param uri URI
    * @param documentUri whether specified URI is a document URI or any supported URI
    *
    * @return {@code true} if specified URI is compatible with this repository
    *
    * @see #supportsUri(URI, boolean)
    */
   public static boolean isCompatibleUri(URI uri, boolean documentUri) {
      validate(uri, "uri", isNotNull()).orThrow();

      if (!(uri.isAbsolute()
            && uri.getScheme().equals(URI_SCHEME)
            && uri.getAuthority() != null
            && uri.getHost() != null
            && uri.getFragment() == null
            && uri.getQuery() == null
            && uri.getPath() != null)) {
         return false;
      }

      return !documentUri || !Paths.get(uri.getPath()).equals(Paths.get("/"));
   }

   @Override
   public boolean supportsUri(URI uri, boolean documentUri) {
      return transformerUriAdapter.supportsUri(uri, documentUri);
   }

   @Override
   public URI toUri() {
      return transformerUriAdapter.toUri();
   }

   @Override
   public URI toUri(DocumentPath documentId) {
      return transformerUriAdapter.toUri(documentId);
   }

   @Override
   public Optional<Document> findDocumentByUri(URI documentUri) {
      return transformerUriAdapter.findDocumentByUri(documentUri);
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryByUri(URI documentUri) {
      return transformerUriAdapter.findDocumentEntryByUri(documentUri);
   }

   /**
    * Closes {@link CachingSessionFactory} pool sessions if needed.
    */
   @Override
   public void close() {
      if (!closed) {
         closed = true;

         if (!sharedSession) {
            if (this.sftpSessionFactory instanceof CachingSessionFactory) {
               CachingSessionFactory<LsEntry> cachingSessionFactory =
                     (CachingSessionFactory<LsEntry>) this.sftpSessionFactory;

               cachingSessionFactory.destroy();
               checkPoolActiveSessions(cachingSessionFactory);
            }
         }
      }
   }

   /**
    * Returns low-level session caching pool.
    */
   @SuppressWarnings("unchecked")
   private static SimplePool<Session<LsEntry>> sessionSimplePool(CachingSessionFactory<LsEntry> cachingSessionFactory)
         throws NoSuchFieldException, IllegalAccessException {
      Field poolField = cachingSessionFactory.getClass().getDeclaredField("pool");
      poolField.setAccessible(true);

      return (SimplePool<Session<LsEntry>>) poolField.get(cachingSessionFactory);
   }

   /**
    * Checks for remaining active sessions in caching pool.
    */
   private static void checkPoolActiveSessions(CachingSessionFactory<LsEntry> cachingSessionFactory) {
      try {
         SimplePool<Session<LsEntry>> pool = sessionSimplePool(cachingSessionFactory);

         if (pool.getActiveCount() > 0) {
            throw new IllegalStateException(String.format(
                  "%s active sessions not released in caching pool on repository close, reasons can be : bad library usage => you MUST close (try-with-resources) EVERY DocumentRepository instances, and EVERY returned (Stream<Document|DocumentEntry> streams, or Document content's inputStream/outputStream)",
                  pool.getActiveCount()));
         }
      } catch (Exception e) {
         throw sneakyThrow(e);
      }
   }

   /**
    * Real SFTP URI adapter to be used by {@link DocumentTransformerUriAdapter}.
    * <p>
    * For security purpose, user password is never part of the URI (and it does not help to uniquely
    * identify
    * a resource).
    *
    * @see <a href="https://datatracker.ietf.org/doc/html/draft-ietf-secsh-scp-sftp-ssh-uri-04">SFTP URI
    *       RFC</a>
    */
   public class SftpUriAdapter implements UriAdapter {

      @Override
      public boolean supportsUri(URI uri, boolean documentUri) {
         validate(uri, "uri", isNotNull()).orThrow();

         if (!isCompatibleUri(uri, documentUri)) {
            return false;
         }

         if (!(sftpDocumentConfig.host().equals(uri.getHost()) && sftpDocumentConfig
               .port()
               .equals(uri.getPort() == -1 ? DEFAULT_PORT : uri.getPort()) && uriUser(uri)
                     .map(username -> sftpDocumentConfig.username().equals(username))
                     .orElse(true))) {
            return false;
         }

         if (!basePath.toString().isEmpty() && !uriPath(uri).startsWith(basePath)) {
            return false;
         }

         return !documentUri || !uriPath(uri).equals(basePath);
      }

      /**
       * Returns username from URI if any.
       *
       * @param uri URI
       *
       * @return username or empty
       */
      private Optional<String> uriUser(URI uri) {
         return nullable(uri.getUserInfo()).map(ui -> StringUtils.split(ui, ":", 2)[0]);
      }

      /**
       * Returns the base path from the specified URI.
       * Returned base path can be relative or absolute depending on URI syntax :
       * <ul>
       *    <li>{@code sftp://<host>/relative-base-path}</li>
       *    <li>{@code sftp://<host>//absolute-base-path} (one or more extra slashes)</li>
       * </ul>
       *
       * @param uri uri to parse
       *
       * @return base path from URI
       */
      private Path uriPath(URI uri) {
         return Paths.get(removeStart(uri.getPath(), "/"));
      }

      @Override
      public URI toUri() {
         try {
            return new URI(URI_SCHEME,
                           sftpDocumentConfig.username(),
                           sftpDocumentConfig.host(),
                           sftpDocumentConfig.port(),
                           "/" + basePath.toString(),
                           null,
                           null);
         } catch (URISyntaxException e) {
            throw new IllegalStateException(e);
         }
      }

      @Override
      public URI toUri(DocumentPath documentId) {
         validate(documentId, "documentId", isNotNull()).orThrow();

         try {
            return new URI(URI_SCHEME,
                           sftpDocumentConfig.username(),
                           sftpDocumentConfig.host(),
                           sftpDocumentConfig.port(),
                           "/" + basePath.resolve(documentId.value()).toString(),
                           null,
                           null);
         } catch (URISyntaxException e) {
            throw new IllegalStateException(e);
         }
      }

      @Override
      public Optional<Document> findDocumentByUri(URI documentUri) {
         validate(documentUri,
                  "documentUri",
                  UriRules
                        .hasNoTraversal()
                        .andValue(satisfies(this::supportsDocumentUri,
                                            "'%s' must be supported",
                                            validatingObject()))).orThrow();

         return findDocumentById(documentId(documentUri));
      }

      @Override
      public Optional<DocumentEntry> findDocumentEntryByUri(URI documentUri) {
         validate(documentUri,
                  "documentUri",
                  UriRules
                        .hasNoTraversal()
                        .andValue(satisfies(this::supportsDocumentUri,
                                            "'%s' must be supported",
                                            validatingObject()))).orThrow();

         return findDocumentEntryById(documentId(documentUri));
      }

      /**
       * Extracts document identifier from specified document URI.
       *
       * @param documentUri document URI
       *
       * @return document identifier
       */
      protected DocumentPath documentId(URI documentUri) {
         return DocumentPath.of(basePath.relativize(uriPath(documentUri)));
      }
   }

   private WriteStrategyFactory writeStrategyFactory() {
      return writeContext -> {
         Class<? extends WriteStrategy> writeStrategyClass = sftpDocumentConfig.writeStrategyClass();

         try {
            return writeStrategyClass.getConstructor(WriteContext.class).newInstance(writeContext);
         } catch (InstantiationException | IllegalAccessException | NoSuchMethodException |
                  InvocationTargetException e) {
            throw new IllegalStateException(String.format("Can't instantiate '%s' write strategy",
                                                          writeStrategyClass.getSimpleName()), e);
         }
      };
   }

   /**
    * Generates a hash unique for a given server storage path to identify a similar repository.
    */
   private int sameRepositoryHash(SftpDocumentConfig sftpDocumentConfig) {
      return Objects.hash(sftpDocumentConfig.host(),
                          sftpDocumentConfig.port(),
                          sftpDocumentConfig.username(),
                          sftpDocumentConfig.basePath());
   }

   private static SessionFactory<LsEntry> sftpSessionFactory(SftpDocumentConfig sftpDocumentConfig,
                                                             Consumer<DefaultSftpSessionFactory> clientConfigurer) {
      notNull(sftpDocumentConfig, "sftpDocumentConfig");

      SessionFactory<LsEntry> factory;

      DefaultSftpSessionFactory defaultFactory = new DefaultSftpSessionFactory(true);
      defaultFactory.setHost(sftpDocumentConfig.host());
      defaultFactory.setPort(sftpDocumentConfig.port());
      defaultFactory.setUser(sftpDocumentConfig.username());
      defaultFactory.setEnableDaemonThread(SESSION_DAEMON_THREAD);
      defaultFactory.setChannelConnectTimeout(sftpDocumentConfig.connectTimeout());
      defaultFactory.setTimeout((int) sftpDocumentConfig.socketTimeout().toMillis());
      if (sftpDocumentConfig.password() == null) {
         defaultFactory.setPrivateKey(new ByteArrayResource(sftpDocumentConfig.privateKey()));
         defaultFactory.setPrivateKeyPassphrase(sftpDocumentConfig.privateKeyPassphrase());
      } else {
         defaultFactory.setPassword(sftpDocumentConfig.password());
      }
      defaultFactory.setAllowUnknownKeys(sftpDocumentConfig.allowUnknownKeys());
      defaultFactory.setServerAliveInterval((int) sftpDocumentConfig.keepAliveInterval().toMillis());

      clientConfigurer.accept(defaultFactory);

      if (sftpDocumentConfig.sessionCaching()) {
         CachingSessionFactory<LsEntry> cachingSessionFactory =
               new CachingSessionFactory<>(defaultFactory, sftpDocumentConfig.sessionPoolSize());
         cachingSessionFactory.setTestSession(CACHING_SESSION_TEST_SESSION);
         cachingSessionFactory.setSessionWaitTimeout(sftpDocumentConfig.sessionPoolWaitTimeout()
                                                           .equals(Duration.ZERO)
                                                     ? Long.MAX_VALUE
                                                     : sftpDocumentConfig
                                                           .sessionPoolWaitTimeout()
                                                           .toMillis());
         factory = cachingSessionFactory;
      } else {
         factory = defaultFactory;
      }

      return factory;
   }

   private Predicate<Path> directoryFilter(Specification<DocumentEntry> specification) {
      if (specification instanceof DocumentEntrySpecification) {
         DocumentEntrySpecification documentEntrySpecification = (DocumentEntrySpecification) specification;
         return documentEntrySpecification::satisfiedBySubPath;
      } else {
         return __ -> true;
      }
   }

   /**
    * Gets document entry for backend-space file.
    *
    * @param session SFTP session
    * @param sftpFile backend-space file
    */
   private Optional<DocumentEntry> documentEntry(Session<LsEntry> session, Path sftpFile) {
      notNull(session, "session");
      notNull(sftpFile, "sftpFile");

      Path sftpFileParent = nullable(sftpFile.getParent(), () -> Paths.get(""));

      return listDocumentEntries(session, sftpFileParent, __ -> false)
            .filter(entry -> entry.documentId().value().getFileName().equals(sftpFile.getFileName()))
            .findAny();
   }

   /**
    * Lists recursively all document entries from backend.
    *
    * @param session SFTP session
    */
   private Stream<DocumentEntry> listAllDocumentEntries(Session<LsEntry> session) {
      return listDocumentEntries(session, basePath, __ -> true);
   }

   /**
    * Lists document entries from specified backend-space directory.
    *
    * @param session SFTP session
    * @param baseDirectory backend-space directory to search from
    * @param directoryFilter user-space directory filter.Use {@code __ -> false} to disable recursive
    *       search
    *
    * @return filtered document entries
    */
   private Stream<DocumentEntry> listDocumentEntries(Session<LsEntry> session,
                                                     Path baseDirectory,
                                                     Predicate<Path> directoryFilter) {
      notNull(session, "session");
      notNull(baseDirectory, "baseDirectory");
      notNull(directoryFilter, "directoryFilter");

      String sessionBaseDirectory = baseDirectory.toString().isEmpty() ? "." : baseDirectory.toString();

      try {
         if (session.exists(sessionBaseDirectory)) {
            return Stream
                  .of(session.list(sessionBaseDirectory))
                  .filter(traversalEntry().negate())
                  .flatMap(lsEntry -> {
                     Path sftpPath = baseDirectory.resolve(lsEntry.getFilename());

                     if (lsEntry.getAttrs().isDir()) {
                        if (directoryFilter.test(documentDirectory(sftpPath))) {
                           return listDocumentEntries(session, sftpPath, directoryFilter);
                        } else {
                           return Stream.empty();
                        }
                     } else if (lsEntry.getAttrs().isReg()) {
                        return Stream.of(lsEntry).map(entry -> sftpDocumentEntry(sftpPath, entry));
                     } else {
                        return Stream.empty();
                     }
                  });
         }
      } catch (IOException | UncheckedIOException e) {
         log.warn("Error while accessing '{}' directory : {}", sessionBaseDirectory, e.getMessage());
      }

      return Stream.empty();
   }

   private Predicate<LsEntry> traversalEntry() {
      return lsEntry -> {
         String filename = lsEntry.getFilename();

         return filename.equals(".") || filename.equals("..");
      };
   }

   private DocumentEntry sftpDocumentEntry(Path sftpFile, LsEntry entry) {
      return new DocumentEntryBuilder()
            .<DocumentEntryBuilder>reconstitute()
            .documentId(documentId(sftpFile))
            .metadata(sftpDocumentMetadata(sftpFile, entry))
            .build();
   }

   private DocumentMetadata sftpDocumentMetadata(Path sftpFile, LsEntry entry) {
      return new DocumentMetadataBuilder()
            .<DocumentMetadataBuilder>reconstitute()
            .documentPath(documentId(sftpFile).value())
            .contentSize(entry.getAttrs().getSize())
            .creationDate(null)
            .lastUpdateDate(Instant.ofEpochSecond(entry.getAttrs().getMTime()))
            .build();
   }

   /**
    * Generates an input content using specified session instance. SessionInstance will be closed on input
    * content closing.
    *
    * @param sessionInstance session instance
    * @param sftpFile backend-space file
    * @param contentSize optional content size
    *
    * @return input content
    */
   private InputStreamDocumentContent sftpDocumentContent(SessionInstance sessionInstance,
                                                          Path sftpFile,
                                                          Long contentSize) {
      try {
         return new InputStreamDocumentContentBuilder()
               .<InputStreamDocumentContentBuilder>reconstitute()
               .content(sessionInstance.session().readRaw(sftpFile.toString()), contentSize)
               .finalize(__ -> sessionInstance.close())
               .build();
      } catch (UncheckedIOException e) {
         throw new DocumentAccessException(e);
      } catch (IOException e) {
         throw new DocumentAccessException(e);
      }
   }

   /**
    * Generates an input content using specified session. Session won't be closed on input
    * content closing.
    *
    * @param session SFTP session
    * @param sftpFile backend-space file
    * @param contentSize optional content size
    *
    * @return input content
    */
   private InputStreamDocumentContent sftpDocumentContent(Session<LsEntry> session,
                                                          Path sftpFile,
                                                          Long contentSize) {
      try {
         return new InputStreamDocumentContentBuilder()
               .<InputStreamDocumentContentBuilder>reconstitute()
               .content(session.readRaw(sftpFile.toString()), contentSize)
               .finalize(__ -> uncheckedRunnable(session::finalizeRaw))
               .build();
      } catch (UncheckedIOException e) {
         throw new DocumentAccessException(e);
      } catch (IOException e) {
         throw new DocumentAccessException(e);
      }
   }

   /**
    * Generates real SFTP file path for specified document.
    *
    * @param documentId document id
    *
    * @return document real SFTP file path
    */
   private Path sftpFile(DocumentPath documentId) {
      return basePath.resolve(documentId.value());
   }

   /**
    * Generates real SFTP directory path for specified document directory.
    *
    * @param documentDirectory document directory
    *
    * @return document real SFTP directory path
    */
   private Path sftpDirectory(Path documentDirectory) {
      return basePath.resolve(documentDirectory);
   }

   /**
    * Returns document identifier from SFTP document file. Reverses real path to generate a
    * logical document path.
    *
    * @param sftpFile SFTP document file
    *
    * @return document identifier from SFTP file
    */
   private DocumentPath documentId(Path sftpFile) {
      return DocumentPath.of(basePath.relativize(sftpFile));
   }

   /**
    * Returns document directory from SFTP document directory. Reverses real path to generate a
    * logical document directory path.
    *
    * @param sftpDirectory SFTP document directory
    *
    * @return document directory
    */
   private Path documentDirectory(Path sftpDirectory) {
      return basePath.relativize(sftpDirectory);
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", SftpDocumentRepository.class.getSimpleName() + "[", "]")
            .add("basePath=" + basePath)
            .toString();
   }

   /** Java < 10 InputStream::transferTo implementation. */
   public static long transferTo(InputStream in, OutputStream out) throws IOException {
      Objects.requireNonNull(in, "in");
      Objects.requireNonNull(out, "out");

      final int TRANSFER_BUFFER_SIZE = MechanicalSympathy.generalBufferSize();

      long transferred = 0;
      byte[] buffer = new byte[TRANSFER_BUFFER_SIZE];
      int read;
      while ((read = in.read(buffer, 0, TRANSFER_BUFFER_SIZE)) >= 0) {
         out.write(buffer, 0, read);
         transferred += read;
      }
      return transferred;
   }

   /**
    * Manages complex SFTP session initialization, release and ownership borrowing.
    * Session should be closed by object borrowing it, after it has finished to read from it, or failed.
    * Also, if borrowing object can't be generated, session must be closed automatically.
    * <p>
    * If the same session is borrowed by multiple objects, this instance store a borrowing counter that is
    * decreased each time a borrowing object    #returnSession() returns    a session, the last object
    * returning the session will effectively close it. This is uber important that <em>all returned</em>
    * documents content input/output streams, and {@link Document}, are correctly closed using
    * try-with-resources or alike. Otherwise, sessions won't be released and session pool will be exhausting.
    */
   public class SessionInstance implements AutoCloseable {

      private final Session<LsEntry> session;

      private int borrowCount = 0;

      /**
       * Returns a new session instance
       *
       * @param sessionFactory session factory
       *
       * @throws DocumentAccessException if an error occurs while instantiating session
       * @implSpec this constructor must wrap any exception to {@link DocumentAccessException}
       */
      public SessionInstance(SessionFactory<LsEntry> sessionFactory) {
         try {
            session = sessionFactory.getSession();
         } catch (PoolItemNotAvailableException e) {
            throw checkBadLibraryUsage(e);
         } catch (Exception e) {
            throw new DocumentAccessException(e.getMessage(), e);
         }
         borrowCount += 1;
      }

      private DocumentAccessException checkBadLibraryUsage(Exception e) {
         if (SftpDocumentRepository.this.sftpSessionFactory instanceof CachingSessionFactory) {
            CachingSessionFactory<LsEntry> cachingSessionFactory =
                  (CachingSessionFactory<LsEntry>) SftpDocumentRepository.this.sftpSessionFactory;

            try {
               SimplePool<Session<LsEntry>> sessionSimplePool = sessionSimplePool(cachingSessionFactory);

               if (sessionSimplePool.getActiveCount() == sessionSimplePool.getPoolSize()) {
                  return new DocumentAccessException(
                        "Timed out while waiting to acquire a pool entry, reasons can be : too much concurrent SFTP operations on the same repository => increase pool size in configuration | bad library usage => you MUST close (try-with-resources) EVERY DocumentRepository instances, and EVERY returned (Stream<Document|DocumentEntry> streams, or Document content's inputStream/outputStream)",
                        e);
               }

            } catch (Exception e2) {
               throw sneakyThrow(e2);
            }
         }

         return new DocumentAccessException(e.getMessage(), e);
      }

      /**
       * Simply returns current session instance.
       *
       * @return current session instance
       */
      public Session<LsEntry> session() {
         return session;
      }

      public <T> T borrowsSession(BorrowingSessionFunction<T> function) {
         int previousBorrowCount = borrowCount;

         borrowCount += 1;
         try {
            return function.apply(this);
         } catch (IOException e) {
            borrowCount = previousBorrowCount;
            throw sneakyThrow(e); // FIXME throw e (IOException) ? why this sneakythrow if the only reason for BorrowingSessionFunction is to throw an IOException ?
         } catch (Exception e) {
            borrowCount = previousBorrowCount;
            throw e;
         }
      }

      @Override
      public void close() {
         if (borrowCount <= 0) {
            throw new IllegalStateException("Trying to close a session instance with borrowCount == "
                                            + borrowCount
                                            + " <= 0");
         }

         borrowCount -= 1;
         if (borrowCount == 0) {
            session.close();
         }
      }
   }

   @FunctionalInterface
   public interface BorrowingSessionFunction<T> {
      T apply(SessionInstance session) throws IOException;
   }

}
