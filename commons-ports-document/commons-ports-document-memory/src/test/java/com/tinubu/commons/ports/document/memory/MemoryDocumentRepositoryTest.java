/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.memory;

import static com.tinubu.commons.ddd2.criterion.Criterion.Flag.IGNORE_CASE;
import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.parseMimeType;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.ByteArrayInputStream;
import java.io.StringReader;
import java.nio.file.Path;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.criterion.Criterion.CriterionBuilder;
import com.tinubu.commons.ddd2.criterion.Criterion.Flags;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentEntryCriteria.DocumentEntryCriteriaBuilder;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.testsuite.CommonDocumentRepositoryTest;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ZipArchiver;

public class MemoryDocumentRepositoryTest extends CommonDocumentRepositoryTest {

   private MemoryDocumentRepository documentRepository;

   @BeforeEach
   public void configureDocumentRepository() {
      this.documentRepository = new MemoryDocumentRepository(false);
   }

   @AfterEach
   public void closeDocumentRepository() {
      this.documentRepository.close();
   }

   @Override
   protected DocumentRepository documentRepository() {
      return documentRepository;
   }

   @Override
   protected Document zipTransformer(DocumentPath zipPath, List<Document> documents) {
      return new ZipArchiver(zipPath).compress(documents);
   }

   @Test
   public void testFindDocumentByIdWhenCaseSensitive() {
      this.documentRepository = new MemoryDocumentRepository(false);

      Path documentPath = path("path/pathfile1.pdf");

      try {
         createDocuments(documentPath);

         assertThat(documentRepository().findDocumentById(DocumentPath.of("path/PATHFILE1.PDF"))).isEmpty();
         assertThat(documentRepository().findDocumentById(DocumentPath.of("path/pathfile1.pdf"))).hasValueSatisfying(
               document -> {
                  assertThat(document)
                        .usingComparator(contentAgnosticDocumentComparator())
                        .isEqualTo(new DocumentBuilder()
                                         .<DocumentBuilder>reconstitute()
                                         .documentId(DocumentPath.of("path/pathfile1.pdf"))
                                         .streamContent(new ByteArrayInputStream("path/pathfile1.pdf".getBytes(
                                                              UTF_8)),
                                                        document.metadata().contentEncoding().orElse(null),
                                                        isSupportingContentLength() ? 18L : null)
                                         .metadata(new DocumentMetadataBuilder()
                                                         .<DocumentMetadataBuilder>reconstitute()
                                                         .documentPath(documentPath)
                                                         .contentSize(isSupportingContentLength()
                                                                      ? 18L
                                                                      : null)
                                                         .contentType(parseMimeType("application/pdf"), UTF_8)
                                                         .chain(synchronizeExpectedMetadata(document.metadata()))
                                                         .build())
                                         .build());
               });

      } finally {
         deleteDocuments(documentPath);
      }
   }

   @Test
   public void testFindDocumentByIdWhenCaseInsensitive() {
      this.documentRepository = new MemoryDocumentRepository(true);

      Path documentPath = path("path/pathfile1.pdf");

      try {
         createDocuments(documentPath);

         assertThat(documentRepository().findDocumentById(DocumentPath.of("path/PATHFILE1.PDF"))).hasValueSatisfying(
               document -> {
                  assertThat(document)
                        .usingComparator(contentAgnosticDocumentComparator())
                        .isEqualTo(new DocumentBuilder()
                                         .<DocumentBuilder>reconstitute()
                                         .documentId(DocumentPath.of("path/PATHFILE1.PDF"))
                                         .streamContent(new ByteArrayInputStream("path/pathfile1.pdf".getBytes(
                                                              UTF_8)),
                                                        document.metadata().contentEncoding().orElse(null),
                                                        isSupportingContentLength() ? 18L : null)
                                         .metadata(new DocumentMetadataBuilder()
                                                         .<DocumentMetadataBuilder>reconstitute()
                                                         .documentPath(documentPath)
                                                         .contentSize(isSupportingContentLength()
                                                                      ? 18L
                                                                      : null)
                                                         .contentType(parseMimeType("application/pdf"), UTF_8)
                                                         .chain(synchronizeExpectedMetadata(document.metadata()))
                                                         .build())
                                         .build());
               });
         assertThat(documentRepository().findDocumentById(DocumentPath.of("path/pathfile1.pdf"))).hasValueSatisfying(
               document -> {
                  assertThat(document)
                        .usingComparator(contentAgnosticDocumentComparator())
                        .isEqualTo(new DocumentBuilder()
                                         .<DocumentBuilder>reconstitute()
                                         .documentId(DocumentPath.of("path/pathfile1.pdf"))
                                         .streamContent(new ByteArrayInputStream("path/pathfile1.pdf".getBytes(
                                                              UTF_8)),
                                                        document.metadata().contentEncoding().orElse(null),
                                                        isSupportingContentLength() ? 18L : null)
                                         .metadata(new DocumentMetadataBuilder()
                                                         .<DocumentMetadataBuilder>reconstitute()
                                                         .documentPath(documentPath)
                                                         .contentSize(isSupportingContentLength()
                                                                      ? 18L
                                                                      : null)
                                                         .contentType(parseMimeType("application/pdf"), UTF_8)
                                                         .chain(synchronizeExpectedMetadata(document.metadata()))
                                                         .build())
                                         .build());
               });

      } finally {
         deleteDocuments(documentPath);
      }
   }

   @Test
   public void testFindDocumentBySpecificationWhenCaseSensitive() {
      this.documentRepository = new MemoryDocumentRepository(false);

      String[] createDocuments = new String[] {
            "path/pathfile1.pdf", "path/pathfile2.pdf" };

      try {
         createDocuments(createDocuments);

         List<DocumentEntry> documentEntries = documentRepository()
               .findDocumentEntriesBySpecification(new DocumentEntryCriteriaBuilder()
                                                         .documentPath(CriterionBuilder.in(path(
                                                                                                 "path/PATHFILE1.PDF"),
                                                                                           path("path/pathfile2.pdf")))
                                                         .build())
               .collect(toList());

         assertThat(documentEntries)
               .extracting(entry -> entry.documentId().stringValue())
               .containsExactlyInAnyOrder("path/pathfile2.pdf");
      } finally {
         deleteDocuments(createDocuments);
      }
   }

   @Test
   public void testFindDocumentBySpecificationWhenCaseInSensitive() {
      this.documentRepository = new MemoryDocumentRepository(true);

      String[] createDocuments = new String[] {
            "path/pathfile1.pdf", "path/pathfile2.pdf" };

      try {
         createDocuments(createDocuments);

         List<DocumentEntry> documentEntries = documentRepository()
               .findDocumentEntriesBySpecification(new DocumentEntryCriteriaBuilder()
                                                         .documentPath(CriterionBuilder.match(Flags.of(
                                                               IGNORE_CASE), path("PATH/*.PDF")))
                                                         .build())
               .collect(toList());

         assertThat(documentEntries)
               .extracting(entry -> entry.documentId().stringValue())
               .containsExactlyInAnyOrder("path/pathfile1.pdf", "path/pathfile2.pdf");
      } finally {
         deleteDocuments(createDocuments);
      }
   }

   @Test
   public void testSaveDocumentWhenStreamClosed() {
      DocumentPath test = DocumentPath.of("test");

      try (StringReader content = new StringReader("content")) {
         Document document = new DocumentBuilder().documentId(test).streamContent(content, UTF_8).build();
         assertThat(documentRepository().saveDocument(document, false)).isPresent();
      }

      assertThat(documentRepository().findDocumentById(test))
            .isPresent()
            .hasValueSatisfying(d -> assertThat(d.content().stringContent()).isEqualTo("content"));
   }

   @Test
   public void testSaveDocumentWhenMultipleReads() {
      DocumentPath test = DocumentPath.of("test");

      try (StringReader content = new StringReader("content")) {
         Document document = new DocumentBuilder().documentId(test).streamContent(content, UTF_8).build();
         assertThat(documentRepository().saveDocument(document, false)).isPresent();
      }

      assertThat(documentRepository().findDocumentById(test))
            .isPresent()
            .hasValueSatisfying(d -> assertThat(d.content().stringContent()).isEqualTo("content"));
      assertThat(documentRepository().findDocumentById(test))
            .isPresent()
            .hasValueSatisfying(d -> assertThat(d.content().stringContent()).isEqualTo("content"));
   }

   @Nested
   public class UriAdapter {

      @Test
      public void testToDocumentUriWhenNominal() {
         assertThat(documentRepository().toUri(DocumentPath.of("file.txt"))).isEqualTo(uri("memory:/file.txt"));
         assertThat(documentRepository().toUri(DocumentPath.of("path/file.txt"))).isEqualTo(uri(
               "memory:/path/file.txt"));
      }

      @Test
      public void testSupportsDocumentUriWhenNominal() {
         assertThat(documentRepository().supportsUri(uri("memory:/path/test.txt"), true)).isTrue();
      }

      @Test
      public void testSupportsDocumentUriWhenRepositoryUri() {
         assertThat(documentRepository().supportsUri(uri("memory:/"), true)).isFalse();
      }

      @Test
      public void testSupportsDocumentRepositoryUriWhenAlternativeForms() {
         assertThat(documentRepository().supportsUri(uri("memory:///path/test.txt"), true)).isTrue();
         assertThat(documentRepository().supportsUri(uri("memory:path/test.txt"), true)).isFalse();
         assertThat(documentRepository().supportsUri(uri("memory://host/path/test.txt"), true)).isFalse();
         assertThat(documentRepository().supportsUri(uri("memory:/path/test.txt#fragment"), true)).isFalse();
         assertThat(documentRepository().supportsUri(uri("memory:/path/test.txt?query"), true)).isFalse();
      }

      @Test
      public void testToRepositoryUriWhenNominal() {
         assertThat(documentRepository().toUri()).isEqualTo(uri("memory:/"));
      }

      @Test
      public void testSupportsRepositoryUriWhenNominal() {
         assertThat(documentRepository().supportsUri(uri("memory:/"), false)).isTrue();
      }

      @Test
      public void testSupportsRepositoryUriWhenDocumentUri() {
         assertThat(documentRepository().supportsUri(uri("memory:/path/test.txt"), false)).isTrue();
      }

   }

}