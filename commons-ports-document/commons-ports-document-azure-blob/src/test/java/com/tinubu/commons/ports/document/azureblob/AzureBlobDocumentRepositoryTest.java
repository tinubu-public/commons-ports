/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.azureblob;

import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_PDF;
import static com.tinubu.commons.lang.util.CollectionUtils.entry;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.ports.document.azureblob.AzureBlobDocumentRepository.BlobType.APPEND;
import static com.tinubu.commons.ports.document.azureblob.AzureBlobDocumentRepository.BlobType.BLOCK;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.UnaryOperator;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.output.Slf4jLogConsumer;
import org.testcontainers.images.PullPolicy;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import com.azure.storage.blob.BlobContainerClient;
import com.azure.storage.blob.BlobServiceClient;
import com.azure.storage.blob.BlobServiceClientBuilder;
import com.azure.storage.blob.models.BlobType;
import com.azure.storage.common.StorageSharedKeyCredential;
import com.tinubu.commons.ports.document.azureblob.AzureBlobDocumentConfig.AzureAuthentication;
import com.tinubu.commons.ports.document.azureblob.AzureBlobDocumentConfig.AzureBlobDocumentConfigBuilder;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentMetadata;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata.OpenDocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.testsuite.CommonDocumentRepositoryTest;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ZipArchiver;

@Testcontainers(disabledWithoutDocker = true)
public class AzureBlobDocumentRepositoryTest {
   private static final Logger log = LoggerFactory.getLogger(AzureBlobDocumentRepositoryTest.class);

   public static final DockerImageName AZURITE_DOCKER_IMAGE =
         DockerImageName.parse("mcr.microsoft.com/azure-storage/azurite").withTag("latest");
   private static final String ACCOUNT_NAME = "devstoreaccount1";
   private static final String ACCOUNT_KEY =
         "Eby8vdM02xNOcqFlqUwJPLlmEtlCDXJ1OUzFT50uSRZ6IFsuFq2UVErCz4I6tq/K1SZFPTOtr/KBHBeksoGMGw==";
   public static final String CONTAINER_NAME = "test-container";

   private AzureBlobDocumentRepository documentRepository;

   @Container
   private static final GenericContainer<?> azurite = new GenericContainer<>(AZURITE_DOCKER_IMAGE)
         .withImagePullPolicy(PullPolicy.alwaysPull())
         .withExposedPorts(10000)
         .withLogConsumer(new Slf4jLogConsumer(log));

   @AfterAll
   public static void afterAll() {
      azurite.close();
   }

   public abstract class AbstractAzureDocumentRepositoryTest extends CommonDocumentRepositoryTest {

      @BeforeEach
      public void configureDocumentRepository() {
         assertThat(azurite.isRunning()).isTrue();

         AzureBlobDocumentRepositoryTest.this.documentRepository =
               new AzureBlobDocumentRepository(azureBlobConfig().build());
      }

      @AfterEach
      public void closeDocumentRepository() {
         AzureBlobDocumentRepositoryTest.this.documentRepository.close();
      }

      @Override
      protected UnaryOperator<DocumentMetadataBuilder> synchronizeExpectedMetadata(DocumentMetadata actual) {
         return builder -> builder
               .chain(super.synchronizeExpectedMetadata(actual))
               .attributes(actual.attributes());
      }

      protected AzureBlobDocumentConfigBuilder azureBlobConfig() {
         return new AzureBlobDocumentConfigBuilder()
               .endpoint(azureBlobEndpoint("http",
                                           azurite.getHost(),
                                           azurite.getFirstMappedPort(),
                                           ACCOUNT_NAME).toString())
               .authentication(AzureAuthentication.of(ACCOUNT_NAME, ACCOUNT_KEY))
               .containerName(CONTAINER_NAME)
               .createIfMissingContainer(true)
               .defaultBlobType(BLOCK);
      }

      private StorageSharedKeyCredential storageCredential(AzureBlobDocumentConfig azureBlobDocumentConfig) {
         return new StorageSharedKeyCredential(azureBlobDocumentConfig.authentication().storageAccountName(),
                                               azureBlobDocumentConfig.authentication().storageAccountKey());
      }

      private BlobServiceClient blobServiceClient(AzureBlobDocumentConfig azureBlobDocumentConfig) {
         BlobServiceClientBuilder blobServiceClientBuilder = new BlobServiceClientBuilder();

         if (!StringUtils.isBlank(azureBlobDocumentConfig.connectionString())) {
            blobServiceClientBuilder.connectionString(azureBlobDocumentConfig.connectionString());
         } else {
            blobServiceClientBuilder.endpoint(azureBlobDocumentConfig.endpoint());
            blobServiceClientBuilder.credential(storageCredential(azureBlobDocumentConfig)).buildClient();
         }

         return blobServiceClientBuilder.buildClient();
      }

      protected BlobContainerClient blobContainerClient(AzureBlobDocumentConfig azureBlobDocumentConfig) {
         String containerName = azureBlobDocumentConfig.containerName();
         BlobServiceClient blobServiceClient = blobServiceClient(azureBlobDocumentConfig);
         BlobContainerClient blobContainerClient = blobServiceClient.getBlobContainerClient(containerName);

         if (!blobContainerClient.exists()) {
            if (azureBlobDocumentConfig.createIfMissingContainer()) {
               blobContainerClient.create();
               log.debug("Created missing '{}' container", containerName);
            } else {
               throw new DocumentAccessException(String.format("Unknown '%s' container", containerName));
            }
         }

         return blobContainerClient;
      }

      @Override
      protected DocumentRepository documentRepository() {
         return documentRepository;
      }

      @Override
      protected Document zipTransformer(DocumentPath zipPath, List<Document> documents) {
         return new ZipArchiver(zipPath).compress(documents);
      }

      @Override
      protected boolean isUpdatingCreationDateOnOverwrite() {
         return true;
      }

      @Override
      protected boolean isSupportingRepositoryUri() {
         return false;
      }

      @Override
      protected boolean isSupportingDocumentUri() {
         return false;
      }

      @Override
      protected long chunkSize() {
         return 2L * 1024 * 1024;
      }

   }

   @Nested
   public class WhenBlockBlob extends AbstractAzureDocumentRepositoryTest {
      @Override
      protected AzureBlobDocumentConfigBuilder azureBlobConfig() {
         return super.azureBlobConfig().defaultBlobType(BLOCK);
      }

      @Test
      public void testOpenDocumentWhenForceBlobTypeWithAttribute() {
         DocumentPath blockDocument = DocumentPath.of("block");
         DocumentPath appendDocument = DocumentPath.of("append");
         DocumentPath unknownDocument = DocumentPath.of("unknown");

         deleteDocuments(blockDocument, appendDocument, unknownDocument);

         try {
            documentRepository()
                  .openDocument(blockDocument,
                                false,
                                false,
                                new OpenDocumentMetadataBuilder()
                                      .attributes(map(entry("azure.blob.type", "BloCk")))
                                      .build())
                  .ifPresent(document -> document.content().close());

            assertThat(blobContainerClient(azureBlobConfig().build())
                             .getBlobClient(blockDocument.stringValue())
                             .getProperties()
                             .getBlobType()).isEqualTo(BlobType.BLOCK_BLOB);

            documentRepository()
                  .openDocument(appendDocument,
                                false,
                                false,
                                new OpenDocumentMetadataBuilder()
                                      .attributes(map(entry("azure.blob.type", "ApPeNd")))
                                      .build())
                  .ifPresent(document -> document.content().close());

            assertThat(blobContainerClient(azureBlobConfig().build())
                             .getBlobClient(appendDocument.stringValue())
                             .getProperties()
                             .getBlobType()).isEqualTo(BlobType.APPEND_BLOB);

            documentRepository()
                  .openDocument(unknownDocument,
                                false,
                                false,
                                new OpenDocumentMetadataBuilder()
                                      .attributes(map(entry("azure.blob.type", "unknown")))
                                      .build())
                  .ifPresent(document -> document.content().close());

            assertThat(blobContainerClient(azureBlobConfig().build())
                             .getBlobClient(unknownDocument.stringValue())
                             .getProperties()
                             .getBlobType()).isEqualTo(BlobType.BLOCK_BLOB);

         } finally {
            deleteDocuments(blockDocument, appendDocument, unknownDocument);
         }

      }

      @Test
      public void testSaveDocumentWhenForceBlobTypeWithAttribute() {
         DocumentPath blockDocument = DocumentPath.of("block");
         DocumentPath appendDocument = DocumentPath.of("append");
         DocumentPath unknownDocument = DocumentPath.of("unknown");

         deleteDocuments(blockDocument, appendDocument, unknownDocument);

         try {
            assertThat(documentRepository().saveDocument(new DocumentBuilder()
                                                               .documentId(blockDocument)
                                                               .streamContent("content", UTF_8)
                                                               .attributes(map(entry("azure.blob.type",
                                                                                     "BloCk")))
                                                               .build(), true)).isPresent();

            assertThat(blobContainerClient(azureBlobConfig().build())
                             .getBlobClient(blockDocument.stringValue())
                             .getProperties()
                             .getBlobType()).isEqualTo(BlobType.BLOCK_BLOB);

            assertThat(documentRepository().saveDocument(new DocumentBuilder()
                                                               .documentId(appendDocument)
                                                               .streamContent("content", UTF_8)
                                                               .attributes(map(entry("azure.blob.type",
                                                                                     "ApPeNd")))
                                                               .build(), true)).isPresent();

            assertThat(blobContainerClient(azureBlobConfig().build())
                             .getBlobClient(appendDocument.stringValue())
                             .getProperties()
                             .getBlobType()).isEqualTo(BlobType.APPEND_BLOB);

            assertThat(documentRepository().saveDocument(new DocumentBuilder()
                                                               .documentId(appendDocument)
                                                               .streamContent("content", UTF_8)
                                                               .attributes(map(entry("azure.blob.type",
                                                                                     "unknown")))
                                                               .build(), true)).isPresent();

            assertThat(blobContainerClient(azureBlobConfig().build())
                             .getBlobClient(appendDocument.stringValue())
                             .getProperties()
                             .getBlobType()).isEqualTo(BlobType.BLOCK_BLOB);

         } finally {
            deleteDocuments(blockDocument, appendDocument, unknownDocument);
         }

      }

      /**
       * Checks append mode for block blob types.
       */
      @Test
      public void testOpenDocumentWhenAppendInBlockMode() {
         Path documentPath = path("path/pathfile1.pdf");
         DocumentPath testDocument = DocumentPath.of(documentPath);

         try {
            OpenDocumentMetadata metadata =
                  new OpenDocumentMetadataBuilder().contentType(APPLICATION_PDF).build();

            createDocument(testDocument);

            assertThatIllegalArgumentException()
                  .isThrownBy(() -> documentRepository().openDocument(testDocument, false, true, metadata))
                  .withMessage("Append mode not supported for 'BLOCK' blobs");
            assertThatIllegalArgumentException()
                  .isThrownBy(() -> documentRepository().openDocument(testDocument, true, true, metadata))
                  .withMessage("Append mode not supported for 'BLOCK' blobs");

         } finally {
            deleteDocuments(documentPath);
         }
      }

   }

   @Nested
   public class WhenAppendBlob extends AbstractAzureDocumentRepositoryTest {
      @Override
      protected AzureBlobDocumentConfigBuilder azureBlobConfig() {
         return super.azureBlobConfig().defaultBlobType(APPEND);
      }

      @Test
      public void testOpenDocumentWhenForceBlobTypeWithAttribute() {
         DocumentPath blockDocument = DocumentPath.of("block");
         DocumentPath appendDocument = DocumentPath.of("append");
         DocumentPath unknownDocument = DocumentPath.of("unknown");

         deleteDocuments(blockDocument, appendDocument, unknownDocument);

         try {
            documentRepository()
                  .openDocument(blockDocument,
                                false,
                                false,
                                new OpenDocumentMetadataBuilder()
                                      .attributes(map(entry("azure.blob.type", "BloCk")))
                                      .build())
                  .ifPresent(document -> document.content().close());

            assertThat(blobContainerClient(azureBlobConfig().build())
                             .getBlobClient(blockDocument.stringValue())
                             .getProperties()
                             .getBlobType()).isEqualTo(BlobType.BLOCK_BLOB);

            documentRepository()
                  .openDocument(appendDocument,
                                false,
                                false,
                                new OpenDocumentMetadataBuilder()
                                      .attributes(map(entry("azure.blob.type", "ApPeNd")))
                                      .build())
                  .ifPresent(document -> document.content().close());

            assertThat(blobContainerClient(azureBlobConfig().build())
                             .getBlobClient(appendDocument.stringValue())
                             .getProperties()
                             .getBlobType()).isEqualTo(BlobType.APPEND_BLOB);

            documentRepository()
                  .openDocument(unknownDocument,
                                false,
                                false,
                                new OpenDocumentMetadataBuilder()
                                      .attributes(map(entry("azure.blob.type", "unknown")))
                                      .build())
                  .ifPresent(document -> document.content().close());

            assertThat(blobContainerClient(azureBlobConfig().build())
                             .getBlobClient(unknownDocument.stringValue())
                             .getProperties()
                             .getBlobType()).isEqualTo(BlobType.APPEND_BLOB);

         } finally {
            deleteDocuments(blockDocument, appendDocument, unknownDocument);
         }

      }

      @Test
      public void testSaveDocumentWhenForceBlobTypeWithAttribute() {
         DocumentPath blockDocument = DocumentPath.of("block");
         DocumentPath appendDocument = DocumentPath.of("append");
         DocumentPath unknownDocument = DocumentPath.of("unknown");

         deleteDocuments(blockDocument, appendDocument, unknownDocument);

         try {
            assertThat(documentRepository().saveDocument(new DocumentBuilder()
                                                               .documentId(blockDocument)
                                                               .streamContent("content", UTF_8)
                                                               .attributes(map(entry("azure.blob.type",
                                                                                     "BloCk")))
                                                               .build(), true)).isPresent();

            assertThat(blobContainerClient(azureBlobConfig().build())
                             .getBlobClient(blockDocument.stringValue())
                             .getProperties()
                             .getBlobType()).isEqualTo(BlobType.BLOCK_BLOB);

            assertThat(documentRepository().saveDocument(new DocumentBuilder()
                                                               .documentId(appendDocument)
                                                               .streamContent("content", UTF_8)
                                                               .attributes(map(entry("azure.blob.type",
                                                                                     "ApPeNd")))
                                                               .build(), true)).isPresent();

            assertThat(blobContainerClient(azureBlobConfig().build())
                             .getBlobClient(appendDocument.stringValue())
                             .getProperties()
                             .getBlobType()).isEqualTo(BlobType.APPEND_BLOB);

            assertThat(documentRepository().saveDocument(new DocumentBuilder()
                                                               .documentId(appendDocument)
                                                               .streamContent("content", UTF_8)
                                                               .attributes(map(entry("azure.blob.type",
                                                                                     "unknown")))
                                                               .build(), true)).isPresent();

            assertThat(blobContainerClient(azureBlobConfig().build())
                             .getBlobClient(appendDocument.stringValue())
                             .getProperties()
                             .getBlobType()).isEqualTo(BlobType.APPEND_BLOB);

         } finally {
            deleteDocuments(blockDocument, appendDocument, unknownDocument);
         }

      }

   }

   @Nested
   public class WhenNominalContainerBasePath extends AbstractAzureDocumentRepositoryTest {
      @Override
      protected AzureBlobDocumentConfigBuilder azureBlobConfig() {
         return super.azureBlobConfig().containerBasePath(Paths.get("basePath"));
      }
   }

   @Nested
   public class WhenEmptyContainerBasePath extends AbstractAzureDocumentRepositoryTest {
      @Override
      protected AzureBlobDocumentConfigBuilder azureBlobConfig() {
         return super.azureBlobConfig().containerBasePath(Paths.get(""));
      }
   }

   @Nested
   public class WhenSlashTerminatedContainerBasePath extends AbstractAzureDocumentRepositoryTest {
      @Override
      protected AzureBlobDocumentConfigBuilder azureBlobConfig() {
         return super.azureBlobConfig().containerBasePath(Paths.get("basePath/"));
      }
   }

   @Nested
   public class WhenContainerBasePathAndSubPath extends AbstractAzureDocumentRepositoryTest {
      @Override
      protected AzureBlobDocumentConfigBuilder azureBlobConfig() {
         return super.azureBlobConfig().containerBasePath(Paths.get("basePath/subBasePath"));
      }
   }

   private static URI azureBlobEndpoint(String protocol, String host, int port, String accountName) {
      notBlank(protocol, "protocol");
      notBlank(host, "host");
      notBlank(accountName, "accountName");

      return uri(protocol, host, port).resolve(accountName);
   }

   private static URI uri(String protocol, String host, int port) {
      try {
         return new URI(protocol, null, host, port, "/", null, null);
      } catch (URISyntaxException e) {
         throw new IllegalStateException(e);
      }
   }
}
