/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.resilience4j;

import static org.mockito.Mockito.spy;

import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;

import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.repository.TestDocumentRepository;
import com.tinubu.commons.ports.document.domain.testsuite.CommonDocumentRepositoryTest;
import com.tinubu.commons.ports.document.resilience4j.Resilience4jDocumentConfig.Resilience4jDocumentConfigBuilder;
import com.tinubu.commons.ports.document.transformer.transformers.zip.ZipArchiver;
import com.tinubu.commons.ports.resilience4j.Resilience4jConfig;
import com.tinubu.commons.ports.resilience4j.Resilience4jConfig.Resilience4jConfigBuilder;
import com.tinubu.commons.ports.resilience4j.event.Resilience4jEvent;

public abstract class CommonResilience4jDocumentRepositoryTest extends CommonDocumentRepositoryTest {

   private final boolean eagerLoadDocument;

   private TestDocumentRepository realDocumentRepository;
   private DocumentRepository documentRepository;

   public CommonResilience4jDocumentRepositoryTest(boolean eagerLoadDocument) {
      this.eagerLoadDocument = eagerLoadDocument;
   }

   public CommonResilience4jDocumentRepositoryTest() {
      this(false);
   }

   protected TestDocumentRepository realDocumentRepository() {
      return realDocumentRepository;
   }

   @BeforeEach
   public void configureDocumentRepository() {
      this.realDocumentRepository = spy(new TestDocumentRepository());
      this.documentRepository = newResilience4jDocumentRepository(realDocumentRepository());

      this.documentRepository.registerEventLoggingListener(Resilience4jEvent.class);
   }

   @AfterEach
   public void closeDocumentRepository() {
      this.documentRepository.close();
   }

   @Override
   protected DocumentRepository documentRepository() {
      return documentRepository;
   }

   @Override
   protected Document zipTransformer(DocumentPath zipPath, List<Document> documents) {
      return new ZipArchiver(zipPath).compress(documents);
   }

   protected Resilience4jDocumentRepository newResilience4jDocumentRepository(DocumentRepository realDocumentRepository) {
      return new Resilience4jDocumentRepository(new Resilience4jDocumentConfigBuilder()
                                                      .resilience(resilience4jConfig())
                                                      .build(),
                                                realDocumentRepository,
                                                "test",
                                                eagerLoadDocument);
   }

   protected Resilience4jConfig resilience4jConfig() {
      return new Resilience4jConfigBuilder().build();
   }

   @Override
   @Disabled("FIXME openDocument in append mode not working")
   public void testOpenDocumentWhenOverwriteAndAppend() { }

   @Override
   @Disabled("FIXME openDocument in append mode not working")
   public void testOpenDocumentWhenNotOverwriteAndAppend() { }

   @Override
   @Disabled("DocumentTransformerUriAdapter not available in domain")
   public void testFindDocumentByUriWhenJarTransformerUriAdapter() { }

   @Override
   @Disabled("DocumentTransformerUriAdapter not available in domain")
   public void testFindDocumentByUriWhenJarTransformerUriAdapterWithBadUrl() { }

   @Override
   @Disabled("DocumentTransformerUriAdapter not available in domain")
   public void testFindDocumentByUriWhenJarTransformerUriAdapterWithEarUrl() { }

   @Override
   @Disabled("DocumentTransformerUriAdapter not available in domain")
   public void testFindDocumentByUriWhenJarTransformerUriAdapterWithWarUrl() { }

   @Override
   @Disabled("DocumentTransformerUriAdapter not available in domain")
   public void testFindDocumentByUriWhenNullParameter() { }

   @Override
   @Disabled("DocumentTransformerUriAdapter not available in domain")
   public void testFindDocumentByUriWhenUnsupportedUri() { }

   @Override
   @Disabled("DocumentTransformerUriAdapter not available in domain")
   public void testFindDocumentByUriWhenZipTransformerUriAdapter() { }
}
