/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.resilience4j;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.ClassRules.isInstanceOf;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.lang.util.CheckedSupplier.checkedSupplier;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.ExceptionUtils.smartFinalize;
import static com.tinubu.commons.lang.util.StreamUtils.stream;

import java.net.URI;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.stream.Stream;

import com.tinubu.commons.ddd2.domain.event.DomainEvent;
import com.tinubu.commons.ddd2.domain.event.DomainEventListener;
import com.tinubu.commons.ddd2.domain.repository.Repository;
import com.tinubu.commons.ddd2.domain.specification.Specification;
import com.tinubu.commons.lang.util.CheckedRunnable;
import com.tinubu.commons.lang.util.CheckedSupplier;
import com.tinubu.commons.lang.validation.CheckReturnValue;
import com.tinubu.commons.ports.document.domain.AbstractDocumentRepository;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata;
import com.tinubu.commons.ports.document.domain.OutputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.capability.RepositoryCapability;
import com.tinubu.commons.ports.document.domain.metrology.DocumentRepositoryMetrology;
import com.tinubu.commons.ports.document.domain.processor.DocumentProcessor;
import com.tinubu.commons.ports.document.domain.processor.common.DocumentRenamer;
import com.tinubu.commons.ports.resilience4j.Resilience4jDecorator;

/**
 * Resilience4j wrapper for any delegated document repository.
 * <p>
 * The following resilience patterns are supported :
 * <ul>
 *    <li>Bulkhead (thread-based)</li>
 *    <li>Time limiter (only if Bulkhead is enabled)</li>
 *    <li>Rate limiter</li>
 *    <!--<li>Circuit-breaker</li>-->
 *    <li>Retry</li>
 * </ul>
 * <p>
 * Patterns encapsulation is Retry(Circuit-breaker(Rate limiter(Time limiter(Bulkhead)))).
 * <p>
 * Resilience patterns only apply to encapsulated repository operation, hence, {@link Stream stream} results,
 * or returned document {@link InputStreamDocumentContent content stream}, are out-of-scope of the resilience.
 * You can either :
 * <ul>
 *    <li>Force stream results and stream content loading inside the wrapper, using {@code eagerLoadDocument} option</li>
 *    <li>Manage yourself the resiliency when effectively consuming/or producing streams in user-code, for example using {@link #resilientCall(CheckedSupplier)} or {@link #resilientCall(CheckedRunnable)}</li>
 * </ul>
 * {@link #openDocument(DocumentPath, boolean, boolean)}-like operations are not concerned by
 * {@code eagerLoadDocument} option as it has no sense, so you'll have to manage the resiliency in user-code when pushing content to document's {@link OutputStreamDocumentContent}.
 *
 * <p>
 * This repository has the following specific behaviors :
 * <ul>
 *    <li>{@link #sameRepositoryAs(Repository)} compares underlying delegate instead of itself</li>
 *    <li>Encapsulated delegate repository is closed on {@link #close()}</li>
 * </ul>
 */
public class Resilience4jDocumentRepository implements DocumentRepository {

   private final Resilience4jDocumentConfig config;
   private final DocumentRepository delegate;
   private final String delegateName;
   private final boolean eagerLoadDocument;

   private final Resilience4jDecorator resilience4jDecorator;

   /**
    * Creates a Resilience4j enabled repository wrapper.
    *
    * @param config resilience configuration
    * @param delegate delegated real repository
    * @param delegateName unique name for the delegated repository to identify resilience patterns
    *       against it
    * @param eagerLoadDocument whether to eagerly load {@link Stream stream} results and
    *       {@link InputStreamDocumentContent content stream} within the resilience patterns
    */
   public Resilience4jDocumentRepository(Resilience4jDocumentConfig config,
                                         DocumentRepository delegate,
                                         String delegateName,
                                         boolean eagerLoadDocument) {
      this.config = validate(config, "config", isNotNull()).orThrow();
      this.delegate =
            validate(delegate, "delegate", isInstanceOf(value(AbstractDocumentRepository.class))).orThrow();
      this.delegateName = validate(delegateName, "delegateName", isNotBlank()).orThrow();
      this.eagerLoadDocument = eagerLoadDocument;

      this.resilience4jDecorator = new Resilience4jDecorator(config.resilience(),
                                                             ((AbstractDocumentRepository) delegate).eventService(),
                                                             delegateName,
                                                             list(DocumentAccessException.class));
   }

   /**
    * Creates a Resilience4j enabled repository wrapper.
    * Documents won't be eagerly loaded.
    *
    * @param config resilience configuration
    * @param delegate delegated real repository
    * @param delegateName unique name for the delegated repository to identify resilience patterns
    *       against it
    */
   public Resilience4jDocumentRepository(Resilience4jDocumentConfig config,
                                         DocumentRepository delegate,
                                         String delegateName) {
      this(config, delegate, delegateName, false);
   }

   /**
    * Creates a Resilience4j enabled repository wrapper.
    * Delegated repository name will be automatically generated from class hashcode.
    *
    * @param config resilience configuration
    * @param delegate delegated real repository
    * @param eagerLoadDocument whether to eagerly load {@link Stream stream} results and
    *       {@link InputStreamDocumentContent content stream} within the resilience patterns
    */
   public Resilience4jDocumentRepository(Resilience4jDocumentConfig config,
                                         DocumentRepository delegate,
                                         boolean eagerLoadDocument) {
      this(config, delegate, Resilience4jDecorator.delegateName(delegate), eagerLoadDocument);
   }

   /**
    * Creates a Resilience4j enabled repository wrapper.
    * Documents won't be eagerly loaded. Delegated repository name will be automatically generated from class
    * hashcode.
    *
    * @param config resilience configuration
    * @param delegate delegated real repository
    */
   public Resilience4jDocumentRepository(Resilience4jDocumentConfig config, DocumentRepository delegate) {
      this(config, delegate, Resilience4jDecorator.delegateName(delegate), false);
   }

   /**
    * Call delegated operation with resilience patterns.
    * Delegated operation is wrapped with resilience decorators.
    *
    * @param supplier operation
    * @param <T> operation return type
    *
    * @return operation result
    */
   public <T> T resilientCall(CheckedSupplier<T> supplier) {
      return resilience4jDecorator.resilientCall(supplier);
   }

   /**
    * Call delegated operation with resilience patterns.
    * Delegated operation is wrapped with resilience decorators.
    *
    * @param supplier operation
    * @param unRetryable whether call is retryable, this disables retry pattern if called operation does
    *       not support it
    * @param <T> operation return type
    *
    * @return operation result
    */
   public <T> T resilientCall(CheckedSupplier<T> supplier, boolean unRetryable) {
      return resilience4jDecorator.resilientCall(supplier, unRetryable);
   }

   /**
    * Call delegated operation with resilience patterns.
    * Delegated operation is wrapped with resilience decorators.
    *
    * @param runnable operation
    * @param <T> operation return type
    *
    * @return operation result
    */
   public <T> T resilientCall(CheckedRunnable runnable) {
      return resilience4jDecorator.resilientCall(checkedSupplier(runnable));
   }

   /**
    * Call delegated operation with resilience patterns.
    * Delegated operation is wrapped with resilience decorators.
    *
    * @param runnable operation
    * @param unRetryable whether call is retryable, this disables retry pattern if called operation does
    *       not support it
    * @param <T> operation return type
    *
    * @return operation result
    */
   public <T> T resilientCall(CheckedRunnable runnable, boolean unRetryable) {
      return resilience4jDecorator.resilientCall(checkedSupplier(runnable), unRetryable);
   }

   @Override
   public HashSet<RepositoryCapability> capabilities() { return delegate.capabilities(); }

   @Override
   public boolean hasCapability(RepositoryCapability capability) { return delegate.hasCapability(capability); }

   @Override
   // FIXME share resilience patterns state (ResilienceContext ?) with new repo if shareContext
   public Resilience4jDocumentRepository subPath(Path subPath, boolean shareContext) {
      return new Resilience4jDocumentRepository(config, delegate.subPath(subPath, shareContext),
                                                delegateName,
                                                eagerLoadDocument);
   }

   @Override
   @CheckReturnValue
   public Optional<Document> openDocument(DocumentPath documentId,
                                          boolean overwrite,
                                          boolean append,
                                          OpenDocumentMetadata metadata) {
      return resilientCall(() -> delegate.openDocument(documentId, overwrite, append, metadata));
   }

   @Override
   @CheckReturnValue
   public Optional<Document> openDocument(DocumentPath documentId, boolean overwrite, boolean append) {
      return resilientCall(() -> delegate.openDocument(documentId, overwrite, append));
   }

   @Override
   @CheckReturnValue
   public Optional<Document> openDocument(DocumentPath documentId, boolean overwrite) {
      return resilientCall(() -> delegate.openDocument(documentId, overwrite));
   }

   @Override
   public Optional<Document> findDocumentById(DocumentPath documentId) {
      return resilientCall(() -> eagerLoadDocument(delegate.findDocumentById(documentId)));
   }

   @Override
   public Stream<Document> findDocumentsBySpecification(Path basePath,
                                                        Specification<DocumentEntry> specification) {
      return resilientCall(() -> eagerLoadDocument(delegate.findDocumentsBySpecification(basePath,
                                                                                         specification)));
   }

   @Override
   public Stream<Document> findDocumentsBySpecification(Specification<DocumentEntry> specification) {
      return resilientCall(() -> eagerLoadDocument(delegate.findDocumentsBySpecification(specification)));
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryById(DocumentPath documentId) {
      return resilientCall(() -> delegate.findDocumentEntryById(documentId));
   }

   @Override
   public Stream<DocumentEntry> findDocumentEntriesBySpecification(Path basePath,
                                                                   Specification<DocumentEntry> specification) {
      return resilientCall(() -> delegate.findDocumentEntriesBySpecification(basePath, specification));
   }

   @Override
   public Stream<DocumentEntry> findDocumentEntriesBySpecification(Specification<DocumentEntry> specification) {
      return resilientCall(() -> delegate.findDocumentEntriesBySpecification(specification));
   }

   @Override
   @CheckReturnValue
   public Optional<DocumentEntry> saveDocument(Document document, boolean overwrite) {
      return resilientCall(() -> delegate.saveDocument(document, overwrite));
   }

   @Override
   @CheckReturnValue
   public Optional<Document> saveAndReturnDocument(Document document, boolean overwrite) {
      return resilientCall(() -> delegate.saveAndReturnDocument(document, overwrite));
   }

   @Override
   @CheckReturnValue
   public Optional<DocumentEntry> deleteDocumentById(DocumentPath documentId) {
      return resilientCall(() -> delegate.deleteDocumentById(documentId));
   }

   @Override
   @CheckReturnValue
   public List<DocumentEntry> deleteDocumentsBySpecification(Path basePath,
                                                             Specification<DocumentEntry> specification) {
      return resilientCall(() -> delegate.deleteDocumentsBySpecification(basePath, specification));
   }

   @Override
   @CheckReturnValue
   public Optional<DocumentEntry> transferDocument(DocumentPath documentId,
                                                   DocumentRepository targetDocumentRepository,
                                                   boolean overwrite,
                                                   DocumentProcessor processor) {
      return resilientCall(() -> delegate.transferDocument(documentId,
                                                           targetDocumentRepository,
                                                           overwrite,
                                                           processor));
   }

   @Override
   @CheckReturnValue
   public Optional<DocumentEntry> transferDocument(DocumentPath documentId,
                                                   DocumentRepository targetDocumentRepository,
                                                   boolean overwrite,
                                                   DocumentRenamer renamer) {
      return resilientCall(() -> delegate.transferDocument(documentId,
                                                           targetDocumentRepository,
                                                           overwrite,
                                                           renamer));
   }

   @Override
   @CheckReturnValue
   public Optional<DocumentEntry> transferDocument(DocumentPath documentId,
                                                   DocumentRepository targetDocumentRepository,
                                                   boolean overwrite) {
      return resilientCall(() -> delegate.transferDocument(documentId, targetDocumentRepository, overwrite));
   }

   @Override
   public List<DocumentEntry> transferDocuments(Specification<DocumentEntry> documentSpecification,
                                                DocumentRepository targetDocumentRepository,
                                                boolean overwrite,
                                                DocumentProcessor processor) {
      return resilientCall(() -> delegate.transferDocuments(documentSpecification,
                                                            targetDocumentRepository,
                                                            overwrite,
                                                            processor));
   }

   @Override
   public List<DocumentEntry> transferDocuments(Specification<DocumentEntry> documentSpecification,
                                                DocumentRepository targetDocumentRepository,
                                                boolean overwrite,
                                                DocumentRenamer renamer) {
      return resilientCall(() -> delegate.transferDocuments(documentSpecification,
                                                            targetDocumentRepository,
                                                            overwrite,
                                                            renamer));
   }

   @Override
   public List<DocumentEntry> transferDocuments(Specification<DocumentEntry> documentSpecification,
                                                DocumentRepository targetDocumentRepository,
                                                boolean overwrite) {
      return resilientCall(() -> delegate.transferDocuments(documentSpecification,
                                                            targetDocumentRepository,
                                                            overwrite));
   }

   @Override
   public <E extends DomainEvent> void registerEventListener(DomainEventListener<? super E> eventListener,
                                                             Class<? extends E>... eventClasses) {
      delegate.registerEventListener(eventListener, eventClasses);
   }

   @Override
   public void registerWriteEventsListener(DomainEventListener<? super DomainEvent> eventListener) {
      delegate.registerWriteEventsListener(eventListener);
   }

   @Override
   public void registerAccessEventsListener(DomainEventListener<? super DomainEvent> eventListener) {
      delegate.registerAccessEventsListener(eventListener);
   }

   @Override
   public <E extends DomainEvent> void registerEventLoggingListener(Class<? extends E>... eventClasses) {
      delegate.registerEventLoggingListener(eventClasses);
   }

   @Override
   public void registerWriteEventsLoggingListener() { delegate.registerWriteEventsLoggingListener(); }

   @Override
   public void registerAccessEventsLoggingListener() { delegate.registerAccessEventsLoggingListener(); }

   @Override
   public <T extends DocumentRepositoryMetrology> void registerMetrology(T metrology) {
      delegate.registerMetrology(metrology);
   }

   @Override
   public void unregisterEventListeners() { delegate.unregisterEventListeners(); }

   @Override
   public void close() {
      try {
         smartFinalize(delegate::close, resilience4jDecorator::close);
      } catch (Exception e) {
         throw new IllegalStateException(e.getMessage(), e);
      }
   }

   @Override
   public Stream<Document> findBySpecification(Specification<Document> specification) {
      return resilientCall(() -> eagerLoadDocument(delegate.findBySpecification(specification)));
   }

   @Override
   public Stream<Document> findAll() { return resilientCall(() -> eagerLoadDocument(delegate.findAll())); }

   @Override
   public Optional<Document> findById(DocumentPath documentId) {
      return resilientCall(() -> eagerLoadDocument(delegate.findById(documentId)));
   }

   @Override
   @CheckReturnValue
   public Optional<Document> save(Document document) {
      return resilientCall(() -> eagerLoadDocument(delegate.save(document)));
   }

   @Override
   @CheckReturnValue
   public Optional<Document> delete(DocumentPath documentId) {
      return resilientCall(() -> eagerLoadDocument(delegate.delete(documentId)));
   }

   @Override
   public boolean sameRepositoryAs(Repository<Document, DocumentPath> documentRepository) {
      validate(documentRepository, "documentRepository", isNotNull()).orThrow();

      Repository<Document, DocumentPath> comparingRepository = documentRepository;

      if (documentRepository instanceof Resilience4jDocumentRepository) {
         comparingRepository = ((Resilience4jDocumentRepository) documentRepository).delegate;
      }

      return delegate.sameRepositoryAs(comparingRepository);
   }

   @Override
   public URI toUri() { return delegate.toUri(); }

   @Override
   public URI toUri(DocumentPath documentId) { return delegate.toUri(documentId); }

   @Override
   public boolean supportsUri(URI uri, boolean documentUri) { return delegate.supportsUri(uri, documentUri); }

   @Override
   public boolean supportsDocumentUri(URI documentUri) { return delegate.supportsDocumentUri(documentUri); }

   @Override
   public Optional<Document> findDocumentByUri(URI documentUri) {
      return resilientCall(() -> eagerLoadDocument(delegate.findDocumentByUri(documentUri)));
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryByUri(URI documentUri) {
      return resilientCall(() -> delegate.findDocumentEntryByUri(documentUri));
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", Resilience4jDocumentRepository.class.getSimpleName() + "[", "]")
            .add("delegate=" + delegate)
            .add("eagerLoadDocument=" + eagerLoadDocument)
            .toString();
   }

   private Document eagerLoadDocument(Document document) {
      if (eagerLoadDocument) {
         return document.loadContent();
      } else {
         return document;
      }
   }

   @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
   private Optional<Document> eagerLoadDocument(Optional<Document> document) {
      if (eagerLoadDocument) {
         return document.map(this::eagerLoadDocument);
      } else {
         return document;
      }
   }

   private Stream<Document> eagerLoadDocument(Stream<Document> documents) {
      if (eagerLoadDocument) {
         return stream(list(documents.map(this::eagerLoadDocument)));
      } else {
         return documents;
      }
   }

}