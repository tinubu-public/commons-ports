#### Build environment image ####

FROM maven:3.8.4-openjdk-17-slim AS build

ENV MAVEN_OPTS=""
ENV MAVEN_CLI_OPTS="-B --show-version -Dstyle.color=always"
ENV BUILD_DIR="/build"

WORKDIR "$BUILD_DIR"

# Caching dependencies
COPY .mvn .mvn
COPY pom.xml .
COPY commons-ports-resilience4j/pom.xml commons-ports-resilience4j/
COPY commons-ports-document/pom.xml commons-ports-document/
COPY commons-ports-document/commons-ports-document-domain/pom.xml commons-ports-document/commons-ports-document-domain/
COPY commons-ports-document/commons-ports-document-transformer/pom.xml commons-ports-document/commons-ports-document-transformer/
COPY commons-ports-document/commons-ports-document-memory/pom.xml commons-ports-document/commons-ports-document-memory/
COPY commons-ports-document/commons-ports-document-classpath/pom.xml commons-ports-document/commons-ports-document-classpath/
COPY commons-ports-document/commons-ports-document-fs/pom.xml commons-ports-document/commons-ports-document-fs/
COPY commons-ports-document/commons-ports-document-sftp/pom.xml commons-ports-document/commons-ports-document-sftp/
COPY commons-ports-document/commons-ports-document-aws-s3/pom.xml commons-ports-document/commons-ports-document-aws-s3/
COPY commons-ports-document/commons-ports-document-azure-blob/pom.xml commons-ports-document/commons-ports-document-azure-blob/
COPY commons-ports-document/commons-ports-document-git/pom.xml commons-ports-document/commons-ports-document-git/
COPY commons-ports-document/commons-ports-document-resilience4j/pom.xml commons-ports-document/commons-ports-document-resilience4j/
COPY commons-ports-message/pom.xml commons-ports-message/
COPY commons-ports-message/commons-ports-message-domain/pom.xml commons-ports-message/commons-ports-message-domain/
COPY commons-ports-message/commons-ports-message-resilience4j/pom.xml commons-ports-message/commons-ports-message-resilience4j/
COPY commons-ports-message/commons-ports-message-smtp/pom.xml commons-ports-message/commons-ports-message-smtp/
COPY commons-ports-spring/pom.xml commons-ports-spring/
RUN mvn $MAVEN_CLI_OPTS dependency:go-offline -Dsilent=true

COPY . .

ARG SITE=false
ARG SITE_EXPORT=/export/site.tar.gz
ARG SKIP_TESTS=false
ARG TESTS_EXPORT=/export/tests.tar.gz
ARG CI_JOB_TOKEN
ENV CI_JOB_TOKEN="$CI_JOB_TOKEN"
ARG CI_PROJECT_ID
ENV CI_PROJECT_ID="$CI_PROJECT_ID"
ARG MAVEN_PHASE=deploy

RUN set -e; \
    cp .docker/buildsdk /bin; \
    eval buildsdk setx mvn $MAVEN_CLI_OPTS -s .mvn/maven-settings.xml clean $MAVEN_PHASE $($SKIP_TESTS && echo "-DskipTests") -DenableJacoco; \
    if ! $SKIP_TESTS; then \
    buildsdk report_jacoco_coverage; \
    mkdir -p "$(dirname "$TESTS_EXPORT")" && tar czf "$TESTS_EXPORT" $(find . -path '*/target/*-reports/TEST-*.xml'); \
    fi; \
    if $SITE; then \
       buildsdk setx mvn $MAVEN_CLI_OPTS antora:antora && (cd target/site && mkdir -p "$(dirname "$SITE_EXPORT")" && tar czf "$SITE_EXPORT" .); \
    fi