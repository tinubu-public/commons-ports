<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ Licensed to the Apache Software Foundation (ASF) under one
  ~ or more contributor license agreements.  See the NOTICE file
  ~ distributed with this work for additional information
  ~ regarding copyright ownership.  The ASF licenses this file
  ~ to you under the Apache License, Version 2.0 (the
  ~ "License"); you may not use this file except in compliance
  ~ with the License.  You may obtain a copy of the License at
  ~
  ~      http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  ~
  -->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
   <modelVersion>4.0.0</modelVersion>

   <parent>
      <groupId>com.tinubu.maven-parent</groupId>
      <artifactId>maven-parent</artifactId>
      <version>1.3.0</version>
   </parent>

   <groupId>com.tinubu.commons-ports</groupId>
   <artifactId>commons-ports</artifactId>
   <version>1.24.0-SNAPSHOT</version>
   <packaging>pom</packaging>

   <name>Commons ports</name>
   <description>Commons hexagonal ports</description>

   <properties>

      <java.version>11</java.version>
      <surefire.argLine>-Xmx1024m</surefire.argLine> <!-- Unit tests JVM options -->

      <!-- Dependencies version -->

      <tinubu-commons-lang.version>1.22.0</tinubu-commons-lang.version>
      <tinubu-commons-test.version>1.6.0</tinubu-commons-test.version>
      <tinubu-commons-ddd2.version>2.15.0</tinubu-commons-ddd2.version>

      <spring-boot.version>2.6.6</spring-boot.version>
      <commons-lang3.version>3.10</commons-lang3.version>
      <commons-io.version>2.6</commons-io.version>
      <commons-codec.version>1.15</commons-codec.version>
      <sshd.version>2.7.0</sshd.version>
      <azure-storage-blob.version>12.20.1</azure-storage-blob.version>
      <reactor.version>3.4.12</reactor.version>
      <testcontainers.version>1.16.2</testcontainers.version>
      <woodstox.version>6.4.0</woodstox.version>
      <aws-sdk.version>2.20.58</aws-sdk.version>
      <jaxb.version>2.3.1</jaxb.version>
      <zip4j.version>2.11.2</zip4j.version>
      <jgit.version>6.6.0.202305301015-r</jgit.version>
      <greenmail.version>1.6.14</greenmail.version>
      <resilience4j.version>1.7.0</resilience4j.version>

   </properties>

   <licenses>
      <license>
         <name>Apache License v2.0</name>
         <url>https://www.apache.org/licenses/LICENSE-2.0</url>
      </license>
   </licenses>

   <developers>
      <developer>
         <id>hdr</id>
         <name>Hugo de Paix de Coeur</name>
         <organization>Tinubu</organization>
      </developer>
   </developers>

   <modules>
      <module>commons-ports-resilience4j</module>
      <module>commons-ports-document</module>
      <module>commons-ports-message</module>
      <module>commons-ports-spring</module>
   </modules>

   <distributionManagement>
      <repository>
         <id>gitlab-project</id>
         <url>https://gitlab.com/api/v4/projects/${env.CI_PROJECT_ID}/packages/maven</url>
      </repository>
      <snapshotRepository>
         <id>gitlab-project</id>
         <url>https://gitlab.com/api/v4/projects/${env.CI_PROJECT_ID}/packages/maven</url>
      </snapshotRepository>
   </distributionManagement>

   <repositories>
      <!-- Parent repository -->
      <repository>
         <id>gitlab-tinubu</id>
         <url>https://gitlab.com/api/v4/groups/6532674/-/packages/maven</url>
      </repository>
   </repositories>

   <dependencyManagement>
      <dependencies>

         <!-- External -->

         <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-lang3</artifactId>
            <version>${commons-lang3.version}</version>
         </dependency>
         <dependency>
            <groupId>commons-io</groupId>
            <artifactId>commons-io</artifactId>
            <version>${commons-io.version}</version>
         </dependency>
         <dependency>
            <groupId>commons-codec</groupId>
            <artifactId>commons-codec</artifactId>
            <version>${commons-codec.version}</version>
         </dependency>
         <dependency>
            <groupId>com.tinubu.commons-lang</groupId>
            <artifactId>commons-lang</artifactId>
            <version>${tinubu-commons-lang.version}</version>
         </dependency>
         <dependency>
            <groupId>com.tinubu.commons-test</groupId>
            <artifactId>commons-test</artifactId>
            <version>${tinubu-commons-test.version}</version>
         </dependency>
         <dependency>
            <groupId>com.tinubu.commons-ddd</groupId>
            <artifactId>commons-ddd2</artifactId>
            <version>${tinubu-commons-ddd2.version}</version>
         </dependency>
         <dependency>
            <groupId>com.azure</groupId>
            <artifactId>azure-storage-blob</artifactId>
            <version>${azure-storage-blob.version}</version>
         </dependency>
         <dependency>
            <groupId>javax.xml.bind</groupId>
            <artifactId>jaxb-api</artifactId>
            <version>${jaxb.version}</version>
         </dependency>
         <dependency>
            <groupId>net.lingala.zip4j</groupId>
            <artifactId>zip4j</artifactId>
            <version>${zip4j.version}</version>
         </dependency>
         <dependency>
            <groupId>org.eclipse.jgit</groupId>
            <artifactId>org.eclipse.jgit</artifactId>
            <version>${jgit.version}</version>
         </dependency>
         <dependency>
            <groupId>io.github.resilience4j</groupId>
            <artifactId>resilience4j-all</artifactId>
            <version>${resilience4j.version}</version>
         </dependency>

         <!-- Disambiguation -->

         <dependency>
            <groupId>io.projectreactor</groupId>
            <artifactId>reactor-core</artifactId>
            <version>${reactor.version}</version>
         </dependency>
         <dependency>
            <groupId>com.fasterxml.woodstox</groupId>
            <artifactId>woodstox-core</artifactId>
            <version>${woodstox.version}</version>
         </dependency>

         <!-- Test -->

         <dependency>
            <groupId>org.apache.sshd</groupId>
            <artifactId>sshd-sftp</artifactId>
            <version>${sshd.version}</version>
         </dependency>
         <dependency>
            <groupId>com.icegreen</groupId>
            <artifactId>greenmail-junit5</artifactId>
            <version>${greenmail.version}</version>
         </dependency>

         <!-- BOM - must be set in last position -->

         <dependency>
            <groupId>org.testcontainers</groupId>
            <artifactId>testcontainers-bom</artifactId>
            <version>${testcontainers.version}</version>
            <type>pom</type>
            <scope>import</scope>
         </dependency>
         <!-- Fixes spring-boot-dependencies -->
         <dependency>
            <groupId>net.bytebuddy</groupId>
            <artifactId>byte-buddy</artifactId>
            <version>1.12.8</version>
         </dependency>
         <dependency>
            <groupId>net.bytebuddy</groupId>
            <artifactId>byte-buddy-agent</artifactId>
            <version>1.12.8</version>
         </dependency>
         <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-dependencies</artifactId>
            <version>${spring-boot.version}</version>
            <type>pom</type>
            <scope>import</scope>
            <!-- Note : Unsupported for now, see fixes above (https://issues.apache.org/jira/browse/MNG-5600) -->
            <exclusions>
               <exclusion>
                  <groupId>net.bytebuddy</groupId>
                  <artifactId>*</artifactId>
               </exclusion>
            </exclusions>
         </dependency>
         <dependency>
            <groupId>software.amazon.awssdk</groupId>
            <artifactId>bom</artifactId>
            <version>${aws-sdk.version}</version>
            <type>pom</type>
            <scope>import</scope>
         </dependency>

      </dependencies>
   </dependencyManagement>

   <build>
      <pluginManagement>
         <plugins>
            <plugin>
               <groupId>org.antora</groupId>
               <artifactId>antora-maven-plugin</artifactId>
               <version>1.0.0-alpha.2</version>
               <configuration>
                  <playbook>.antora-playbook.yml</playbook>
                  <packages>
                     <package>@antora/lunr-extension</package>
                     <package>asciidoctor-plantuml</package>
                  </packages>
               </configuration>
            </plugin>
         </plugins>
      </pluginManagement>
   </build>

   <dependencies>

      <!-- External -->

      <dependency>
         <groupId>org.slf4j</groupId>
         <artifactId>slf4j-api</artifactId>
      </dependency>

      <!-- Tests -->

      <dependency>
         <groupId>org.junit.jupiter</groupId>
         <artifactId>junit-jupiter-api</artifactId>
         <scope>test</scope>
      </dependency>
      <dependency>
         <groupId>org.junit.jupiter</groupId>
         <artifactId>junit-jupiter-engine</artifactId>
         <scope>test</scope>
      </dependency>
      <dependency>
         <groupId>org.junit.jupiter</groupId>
         <artifactId>junit-jupiter-params</artifactId>
         <scope>test</scope>
      </dependency>
      <dependency>
         <groupId>org.mockito</groupId>
         <artifactId>mockito-inline</artifactId>
         <scope>test</scope>
      </dependency>
      <dependency>
         <groupId>org.assertj</groupId>
         <artifactId>assertj-core</artifactId>
         <scope>test</scope>
      </dependency>
      <dependency>
         <groupId>org.mockito</groupId>
         <artifactId>mockito-junit-jupiter</artifactId>
         <scope>test</scope>
      </dependency>

   </dependencies>

</project>

