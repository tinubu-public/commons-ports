/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.message.smtp;

import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.MULTIPART_MIXED;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.MULTIPART_RELATED;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_CSS;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_HTML;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_PLAIN;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.ports.message.smtp.PartAssert.MULTIPART_ALTERNATIVE;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.clearInvocations;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.function.Consumer;

import javax.mail.internet.MimeMessage;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.mail.MailSenderAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.javamail.JavaMailSender;

import com.icegreen.greenmail.configuration.GreenMailConfiguration;
import com.icegreen.greenmail.junit5.GreenMailExtension;
import com.icegreen.greenmail.util.ServerSetupTest;
import com.tinubu.commons.ddd2.domain.event.DomainEvent;
import com.tinubu.commons.ddd2.domain.event.DomainEventListener;
import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.lang.datetime.ApplicationClock;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.message.domain.Message;
import com.tinubu.commons.ports.message.domain.Message.MessageBuilder;
import com.tinubu.commons.ports.message.domain.MessageAddress;
import com.tinubu.commons.ports.message.domain.MessageContent.MessageContentBuilder;
import com.tinubu.commons.ports.message.domain.MessageDocument.MessageDocumentBuilder;
import com.tinubu.commons.ports.message.domain.MessageIOException;
import com.tinubu.commons.ports.message.domain.MessageService;
import com.tinubu.commons.ports.message.domain.event.MessageSent;
import com.tinubu.commons.ports.message.domain.testsuite.BaseMessageServiceTest;

@SpringBootTest(classes = SmtpMessageServiceTest.class)
@ImportAutoConfiguration({
      MailSenderAutoConfiguration.class /* JavaMail initialization */ })
class SmtpMessageServiceTest extends BaseMessageServiceTest {

   private static final ZonedDateTime FIXED_DATE =
         ZonedDateTime.of(LocalDateTime.of(2021, 11, 10, 14, 0, 0), ZoneId.of("UTC"));
   private static final String USERNAME = "username";
   private static final String PASSWORD = "secret";

   @Autowired
   JavaMailSender javaMailSender;

   private SmtpMessageService smtp;

   @RegisterExtension
   static GreenMailExtension greenMail =
         new GreenMailExtension(ServerSetupTest.SMTP).withConfiguration(GreenMailConfiguration
                                                                              .aConfig()
                                                                              .withUser(USERNAME, PASSWORD));

   @BeforeEach
   public void configureSmtpMessageService() {
      smtp = new SmtpMessageService(javaMailSender);
   }

   @BeforeEach
   public void setApplicationClock() {
      ApplicationClock.setFixedClock(FIXED_DATE);
   }

   @Override
   protected MessageService messageService() {
      return smtp;
   }

   @Test
   void sendWhenNominal() {
      DomainEventListener<DomainEvent> domainEventListener = instrumentMessageServiceListeners();

      clearInvocations(domainEventListener);

      Message message = new MessageBuilder()
            .fromAddress(MessageAddress.of("from@domain.tld"))
            .toAddress(MessageAddress.of("to@domain.tld"))
            .replyToAddress(MessageAddress.of("reply-to@domain.tld"))
            .ccAddresses(MessageAddress.of("cc1@domain.tld"), MessageAddress.of("cc2@domain.tld"))
            .subject("subject")
            .content(MessageContentBuilder.of("<strong>content</strong>", TEXT_HTML))
            .build();

      smtp.sendMessage(message);

      Consumer<MimeMessage> isValidMessage = receivedMessage -> PartAssert
            .assertThat(receivedMessage)
            .hasHeader("subject", "subject")
            .hasHeader("from", "from@domain.tld")
            .hasHeader("to", "to@domain.tld")
            .hasHeader("cc", "cc1@domain.tld, cc2@domain.tld")
            .hasHeader("reply-to", "reply-to@domain.tld")
            .hasContentType(TEXT_HTML, "charset=UTF-8")
            .hasContent("<strong>content</strong>");

      assertThat(greenMail.getReceivedMessages()).satisfiesExactly(isValidMessage,
                                                                   isValidMessage,
                                                                   isValidMessage);

      verify(domainEventListener).accept(any(MessageSent.class));
      verifyNoMoreInteractions(domainEventListener);
   }

   @Test
   void sendWhenAddressWithPersonal() {
      Message message = new MessageBuilder()
            .fromAddress(MessageAddress.of("from@domain.tld", "From"))
            .toAddress(MessageAddress.of("to@domain.tld", "To"))
            .replyToAddress(MessageAddress.of("reply-to@domain.tld", "Reply to"))
            .ccAddresses(MessageAddress.of("cc1@domain.tld", "Cc 1"),
                         MessageAddress.of("cc2@domain.tld", "Cc 2"))
            .subject("subject")
            .content(MessageContentBuilder.of("content", TEXT_PLAIN))
            .build();

      smtp.sendMessage(message);

      Consumer<MimeMessage> isValidMessage = receivedMessage -> PartAssert
            .assertThat(receivedMessage)
            .hasHeader("subject", "subject")
            .hasHeader("from", "From <from@domain.tld>")
            .hasHeader("to", "To <to@domain.tld>")
            .hasHeader("cc", "Cc 1 <cc1@domain.tld>, Cc 2 <cc2@domain.tld>")
            .hasHeader("reply-to", "Reply to <reply-to@domain.tld>")
            .hasContentType(TEXT_PLAIN, "charset=UTF-8")
            .hasContent("content");

      assertThat(greenMail.getReceivedMessages()).satisfiesExactly(isValidMessage,
                                                                   isValidMessage,
                                                                   isValidMessage);
   }

   @Test
   void sendWhenNoReplyTo() {
      Message message = new MessageBuilder()
            .fromAddress(MessageAddress.of("from@domain.tld"))
            .toAddress(MessageAddress.of("to@domain.tld"))
            .subject("subject")
            .content(MessageContentBuilder.of("content", TEXT_PLAIN))
            .build();

      smtp.sendMessage(message);

      assertThat(greenMail.getReceivedMessages()).satisfiesExactly((Consumer<MimeMessage>) receivedMessage -> PartAssert
            .assertThat(receivedMessage)
            .hasHeader("subject", "subject")
            .hasHeader("from", "from@domain.tld")
            .hasHeader("to", "to@domain.tld")
            .hasNoHeader("reply-to")
            .hasContentType(TEXT_PLAIN, "charset=UTF-8")
            .hasContent("content"));
   }

   @Test
   void sendWhenBccAddresses() {
      Message message = new MessageBuilder()
            .fromAddress(MessageAddress.of("from@domain.tld"))
            .toAddress(MessageAddress.of("to@domain.tld"))
            .replyToAddress(MessageAddress.of("reply-to@domain.tld"))
            .bccAddresses(list(MessageAddress.of("bcc@domain.tld")))
            .subject("subject")
            .content(MessageContentBuilder.of("content", TEXT_PLAIN))
            .build();

      smtp.sendMessage(message);

      Consumer<MimeMessage> isValidMessage = receivedMessage -> {
         PartAssert
               .assertThat(receivedMessage)
               .hasHeader("subject", "subject")
               .hasHeader("from", "from@domain.tld")
               .hasHeader("to", "to@domain.tld")
               .hasHeader("reply-to", "reply-to@domain.tld")
               .hasContentType(TEXT_PLAIN, "charset=UTF-8")
               .hasContent("content");
         PartAssert
               .assertThat(receivedMessage)
               .as("BCC addresses must not be present as it is hidden")
               .hasNoHeader("bcc");
      };

      assertThat(greenMail.getReceivedMessages()).satisfiesExactly(isValidMessage, isValidMessage);
   }

   @Test
   void sendWhenOnlyCcAddresses() {
      Message message = new MessageBuilder()
            .fromAddress(MessageAddress.of("from@domain.tld"))
            .replyToAddress(MessageAddress.of("reply-to@domain.tld"))
            .ccAddresses(list(MessageAddress.of("cc@domain.tld")))
            .subject("subject")
            .content(MessageContentBuilder.of("content", TEXT_PLAIN))
            .build();

      smtp.sendMessage(message);

      assertThat(greenMail.getReceivedMessages()).satisfiesExactly(receivedMessage -> PartAssert
            .assertThat(receivedMessage)
            .hasHeader("subject", "subject")
            .hasNoHeader("to")
            .hasHeader("from", "from@domain.tld")
            .hasHeader("reply-to", "reply-to@domain.tld")
            .hasHeader("cc", "cc@domain.tld")
            .hasContentType(TEXT_PLAIN, "charset=UTF-8")
            .hasContent("content"));
   }

   @Test
   void sendWhenOnlyBccAddresses() {
      Message message = new MessageBuilder()
            .fromAddress(MessageAddress.of("from@domain.tld"))
            .replyToAddress(MessageAddress.of("reply-to@domain.tld"))
            .bccAddresses(list(MessageAddress.of("bcc@domain.tld")))
            .subject("subject")
            .content(MessageContentBuilder.of("content", TEXT_PLAIN))
            .build();

      smtp.sendMessage(message);

      assertThat(greenMail.getReceivedMessages()).satisfiesExactly(receivedMessage -> PartAssert
            .assertThat(receivedMessage)
            .hasHeader("subject", "subject")
            .hasNoHeader("to")
            .hasHeader("from", "from@domain.tld")
            .hasHeader("reply-to", "reply-to@domain.tld")
            .hasNoHeader("bcc")
            .hasContentType(TEXT_PLAIN, "charset=UTF-8")
            .hasContent("content"));
   }

   @Test
   void sendWhenSimpleTextPlain() {
      Message message = new MessageBuilder()
            .fromAddress(MessageAddress.of("from@domain.tld"))
            .toAddress(MessageAddress.of("to@domain.tld"))
            .subject("subject")
            .content(MessageContentBuilder.of("content", TEXT_PLAIN))
            .build();

      smtp.sendMessage(message);

      assertThat(greenMail.getReceivedMessages()).satisfiesExactly(receivedMessage -> PartAssert
            .assertThat(receivedMessage)
            .hasHeader("subject", "subject")
            .hasHeader("from", "from@domain.tld")
            .hasHeader("to", "to@domain.tld")
            .hasContentType(TEXT_PLAIN, "charset=UTF-8")
            .hasContent("content"));
   }

   @Test
   void sendWhenSimpleTextHtml() {
      Message message = new MessageBuilder()
            .fromAddress(MessageAddress.of("from@domain.tld"))
            .toAddress(MessageAddress.of("to@domain.tld"))
            .subject("subject")
            .content(MessageContentBuilder.of("<strong>content</strong>", TEXT_HTML))
            .build();

      smtp.sendMessage(message);

      assertThat(greenMail.getReceivedMessages()).satisfiesExactly(receivedMessage -> PartAssert
            .assertThat(receivedMessage)
            .hasHeader("subject", "subject")
            .hasHeader("from", "from@domain.tld")
            .hasHeader("to", "to@domain.tld")
            .hasContentType(TEXT_HTML, "charset=UTF-8")
            .hasContent("<strong>content</strong>"));

   }

   @Test
   void sendWhenSimpleHtmlWithTextAlternative() {
      Message message = new MessageBuilder()
            .fromAddress(MessageAddress.of("from@domain.tld"))
            .toAddress(MessageAddress.of("to@domain.tld"))
            .subject("subject")
            .content(MessageContentBuilder.of("<strong>content</strong>", TEXT_HTML))
            .build();

      smtp.sendMessage(message, MessageContentBuilder.of("alternative", TEXT_PLAIN));

      assertThat(greenMail.getReceivedMessages()).satisfiesExactly(receivedMessage -> {
         PartAssert
               .assertThat(receivedMessage)
               .hasHeader("subject", "subject")
               .hasHeader("from", "from@domain.tld")
               .hasHeader("to", "to@domain.tld")
               .hasContentType(MULTIPART_MIXED)
               .hasContent(related -> PartAssert
                     .assertThat(related)
                     .hasContentType(MULTIPART_RELATED)
                     .hasContent(alternative -> PartAssert
                           .assertThat(alternative)
                           .hasContentType(MULTIPART_ALTERNATIVE)
                           .hasContent(text -> PartAssert
                                             .assertThat(text)
                                             .hasContentType(TEXT_PLAIN, "charset=UTF-8")
                                             .hasContent("alternative"),
                                       html -> PartAssert
                                             .assertThat(html)
                                             .hasContentType(TEXT_HTML, "charset=UTF-8")
                                             .hasContent("<strong>content</strong>"))));
      });
   }

   @Test
   void sendWhenInvalidAddresses() {
      DomainEventListener<DomainEvent> domainEventListener = instrumentMessageServiceListeners();

      clearInvocations(domainEventListener);

      Message message = new MessageBuilder()
            .fromAddress(MessageAddress.of("from@"))
            .toAddress(MessageAddress.of("to@"))
            .subject("subject")
            .content(MessageContentBuilder.of("<strong>content</strong>", TEXT_HTML))
            .build();

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> smtp.sendMessage(message))
            .withMessage(
                  "Invariant validation error > Context [Message[fromAddress=MessageAddress[address=from@,personal=<null>],replyToAddress=<null>,toAddresses=[MessageAddress[address=to@,personal=<null>]],ccAddresses=[],bccAddresses=[],subject=subject,content=MessageContent[content=<hidden-value>,contentType=text/html,inlines=[]],attachments=[]]] > {message.fromAddress} 'message.fromAddress' is not a valid RFC 822 address");

      verifyNoMoreInteractions(domainEventListener);
   }

   @Test
   void sendWhenInvalidOptionalAddresses() {
      Message message = new MessageBuilder()
            .fromAddress(MessageAddress.of("from@domain.tld"))
            .replyToAddress(MessageAddress.of("reply-to@"))
            .toAddresses(MessageAddress.of("to@domain.tld"),
                         MessageAddress.of("to1@"),
                         MessageAddress.of("to2@"))
            .ccAddresses(MessageAddress.of("cc1@"), MessageAddress.of("cc2@"))
            .bccAddresses(MessageAddress.of("bcc1@"), MessageAddress.of("bcc2@"))
            .subject("subject")
            .content(MessageContentBuilder.of("<strong>content</strong>", TEXT_HTML))
            .build();

      smtp.sendMessage(message);

      assertThat(greenMail.getReceivedMessages()).satisfiesExactly(receivedMessage -> PartAssert
            .assertThat(receivedMessage)
            .hasHeader("subject", "subject")
            .hasHeader("from", "from@domain.tld")
            .hasHeader("to", "to@domain.tld")
            .hasContentType(TEXT_HTML, "charset=UTF-8")
            .hasContent("<strong>content</strong>"));
   }

   @Test
   void sendWhenAllRecipientsAreInvalidAddresses() {
      Message message = new MessageBuilder()
            .fromAddress(MessageAddress.of("from@domain.tld"))
            .toAddress(MessageAddress.of("to@"))
            .subject("subject")
            .content(MessageContentBuilder.of("<strong>content</strong>", TEXT_HTML))
            .build();

      assertThatExceptionOfType(MessageIOException.class)
            .isThrownBy(() -> smtp.sendMessage(message))
            .withMessage("Failed messages: javax.mail.SendFailedException: No recipient addresses");
   }

   @Test
   void sendWhenNoContent() {
      Message message = new MessageBuilder()
            .fromAddress(MessageAddress.of("from@domain.tld"))
            .toAddress(MessageAddress.of("to@domain.tld"))
            .subject("subject")
            .build();

      smtp.sendMessage(message);

      assertThat(greenMail.getReceivedMessages()).satisfiesExactly(receivedMessage -> PartAssert
            .assertThat(receivedMessage)
            .hasHeader("subject", "subject")
            .hasHeader("from", "from@domain.tld")
            .hasHeader("to", "to@domain.tld")
            .hasContentType(TEXT_PLAIN, "charset=UTF-8")
            .hasEmptyContent());
   }

   @Test
   void sendWhenNoContentButAttachments() {
      Message message = new MessageBuilder()
            .fromAddress(MessageAddress.of("from@domain.tld"))
            .toAddress(MessageAddress.of("to@domain.tld"))
            .subject("subject")
            .attachments(new MessageDocumentBuilder()
                               .documentId(DocumentPath.of("attachment.txt"))
                               .loadedContent("attachment content", UTF_8)
                               .build())
            .build();

      smtp.sendMessage(message);

      assertThat(greenMail.getReceivedMessages()).satisfiesExactly(receivedMessage -> PartAssert
            .assertThat(receivedMessage)
            .hasHeader("subject", "subject")
            .hasHeader("from", "from@domain.tld")
            .hasHeader("to", "to@domain.tld")
            .hasContentType(MULTIPART_MIXED)
            .hasContent(related -> PartAssert
                              .assertThat(related)
                              .hasContentType(MULTIPART_RELATED)
                              .hasContent(content -> PartAssert
                                    .assertThat(content)
                                    .hasContentType(TEXT_PLAIN, "charset=UTF-8")
                                    .hasEmptyContent()),
                        attachment -> PartAssert
                              .assertThat(attachment)
                              .hasContentType(TEXT_PLAIN, "charset=UTF-8", "name=attachment.txt")
                              .hasDisposition("attachment")
                              .hasFileName("attachment.txt")
                              .hasContent("attachment content")));

   }

   @Test
   void sendWhenNoSubject() {
      Message message = new MessageBuilder()
            .fromAddress(MessageAddress.of("from@domain.tld"))
            .toAddress(MessageAddress.of("to@domain.tld"))
            .build();

      smtp.sendMessage(message);

      assertThat(greenMail.getReceivedMessages()).satisfiesExactly(receivedMessage -> PartAssert
            .assertThat(receivedMessage)
            .hasNoHeader("subject")
            .hasHeader("from", "from@domain.tld")
            .hasHeader("to", "to@domain.tld")
            .hasContentType(TEXT_PLAIN, "charset=UTF-8")
            .hasEmptyContent());

   }

   @Test
   void sendWhenAttachments() {
      Message message = new MessageBuilder()
            .fromAddress(MessageAddress.of("from@domain.tld"))
            .toAddress(MessageAddress.of("to@domain.tld"))
            .replyToAddress(MessageAddress.of("reply-to@domain.tld", "Reply-To"))
            .subject("subject")
            .content(MessageContentBuilder.of("content", TEXT_PLAIN))
            .attachments(new MessageDocumentBuilder()
                               .documentId(DocumentPath.of("attachment.txt"))
                               .loadedContent("attachment content", UTF_8)
                               .build())
            .build();

      smtp.sendMessage(message);

      assertThat(greenMail.getReceivedMessages()).satisfiesExactly(receivedMessage -> PartAssert
            .assertThat(receivedMessage)
            .hasHeader("subject", "subject")
            .hasHeader("from", "from@domain.tld")
            .hasHeader("to", "to@domain.tld")
            .hasContentType(MULTIPART_MIXED)
            .hasContent(related -> PartAssert
                              .assertThat(related)
                              .hasContentType(MULTIPART_RELATED)
                              .hasContent(content -> PartAssert
                                    .assertThat(content)
                                    .hasContentType(TEXT_PLAIN, "charset=UTF-8")
                                    .hasContent("content")),
                        attachment -> PartAssert
                              .assertThat(attachment)
                              .hasContentType(TEXT_PLAIN, "charset=UTF-8", "name=attachment.txt")
                              .hasDisposition("attachment")
                              .hasFileName("attachment.txt")
                              .hasContent("attachment content")));
   }

   @Test
   void sendWhenInlinesWithExplicitId() {
      Message message = new MessageBuilder()
            .fromAddress(MessageAddress.of("from@domain.tld"))
            .toAddress(MessageAddress.of("to@domain.tld"))
            .subject("subject")
            .content(MessageContentBuilder.of("<strong>content</strong>",
                                              TEXT_HTML,
                                              new MessageDocumentBuilder()
                                                    .messageDocumentId("inline-id")
                                                    .documentId(DocumentPath.of("inline.txt"))
                                                    .loadedContent("inline content", UTF_8)
                                                    .build()))
            .build();

      smtp.sendMessage(message);

      assertThat(greenMail.getReceivedMessages()).satisfiesExactly(receivedMessage -> PartAssert
            .assertThat(receivedMessage)
            .hasHeader("subject", "subject")
            .hasHeader("from", "from@domain.tld")
            .hasHeader("to", "to@domain.tld")
            .hasContentType(MULTIPART_MIXED)
            .hasContent(related -> PartAssert
                  .assertThat(related)
                  .hasContentType(MULTIPART_RELATED)
                  .hasContent(content -> PartAssert
                                    .assertThat(content)
                                    .hasContentType(TEXT_HTML, "charset=UTF-8")
                                    .hasContent("<strong>content</strong>"),
                              inline -> PartAssert
                                    .assertThat(inline)
                                    .hasContentType(TEXT_PLAIN, "charset=UTF-8")
                                    .hasDisposition("inline")
                                    .hasContentId("<inline-id>")
                                    .hasContent("inline content"))));
   }

   @Test
   void sendWhenInlinesWithGeneratedId() {
      Message message = new MessageBuilder()
            .fromAddress(MessageAddress.of("from@domain.tld"))
            .toAddress(MessageAddress.of("to@domain.tld"))
            .subject("subject")
            .content(MessageContentBuilder.of("<strong>content</strong>",
                                              TEXT_HTML,
                                              new MessageDocumentBuilder()
                                                    .documentId(DocumentPath.of("inline.txt"))
                                                    .loadedContent("inline content", UTF_8)
                                                    .build()))
            .build();

      smtp.sendMessage(message);

      assertThat(greenMail.getReceivedMessages()).satisfiesExactly(receivedMessage -> PartAssert
            .assertThat(receivedMessage)
            .hasHeader("subject", "subject")
            .hasHeader("from", "from@domain.tld")
            .hasHeader("to", "to@domain.tld")
            .hasContentType(MULTIPART_MIXED)
            .hasContent(related -> PartAssert
                  .assertThat(related)
                  .hasContentType(MULTIPART_RELATED)
                  .hasContent(content -> PartAssert
                                    .assertThat(content)
                                    .hasContentType(TEXT_HTML, "charset=UTF-8")
                                    .hasContent("<strong>content</strong>"),
                              inline -> PartAssert
                                    .assertThat(inline)
                                    .hasContentType(TEXT_PLAIN, "charset=UTF-8")
                                    .hasDisposition("inline")
                                    .isGeneratedContentId()
                                    .hasContent("inline content"))));
   }

   @Test
   void sendWhenAlternativeAndAttachmentsAndInlines() {
      Message message = new MessageBuilder()
            .fromAddress(MessageAddress.of("from@domain.tld"))
            .toAddress(MessageAddress.of("to@domain.tld"))
            .subject("subject")
            .content(MessageContentBuilder.of("<strong>content</strong>",
                                              TEXT_HTML,
                                              new MessageDocumentBuilder()
                                                    .documentId(DocumentPath.of("inline.txt"))
                                                    .loadedContent("inline content", UTF_8)
                                                    .build()))
            .attachments(new MessageDocumentBuilder()
                               .documentId(DocumentPath.of("attachment.txt"))
                               .loadedContent("attachment content", UTF_8)
                               .build())
            .build();

      smtp.sendMessage(message, MessageContentBuilder.of("alternative", TEXT_PLAIN));

      assertThat(greenMail.getReceivedMessages()).satisfiesExactly(receivedMessage -> {
         PartAssert
               .assertThat(receivedMessage)
               .hasHeader("subject", "subject")
               .hasHeader("from", "from@domain.tld")
               .hasHeader("to", "to@domain.tld")
               .hasContentType(MULTIPART_MIXED)
               .hasContent(related -> PartAssert
                                 .assertThat(related)
                                 .hasContentType(MULTIPART_RELATED)
                                 .hasContent(alternative -> PartAssert
                                                   .assertThat(alternative)
                                                   .hasContentType(MULTIPART_ALTERNATIVE)
                                                   .hasContent(text -> PartAssert
                                                                     .assertThat(text)
                                                                     .hasContentType(TEXT_PLAIN, "charset=UTF-8")
                                                                     .hasContent("alternative"),
                                                               html -> PartAssert
                                                                     .assertThat(html)
                                                                     .hasContentType(TEXT_HTML, "charset=UTF-8")
                                                                     .hasContent("<strong>content</strong>")),
                                             inline -> PartAssert
                                                   .assertThat(inline)
                                                   .hasContentType(TEXT_PLAIN, "charset=UTF-8")
                                                   .hasDisposition("inline")
                                                   .isGeneratedContentId()
                                                   .hasContent("inline content")),
                           attachment -> PartAssert
                                 .assertThat(attachment)
                                 .hasContentType(TEXT_PLAIN, "charset=UTF-8", "name=attachment.txt")
                                 .hasDisposition("attachment")
                                 .hasFileName("attachment.txt")
                                 .hasContent("attachment content"));
      });
   }

   @Test
   void sendWhenUnsupportedContentType() {
      Message message = new MessageBuilder()
            .fromAddress(MessageAddress.of("from@domain.tld"))
            .toAddress(MessageAddress.of("to@domain.tld"))
            .content(MessageContentBuilder.of("content", TEXT_CSS))
            .build();

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> smtp.sendMessage(message))
            .withMessage(
                  "Invariant validation error > Context [Message[fromAddress=MessageAddress[address=from@domain.tld,personal=<null>],replyToAddress=<null>,toAddresses=[MessageAddress[address=to@domain.tld,personal=<null>]],ccAddresses=[],bccAddresses=[],subject=<null>,content=MessageContent[content=<hidden-value>,contentType=text/css,inlines=[]],attachments=[]]] > {message.hasSupportedContentType} 'message.content=text/css' must be equal to 'text/plain' (type and subtype only)");
   }

   @Test
   void sendWhenDuplicatedDocumentIdentifiersBecauseOfAlternatives() {
      Message message = new MessageBuilder()
            .fromAddress(MessageAddress.of("from@domain.tld"))
            .toAddress(MessageAddress.of("to@domain.tld"))
            .subject("subject")
            .content(MessageContentBuilder.of("content",
                                              TEXT_HTML,
                                              new MessageDocumentBuilder()
                                                    .messageDocumentId("sameid")
                                                    .documentId(DocumentPath.of("content-inline.txt"))
                                                    .loadedContent("content inline content", UTF_8)
                                                    .build()))
            .build();

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> smtp.sendMessage(message,
                                               MessageContentBuilder.of("alternative content",
                                                                        TEXT_PLAIN,
                                                                        new MessageDocumentBuilder()
                                                                              .messageDocumentId("sameid")
                                                                              .documentId(DocumentPath.of(
                                                                                    "alternative-inline.txt"))
                                                                              .loadedContent(
                                                                                    "alternative inline content",
                                                                                    UTF_8)
                                                                              .build())))
            .withMessage(
                  "Invariant validation error > Context [Message[fromAddress=MessageAddress[address=from@domain.tld,personal=<null>],replyToAddress=<null>,toAddresses=[MessageAddress[address=to@domain.tld,personal=<null>]],ccAddresses=[],bccAddresses=[],subject=subject,content=MessageContent[content=<hidden-value>,contentType=text/html,inlines=[sameid->DocumentPath[value=content-inline.txt,newObject=false]]],attachments=[]],MessageContent[content=<hidden-value>,contentType=text/plain,inlines=[sameid->DocumentPath[value=alternative-inline.txt,newObject=false]]]] > {message.hasUniqueMessageDocumentIds} 'message.documents=[MessageDocument[documentId=DocumentPath[value=content-inline.txt,newObject=false],metadata=DocumentMetadata[documentPath=content-inline.txt,contentType=text/plain;charset=UTF-8,contentSize=22,creationDate=2021-11-10T14:00:00Z,lastUpdateDate=2021-11-10T14:00:00Z,attributes={}],content=LoadedDocumentContent[content=<hidden-value>,contentEncoding=UTF-8],outputBufferSize=8192,inputBufferSize=8192,messageDocumentId=sameid], MessageDocument[documentId=DocumentPath[value=alternative-inline.txt,newObject=false],metadata=DocumentMetadata[documentPath=alternative-inline.txt,contentType=text/plain;charset=UTF-8,contentSize=26,creationDate=2021-11-10T14:00:00Z,lastUpdateDate=2021-11-10T14:00:00Z,attributes={}],content=LoadedDocumentContent[content=<hidden-value>,contentEncoding=UTF-8],outputBufferSize=8192,inputBufferSize=8192,messageDocumentId=sameid]]' has duplicates : {sameid->[MessageDocument[documentId=DocumentPath[value=content-inline.txt,newObject=false],metadata=DocumentMetadata[documentPath=content-inline.txt,contentType=text/plain;charset=UTF-8,contentSize=22,creationDate=2021-11-10T14:00:00Z,lastUpdateDate=2021-11-10T14:00:00Z,attributes={}],content=LoadedDocumentContent[content=<hidden-value>,contentEncoding=UTF-8],outputBufferSize=8192,inputBufferSize=8192,messageDocumentId=sameid],MessageDocument[documentId=DocumentPath[value=alternative-inline.txt,newObject=false],metadata=DocumentMetadata[documentPath=alternative-inline.txt,contentType=text/plain;charset=UTF-8,contentSize=26,creationDate=2021-11-10T14:00:00Z,lastUpdateDate=2021-11-10T14:00:00Z,attributes={}],content=LoadedDocumentContent[content=<hidden-value>,contentEncoding=UTF-8],outputBufferSize=8192,inputBufferSize=8192,messageDocumentId=sameid]]}");
   }

   @Test
   void sendWhenTooManyAlternatives() {
      Message message = new MessageBuilder()
            .fromAddress(MessageAddress.of("from@domain.tld"))
            .toAddress(MessageAddress.of("to@domain.tld"))
            .subject("subject")
            .content(MessageContentBuilder.of("content", TEXT_HTML))
            .build();

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> smtp.sendMessage(message,
                                               MessageContentBuilder.of("alternative content", TEXT_PLAIN),
                                               MessageContentBuilder.of("alternative content", TEXT_PLAIN)))
            .withMessage(
                  "Invariant validation error > Context [Message[fromAddress=MessageAddress[address=from@domain.tld,personal=<null>],replyToAddress=<null>,toAddresses=[MessageAddress[address=to@domain.tld,personal=<null>]],ccAddresses=[],bccAddresses=[],subject=subject,content=MessageContent[content=<hidden-value>,contentType=text/html,inlines=[]],attachments=[]],MessageContent[content=<hidden-value>,contentType=text/plain,inlines=[]],MessageContent[content=<hidden-value>,contentType=text/plain,inlines=[]]] > {alternatives.isValid} 'alternatives.size=2' must be less than or equal to '1'");
   }

   @Test
   void sendWhenInvalidAlternativeContentTypeHtml() {
      Message message = new MessageBuilder()
            .fromAddress(MessageAddress.of("from@domain.tld"))
            .toAddress(MessageAddress.of("to@domain.tld"))
            .subject("subject")
            .content(MessageContentBuilder.of("content", TEXT_HTML))
            .build();

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> smtp.sendMessage(message,
                                               MessageContentBuilder.of("alternative content", TEXT_HTML)))
            .withMessage(
                  "Invariant validation error > Context [Message[fromAddress=MessageAddress[address=from@domain.tld,personal=<null>],replyToAddress=<null>,toAddresses=[MessageAddress[address=to@domain.tld,personal=<null>]],ccAddresses=[],bccAddresses=[],subject=subject,content=MessageContent[content=<hidden-value>,contentType=text/html,inlines=[]],attachments=[]],MessageContent[content=<hidden-value>,contentType=text/html,inlines=[]]] > {alternatives.isValid} 'alternatives=[MessageContent[content=<hidden-value>,contentType=text/html,inlines=[]]]' > 'alternatives[0]=text/html' must be equal to 'text/plain' (type and subtype only)");
   }

   @Test
   void sendWhenInvalidAlternativeContentTypeText() {
      Message message = new MessageBuilder()
            .fromAddress(MessageAddress.of("from@domain.tld"))
            .toAddress(MessageAddress.of("to@domain.tld"))
            .subject("subject")
            .content(MessageContentBuilder.of("content", TEXT_PLAIN))
            .build();

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> smtp.sendMessage(message,
                                               MessageContentBuilder.of("alternative content", TEXT_PLAIN)))
            .withMessage(
                  "Invariant validation error > Context [Message[fromAddress=MessageAddress[address=from@domain.tld,personal=<null>],replyToAddress=<null>,toAddresses=[MessageAddress[address=to@domain.tld,personal=<null>]],ccAddresses=[],bccAddresses=[],subject=subject,content=MessageContent[content=<hidden-value>,contentType=text/plain,inlines=[]],attachments=[]],MessageContent[content=<hidden-value>,contentType=text/plain,inlines=[]]] > {alternatives.isValid} 'alternatives.size=1' must be equal to zero");
   }

   @Test
   void sendWhenInvalidHtmlContent() {
      Message message = new MessageBuilder()
            .fromAddress(MessageAddress.of("from@domain.tld"))
            .toAddress(MessageAddress.of("to@domain.tld"))
            .content(MessageContentBuilder.of("<strong content</str>", TEXT_HTML))
            .build();

      smtp.sendMessage(message);

      assertThat(greenMail.getReceivedMessages()).satisfiesExactly((Consumer<MimeMessage>) receivedMessage -> PartAssert
            .assertThat(receivedMessage)
            .as("HTML content must not be validated or normalized")
            .hasContentType(TEXT_HTML, "charset=UTF-8")
            .hasContent("<strong content</str>"));
   }

}
