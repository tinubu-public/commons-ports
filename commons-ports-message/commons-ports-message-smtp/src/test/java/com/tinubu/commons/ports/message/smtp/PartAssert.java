/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.message.smtp;

import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.parseMimeType;
import static com.tinubu.commons.lang.util.CheckedFunction.uncheckedFunction;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.listConcat;
import static com.tinubu.commons.lang.util.ExceptionUtils.sneakyThrow;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.stream.Collectors.joining;
import static java.util.stream.IntStream.range;
import static org.assertj.core.api.InstanceOfAssertFactories.type;

import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;
import java.util.regex.Pattern;

import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;

import org.apache.commons.io.IOUtils;
import org.assertj.core.api.AbstractAssert;
import org.assertj.core.api.Assertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinubu.commons.lang.mimetype.MimeType;

public class PartAssert extends AbstractAssert<PartAssert, Part> {

   public static final MimeType MULTIPART = parseMimeType("multipart/*");
   public static final MimeType MULTIPART_MIXED = parseMimeType("multipart/mixed");
   public static final MimeType MULTIPART_RELATED = parseMimeType("multipart/related");
   public static final MimeType MULTIPART_ALTERNATIVE = parseMimeType("multipart/alternative");
   public static final Pattern UUID_CID_PATTERN =
         Pattern.compile("<[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}>");

   private static Logger log = LoggerFactory.getLogger(PartAssert.class);

   private PartAssert(Part actual) {
      super(actual, PartAssert.class);
   }

   public static PartAssert assertThat(Part actual) {
      return new PartAssert(actual);
   }

   public PartAssert debugContent() {
      return debugContent("actual");
   }

   public PartAssert debugContent(String name) {
      isNotNull();

      try {
         log.debug("Debug '{} {}' content (content-type={}) :\n{}",
                   actual.getClass().getSimpleName(),
                   name,
                   parseMimeType(actual.getContentType()).strippedParameters(),
                   IOUtils.toString(actual.getInputStream(), UTF_8));
      } catch (MessagingException | IOException e) {
         throw sneakyThrow(e);
      }

      return this;
   }

   public PartAssert matchesMimeType(MimeType mimeType) {
      isNotNull();

      try {
         if (!actual.isMimeType(mimeType.toString())) {
            failWithMessage("Expected Part to match '%s' MIME type but was '%s'",
                            mimeType,
                            actual.getContentType());
         }
         return this;
      } catch (MessagingException e) {
         throw sneakyThrow(e);
      }
   }

   public PartAssert hasContentType(MimeType contentType, String... attributes) {
      isNotNull();

      try {
         String[] headerAttributes = actual.getContentType().split("\\s*;\\s*");
         Assertions
               .assertThat(headerAttributes)
               .as(inheritDescription(
                     "Expected Part to contain '%s' content-type and %s attributes but was '%s'",
                     contentType,
                     stream(attributes).collect(joining(";", "[", "]")),
                     actual.getContentType()))
               .containsAll(listConcat(stream(contentType.toString()), stream(attributes)));
         return this;
      } catch (MessagingException e) {
         throw sneakyThrow(e);
      }
   }

   public PartAssert hasExactlyContentType(MimeType contentType, String... attributes) {
      isNotNull();

      try {
         String[] headerAttributes = actual.getContentType().split("\\s*;\\s*");
         Assertions
               .assertThat(headerAttributes)
               .as(inheritDescription(
                     "Expected Part to contain '%s' content-type and %s attributes but was '%s'",
                     contentType,
                     stream(attributes).collect(joining(";", "[", "]")),
                     actual.getContentType()))
               .containsExactlyInAnyOrderElementsOf(listConcat(stream(contentType.toString()),
                                                               stream(attributes)));
         return this;
      } catch (MessagingException e) {
         throw sneakyThrow(e);
      }
   }

   @SafeVarargs
   public final PartAssert contentType(Consumer<? super String>... requirements) {
      isNotNull();

      try {
         Assertions.assertThat(actual.getContentType()).as(inheritDescription()).satisfies(requirements);
         return this;
      } catch (MessagingException e) {
         throw sneakyThrow(e);
      }
   }

   public PartAssert hasDisposition(String disposition) {
      isNotNull();

      try {
         if (actual.getDisposition() == null || !actual.getDisposition().equalsIgnoreCase(disposition)) {
            failWithMessage("Expected Part to have '%s' disposition but was '%s'",
                            disposition,
                            actual.getDisposition());
         }
         return this;
      } catch (MessagingException e) {
         throw sneakyThrow(e);
      }
   }

   public PartAssert hasFileName(String fileName) {
      isNotNull();

      try {
         if (actual.getFileName() == null || !actual.getFileName().equals(fileName)) {
            failWithMessage("Expected Part to have '%s' fileName but was '%s'",
                            fileName,
                            actual.getDisposition());
         }
         return this;
      } catch (MessagingException e) {
         throw sneakyThrow(e);
      }
   }

   public PartAssert fileName(Consumer<? super String> requirement) {
      isNotNull();

      try {
         Assertions.assertThat(actual.getFileName()).as(inheritDescription()).satisfies(requirement);
         return this;
      } catch (MessagingException e) {
         throw sneakyThrow(e);
      }
   }

   public PartAssert hasContentId(String contentId) {
      return contentId(cid -> Assertions.assertThat(cid).as(inheritDescription()).isEqualTo(contentId));
   }

   public PartAssert isGeneratedContentId() {
      return contentId(cid -> Assertions
            .assertThat(cid)
            .as(inheritDescription())
            .containsPattern(UUID_CID_PATTERN));
   }

   @SafeVarargs
   public final PartAssert contentId(Consumer<? super String>... requirements) {
      return header("content-id", requirements);
   }

   /**
    * Checks part have specified header (case-insensitive) one or more times, exactly matching specified
    * values.
    *
    * @param header header name
    * @param values header values if present multiple times
    */
   public PartAssert hasHeader(String header, String... values) {
      isNotNull();

      try {
         Assertions
               .assertThat(actual.getHeader(header))
               .as(inheritDescription("'%s' header must have %s values",
                                      header,
                                      stream(values).collect(joining(",", "[", "]"))))
               .containsExactly(values);
         return this;
      } catch (MessagingException e) {
         throw sneakyThrow(e);
      }
   }

   @SafeVarargs
   public final PartAssert header(String header, Consumer<? super String>... requirements) {
      isNotNull();

      try {
         Assertions
               .assertThat(actual.getHeader(header))
               .as(inheritDescription())
               .satisfiesExactly(requirements);
         return this;
      } catch (MessagingException e) {
         throw sneakyThrow(e);
      }
   }

   public PartAssert hasNoHeader(String header) {
      isNotNull();

      try {
         Assertions
               .assertThat(actual.getHeader(header))
               .as(inheritDescription("'%s' header must not be present", header))
               .isNull();
         return this;
      } catch (MessagingException e) {
         throw sneakyThrow(e);
      }
   }

   public PartAssert hasContent(String content) {
      isNotNull();

      try {
         Object partContent = actual.getContent();
         if (!(partContent instanceof String && partContent.equals(content))) {
            failWithMessage("Expected Part to have '%s' content but was '%s'", content, actual.getContent());
         }
         return this;
      } catch (MessagingException | IOException e) {
         throw sneakyThrow(e);
      }
   }

   public PartAssert hasEmptyContent() {
      return hasContent("");
   }

   @SafeVarargs
   public final PartAssert hasContent(Consumer<? super Part>... requirements) {
      isNotNull();
      matchesMimeType(MULTIPART);

      try {
         Assertions.assertThat(actual.getContent()).as(inheritDescription())
               .asInstanceOf(type(Multipart.class))
               .satisfies(multipart -> {
                  List<Part> parts = list(range(0, multipart.getCount())
                                                .boxed()
                                                .map(uncheckedFunction(multipart::getBodyPart))
                                                .map(Part.class::cast));

                  Assertions.assertThat(parts).satisfiesExactly(requirements);
               });

         return this;
      } catch (MessagingException | IOException e) {
         throw sneakyThrow(e);
      }
   }

   protected String inheritDescription(String description, Object... args) {
      String composedDescription = "";
      if (getWritableAssertionInfo().hasDescription()) {
         composedDescription = getWritableAssertionInfo().descriptionText() + " > ";
      }
      return composedDescription + String.format(description, args);
   }

   protected String inheritDescription() {
      String composedDescription = "";
      if (getWritableAssertionInfo().hasDescription()) {
         composedDescription = getWritableAssertionInfo().descriptionText();
      }
      return composedDescription;
   }

}
