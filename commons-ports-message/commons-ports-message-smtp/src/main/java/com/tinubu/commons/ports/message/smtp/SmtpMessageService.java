/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.message.smtp;

import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObjectName;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.satisfiesValue;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.as;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.property;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.allSatisfies;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.size;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isLessThanOrEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.NumberRules.isZero;
import static com.tinubu.commons.ddd2.invariant.rules.OptionalRules.optionalValue;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_OCTET_STREAM;
import static com.tinubu.commons.lang.util.CheckedConsumer.uncheckedConsumer;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.util.StreamUtils.streamConcat;
import static com.tinubu.commons.lang.util.Try.handleException;
import static com.tinubu.commons.ports.message.domain.rules.MessageContentRules.isHtml;
import static com.tinubu.commons.ports.message.domain.rules.MessageContentRules.isTextPlain;
import static javax.mail.internet.MimeUtility.mimeCharset;
import static org.springframework.mail.javamail.MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED;
import static org.springframework.mail.javamail.MimeMessageHelper.MULTIPART_MODE_NO;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import javax.activation.DataSource;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;

import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Lazy;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import com.tinubu.commons.ddd2.domain.event.RegistrableDomainEventService;
import com.tinubu.commons.ddd2.domain.event.SynchronousDomainEventService;
import com.tinubu.commons.ddd2.invariant.Invariant;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ddd2.invariant.Invariants;
import com.tinubu.commons.ddd2.invariant.formatter.FastStringFormat;
import com.tinubu.commons.ddd2.invariant.rules.ArrayRules;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.lang.util.Try;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.message.domain.AbstractMessageService;
import com.tinubu.commons.ports.message.domain.Message;
import com.tinubu.commons.ports.message.domain.MessageAddress;
import com.tinubu.commons.ports.message.domain.MessageContent;
import com.tinubu.commons.ports.message.domain.MessageDocument;
import com.tinubu.commons.ports.message.domain.MessageIOException;
import com.tinubu.commons.ports.message.domain.MessageService;
import com.tinubu.commons.ports.message.domain.rules.MessageRules;

/**
 * SMTP {@link MessageService} adapter implementation.
 *
 * @implNote Component is {@link Lazy} and named to support customized adapter initialization and
 *       referencing.
 */
// TODO configurable MULTIPART MODE ?
// FIXME close document input stream ? introduce a custom, streamed, auto-closeable, Datasource ?
public class SmtpMessageService extends AbstractMessageService {
   /** Multipart mode to use for client compatibility. */
   private static final int DEFAULT_MULTIPART_MODE = MULTIPART_MODE_MIXED_RELATED;
   /** Fallback MIME charset to use for address's personal encoding when not pure US-ASCII. */
   private static final String FALLBACK_PERSONAL_MIME_CHARSET = mimeCharset(StandardCharsets.UTF_8.name());
   /** Default content-type for attachments and inlines. */
   private static final MimeType DEFAULT_ATTACHMENT_CONTENT_TYPE = APPLICATION_OCTET_STREAM;

   private static final Logger log = LoggerFactory.getLogger(SmtpMessageService.class);

   private final JavaMailSender javaMailSender;

   public SmtpMessageService(JavaMailSender javaMailSender, RegistrableDomainEventService eventService) {
      super(eventService);
      this.javaMailSender = javaMailSender;
   }

   public SmtpMessageService(JavaMailSender javaMailSender) {
      this(javaMailSender, new SynchronousDomainEventService());
   }

   @Override
   public void sendMessage(Message message, MessageContent... alternatives) {
      validateMessages(message, alternatives);

      StopWatch watch = StopWatch.createStarted();

      try {
         MimeMessage mimeMessage = javaMailSender.createMimeMessage();

         MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, multiPartMode(message, alternatives));

         helper.setFrom(internetAddress(message.fromAddress()).orElseThrow());
         message
               .replyToAddress()
               .ifPresent(replyTo -> internetAddress(replyTo).ifSuccess(uncheckedConsumer(helper::setReplyTo)));
         helper.setTo(internetAddresses(message.toAddresses()));
         helper.setCc(internetAddresses(message.ccAddresses()));
         helper.setBcc(internetAddresses(message.bccAddresses()));
         message.subject().ifPresent(uncheckedConsumer(helper::setSubject));

         if (message.content().isPresent()) {
            MessageContent messageContent = message.content().get();

            if (alternatives.length > 0) {
               helper.setText(alternatives[0].content(), messageContent.content());
            } else {
               helper.setText(messageContent.content(), messageContent.isHtml());
            }

            for (MessageDocument inline : messageContent.inlines()) {
               helper.addInline(inline.messageDocumentId(), documentDataSource(inline));
            }
         } else {
            helper.setText("", false);
         }

         for (MessageDocument attachment : message.attachments()) {
            helper.addAttachment(attachment.metadata().documentName(), documentDataSource(attachment));
         }

         javaMailSender.send(mimeMessage);

         messageSent(message, watch);
      } catch (MessagingException | MailException | IOException e) {
         throw new MessageIOException(e.getMessage(), e);
      }
   }

   private static Try<InternetAddress, Exception> internetAddress(MessageAddress address) {
      return handleException(() -> {
         InternetAddress ia = new InternetAddress(address.address());
         ia.setPersonal(address.personal(), FALLBACK_PERSONAL_MIME_CHARSET);
         return ia;
      });
   }

   private static InternetAddress[] internetAddresses(List<MessageAddress> addresses) {
      return addresses
            .stream()
            .flatMap(address -> internetAddress(address).stream())
            .toArray(InternetAddress[]::new);
   }

   /**
    * Defines the multipart mode for the given message. Result nan be no multipart.
    *
    * @param message message
    *
    * @return multipart mode
    */
   private static int multiPartMode(Message message, MessageContent... alternatives) {
      boolean noMultipart = message.attachments().isEmpty() && message
            .content()
            .map(MessageContent::inlines)
            .orElse(list())
            .isEmpty() && alternatives.length == 0;

      return noMultipart ? MULTIPART_MODE_NO : DEFAULT_MULTIPART_MODE;
   }

   /**
    * Validates message with SMTP restrictions.
    *
    * @param message message to validate
    *
    * @throws InvariantValidationException if validation fails
    */
   private static void validateMessages(Message message, MessageContent... alternatives) {
      Invariants invariants = Invariants
            .empty()
            .withInvariants(isValidMessage(message, "message"))
            .withInvariants(isValidAlternatives(message, alternatives),
                            hasUniqueMessageDocumentIds(message, "message", alternatives))
            .context(streamConcat(stream(message), stream(alternatives)).toArray(Object[]::new));

      invariants.validate("default").orThrow();
      invariants.validate("optional").orLog(log);
   }

   private static Invariant<MessageContent[]> isValidAlternatives(Message message,
                                                                  MessageContent[] alternatives) {
      return invariant(alternatives, "isValid", "alternatives",
                       ArrayRules.list(size(isLessThanOrEqualTo(value(1)).andValue(isZero().ifIsSatisfied(
                             message,
                             MessageRules.isTextPlain()))).<List<MessageContent>>andValue(allSatisfies(
                             isTextPlain()).ifIsSatisfied(message, MessageRules.isHtml()))));
   }

   private static Invariant<Message> hasUniqueMessageDocumentIds(Message message,
                                                                 String objectName,
                                                                 MessageContent[] alternatives) {
      return invariant(message,
                       "hasUniqueMessageDocumentIds",
                       objectName,
                       MessageRules.hasUniqueMessageDocumentIds(value(alternatives, "alternatives")));
   }

   private static Invariants isValidMessage(Message message, String objectName) {
      return Invariants.of(hasValidAddress(message, objectName, "fromAddress", Message::fromAddress),
                           hasValidOptionalAddress(message,
                                                   objectName,
                                                   "replyToAddress",
                                                   Message::replyToAddress).groups("optional"),
                           hasValidAddresses(message, objectName, "toAddresses", Message::toAddresses).groups(
                                 "optional"),
                           hasValidAddresses(message, objectName, "ccAddresses", Message::ccAddresses).groups(
                                 "optional"),
                           hasValidAddresses(message,
                                             objectName,
                                             "bccAddresses",
                                             Message::bccAddresses).groups("optional"),
                           invariant(message,
                                     "hasSupportedContentType",
                                     objectName,
                                     hasSupportedContentType()));
   }

   private static <T> Invariant<T> invariant(T object,
                                             String invariantName,
                                             String objectName,
                                             InvariantRule<T> rule) {
      return Invariant.of(() -> object, rule).name(objectName + "." + invariantName).objectName(objectName);
   }

   private static Invariant<Message> hasValidAddress(Message message, String objectName, String addressField,
                                                     Function<Message, MessageAddress> address) {
      return invariant(message, addressField, objectName, property(address, addressField, hasValidAddress()));
   }

   private static Invariant<Message> hasValidAddresses(Message message,
                                                       String objectName,
                                                       String addressesField,
                                                       Function<Message, List<MessageAddress>> addresses) {
      return invariant(message, addressesField,
                       objectName,
                       property(addresses, addressesField, allSatisfies(hasValidAddress())));
   }

   private static Invariant<Message> hasValidOptionalAddress(Message message,
                                                             String objectName,
                                                             String addressField,
                                                             Function<Message, Optional<MessageAddress>> address) {
      return invariant(message, addressField,
                       objectName,
                       property(address, addressField, optionalValue(hasValidAddress())));
   }

   /**
    * Checks if message address is a valid RFC 822 address.
    *
    * @return invariant rule
    */
   private static InvariantRule<MessageAddress> hasValidAddress() {
      return as(MessageAddress::address, satisfiesValue(address -> {
         try {
            new InternetAddress(address);
            return true;
         } catch (AddressException e) {
            return false;
         }
      }, FastStringFormat.of("'", validatingObjectName(), "' is not a valid RFC 822 address"))).ruleContext(
            "EmailAddress.hasValidAddress");
   }

   /**
    * Checks if message content content-type is supported.
    *
    * @return invariant rule
    */
   public static InvariantRule<Message> hasSupportedContentType() {
      return property(Message::content, "content", optionalValue(isHtml().orValue(isTextPlain())));
   }

   /**
    * Builds {@link DataSource} from {@link Document}. Ensures data source has correct content encoding from
    * specified document.
    *
    * @param document document to builds data source from
    *
    * @throws IOException if an I/O error occurs
    */
   private DataSource documentDataSource(Document document) throws IOException {
      try (InputStream documentContent = document.content().inputStreamContent()) {
         return new ByteArrayDataSource(documentContent, documentContentType(document));
      }
   }

   /**
    * Builds document content type MIME as a String including the charset from document encoding if any.
    *
    * @param document document to extract content type
    *
    * @return document content type including the charset if any
    */
   private String documentContentType(Document document) {
      MimeType contentType = document.metadata().contentType().orElse(DEFAULT_ATTACHMENT_CONTENT_TYPE);

      return contentType.toString();
   }

}
