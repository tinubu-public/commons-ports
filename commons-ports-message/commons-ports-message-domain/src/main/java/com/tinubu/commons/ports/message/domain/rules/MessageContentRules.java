/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.message.domain.rules;

import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.as;
import static com.tinubu.commons.ddd2.invariant.rules.MimeTypeRules.isTypeAndSubtypeEqualTo;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_XHTML;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_HTML;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_PLAIN;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry;
import com.tinubu.commons.ports.message.domain.MessageContent;

/**
 * {@link MessageContent} validation rules.
 */
public class MessageContentRules {

   /**
    * Checks if message content is {@link PresetMimeTypeRegistry#TEXT_HTML} or
    * {@link PresetMimeTypeRegistry#APPLICATION_XHTML}.
    */
   public static InvariantRule<MessageContent> isHtml() {
      return as(MessageContent::contentType,
                isTypeAndSubtypeEqualTo(value(APPLICATION_XHTML)).orValue(isTypeAndSubtypeEqualTo(value(
                      TEXT_HTML))));
   }

   /**
    * Checks if message content is {@link PresetMimeTypeRegistry#TEXT_PLAIN}.
    */
   public static InvariantRule<MessageContent> isTextPlain() {
      return as(MessageContent::contentType, isTypeAndSubtypeEqualTo(value(TEXT_PLAIN)));
   }
}
