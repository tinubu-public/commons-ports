/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.message.domain;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.lang.util.StreamUtils.stream;

import org.apache.commons.lang3.time.StopWatch;

import com.tinubu.commons.ddd2.domain.event.DomainEvent;
import com.tinubu.commons.ddd2.domain.event.DomainEventListener;
import com.tinubu.commons.ddd2.domain.event.RegistrableDomainEventService;
import com.tinubu.commons.ddd2.domain.event.SynchronousDomainEventService;
import com.tinubu.commons.ports.message.domain.event.MessageSent;
import com.tinubu.commons.ports.message.domain.event.MessageServiceEvent;

public abstract class AbstractMessageService implements MessageService {

   protected final RegistrableDomainEventService eventService;

   public AbstractMessageService(RegistrableDomainEventService eventService) {
      this.eventService = validate(eventService, "eventService", isNotNull()).orThrow();
   }

   public AbstractMessageService() {
      this(new SynchronousDomainEventService());
   }

   public RegistrableDomainEventService eventService() {
      return eventService;
   }

   @Override
   @SuppressWarnings("unchecked")
   public <E extends DomainEvent> void registerEventListener(DomainEventListener<? super E> eventListener,
                                                             Class<? extends E>... eventClasses) {
      stream(eventClasses).forEach(eventClass -> this.eventService.registerEventListener(eventClass,
                                                                                         eventListener));
   }

   @Override
   public void unregisterEventListeners() {
      eventService.unregisterEventListeners();
   }

   public void publishEvent(MessageServiceEvent event) {
      eventService.publishEvent(event);
   }

   public void messageSent(Message message, StopWatch watch) {
      publishEvent(new MessageSent(this, message, watch));
   }

}
