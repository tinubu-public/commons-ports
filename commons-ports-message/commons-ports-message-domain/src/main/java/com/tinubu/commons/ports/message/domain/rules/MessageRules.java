/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.message.domain.rules;

import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.as;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.property;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.hasNoDuplicates;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.isNotEmpty;
import static com.tinubu.commons.ddd2.invariant.rules.OptionalRules.optionalValue;
import static com.tinubu.commons.lang.util.CollectionUtils.listConcat;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.util.StreamUtils.streamConcat;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.Objects;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.ParameterValue;
import com.tinubu.commons.ddd2.invariant.formatter.FastStringFormat;
import com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry;
import com.tinubu.commons.ports.message.domain.Message;
import com.tinubu.commons.ports.message.domain.MessageContent;
import com.tinubu.commons.ports.message.domain.MessageDocument;

/**
 * {@link Message} validation rules.
 */
public class MessageRules {

   /**
    * All message documents, including attachments and inlines, must have a unique identifier.
    *
    * @param alternatives optional alternative contents
    *
    * @return invariant rule
    */
   public static InvariantRule<Message> hasUniqueMessageDocumentIds(ParameterValue<MessageContent[]> alternatives) {
      notNull(alternatives, "alternatives");

      return property(message -> listConcat(stream(message.attachments()),
                                            streamConcat(stream(message.content()),
                                                         stream(alternatives.value())).flatMap(c -> stream(c.inlines()))),
                      "documents",
                      hasNoDuplicates(MessageDocument::messageDocumentId));
   }

   public static InvariantRule<Message> hasUniqueMessageDocumentIds() {
      return hasUniqueMessageDocumentIds(value(new MessageContent[0]));
   }

   /**
    * At least one address must be specified among {@link Message#toAddresses()},
    * {@link Message#ccAddresses()},
    * {@link Message#bccAddresses()}.
    *
    * @return invariant rule
    */
   public static InvariantRule<Message> hasAtLeastOneAddress() {
      return as(message -> listConcat(stream(message.toAddresses()),
                                      stream(message.ccAddresses()),
                                      stream(message.bccAddresses()).filter(Objects::nonNull)),
                isNotEmpty(FastStringFormat.of(
                      "At least one address must be present among to/cc/bcc addresses")));
   }

   /**
    * Checks if message content is {@link PresetMimeTypeRegistry#TEXT_HTML} or
    * {@link PresetMimeTypeRegistry#APPLICATION_XHTML}.
    */
   public static InvariantRule<Message> isHtml() {
      return property(Message::content, "content", optionalValue(MessageContentRules.isHtml()));
   }

   /**
    * Checks if message content is {@link PresetMimeTypeRegistry#TEXT_PLAIN}.
    */
   public static InvariantRule<Message> isTextPlain() {
      return property(Message::content, "content", optionalValue(MessageContentRules.isTextPlain()));
   }

}

