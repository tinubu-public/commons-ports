/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.message.domain;

import static com.tinubu.commons.lang.util.CollectionUtils.list;

import com.tinubu.commons.ddd2.domain.event.DomainEvent;
import com.tinubu.commons.ddd2.domain.event.DomainEventListener;
import com.tinubu.commons.ports.message.domain.event.LoggingMessageServiceListener;
import com.tinubu.commons.ports.message.domain.event.MessageSent;
import com.tinubu.commons.ports.message.domain.metrology.MessageServiceMetrology;

/**
 * Message communication service.
 *
 * @implSpec Service implementations can implement {@link #close()} to release resources on
 *       service release.
 */
public interface MessageService extends AutoCloseable {

   /**
    * Sends a message with optional alternatives.
    * Supported messages content format and content-type depends on implementation capabilities.
    *
    * @param message message to send
    * @param alternatives message alternative content
    *
    * @throws MessageIOException if an error occurs while sending message
    */
   void sendMessage(Message message, MessageContent... alternatives);

   /**
    * Registers event listeners for this service.
    *
    * @param eventListener event listener
    * @param eventClasses event type classes
    * @param <E> event type
    */
   @SuppressWarnings("unchecked")
   <E extends DomainEvent> void registerEventListener(DomainEventListener<? super E> eventListener,
                                                      Class<? extends E>... eventClasses);

   /**
    * Registers logging event listener for this service.
    *
    * @param eventClasses event type classes
    * @param <E> event type
    */
   @SuppressWarnings("unchecked")
   default <E extends DomainEvent> void registerEventLoggingListener(Class<? extends E>... eventClasses) {
      registerEventListener(new LoggingMessageServiceListener<>(), eventClasses);
   }

   /**
    * Registers logging event listener for all events for this message service.
    */
   @SuppressWarnings("unchecked")
   default void registerEventsLoggingListener() {
      list(MessageSent.class).forEach(this::registerEventLoggingListener);
   }

   /**
    * Registers a new {@link MessageServiceMetrology metrology} component.
    *
    * @param metrology metrology component
    */
   default <T extends MessageServiceMetrology> void registerMetrology(T metrology) {
      registerEventListener(metrology, DomainEvent.class);
   }

   /**
    * Unregisters all event listeners.
    * Note that this also unregisters metrology.
    */
   void unregisterEventListeners();

   /**
    * Default no-op close operation.
    */
   @Override
   default void close() { }

}
