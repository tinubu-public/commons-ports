/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.message.domain;

import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.parseMimeType;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_HTML;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.lang.datetime.ApplicationClock;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.message.domain.Message.MessageBuilder;
import com.tinubu.commons.ports.message.domain.MessageContent.MessageContentBuilder;
import com.tinubu.commons.ports.message.domain.MessageDocument.MessageDocumentBuilder;

@TestInstance(Lifecycle.PER_CLASS)
public class MessageTest {

   private static final ZonedDateTime FIXED_DATE =
         ZonedDateTime.of(LocalDateTime.of(2021, 11, 10, 14, 0, 0), ZoneId.of("UTC"));

   @BeforeEach
   public void setApplicationClock() {
      ApplicationClock.setFixedClock(FIXED_DATE);
   }

   @Test
   public void testMessageWhenNominal() {
      assertThat(new MessageBuilder()
                       .fromAddress(MessageAddress.of("noreply@domain.tld", "No reply"))
                       .toAddress(MessageAddress.of("test@domain.tld"))
                       .subject("subject")
                       .content(MessageContentBuilder.of("content", parseMimeType("text/plain")))
                       .attachments(new MessageDocumentBuilder()
                                          .documentId(DocumentPath.of("document.txt"))
                                          .loadedContent("document content", StandardCharsets.UTF_8)
                                          .build())).isNotNull();
   }

   @Test
   public void testMessageWhenEmptyContent() {
      assertThat(new MessageBuilder()
                       .fromAddress(MessageAddress.of("noreply@domain.tld", "No reply"))
                       .toAddress(MessageAddress.of("test@domain.tld"))
                       .subject("subject")
                       .content(MessageContentBuilder.of("", parseMimeType("text/plain")))
                       .build()).isNotNull();
   }

   @Test
   public void testMessageWhenNoContent() {
      assertThat(new MessageBuilder()
                       .fromAddress(MessageAddress.of("noreply@domain.tld", "No reply"))
                       .toAddress(MessageAddress.of("test@domain.tld"))
                       .subject("subject")
                       .build()).isNotNull();
   }

   @Test
   public void testMessageWhenDuplicatedMessageDocumentIds() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new MessageBuilder()
                  .fromAddress(MessageAddress.of("noreply@domain.tld"))
                  .toAddress(MessageAddress.of("test@domain.tld"))
                  .subject("subject")
                  .content(MessageContentBuilder.of("content",
                                                    parseMimeType("text/plain"),
                                                    new MessageDocumentBuilder()
                                                          .messageDocumentId("sameid")
                                                          .documentId(DocumentPath.of("inline1.txt"))
                                                          .loadedContent("inline1 content",
                                                                         StandardCharsets.UTF_8)
                                                          .build(),
                                                    new MessageDocumentBuilder()
                                                          .messageDocumentId("sameid")
                                                          .documentId(DocumentPath.of("inline2.txt"))
                                                          .loadedContent("inline2 content",
                                                                         StandardCharsets.UTF_8)
                                                          .build()))
                  .attachments(new MessageDocumentBuilder()
                                     .messageDocumentId("sameid")
                                     .documentId(DocumentPath.of("attachment1.txt"))
                                     .loadedContent("attachment1 content", StandardCharsets.UTF_8)
                                     .build(),
                               new MessageDocumentBuilder()
                                     .messageDocumentId("sameid")
                                     .documentId(DocumentPath.of("attachment2.txt"))
                                     .loadedContent("attachment2 content", StandardCharsets.UTF_8)
                                     .build())
                  .build())
            .withMessage(
                  "Invariant validation error > Context [Message[fromAddress=MessageAddress[address=noreply@domain.tld,personal=<null>],replyToAddress=<null>,toAddresses=[MessageAddress[address=test@domain.tld,personal=<null>]],ccAddresses=[],bccAddresses=[],subject=subject,content=MessageContent[content=<hidden-value>,contentType=text/plain,inlines=[sameid->DocumentPath[value=inline1.txt,newObject=false],sameid->DocumentPath[value=inline2.txt,newObject=false]]],attachments=[sameid->DocumentPath[value=attachment1.txt,newObject=false],sameid->DocumentPath[value=attachment2.txt,newObject=false]]]] > "
                  + "'documents=[MessageDocument[documentId=DocumentPath[value=attachment1.txt,newObject=false],metadata=DocumentMetadata[documentPath=attachment1.txt,contentType=text/plain;charset=UTF-8,contentSize=19,creationDate=2021-11-10T14:00:00Z,lastUpdateDate=2021-11-10T14:00:00Z,attributes={}],content=LoadedDocumentContent[content=<hidden-value>,contentEncoding=UTF-8],outputBufferSize=8192,inputBufferSize=8192,messageDocumentId=sameid], MessageDocument[documentId=DocumentPath[value=attachment2.txt,newObject=false],metadata=DocumentMetadata[documentPath=attachment2.txt,contentType=text/plain;charset=UTF-8,contentSize=19,creationDate=2021-11-10T14:00:00Z,lastUpdateDate=2021-11-10T14:00:00Z,attributes={}],content=LoadedDocumentContent[content=<hidden-value>,contentEncoding=UTF-8],outputBufferSize=8192,inputBufferSize=8192,messageDocumentId=sameid], MessageDocument[documentId=DocumentPath[value=inline1.txt,newObject=false],metadata=DocumentMetadata[documentPath=inline1.txt,contentType=text/plain;charset=UTF-8,contentSize=15,creationDate=2021-11-10T14:00:00Z,lastUpdateDate=2021-11-10T14:00:00Z,attributes={}],content=LoadedDocumentContent[content=<hidden-value>,contentEncoding=UTF-8],outputBufferSize=8192,inputBufferSize=8192,messageDocumentId=sameid], MessageDocument[documentId=DocumentPath[value=inline2.txt,newObject=false],metadata=DocumentMetadata[documentPath=inline2.txt,contentType=text/plain;charset=UTF-8,contentSize=15,creationDate=2021-11-10T14:00:00Z,lastUpdateDate=2021-11-10T14:00:00Z,attributes={}],content=LoadedDocumentContent[content=<hidden-value>,contentEncoding=UTF-8],outputBufferSize=8192,inputBufferSize=8192,messageDocumentId=sameid]]' has duplicates : "
                  + "{"
                  + "sameid->["
                  + "MessageDocument[documentId=DocumentPath[value=attachment1.txt,newObject=false],metadata=DocumentMetadata[documentPath=attachment1.txt,contentType=text/plain;charset=UTF-8,contentSize=19,creationDate=2021-11-10T14:00:00Z,lastUpdateDate=2021-11-10T14:00:00Z,attributes={}],content=LoadedDocumentContent[content=<hidden-value>,contentEncoding=UTF-8],outputBufferSize=8192,inputBufferSize=8192,messageDocumentId=sameid],"
                  + "MessageDocument[documentId=DocumentPath[value=attachment2.txt,newObject=false],metadata=DocumentMetadata[documentPath=attachment2.txt,contentType=text/plain;charset=UTF-8,contentSize=19,creationDate=2021-11-10T14:00:00Z,lastUpdateDate=2021-11-10T14:00:00Z,attributes={}],content=LoadedDocumentContent[content=<hidden-value>,contentEncoding=UTF-8],outputBufferSize=8192,inputBufferSize=8192,messageDocumentId=sameid],"
                  + "MessageDocument[documentId=DocumentPath[value=inline1.txt,newObject=false],metadata=DocumentMetadata[documentPath=inline1.txt,contentType=text/plain;charset=UTF-8,contentSize=15,creationDate=2021-11-10T14:00:00Z,lastUpdateDate=2021-11-10T14:00:00Z,attributes={}],content=LoadedDocumentContent[content=<hidden-value>,contentEncoding=UTF-8],outputBufferSize=8192,inputBufferSize=8192,messageDocumentId=sameid],"
                  + "MessageDocument[documentId=DocumentPath[value=inline2.txt,newObject=false],metadata=DocumentMetadata[documentPath=inline2.txt,contentType=text/plain;charset=UTF-8,contentSize=15,creationDate=2021-11-10T14:00:00Z,lastUpdateDate=2021-11-10T14:00:00Z,attributes={}],content=LoadedDocumentContent[content=<hidden-value>,contentEncoding=UTF-8],outputBufferSize=8192,inputBufferSize=8192,messageDocumentId=sameid]"
                  + "]"
                  + "}");
   }

   @Test
   public void testAddressesInvariantsWhenNoAddresses() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new MessageBuilder()
                  .fromAddress(MessageAddress.of("noreply@domain.tld"))
                  .content(MessageContentBuilder.of("content", parseMimeType("text/plain")))
                  .build())
            .withMessage(
                  "Invariant validation error > Context [Message[fromAddress=MessageAddress[address=noreply@domain.tld,personal=<null>],replyToAddress=<null>,toAddresses=[],ccAddresses=[],bccAddresses=[],subject=<null>,content=MessageContent[content=<hidden-value>,contentType=text/plain,inlines=[]],attachments=[]]] > At least one address must be present among to/cc/bcc addresses");
   }

   @Test
   public void testAddressesInvariantsWhenOneAddressInToOrCcOrBcc() {
      assertThat(new MessageBuilder()
                       .fromAddress(MessageAddress.of("noreply@domain.tld"))
                       .toAddress(MessageAddress.of("test@domain.tld"))
                       .content(MessageContentBuilder.of("content",
                                                         parseMimeType("text/plain")))).isNotNull();

      assertThat(new MessageBuilder()
                       .fromAddress(MessageAddress.of("noreply@domain.tld"))
                       .ccAddresses(MessageAddress.of("test@domain.tld"))
                       .content(MessageContentBuilder.of("content",
                                                         parseMimeType("text/plain")))).isNotNull();

      assertThat(new MessageBuilder()
                       .fromAddress(MessageAddress.of("noreply@domain.tld"))
                       .bccAddresses(MessageAddress.of("test@domain.tld"))
                       .content(MessageContentBuilder.of("content",
                                                         parseMimeType("text/plain")))).isNotNull();
   }

   @Test
   void shouldThrowInvariantValidationExceptionForNullToAddress() {
      MessageBuilder messageBuilder = new MessageBuilder()
            .fromAddress(MessageAddress.of("from@domain.tld"))
            .toAddress(null)
            .content(MessageContentBuilder.of("content", TEXT_HTML));

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(messageBuilder::build)
            .withMessageContaining("{toAddresses} 'toAddresses=[null]' > 'toAddresses[0]' must not be null");
   }

   @Test
   void shouldThrowInvariantValidationExceptionForToEmptyAddress() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new MessageBuilder()
                  .fromAddress(MessageAddress.of("from@domain.tld"))
                  .toAddresses(list(MessageAddress.of(""))))
            .withMessageContaining("'address' must not be blank");
   }

   @ParameterizedTest
   @MethodSource("inputs")
   void shouldThrowInvariantValidationExceptionForCcEmptyAddress(String address, String suffix) {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new MessageBuilder()
                  .fromAddress(MessageAddress.of("from@domain.tld"))
                  .addToAddresses(MessageAddress.of("to@domain.tld", "toName"))
                  .addCcAddresses(MessageAddress.of(address)))
            .withMessageContaining("'address' must not be %s", suffix);
   }

   @ParameterizedTest
   @MethodSource("inputs")
   void shouldThrowInvariantValidationExceptionForBccEmptyAddress(String address, String suffix) {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new MessageBuilder()
                  .fromAddress(MessageAddress.of("from@domain.tld"))
                  .addToAddresses(MessageAddress.of("to@domain.tld", "toName"))
                  .addBccAddresses(MessageAddress.of(address)))
            .withMessageContaining("'address' must not be %s", suffix);
   }

   private Stream<Arguments> inputs() {
      return Stream.of(arguments("", "blank"), arguments(null, "null"));
   }

}