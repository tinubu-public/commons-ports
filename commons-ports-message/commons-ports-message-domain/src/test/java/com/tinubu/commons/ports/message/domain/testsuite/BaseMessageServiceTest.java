/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.message.domain.testsuite;

import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_PLAIN;
import static org.mockito.Mockito.mock;

import org.mockito.Mockito;

import com.tinubu.commons.ddd2.domain.event.DomainEvent;
import com.tinubu.commons.ddd2.domain.event.DomainEventListener;
import com.tinubu.commons.ports.message.domain.Message;
import com.tinubu.commons.ports.message.domain.Message.MessageBuilder;
import com.tinubu.commons.ports.message.domain.MessageAddress;
import com.tinubu.commons.ports.message.domain.MessageContent.MessageContentBuilder;
import com.tinubu.commons.ports.message.domain.MessageService;
import com.tinubu.commons.ports.message.domain.event.MessageSent;

public abstract class BaseMessageServiceTest {

   /**
    * Message service implementation under test.
    *
    * @return message service under test
    */
   protected abstract MessageService messageService();

   /**
    * Instruments message service under test with a mocked listener for all events.
    * This function should not be called several times in a given test method, instead use
    * {@link Mockito#clearInvocations(Object[])} to reset verify invocation counts.
    *
    * @param events events to register
    *
    * @return mocked listener for further test verification
    */
   @SuppressWarnings("unchecked")
   protected DomainEventListener<DomainEvent> instrumentMessageServiceListeners(Class<? extends DomainEvent>... events) {
      DomainEventListener<DomainEvent> domainEventListener =
            (DomainEventListener<DomainEvent>) mock(DomainEventListener.class);
      messageService().registerEventListener(domainEventListener, events);

      return domainEventListener;
   }

   /**
    * Instruments message service under test with a mocked listener for all events.
    * This function should not be called several times in a given test method, instead use
    * {@link Mockito#clearInvocations(Object[])} to reset verify invocation counts.
    *
    * @return mocked listener for further test verification
    */
   @SuppressWarnings("unchecked")
   protected DomainEventListener<DomainEvent> instrumentMessageServiceListeners() {
      return instrumentMessageServiceListeners(MessageSent.class);
   }

   protected Message stubMessage(String subject, String content) {
      return new MessageBuilder()
            .subject(subject)
            .content(new MessageContentBuilder().content(content).contentType(TEXT_PLAIN).build())
            .fromAddress(MessageAddress.of("from@domain.tld"))
            .toAddress(MessageAddress.of("to@domain.tld"))
            .build();
   }
}
