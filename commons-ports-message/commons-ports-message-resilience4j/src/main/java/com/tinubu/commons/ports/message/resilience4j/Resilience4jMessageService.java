/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.message.resilience4j;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.lang.util.CheckedSupplier.checkedSupplier;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.ExceptionUtils.smartFinalize;

import java.util.StringJoiner;

import com.tinubu.commons.ddd2.domain.event.DomainEvent;
import com.tinubu.commons.ddd2.domain.event.DomainEventListener;
import com.tinubu.commons.lang.util.CheckedRunnable;
import com.tinubu.commons.lang.util.CheckedSupplier;
import com.tinubu.commons.ports.message.domain.AbstractMessageService;
import com.tinubu.commons.ports.message.domain.Message;
import com.tinubu.commons.ports.message.domain.MessageContent;
import com.tinubu.commons.ports.message.domain.MessageService;
import com.tinubu.commons.ports.message.domain.metrology.MessageServiceMetrology;
import com.tinubu.commons.ports.resilience4j.Resilience4jDecorator;

/**
 * Resilience4j wrapper for any delegated message service.
 * <p>
 * The following resilience patterns are supported :
 * <ul>
 *    <li>Bulkhead (thread-based)</li>
 *    <li>Time limiter (only if Bulkhead is enabled)</li>
 *    <li>Rate limiter</li>
 *    <!--<li>Circuit-breaker</li>-->
 *    <li>Retry</li>
 * </ul>
 * <p>
 * Patterns encapsulation is Retry(Circuit-breaker(Rate limiter(Time limiter(Bulkhead)))).
 * <p>
 * This repository has the following specific behaviors :
 * <ul>
 *    <li>Encapsulated delegate repository is closed on {@link #close()}</li>
 * </ul>
 */
public class Resilience4jMessageService implements MessageService {

   private final Resilience4jMessageConfig config;
   private final MessageService delegate;

   private final Resilience4jDecorator resilience4jDecorator;

   /**
    * Creates a Resilience4j enabled message service wrapper.
    *
    * @param config resilience configuration
    * @param delegate delegated real message service
    * @param delegateName unique name for the delegated message service to identify resilience patterns
    *       against it
    */
   public Resilience4jMessageService(Resilience4jMessageConfig config,
                                     MessageService delegate,
                                     String delegateName) {
      this.config = validate(config, "config", isNotNull()).orThrow();
      this.delegate = validate(delegate, "delegate", isNotNull()).orThrow();
      validate(delegateName, "delegateName", isNotBlank()).orThrow();

      this.resilience4jDecorator = new Resilience4jDecorator(config.resilience(),
                                                             ((AbstractMessageService) delegate).eventService(),
                                                             delegateName,
                                                             list());
   }

   /**
    * Creates a Resilience4j enabled message service wrapper.
    * Delegated message service name will be automatically generated from class
    * hashcode.
    *
    * @param config resilience configuration
    * @param delegate delegated real message service
    */
   public Resilience4jMessageService(Resilience4jMessageConfig config, MessageService delegate) {
      this(config, delegate, Resilience4jDecorator.delegateName(delegate));
   }

   /**
    * Call delegated operation with resilience patterns.
    * Delegated operation is wrapped with resilience decorators.
    *
    * @param supplier operation
    * @param <T> operation return type
    *
    * @return operation result
    */
   public <T> T resilientCall(CheckedSupplier<T> supplier) {
      return resilience4jDecorator.resilientCall(supplier, false);
   }

   /**
    * Call delegated operation with resilience patterns.
    * Delegated operation is wrapped with resilience decorators.
    *
    * @param supplier operation
    * @param unRetryable whether call is retryable, this disables retry pattern if called operation does
    *       not support it
    * @param <T> operation return type
    *
    * @return operation result
    */
   public <T> T resilientCall(CheckedSupplier<T> supplier, boolean unRetryable) {
      return resilience4jDecorator.resilientCall(supplier, unRetryable);
   }

   /**
    * Call delegated operation with resilience patterns.
    * Delegated operation is wrapped with resilience decorators.
    *
    * @param runnable operation
    * @param <T> operation return type
    *
    * @return operation result
    */
   public <T> T resilientCall(CheckedRunnable runnable) {
      return resilience4jDecorator.resilientCall(checkedSupplier(runnable));
   }

   /**
    * Call delegated operation with resilience patterns.
    * Delegated operation is wrapped with resilience decorators.
    *
    * @param runnable operation
    * @param unRetryable whether call is retryable, this disables retry pattern if called operation does
    *       not support it
    * @param <T> operation return type
    *
    * @return operation result
    */
   public <T> T resilientCall(CheckedRunnable runnable, boolean unRetryable) {
      return resilience4jDecorator.resilientCall(checkedSupplier(runnable), unRetryable);
   }

   @Override
   public void sendMessage(Message message, MessageContent... alternatives) {
      resilientCall(() -> delegate.sendMessage(message, alternatives));
   }

   @Override
   public <E extends DomainEvent> void registerEventListener(DomainEventListener<? super E> eventListener,
                                                             Class<? extends E>... eventClasses) {
      delegate.registerEventListener(eventListener, eventClasses);
   }

   @Override
   public <E extends DomainEvent> void registerEventLoggingListener(Class<? extends E>... eventClasses) {
      delegate.registerEventLoggingListener(eventClasses);
   }

   @Override
   public void registerEventsLoggingListener() {
      delegate.registerEventsLoggingListener();
   }

   @Override
   public <T extends MessageServiceMetrology> void registerMetrology(T metrology) {
      delegate.registerMetrology(metrology);
   }

   @Override
   public void unregisterEventListeners() {
      delegate.unregisterEventListeners();
   }

   @Override
   public void close() {
      try {
         smartFinalize(delegate::close, resilience4jDecorator::close);
      } catch (Exception e) {
         throw new IllegalStateException(e.getMessage(), e);
      }
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", Resilience4jMessageService.class.getSimpleName() + "[", "]")
            .add("delegate=" + delegate)
            .toString();
   }

}