/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.message.resilience4j;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.clearInvocations;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.io.IOException;
import java.time.Duration;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.lang3.time.StopWatch;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.stubbing.Answer;

import com.tinubu.commons.ddd2.domain.event.DomainEvent;
import com.tinubu.commons.ddd2.domain.event.DomainEventListener;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.message.domain.Message;
import com.tinubu.commons.ports.message.domain.event.MessageSent;
import com.tinubu.commons.ports.message.domain.event.MessageServiceEvent;
import com.tinubu.commons.ports.resilience4j.Resilience4jConfig;
import com.tinubu.commons.ports.resilience4j.Resilience4jConfig.BulkheadConfig.BulkheadConfigBuilder;
import com.tinubu.commons.ports.resilience4j.Resilience4jConfig.RateLimiterConfig.RateLimiterConfigBuilder;
import com.tinubu.commons.ports.resilience4j.Resilience4jConfig.Resilience4jConfigBuilder;
import com.tinubu.commons.ports.resilience4j.Resilience4jConfig.RetryConfig.RetryConfigBuilder;
import com.tinubu.commons.ports.resilience4j.Resilience4jConfig.ThreadPoolBulkheadConfig.ThreadPoolBulkheadConfigBuilder;
import com.tinubu.commons.ports.resilience4j.Resilience4jConfig.TimeLimiterConfig.TimeLimiterConfigBuilder;
import com.tinubu.commons.ports.resilience4j.event.FailedRetryEvent;
import com.tinubu.commons.ports.resilience4j.event.FinishedCallBulkheadEvent;
import com.tinubu.commons.ports.resilience4j.event.PermittedCallBulkheadEvent;
import com.tinubu.commons.ports.resilience4j.event.PermittedRateLimiterEvent;
import com.tinubu.commons.ports.resilience4j.event.RejectedCallBulkheadEvent;
import com.tinubu.commons.ports.resilience4j.event.RejectedRateLimiterEvent;
import com.tinubu.commons.ports.resilience4j.event.Resilience4jEvent;
import com.tinubu.commons.ports.resilience4j.event.RetriedRetryEvent;
import com.tinubu.commons.ports.resilience4j.event.SucceededRetryEvent;
import com.tinubu.commons.ports.resilience4j.event.TimedOutTimeLimiterEvent;

import io.github.resilience4j.bulkhead.BulkheadFullException;
import io.github.resilience4j.ratelimiter.RequestNotPermitted;

/**
 * Resilience4j test suite.
 */
public class Resilience4jMessageServiceTest {

   /**
    * Bulkhead pattern tests.
    */
   @Nested
   public class Bulkhead {

      @Nested
      public class DefaultBulkheadWhenNominal extends BaseResilience4jMessageServiceTest {
         @Override
         protected Resilience4jConfig resilience4jConfig() {
            return new Resilience4jConfigBuilder()
                  .bulkhead(new BulkheadConfigBuilder().maxConcurrentCalls(25).build())
                  .build();
         }

         @Test
         public void testSendMessageWhenNominal() {
            DomainEventListener<DomainEvent> eventListener =
                  instrumentMessageServiceListeners(MessageServiceEvent.class, Resilience4jEvent.class);

            clearInvocations(eventListener);
            clearInvocations(realMessageService());

            messageService().sendMessage(stubMessage("subject", "content"));

            verify(realMessageService()).sendMessage(any());
            verify(eventListener).accept(any(MessageSent.class));
            verify(eventListener).accept(any(PermittedCallBulkheadEvent.class));
            verify(eventListener).accept(any(FinishedCallBulkheadEvent.class));
            verifyNoMoreInteractions(eventListener);
         }
      }

      @Nested
      public class DefaultBulkheadWhenFull extends BaseResilience4jMessageServiceTest {
         @Override
         protected Resilience4jConfig resilience4jConfig() {
            return new Resilience4jConfigBuilder()
                  .bulkhead(new BulkheadConfigBuilder().maxConcurrentCalls(0).build())
                  .build();
         }

         @Test
         public void testSendMessageWhenBulkheadFull() {
            DomainEventListener<DomainEvent> eventListener =
                  instrumentMessageServiceListeners(MessageServiceEvent.class, Resilience4jEvent.class);

            clearInvocations(eventListener);
            clearInvocations(realMessageService());

            assertThatExceptionOfType(BulkheadFullException.class)
                  .isThrownBy(() -> messageService().sendMessage(stubMessage("subject", "content")))
                  .withMessage("Bulkhead 'test' is full and does not permit further calls");

            verify(realMessageService(), never()).sendMessage(any());
            verify(eventListener).accept(any(RejectedCallBulkheadEvent.class));
            verifyNoMoreInteractions(eventListener);
         }
      }

      @Nested
      public class DefaultBulkheadWithMaxWaitDurationWhenFull extends BaseResilience4jMessageServiceTest {
         @Override
         protected Resilience4jConfig resilience4jConfig() {
            return new Resilience4jConfigBuilder()
                  .bulkhead(new BulkheadConfigBuilder()
                                  .maxConcurrentCalls(0)
                                  .maxWaitDuration(Duration.ofMillis(2000))
                                  .build())
                  .build();
         }

         @Test
         public void testSendMessageWhenBulkheadFull() {
            DomainEventListener<DomainEvent> eventListener =
                  instrumentMessageServiceListeners(MessageServiceEvent.class, Resilience4jEvent.class);

            clearInvocations(eventListener);
            clearInvocations(realMessageService());

            StopWatch watch = StopWatch.createStarted();

            assertThatExceptionOfType(BulkheadFullException.class)
                  .isThrownBy(() -> messageService().sendMessage(stubMessage("subject", "content")))
                  .withMessage("Bulkhead 'test' is full and does not permit further calls");

            assertThat(watch.getTime())
                  .as("bulkhead must wait for room for maxWaitDuration")
                  .isGreaterThanOrEqualTo(2000);

            verify(realMessageService(), never()).sendMessage(any());
            verify(eventListener).accept(any(RejectedCallBulkheadEvent.class));
            verifyNoMoreInteractions(eventListener);
         }
      }

      @Nested
      public class DefaultBulkheadWithZeroWaitDuration extends BaseResilience4jMessageServiceTest {
         @Override
         protected Resilience4jConfig resilience4jConfig() {
            return new Resilience4jConfigBuilder()
                  .bulkhead(new BulkheadConfigBuilder()
                                  .maxConcurrentCalls(0)
                                  .maxWaitDuration(Duration.ZERO)
                                  .build())
                  .build();
         }

         @Test
         public void testSendMessageWhenBulkheadFull() {
            DomainEventListener<DomainEvent> eventListener =
                  instrumentMessageServiceListeners(MessageServiceEvent.class, Resilience4jEvent.class);

            clearInvocations(eventListener);
            clearInvocations(realMessageService());

            assertThatExceptionOfType(BulkheadFullException.class)
                  .isThrownBy(() -> messageService().sendMessage(stubMessage("subject", "content")))
                  .withMessage("Bulkhead 'test' is full and does not permit further calls");

            verify(realMessageService(), never()).sendMessage(any());
            verify(eventListener).accept(any(RejectedCallBulkheadEvent.class));
            verifyNoMoreInteractions(eventListener);
         }
      }

   }

   /**
    * Thread pool bulkhead pattern tests.
    */
   @Nested
   public class ThreadPoolBulkhead {

      @Nested
      public class DefaultThreadPoolBulkheadWhenNominal extends BaseResilience4jMessageServiceTest {
         @Override
         protected Resilience4jConfig resilience4jConfig() {
            return new Resilience4jConfigBuilder()
                  .threadPoolBulkhead(new ThreadPoolBulkheadConfigBuilder()
                                            .queueCapacity(100)
                                            .keepAliveDuration(Duration.ofMillis(20))
                                            .build())
                  .build();
         }

         @Test
         public void testSendMessageWhenNominal() {
            DomainEventListener<DomainEvent> eventListener =
                  instrumentMessageServiceListeners(MessageServiceEvent.class, Resilience4jEvent.class);

            clearInvocations(eventListener);
            clearInvocations(realMessageService());

            messageService().sendMessage(stubMessage("subject", "content"));

            verify(realMessageService()).sendMessage(any());
            verify(eventListener).accept(any(MessageSent.class));
            verify(eventListener).accept(any(PermittedCallBulkheadEvent.class));
            verify(eventListener).accept(any(FinishedCallBulkheadEvent.class));
            verifyNoMoreInteractions(eventListener);
         }

      }

      @Nested
      public class DefaultThreadPoolBulkheadWhenFull extends BaseResilience4jMessageServiceTest {
         @Override
         protected Resilience4jConfig resilience4jConfig() {
            return new Resilience4jConfigBuilder()
                  .threadPoolBulkhead(new ThreadPoolBulkheadConfigBuilder()
                                            .maxThreadPoolSize(1)
                                            .queueCapacity(1)
                                            .keepAliveDuration(Duration.ofMillis(20))
                                            .build())
                  .build();
         }

         /**
          * Depleting bulkhead by launching 2 long calls in separate threads : 1 to deplete the thread pool, 1
          * to deplete the queue.
          */
         @BeforeEach
         public void depletingBulkhead() throws InterruptedException {

            Message message = stubMessage("long-call", "content");

            doAnswer((Answer<Void>) invocation -> {
               while (!Thread.currentThread().isInterrupted()) {
                  try {
                     Thread.sleep(100);
                  } catch (InterruptedException e) {
                     Thread.currentThread().interrupt();
                  }
               }
               return (Void) invocation.callRealMethod();
            }).when(realMessageService()).sendMessage(message);

            ExecutorService executorService = Executors.newFixedThreadPool(2);

            for (int i = 0; i < 2; i++) {
               executorService.submit(() -> {
                  messageService().sendMessage(message);
               });
            }

            Thread.sleep(1000); // Poorly fix race condition between depleting threads and real test thread
         }

         @Test
         public void testSendMessageWhenBulkheadFull() {
            DomainEventListener<DomainEvent> eventListener =
                  instrumentMessageServiceListeners(MessageServiceEvent.class, Resilience4jEvent.class);

            clearInvocations(eventListener);
            clearInvocations(realMessageService());

            assertThatExceptionOfType(BulkheadFullException.class)
                  .isThrownBy(() -> messageService().sendMessage(stubMessage("subject", "content")))
                  .withMessage("Bulkhead 'test' is full and does not permit further calls");

            verify(realMessageService(), never()).sendMessage(any());
            verify(eventListener).accept(any(RejectedCallBulkheadEvent.class));
            verifyNoMoreInteractions(eventListener);
         }
      }

   }

   /**
    * Time limiter pattern tests.
    */
   @Nested
   public class TimeLimiter {

      @Nested
      public class DefaultTimeLimiterWhenNominal extends BaseResilience4jMessageServiceTest {
         @Override
         protected Resilience4jConfig resilience4jConfig() {
            return new Resilience4jConfigBuilder()
                  .timeLimiter(new TimeLimiterConfigBuilder().timeoutDuration(Duration.ofSeconds(1)).build())
                  .build();
         }
      }

      @Nested
      public class DefaultTimeLimiterWhenTimedOutOperation extends BaseResilience4jMessageServiceTest {
         @Override
         protected Resilience4jConfig resilience4jConfig() {
            return new Resilience4jConfigBuilder()
                  .timeLimiter(new TimeLimiterConfigBuilder().timeoutDuration(Duration.ofSeconds(1)).build())
                  .build();
         }

         @Test
         public void testSendMessageWhenTimedOut() {
            Message testMessage = stubMessage("subject", "content");

            doAnswer((Answer<Void>) invocation -> {
               while (!Thread.currentThread().isInterrupted()) {
                  try {
                     Thread.sleep(100);
                  } catch (InterruptedException e) {
                     Thread.currentThread().interrupt();
                  }
               }
               return (Void) invocation.callRealMethod();
            }).when(realMessageService()).sendMessage(testMessage);

            DomainEventListener<DomainEvent> eventListener =
                  instrumentMessageServiceListeners(MessageServiceEvent.class, Resilience4jEvent.class);

            clearInvocations(eventListener);
            clearInvocations(realMessageService());

            assertThatExceptionOfType(TimeoutException.class)
                  .isThrownBy(() -> messageService().sendMessage(testMessage))
                  .withMessage("TimeLimiter 'test' recorded a timeout exception.");

            verify(realMessageService()).sendMessage(any());
            verify(eventListener).accept(any(TimedOutTimeLimiterEvent.class));
            verifyNoMoreInteractions(eventListener);
         }

      }

   }

   /**
    * Rate limiter pattern tests.
    */
   @Nested
   public class RateLimiter {

      @Nested
      public class DefaultRateLimiterWhenNominal extends BaseResilience4jMessageServiceTest {
         @Override
         protected Resilience4jConfig resilience4jConfig() {
            return new Resilience4jConfigBuilder()
                  .rateLimiter(new RateLimiterConfigBuilder()
                                     .limitForPeriod(50)
                                     .limitRefreshPeriod(Duration.ofNanos(500))
                                     .timeoutDuration(Duration.ofSeconds(5))
                                     .build())
                  .build();
         }

         @Test
         public void testSendMessageWhenNominal() {
            DomainEventListener<DomainEvent> eventListener =
                  instrumentMessageServiceListeners(MessageServiceEvent.class, Resilience4jEvent.class);

            clearInvocations(eventListener);
            clearInvocations(realMessageService());

            messageService().sendMessage(stubMessage("subject", "content"));

            verify(realMessageService()).sendMessage(any());
            verify(eventListener).accept(any(MessageSent.class));
            verify(eventListener).accept(any(PermittedRateLimiterEvent.class));
            verifyNoMoreInteractions(eventListener);
         }

      }

      @Nested
      public class DefaultRateLimiterWhenRejected extends BaseResilience4jMessageServiceTest {
         @Override
         protected Resilience4jConfig resilience4jConfig() {
            return new Resilience4jConfigBuilder()
                  .rateLimiter(new RateLimiterConfigBuilder()
                                     .limitRefreshPeriod(Duration.ofSeconds(10))
                                     .limitForPeriod(1)
                                     .timeoutDuration(Duration.ofSeconds(0))
                                     .build())
                  .build();
         }

         @Test
         public void testSendMessageWhenRejected() {
            DomainEventListener<DomainEvent> eventListener =
                  instrumentMessageServiceListeners(MessageServiceEvent.class, Resilience4jEvent.class);

            messageService().sendMessage(stubMessage("subject", "content"));

            clearInvocations(eventListener);
            clearInvocations(realMessageService());

            assertThatExceptionOfType(RequestNotPermitted.class)
                  .isThrownBy(() -> messageService().sendMessage(stubMessage("subject", "content")))
                  .withMessage("RateLimiter 'test' does not permit further calls");

            verify(realMessageService(), never()).sendMessage(any());
            verify(eventListener).accept(any(RejectedRateLimiterEvent.class));
            verifyNoMoreInteractions(eventListener);
         }

      }

      @Nested
      public class DefaultRateLimiterWhenRejectedWithTimeout extends BaseResilience4jMessageServiceTest {
         @Override
         protected Resilience4jConfig resilience4jConfig() {
            return new Resilience4jConfigBuilder()
                  .rateLimiter(new RateLimiterConfigBuilder()
                                     .limitRefreshPeriod(Duration.ofSeconds(10))
                                     .limitForPeriod(1)
                                     .timeoutDuration(Duration.ofSeconds(2))
                                     .build())
                  .build();
         }

         @Test
         public void testSendMessageWhenRejectedWithTimeout() {
            DomainEventListener<DomainEvent> eventListener =
                  instrumentMessageServiceListeners(MessageServiceEvent.class, Resilience4jEvent.class);

            messageService().sendMessage(stubMessage("subject", "content"));

            clearInvocations(eventListener);
            clearInvocations(realMessageService());

            StopWatch watch = StopWatch.createStarted();
            assertThatExceptionOfType(RequestNotPermitted.class)
                  .isThrownBy(() -> messageService().sendMessage(stubMessage("subject", "content")))
                  .withMessage("RateLimiter 'test' does not permit further calls");

            assertThat(watch.getTime()).isGreaterThanOrEqualTo(2000);

            verify(realMessageService(), never()).sendMessage(any());
            verify(eventListener).accept(any(RejectedRateLimiterEvent.class));
            verifyNoMoreInteractions(eventListener);
         }

      }
   }

   /**
    * Retry pattern tests.
    */
   @Nested
   public class Retry {

      @Nested
      public class DefaultRetryWhenNominal extends BaseResilience4jMessageServiceTest {

         @Override
         protected Resilience4jConfig resilience4jConfig() {
            return new Resilience4jConfigBuilder()
                  .retry(new RetryConfigBuilder().maxAttempts(3).waitDuration(Duration.ofMillis(500)).build())
                  .build();
         }

         @Test
         public void testSendMessageWhenNominal() {
            DomainEventListener<DomainEvent> eventListener =
                  instrumentMessageServiceListeners(MessageServiceEvent.class, Resilience4jEvent.class);

            clearInvocations(eventListener);
            clearInvocations(realMessageService());

            messageService().sendMessage(stubMessage("subject", "content"));

            verify(realMessageService()).sendMessage(any());
            verify(eventListener).accept(any(MessageSent.class));
            verifyNoMoreInteractions(eventListener);
         }

      }

      @Nested
      public class DefaultRetryWhenFailedAttempt extends BaseResilience4jMessageServiceTest {
         @Override
         protected Resilience4jConfig resilience4jConfig() {
            return new Resilience4jConfigBuilder()
                  .retry(new RetryConfigBuilder().maxAttempts(3).waitDuration(Duration.ofMillis(500)).build())
                  .build();
         }

         @Test
         public void testSendMessageWhenFailedAttempt() {
            Message testMessage = stubMessage("subject", "content");

            AtomicInteger callCounter = new AtomicInteger(0);
            doAnswer((Answer<Optional<Document>>) invocation -> {
               if (callCounter.incrementAndGet() <= 2) {
                  throw new IOException("Fake error");
               }
               return (Optional<Document>) invocation.callRealMethod();
            }).when(realMessageService()).sendMessage(testMessage);

            DomainEventListener<DomainEvent> eventListener =
                  instrumentMessageServiceListeners(MessageServiceEvent.class, Resilience4jEvent.class);

            clearInvocations(eventListener);
            clearInvocations(realMessageService());

            messageService().sendMessage(testMessage);

            verify(realMessageService(), times(3)).sendMessage(any());
            verify(eventListener).accept(any(MessageSent.class));
            verify(eventListener, times(2)).accept(any(RetriedRetryEvent.class));
            verify(eventListener).accept(any(SucceededRetryEvent.class));
            verifyNoMoreInteractions(eventListener);
         }

      }

      @Nested
      public class DefaultRetryWhenMaxAttemptFailed extends BaseResilience4jMessageServiceTest {
         @Override
         protected Resilience4jConfig resilience4jConfig() {
            return new Resilience4jConfigBuilder()
                  .retry(new RetryConfigBuilder().maxAttempts(3).waitDuration(Duration.ofMillis(500)).build())
                  .build();
         }

         @Test
         public void testSendMessageWhenMaxAttemptFailed() {
            Message testMessage = stubMessage("subject", "content");

            AtomicInteger callCounter = new AtomicInteger(0);
            doAnswer((Answer<Void>) invocation -> {
               if (callCounter.incrementAndGet() <= 10) {
                  throw new IOException("Fake error");
               }
               return (Void) invocation.callRealMethod();
            }).when(realMessageService()).sendMessage(testMessage);

            DomainEventListener<DomainEvent> eventListener =
                  instrumentMessageServiceListeners(MessageServiceEvent.class, Resilience4jEvent.class);

            clearInvocations(eventListener);
            clearInvocations(realMessageService());

            assertThatExceptionOfType(IOException.class)
                  .isThrownBy(() -> messageService().sendMessage(testMessage))
                  .withMessage("Fake error");

            verify(realMessageService(), times(3)).sendMessage(any());
            verify(eventListener, times(2)).accept(any(RetriedRetryEvent.class));
            verify(eventListener).accept(any(FailedRetryEvent.class));
            verifyNoMoreInteractions(eventListener);
         }

      }

      @Nested
      public class DefaultRetryWhenUnretryableExceptionFailed extends BaseResilience4jMessageServiceTest {
         @Override
         protected Resilience4jConfig resilience4jConfig() {
            return new Resilience4jConfigBuilder()
                  .retry(new RetryConfigBuilder().maxAttempts(3).waitDuration(Duration.ofMillis(500)).build())
                  .build();
         }

         @Test
         public void testSendMessageWhenUnretryableExceptionFailed() {
            Message testMessage = stubMessage("subject", "content");

            AtomicInteger callCounter = new AtomicInteger(0);
            doAnswer((Answer<Void>) invocation -> {
               if (callCounter.incrementAndGet() <= 10) {
                  throw new IllegalStateException("Fake error");
               }
               return (Void) invocation.callRealMethod();
            }).when(realMessageService()).sendMessage(testMessage);

            DomainEventListener<DomainEvent> eventListener =
                  instrumentMessageServiceListeners(MessageServiceEvent.class, Resilience4jEvent.class);

            clearInvocations(eventListener);
            clearInvocations(realMessageService());

            assertThatExceptionOfType(IllegalStateException.class)
                  .isThrownBy(() -> messageService().sendMessage(testMessage))
                  .withMessage("Fake error");

            verify(realMessageService(), times(1)).sendMessage(any());
            verify(eventListener).accept(any(FailedRetryEvent.class));
            verifyNoMoreInteractions(eventListener);
         }

      }

   }

}