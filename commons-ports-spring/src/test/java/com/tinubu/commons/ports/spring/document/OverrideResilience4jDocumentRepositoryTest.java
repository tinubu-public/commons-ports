/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.spring.document;

import static org.assertj.core.api.Assertions.assertThat;

import java.nio.file.Paths;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;

import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.fs.FsDocumentRepository;
import com.tinubu.commons.ports.document.resilience4j.Resilience4jDocumentRepository;
import com.tinubu.commons.ports.spring.autoconfigure.document.CommonsPortsDocumentFsAutoConfiguration;
import com.tinubu.commons.ports.spring.document.OverrideResilience4jDocumentRepositoryTest.TestConfig;
import com.tinubu.commons.ports.spring.document.fs.FsDocumentConfig;
import com.tinubu.commons.ports.spring.document.fs.FsDocumentConfigMapper;
import com.tinubu.commons.ports.spring.resilience4j.Resilience4jConfigMapper;

/**
 * Demonstrates how to override default provided bean configuration.
 * In this case, we want to redefine a repository bean, enabling Resilience4j, with original configuration.
 * <p>
 * Note that since a bean, or a configuration of a given type, is overridden, the default bean/configuration
 * is no more loaded.
 */
@SpringBootTest(classes = TestConfig.class)
@DirtiesContext
@TestPropertySource(properties = {
      "test.document.feature1.fs.enabled=true",
      "commons-ports.document.fs.storage-path=/tmp/test/feature1",
      "commons-ports.document.fs.fail-fast-if-missing-storage-path=false" })
public class OverrideResilience4jDocumentRepositoryTest {

   /**
    * Simulates a real application Spring context with a default configuration,
    * then, override fs repository bean with a custom condition, and a custom bean name, but keep
    * original configuration bean.
    */
   @ImportAutoConfiguration({
         CommonsPortsDocumentFsAutoConfiguration.class })
   public static class TestConfig {

      @Bean("feature1FsDocumentRepository")
      @ConditionalOnProperty(name = "test.document.feature1.fs.enabled")
      public Resilience4jDocumentRepository feature1FsDocumentRepository(
            @Qualifier("commons-ports.document.fsDocumentConfig") FsDocumentConfig fsDocumentConfig) {

         return new Resilience4jDocumentRepository(Resilience4jConfigMapper.repositoryConfig(fsDocumentConfig.getResilience4j()),
                                                   new FsDocumentRepository(FsDocumentConfigMapper.repositoryConfig(
                                                         fsDocumentConfig)));
      }
   }

   @Autowired
   @Qualifier("feature1FsDocumentRepository")
   DocumentRepository feature1DocumentRepository;

   @Autowired
   DocumentRepository unqualifiedDocumentRepository;

   @Autowired
   Resilience4jDocumentRepository qualifiedDocumentRepository;

   @Autowired
   @Qualifier("commons-ports.document.fsDocumentConfig")
   FsDocumentConfig feature1DocumentConfig;

   @Test
   public void testConfiguration() {
      assertThat(feature1DocumentConfig.getStoragePath()).isEqualTo(Paths.get("/tmp/test/feature1"));
   }

   @Test
   public void testNamedDocumentRepository() {
      assertThat(feature1DocumentRepository).isInstanceOf(Resilience4jDocumentRepository.class);
   }

   @Test
   public void testUnqualifiedDocumentRepository() {
      assertThat(unqualifiedDocumentRepository).isInstanceOf(Resilience4jDocumentRepository.class);
   }

   @Test
   public void testQualifiedDocumentRepository() {
      assertThat(qualifiedDocumentRepository).isInstanceOf(Resilience4jDocumentRepository.class);
   }

}
