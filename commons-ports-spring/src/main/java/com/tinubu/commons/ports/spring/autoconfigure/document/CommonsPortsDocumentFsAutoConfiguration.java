/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.spring.autoconfigure.document;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.tinubu.commons.ports.document.fs.FsDocumentRepository;
import com.tinubu.commons.ports.document.resilience4j.Resilience4jDocumentRepository;
import com.tinubu.commons.ports.spring.autoconfigure.base.BasePortsDocumentAutoConfiguration;
import com.tinubu.commons.ports.spring.autoconfigure.base.ConditionalOnEnabledDocumentRepositoryBean;
import com.tinubu.commons.ports.spring.autoconfigure.base.ConditionalOnResilience4jDocumentRepositoryBean;
import com.tinubu.commons.ports.spring.document.fs.FsDocumentConfig;
import com.tinubu.commons.ports.spring.document.fs.FsDocumentConfigMapper;
import com.tinubu.commons.ports.spring.resilience4j.Resilience4jConfigMapper;

/**
 * Filesystem document adapter auto-configuration.
 * <p>
 * This repository is configured only if {@code commons-ports.document[.fs].enabled} property undefined or is
 * set to {@code true}.
 * Default bean names for this repository are :
 * <ul>
 *    <li>FsDocumentConfigPoint : {@value CONFIG_BEAN_NAME}</li>
 *    <li>FsDocumentRepository : {@value REPOSITORY_BEAN_NAME}</li>
 * </ul>
 */
@Configuration
@ConditionalOnClass(FsDocumentRepository.class)
public class CommonsPortsDocumentFsAutoConfiguration extends BasePortsDocumentAutoConfiguration {

   private static final Logger log = LoggerFactory.getLogger(CommonsPortsDocumentFsAutoConfiguration.class);

   private static final String QUALIFIER = "fs";
   private static final String REPOSITORY_BEAN_NAME = "commons-ports.document.fsDocumentRepository";
   private static final String CONFIG_BEAN_NAME = "commons-ports.document.fsDocumentConfig";

   @Bean(CONFIG_BEAN_NAME)
   @ConditionalOnMissingBean(FsDocumentConfig.class)
   @ConditionalOnEnabledDocumentRepositoryBean(qualifier = QUALIFIER)
   public FsDocumentConfig fsDocumentConfig() {
      return new FsDocumentConfig();
   }

   @Bean(REPOSITORY_BEAN_NAME)
   @ConditionalOnMissingBean(FsDocumentRepository.class)
   @ConditionalOnEnabledDocumentRepositoryBean(qualifier = QUALIFIER)
   @ConditionalOnResilience4jDocumentRepositoryBean(qualifier = QUALIFIER, negate = true)
   public FsDocumentRepository fsDocumentRepository(
         @Qualifier(CONFIG_BEAN_NAME) FsDocumentConfig fsDocumentConfig) {

      com.tinubu.commons.ports.document.fs.FsDocumentConfig config =
            FsDocumentConfigMapper.repositoryConfig(fsDocumentConfig);

      log.info("Using '{}' document repository '{}' configuration", QUALIFIER, config);

      return new FsDocumentRepository(config);
   }

   @Configuration
   @ConditionalOnClass(Resilience4jDocumentRepository.class)
   public static class Resilience4jAutoConfiguration {

      @Bean(REPOSITORY_BEAN_NAME)
      @ConditionalOnMissingBean(Resilience4jDocumentRepository.class)
      @ConditionalOnEnabledDocumentRepositoryBean(qualifier = QUALIFIER)
      @ConditionalOnResilience4jDocumentRepositoryBean(qualifier = QUALIFIER)
      public Resilience4jDocumentRepository resilience4jFsDocumentRepository(
            @Qualifier(CONFIG_BEAN_NAME) FsDocumentConfig fsDocumentConfig) {

         com.tinubu.commons.ports.document.resilience4j.Resilience4jDocumentConfig resilience =
               Resilience4jConfigMapper.repositoryConfig(fsDocumentConfig.getResilience4j());

         com.tinubu.commons.ports.document.fs.FsDocumentConfig config =
               FsDocumentConfigMapper.repositoryConfig(fsDocumentConfig);

         log.info("Using '{}' document repository '{}' configuration with '{}' resilience",
                  QUALIFIER,
                  config,
                  resilience);

         return new Resilience4jDocumentRepository(resilience,
                                                   new FsDocumentRepository(config),
                                                   REPOSITORY_BEAN_NAME,
                                                   false);
      }
   }
}