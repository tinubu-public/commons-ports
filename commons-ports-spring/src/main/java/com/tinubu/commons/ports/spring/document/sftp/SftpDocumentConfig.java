/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.spring.document.sftp;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.io.Resource;
import org.springframework.validation.annotation.Validated;

import com.tinubu.commons.ports.document.sftp.strategy.DefaultChunkedWriteStrategy;
import com.tinubu.commons.ports.document.sftp.strategy.WriteStrategy;
import com.tinubu.commons.ports.spring.resilience4j.Resilience4jConfig;

/**
 * SFTP document repository configuration.
 */
@Validated
@ConfigurationProperties(prefix = "commons-ports.document.sftp", ignoreUnknownFields = false)
public class SftpDocumentConfig {

   /** Whether to allow unknown server keys by default. */
   // FIXME set to false by default once tsquare2 has configured them to true
   private static final boolean DEFAULT_ALLOW_UNKNOWN_KEYS = true;
   /**
    * Whether to enable SFTP caching by default.
    */
   private static final boolean DEFAULT_SESSION_CACHING = true;
   /**
    * Default SFTP caching session pool size. Value must be > 0.
    */
   private static final int DEFAULT_SESSION_POOL_SIZE = 10;
   /**
    * Default session wait timeout for available connection in pool. Session wait timeout disabled if
    * set to 0.
    */
   private static final Duration DEFAULT_SESSION_POOL_WAIT_TIMEOUT = Duration.ZERO;
   /**
    * Keep-alive interval, before an alive message is sent to the server.
    */
   private static final Duration DEFAULT_KEEP_ALIVE_INTERVAL = Duration.ofMinutes(1);
   /** Default write strategy. */
   private static final Class<? extends WriteStrategy> DEFAULT_WRITE_STRATEGY =
         DefaultChunkedWriteStrategy.class;
   /**
    * Default connection timeout. Connection timeout disabled if set to 0.
    */
   private static final Duration DEFAULT_CONNECT_TIMEOUT = Duration.ZERO;
   /**
    * Default socket (I/O) timeout. Socket timeout disabled if set to 0.
    */
   private static final Duration DEFAULT_SOCKET_TIMEOUT = Duration.ZERO;

   /**
    * Enable flag for the default repository instance.
    */
   private boolean enabled = true;

   /**
    * SFTP server host.
    */
   @NotBlank
   private String host = "localhost";

   /**
    * SFTP server port.
    */
   @NotNull
   private Integer port = 22;

   /**
    * Whether to allow unknown server keys. Unknown server keys are not allowed by default.
    */
   private boolean allowUnknownKeys = DEFAULT_ALLOW_UNKNOWN_KEYS;

   /**
    * Optional SFTP server authentication username.
    * Use {@code ${user.name}} by default.
    */
   private String username;

   /**
    * Optional SFTP server authentication password.
    * Use either privateKey or password for authentication. Use password authentication if both set.
    */
   private String password;

   /**
    * Optional SFTP server authentication private key.
    * Use either privateKey or password for authentication. Use password authentication if both set.
    * Use {@code ${user.home}/.ssh/id_rsa} by default.
    */
   private Resource privateKey;

   /**
    * Optional SFTP server authentication private key passphrase.
    */
   private String privateKeyPassphrase;

   /**
    * Optional base path to store documents in SFTP server. If omitted, the SFTP server default directory will
    * be used.
    */
   @NotNull
   private Path basePath = Paths.get("");

   /**
    * Whether to use session caching. Enabled by default.
    */
   private boolean sessionCaching = DEFAULT_SESSION_CACHING;

   /**
    * Optional SFTP caching session pool size. Value must be > 0. Default to 10.
    */
   @Min(1)
   private int sessionPoolSize = DEFAULT_SESSION_POOL_SIZE;

   /**
    * SFTP session wait timeout for available connection in pool. Timeout is disabled if set
    * to 0. Disabled by default.
    */
   @NotNull
   private Duration sessionPoolWaitTimeout = DEFAULT_SESSION_POOL_WAIT_TIMEOUT;

   /**
    * Keep-alive interval. Value must be greater than or equal to 0s.
    * If interval duration is set to 0s, keep-alive will be disabled.
    * Default interval is 5m.
    */
   @NotNull
   private Duration keepAliveInterval = DEFAULT_KEEP_ALIVE_INTERVAL;

   @NotNull
   private Class<? extends WriteStrategy> writeStrategyClass = DEFAULT_WRITE_STRATEGY;

   /**
    * SFTP server connection timeout. If timeout is set to 0, no connection timeout will be applied. Timeout
    * is disabled by default.
    */
   public Duration connectTimeout = DEFAULT_CONNECT_TIMEOUT;

   /**
    * SFTP server socket (I/O) timeout. If timeout is set to 0, no socket timeout will be applied. Timeout is
    * disabled by default.
    */
   public Duration socketTimeout = DEFAULT_SOCKET_TIMEOUT;

   /**
    * Resilience4j configuration. No resilience pattern is configured by default.
    *
    * @implSpec Requires {@code com.tinubu.commons-ports:commons-ports-document-resilience4j} module.
    */
   private Resilience4jConfig resilience4j = new Resilience4jConfig();

   public boolean isEnabled() {
      return enabled;
   }

   public void setEnabled(boolean enabled) {
      this.enabled = enabled;
   }

   public String getHost() {
      return host;
   }

   public void setHost(String host) {
      this.host = host;
   }

   public Integer getPort() {
      return port;
   }

   public void setPort(Integer port) {
      this.port = port;
   }

   public boolean isAllowUnknownKeys() {
      return allowUnknownKeys;
   }

   public SftpDocumentConfig setAllowUnknownKeys(boolean allowUnknownKeys) {
      this.allowUnknownKeys = allowUnknownKeys;
      return this;
   }

   public String getUsername() {
      return username;
   }

   public void setUsername(String username) {
      this.username = username;
   }

   public String getPassword() {
      return password;
   }

   public void setPassword(String password) {
      this.password = password;
   }

   public Resource getPrivateKey() {
      return privateKey;
   }

   public void setPrivateKey(Resource privateKey) {
      this.privateKey = privateKey;
   }

   public String getPrivateKeyPassphrase() {
      return privateKeyPassphrase;
   }

   public void setPrivateKeyPassphrase(String privateKeyPassphrase) {
      this.privateKeyPassphrase = privateKeyPassphrase;
   }

   public Path getBasePath() {
      return basePath;
   }

   public void setBasePath(Path basePath) {
      this.basePath = basePath;
   }

   public boolean isSessionCaching() {
      return sessionCaching;
   }

   public SftpDocumentConfig setSessionCaching(boolean sessionCaching) {
      this.sessionCaching = sessionCaching;
      return this;
   }

   public int getSessionPoolSize() {
      return sessionPoolSize;
   }

   public SftpDocumentConfig setSessionPoolSize(int sessionPoolSize) {
      this.sessionPoolSize = sessionPoolSize;
      return this;
   }

   public Duration getSessionPoolWaitTimeout() {
      return sessionPoolWaitTimeout;
   }

   public SftpDocumentConfig setSessionPoolWaitTimeout(Duration sessionPoolWaitTimeout) {
      this.sessionPoolWaitTimeout = sessionPoolWaitTimeout;
      return this;
   }

   public Duration getKeepAliveInterval() { return keepAliveInterval; }

   public void setKeepAliveInterval(Duration keepAliveInterval) {
      this.keepAliveInterval = keepAliveInterval;
   }

   public Class<? extends WriteStrategy> getWriteStrategyClass() {
      return writeStrategyClass;
   }

   public void setWriteStrategyClass(Class<? extends WriteStrategy> writeStrategyClass) {
      this.writeStrategyClass = writeStrategyClass;
   }

   @Deprecated
   public Duration getTimeout() {
      return socketTimeout;
   }

   @Deprecated
   public void setTimeout(Duration timeout) {
      this.connectTimeout = timeout;
      this.socketTimeout = timeout;
      this.sessionPoolWaitTimeout = timeout;
   }

   public Duration getConnectTimeout() {
      return connectTimeout;
   }

   public SftpDocumentConfig setConnectTimeout(Duration connectTimeout) {
      this.connectTimeout = connectTimeout;
      return this;
   }

   public Duration getSocketTimeout() {
      return socketTimeout;
   }

   public SftpDocumentConfig setSocketTimeout(Duration socketTimeout) {
      this.socketTimeout = socketTimeout;
      return this;
   }

   public Resilience4jConfig getResilience4j() {
      return resilience4j;
   }

   public void setResilience4j(Resilience4jConfig resilience4j) {
      this.resilience4j = resilience4j;
   }

}
