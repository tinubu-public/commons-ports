/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.spring.document.azureblob;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import com.tinubu.commons.ports.document.azureblob.AzureBlobDocumentConfig.AzureBlobDocumentConfigBuilder;
import com.tinubu.commons.ports.spring.document.azureblob.AzureBlobDocumentConfig.AzureAuthentication;

/**
 * Azure Blob document repository configuration mapper.
 */
public final class AzureBlobDocumentConfigMapper {

   private AzureBlobDocumentConfigMapper() {
   }

   public static com.tinubu.commons.ports.document.azureblob.AzureBlobDocumentConfig repositoryConfig(com.tinubu.commons.ports.spring.document.azureblob.AzureBlobDocumentConfig config) {
      AzureAuthentication authentication = config.getAuthentication();

      return new com.tinubu.commons.ports.document.azureblob.AzureBlobDocumentConfig.AzureBlobDocumentConfigBuilder()
            .containerName(config.getContainerName())
            .containerBasePath(config.getContainerBasePath())
            .connectionString(config.getConnectionString())
            .endpoint(config.getEndpoint())
            .optionalChain(nullable(config.getAuthentication()),
                           (AzureBlobDocumentConfigBuilder b, AzureAuthentication auth) -> b.authentication(
                                 com.tinubu.commons.ports.document.azureblob.AzureBlobDocumentConfig.AzureAuthentication.of(
                                       authentication.getStorageAccountKey(),
                                       authentication.getStorageAccountName())))
            .createIfMissingContainer(config.isCreateContainerIfMissing())
            .build();
   }

}
